<?php defined('_JEXEC') or die(file_get_contents('index.html'));
/**
 * @package   Fox Contact for Joomla
 * @copyright Copyright (c) 2010 - 2015 Demis Palma. All rights reserved.
 * @license   Distributed under the terms of the GNU General Public License GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * @see       Documentation: http://www.fox.ra.it/forum/2-documentation.html
 */

class FoxHtmlResource
{
	private $js = array();
	private $css = array();
	
	public static function NewInstance()
	{
		return new FoxHtmlResource();
	}
	
	
	public function Add($path, $type)
	{
		if (is_dir(JPATH_ROOT . $path))
		{
			$files = new GlobIterator(JPATH_ROOT . $path . "/*.{$type}");
			foreach ($files as $file)
			{
				$this->{$type}[] = $this->get_versioned_suffix($path . '/' . $file->getFilename());
			}
		
		}
		else
		{
			if (is_file(JPATH_ROOT . "{$path}.{$type}"))
			{
				$this->{$type}[] = $this->get_versioned_suffix("{$path}.{$type}");
			}
			else
			{
				$this->{$type}[] = $this->get_versioned_suffix("{$path}.min.{$type}");
			}
		
		}
		
		return $this;
	}
	
	
	public function ToDocument(JDocument $document)
	{
		foreach ($this->js as $js)
		{
			$document->addScript($js);
		}
		
		foreach ($this->css as $css)
		{
			$document->addStyleSheet($css);
		}
	
	}
	
	
	public function ToJSON()
	{
		return json_encode(get_object_vars($this), JSON_HEX_QUOT | JSON_HEX_APOS | JSON_HEX_TAG | JSON_HEX_AMP);
	}
	
	
	private function get_versioned_suffix($file)
	{
		$time = @filemtime(JPATH_ROOT . $file) or $time = 0;
		$suffix = JDEBUG ? '' : "?v={$time}";
		return JUri::root(true) . $file . $suffix;
	}

}