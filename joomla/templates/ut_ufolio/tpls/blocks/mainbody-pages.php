<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

if (is_array($this->getParam('skip_component_content')) && 
  in_array(JFactory::getApplication()->input->getInt('Itemid'), $this->getParam('skip_component_content'))) 
return;
?>

<div class="pages">

	<?php if ($this->countModules('pages-top')) : ?>
		<div class="wrap sections pages-top <?php $this->_c('pages-top') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('pages-top') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->checkSpotlight('spotlighth', 'pages-left, pages-center, pages-right')) :?>
		<div class="pages-spotlight">
			<div class="container">
				<?php $this->spotlight('spotlighth', 'pages-left, pages-center, pages-right') ;?>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->countModules('pages-bottom')) : ?>
		<div class="wrap sections pages-bottom <?php $this->_c('pages-bottom') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('pages-bottom') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>
</div>
