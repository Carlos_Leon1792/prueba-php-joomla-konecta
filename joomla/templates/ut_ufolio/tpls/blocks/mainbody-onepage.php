<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

if (is_array($this->getParam('skip_component_content')) && 
  in_array(JFactory::getApplication()->input->getInt('Itemid'), $this->getParam('skip_component_content'))) 
return;
?>

<div class="home">

	<?php if ($this->countModules('onepage-1')) : ?>
		<div class="wrap sections onepage-1 <?php $this->_c('onepage-1') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('onepage-1') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

</div>
