<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$config = JFactory::getConfig();
?>

<!-- FOOTER -->
<footer id="t3-footer" class="wrap t3-footer">

	<?php if ($this->checkSpotlight('footnav', 'footer-1, footer-2, footer-3, footer-4, footer-5, footer-6')) : ?>
		<!-- FOOT NAVIGATION -->
		<div class="container">
			<?php $this->spotlight('footnav', 'footer-1, footer-2, footer-3, footer-4, footer-5, footer-6') ?>
		</div>
		<!-- //FOOT NAVIGATION -->
	<?php endif; ?>

	<div class="t3-copyright">
		<div class="container">
			<div class="row">
				<?php if($this->countModules('footer')):?>
				<div class="col-xs-12 footer <?php $this->_c('footer') ?>">
					<jdoc:include type="modules" name="<?php $this->_p('footer') ?>" />
				</div>
				<?php endif; ?>

				<?php if ($this->getParam('t3-rmvlogo', 1)): ?>
				<div class="col-xs-12 copyright">
					<div class="text-center">
						<div class="col-sm-6 designedby">Designed By <a href="http://www.unitemplates.com" target="_blank">Unitemplates</a></div>
						<div class="col-sm-6 poweredby">Powered by <a href="http://t3-framework.org" target="_blank" rel="nofollow">T3</a></div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

</footer>
<!-- //FOOTER -->

<!-- BACK TOP TOP BUTTON -->
 
<div id="back-to-top" data-spy="affix" data-offset-top="680" class="back-to-top hidden-xs hidden-sm affix-top"> 
	<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-up"></i></button>
</div>
 
<script type="text/javascript"> 
	(function($) {
		// Back to top 
		$('#back-to-top').on('click', function(){ 
			$("html, body").animate({scrollTop: 0}, 500); 
			return false; 
		}); 
	})(jQuery);
</script> 
<!-- BACK TO TOP BUTTON -->