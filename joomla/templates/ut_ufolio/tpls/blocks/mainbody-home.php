<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

if (is_array($this->getParam('skip_component_content')) && 
  in_array(JFactory::getApplication()->input->getInt('Itemid'), $this->getParam('skip_component_content'))) 
return;
?>

<div class="home">

	<?php if ($this->countModules('home-1')) : ?>
		<div class="wrap sections home-1 <?php $this->_c('home-1') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-1') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-2')) : ?>
		<div class="wrap sections home-2 <?php $this->_c('home-2') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-2') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-3')) : ?>
		<div class="wrap sections home-3 <?php $this->_c('home-3') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-3') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-4')) : ?>
		<div class="wrap sections home-4 <?php $this->_c('home-4') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-4') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-5')) : ?>
		<div class="wrap sections home-5 <?php $this->_c('home-5') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-5') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->checkSpotlight('spotlighth', 'home-left, home-center, home-right')) :?>
		<div class="home-spotlight">
			<div class="container">
				<?php $this->spotlight('spotlighth', 'home-left, home-center, home-right') ;?>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->countModules('home-bottom-1')) : ?>
		<div class="wrap sections home-bottom-1 <?php $this->_c('home-bottom-1') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-bottom-1') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-bottom-2')) : ?>
		<div class="wrap sections home-bottom-2 <?php $this->_c('home-bottom-2') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-bottom-2') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-bottom-3')) : ?>
		<div class="wrap sections home-bottom-3 <?php $this->_c('home-bottom-3') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-bottom-3') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-bottom-4')) : ?>
		<div class="wrap sections home-bottom-4 <?php $this->_c('home-bottom-4') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-bottom-4') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

	<?php if ($this->countModules('home-bottom-5')) : ?>
		<div class="wrap sections home-bottom-5 <?php $this->_c('home-bottom-5') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('home-bottom-5') ?>" style="UTnotitle" />
		</div>
	<?php endif ?>

</div>
