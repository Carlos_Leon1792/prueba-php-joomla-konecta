jQuery(document).ready(function() {
	//TITLES
	jQuery('.feature .feature-title, .action .action-title, .pricing .pricing-title, .hero .hero-title, .team .team-title, .skill .skill-title, .clients .clients-title, .section-title, .testimonial-6 .name, .ut-ctc .ctc-title').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeInDown', 
	});
	//INTROS
	jQuery('.hero .hero-intro, .action .action-intro, .feature-2 .feature-intro').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated zoomIn', 
	});
	//LINKS
	jQuery('.hero .hero-links a, .action .action-links a').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeInUp multidelay', 
	});
	//ICONS
	jQuery('.feature-1 .feature-icon, .feature-3 .feature-icon, .feature-7 .feature-icon').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated bounceInDown multidelay', 
	});
	jQuery('.feature-2 .feature-item, .testimonial-6 .testimonial-image').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeIn multidelay', 
	});
	jQuery('.team-1').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeIn', 
	});
	jQuery('.skill-1 .skill-row').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated slideInUp multidelay', 
	});
	jQuery('.news .module-title').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeInDown', 
	});
	jQuery('.news .news-ct .news-item').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated zoomIn multidelay', 
	});
	jQuery('.feature-10 .feature-item:nth-child(odd)').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeInLeft multidelay', 
	});
	jQuery('.feature-10 .feature-item:nth-child(even)').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeInRight multidelay', 
	});
	jQuery('.clients-2 .owl-clients-2').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated zoomIn', 
	});
	jQuery('.pricing-1 .pricing-item:nth-child(odd)').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeInUp', 
	});

	//Footer
	jQuery('.t3-footer * .t3-module').addClass("invisible").viewportChecker({
	    classToAdd: 'visible animated fadeIn', 
	});
});