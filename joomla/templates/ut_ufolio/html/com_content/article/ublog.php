<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
JHtml::addIncludePath(T3_PATH . '/html/com_content');
JHtml::addIncludePath(dirname(dirname(__FILE__)));

// Create shortcuts to some parameters.
$params   = $this->item->params;
$images   = json_decode($this->item->images);
$urls     = json_decode($this->item->urls);
$canEdit  = $params->get('access-edit');
$user     = JFactory::getUser();
$info    = $params->get('info_block_position', 2);
$aInfo1 = ($params->get('show_publish_date') || $params->get('show_category') || $params->get('show_author') || $params->get('show_hits'));
$topInfo = ($aInfo1 && $info != 1);

JHtml::_('behavior.caption');
JHtml::_('bootstrap.tooltip');
//Extrafields
$exfield = new JRegistry($this->item->attribs);
?>

<div class="item-page<?php echo $this->pageclass_sfx ?> clearfix">

<!-- Article -->
<div itemscope itemtype="http://schema.org/Article">
	<meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="https://google.com/article" content="" />
	<meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? JFactory::getConfig()->get('language') : $this->item->language; ?>" />

	<?php //Images and Content Types
	if($exfield->get('type')=='standard' || $exfield->get('type')==''):?>
		<?php echo JLayoutHelper::render('joomla.content.fulltext_image', array('item' => $this->item, 'params' => $params)); ?>
	<?php else :?>
		<?php echo JLayoutHelper::render('joomla.content.content_type.types', array('item' => $this->item, 'exfield'=> $exfield, 'id'=>$this->item->id)); ?>
	<?php endif; ?>

	<!-- Aside -->
	<?php if ($topInfo) : ?>
	<aside class="article-aside clearfix">
	  <?php if ($topInfo): ?>
	  <?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'above')); ?>
	  <?php endif; ?>
	</aside>  
	<?php endif; ?>
	<!-- //Aside -->

	<?php if ($params->get('show_title')) : ?>
		<header class="article-header clearfix">
			<h2 class="article-title h1" itemprop="headline">
				<?php //Icon Types
				if ($exfield->get('show_icon')): ?>
					<?php if ($exfield->get('type')=='standard'):?>
				    	<i class="ion-image"></i>
				    <?php endif;?>
					<?php if ($exfield->get('type')=='gallery'):?>
				    	<i class="ion-images"></i>
				    <?php endif;?>
					<?php if ($exfield->get('type')=='video'):?>
				    	<i class="ion-videocamera"></i>
				    <?php endif;?>
					<?php if ($exfield->get('type')=='audio'):?>
				    	<i class="ion-music-note"></i>
				    <?php endif;?>
					<?php if ($exfield->get('type')=='link'):?>
				    	<i class="ion-link"></i>
				    <?php endif;?>
					<?php if ($exfield->get('type')=='quote'):?>
				    	<i class="ion-quote"></i>
				    <?php endif;?>
				<?php endif; ?>
				<?php echo $this->item->title;?>
			</h2>
		</header>
	<?php endif; ?>

	<?php if ($params->get('show_tags', 1) && !empty($this->item->tags)) : ?>
		<?php echo JLayoutHelper::render('joomla.content.tags', $this->item->tags->itemTags); ?>
	<?php endif; ?>

	<?php if (!$params->get('show_intro')) : ?>
		<?php echo $this->item->event->afterDisplayTitle; ?>
	<?php endif; ?>

	<?php echo $this->item->event->beforeDisplayContent; ?>

	<?php if ($params->get('access-view')): ?>


		<div class="article-content clearfix <?php if (isset($this->item->toc)){echo 'col-sm-9 p-l-0';}?>" itemprop="articleBody">
			<?php echo $this->item->text; ?>
		</div>
		<?php if (isset ($this->item->toc)) : ?>
			<div class="col-sm-3 p-r-0 hidden-xs clearfix">
				<?php echo $this->item->toc; ?>
			</div>
		<?php endif; ?>

		<?php
		if (!empty($this->item->pagination) && $this->item->pagination && $this->item->paginationposition && !$this->item->paginationrelative): ?>
			<div class="pagenav-ct col-xs-12">
				<?php echo $this->item->pagination;?>
			</div>
		<?php endif; ?>

	<?php //optional teaser intro text for guests ?>
	<?php elseif ($params->get('show_noauth') == true and  $user->get('guest')) : ?>
		<?php echo $this->item->introtext; ?>
		<?php //Optional link to let them register to see the whole article. ?>
		<?php if ($params->get('show_readmore') && $this->item->fulltext != null) :
			$link1 = JRoute::_('index.php?option=com_users&view=login');
			$link = new JURI($link1);
			?>
			<section class="readmore">
				<a href="<?php echo $link; ?>" itemprop="url">
					<span>
					<?php $attribs = json_decode($this->item->attribs); ?>
					<?php
					if ($attribs->alternative_readmore == null) :
						echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
					elseif ($readmore = $this->item->alternative_readmore) :
						echo $readmore;
						if ($params->get('show_readmore_title', 0) != 0) :
									echo JHtml::_('string.truncate', ($this->item->tit), $params->get('readmore_limit'));
						endif;
					elseif ($params->get('show_readmore_title', 0) == 0) :
						echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
					else :
						echo JText::_('COM_CONTENT_READ_MORE');
								echo JHtml::_('string.truncate', ($this->item->title),$params->get('readmore_limit'));
					endif; ?>
					</span>
				</a>
			</section>
		<?php endif; ?>
	<?php endif; ?>
</div>
<!-- //Article -->

<?php echo $this->item->event->afterDisplayContent; ?>
</div>