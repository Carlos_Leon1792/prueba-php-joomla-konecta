<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.framework');

// Create a shortcut for params.
$params  = & $this->item->params;
$images  = json_decode($this->item->images);
$info    = $params->get('info_block_position', 2);
$aInfo1 = ($params->get('show_publish_date') || $params->get('show_category') || $params->get('show_author') || $params->get('show_hits'));
$topInfo = ($aInfo1 && $info != 1);
//Extrafield
$exfield = new JRegistry($this->item->attribs);
?>

<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00' )) : ?>
<div class="system-unpublished">
<?php endif; ?>

		<?php //images and Content Types
		if ($exfield->get('type')=='standard' || $exfield->get('type')==''):?>
		<div class="item-image">
			<a href="<?php echo $link;?>">
				<img src="<?php echo $images->image_intro;?>" alt="<?php echo $images->image_intro_alt;?>" title="<?php echo $images->image_intro_caption;?>"/>
			</a>
		</div>
		<?php else :?>
			<?php echo JLayoutHelper::render('joomla.content.content_type.types', array('item' => $this->item, 'exfield'=> $exfield, 'id'=>$this->item->id)); ?>
		<?php endif;?>

		<!-- Aside -->
	    <?php if ($topInfo) : ?>
	    <aside class="article-aside clearfix">
	      <?php if ($topInfo): ?>
	      <?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'above')); ?>
	      <?php endif; ?>
	    </aside>  
	    <?php endif; ?>
	    <!-- //Aside -->
  
    <?php if ($params->get('show_title')) : ?>
			<?php echo JLayoutHelper::render('joomla.content.item_title', array('item' => $this->item, 'params' => $params, 'title-tag'=>'h2')); ?>
    <?php endif; ?>

		<div class="article-intro clearfix">
			<?php if (!$params->get('show_intro')) : ?>
				<?php echo $this->item->event->afterDisplayTitle; ?>
			<?php endif; ?>

			<?php echo $this->item->event->beforeDisplayContent; ?>

			<?php echo $this->item->introtext; ?>
		</div>


		<?php if ($params->get('show_readmore') && $this->item->readmore) :
			if ($params->get('access-view')) :
				$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
			else :
				$menu      = JFactory::getApplication()->getMenu();
				$active    = $menu->getActive();
				$itemId    = $active->id;
				$link1     = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
				$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
				$link      = new JURI($link1);
				$link->setVar('return', base64_encode($returnURL));
			endif;
			?>
			<div class="readmore text-right">
				<a href="<?php echo $link; ?>">
					<span>
					<?php if (!$params->get('access-view')) :
						echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
					else :
						echo trim(JText::_('COM_CONTENT_READ_MORE_TITLE'), '.');
					endif; ?>
					</span>
					<i class="ion-ios-arrow-thin-right"></i>
				</a>
			</div>
		<?php endif; ?>

<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != JFactory::getDbo()->getNullDate())) : ?>
</div>
<?php endif; ?>

<?php echo $this->item->event->afterDisplayContent; ?> 
