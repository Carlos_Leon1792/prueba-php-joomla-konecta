<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');

$regex = '@class="([^"]*)"@';
$label = 'class="$1 col-sm-2 control-label"';
$input = 'class="$1 form-control"';

if (isset($this->error)) : ?>
	<div class="contact-error">
		<?php echo $this->error; ?>
	</div>
<?php endif; ?>

<div class="contact-form">
	<form id="contact-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal">
		<fieldset>
			<legend><?php echo JText::_('COM_CONTACT_FORM_LABEL'); ?></legend>
			<div class="form-group">
				<div class="col-xs-12 col-sm-6">
					<?php echo preg_replace($regex, $input, $this->form->getInput('contact_name')); ?>
				</div>
				<div class="col-xs-12 col-sm-6">
					<?php echo preg_replace($regex, $input, $this->form->getInput('contact_email')); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<?php echo preg_replace($regex, $input, $this->form->getInput('contact_subject')); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<?php echo preg_replace($regex, $input, $this->form->getInput('contact_message')); ?>
				</div>
			</div>

			<?php if ($this->params->get('show_email_copy')) { ?>
				<div class="form-group">
					<div class="col-sm-12 emailcopy">
						<div class="checkbox">
							<?php echo $this->form->getInput('contact_email_copy'); ?>
							<?php echo $this->form->getLabel('contact_email_copy'); ?>
						</div>
					</div>
				</div>
			<?php } ?>

			<div class="form-group contact-actions">
				<?php //Dynamically load any additional fields from plugins. ?>
				<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
					<?php if ($fieldset->name != 'contact'):?>
						<?php $fields = $this->form->getFieldset($fieldset->name);?>
						<?php foreach ($fields as $field) : ?>
							<?php if ($field->hidden) : ?>
								<?php echo $field->input;?>
							<?php else:?>
								<div class="col-sm-8 contact-plugins">
								<?php if (!$field->required && $field->type != "Spacer") : ?>
									<span class="optional"><?php echo JText::_('COM_CONTACT_OPTIONAL');?></span>
								<?php endif; ?>
								<?php echo $field->input;?>
								</div>
							<?php endif;?>
						<?php endforeach;?>
					<?php endif ?>
				<?php endforeach;?>
				<div class="col-sm-4 contact-submit">
					<button class="btn btn-primary btn-lg validate" type="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?></button>				
					<input type="hidden" name="option" value="com_contact" />
					<input type="hidden" name="task" value="contact.submit" />
					<input type="hidden" name="return" value="<?php echo $this->return_page;?>" />
					<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script>
jQuery(function($){
$("#jform_contact_name").attr("placeholder", "<?php echo '★ '.JText::_('COM_CONTACT_CONTACT_EMAIL_NAME_DESC');?>");
$("#jform_contact_email").attr("placeholder", "<?php echo '★ '.JText::_('COM_CONTACT_CONTACT_ENTER_VALID_EMAIL');?>");
$("#jform_contact_emailmsg").attr("placeholder", "<?php echo '★ '.JText::_('COM_CONTACT_CONTACT_MESSAGE_SUBJECT_DESC');?>");
$("#jform_contact_message").attr("placeholder", "<?php echo '★ '.JText::_('COM_CONTACT_CONTACT_ENTER_MESSAGE_DESC');?>");
});
</script>