<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$cparams = JComponentHelper::getParams('com_media');

jimport('joomla.html.html.bootstrap');

//Variables for Sidebar
$show_sidebar = ($this->contact->image && $this->params->get('show_image')) || ($this->contact->con_position && $this->params->get('show_position')) || ($this->params->get('show_links')) || (($this->params->get('address_check') > 0) && ($this->contact->address || $this->contact->suburb  || $this->contact->state || $this->contact->country || $this->contact->postcode));
?>
<div class="row contact<?php echo $this->pageclass_sfx?>" itemscope itemtype="http://schema.org/Person">

	<div class="form-container <?php if($show_sidebar){echo'col-sm-8 col-md-9';}else{echo'col-xs-12';}?>">
		<?php if ($this->contact->misc && $this->params->get('show_misc')) : ?>
			<?php if ($this->params->get('presentation_style')=='plain'):?>
				<?php echo '<h3>'. JText::_('COM_CONTACT_OTHER_INFORMATION').'</h3>'; ?>
			<?php endif; ?>
					<div class="contact-miscinfo">
						<?php echo $this->contact->misc; ?>
					</div>
		<?php endif; ?>

		<?php if ($this->params->get('show_email_form') && ($this->contact->email_to || $this->contact->user_id)) : ?>
			<?php if ($this->params->get('presentation_style')=='plain'):?>
				<?php  echo '<h3>'. JText::_('COM_CONTACT_EMAIL_FORM').'</h3>';  ?>
			<?php endif; ?>
			<?php  echo $this->loadTemplate('form');  ?>
		<?php endif; ?>
	</div>

	<?php if($show_sidebar) :?>
		<div class="col-sm-4 col-md-3 contact-sidebar">
			<?php if ($this->contact->image && $this->params->get('show_image')) : ?>
				<div class="thumbnail">
					<?php echo JHtml::_('image', $this->contact->image, JText::_('COM_CONTACT_IMAGE_DETAILS'), array('itemprop' => 'image')); ?>
				</div>
			<?php endif; ?>
				<?php if ($this->contact->con_position && $this->params->get('show_position')) : ?>
				<dl class="contact-position text-center">
					<dt class="hidden"></dt>
					<dd itemprop="jobTitle">
						<h4><?php echo $this->contact->con_position; ?></h4>
					</dd>
				</dl>
			<?php endif; ?>

			<?php echo $this->loadTemplate('address'); ?>
			<?php if ($this->params->get('show_links')) : ?>
				<?php echo $this->loadTemplate('links'); ?>
			<?php endif; ?>
		</div>
	<?php endif;?>
</div>