<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * marker_class: Class based on the selection of text, none, or icons
 */
?>
<dl class="contact-address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	<?php if (($this->params->get('address_check') > 0) &&
		($this->contact->address || $this->contact->suburb  || $this->contact->state || $this->contact->country || $this->contact->postcode)) : ?>
		<?php if ($this->params->get('address_check') > 0) : ?>
			<dt class="h3">
				<span>
					<?php echo JText::_('COM_CONTACT_PROFILE_HEADING'); ?>
				</span>
			</dt>
		<?php endif; ?>
		<?php if ($this->contact->address && $this->params->get('show_street_address')) : ?>
			<dd>
				<i class="fa fa-home"></i>
				<span class="contact-street" itemprop="streetAddress">
					<?php echo $this->contact->address .'<br/>'; ?>
				</span>
			</dd>
		<?php endif; ?>
		<?php if ($this->contact->suburb && $this->params->get('show_suburb')) : ?>
			<dd>
				<i class="fa fa-map-marker"></i>
				<span class="contact-suburb" itemprop="addressLocality">
					<?php echo $this->contact->suburb .'<br/>'; ?>
				</span>
			</dd>
		<?php endif; ?>
		<?php if ($this->contact->state && $this->params->get('show_state')) : ?>
			<dd>
				<i class="fa fa-map"></i>
				<span class="contact-state" itemprop="addressRegion">
					<?php echo $this->contact->state . '<br/>'; ?>
				</span>
			</dd>
		<?php endif; ?>
		<?php if ($this->contact->postcode && $this->params->get('show_postcode')) : ?>
			<dd>
				<i class="fa fa-location-arrow"></i>
				<span class="contact-postcode" itemprop="postalCode">
					<?php echo $this->contact->postcode .'<br/>'; ?>
				</span>
			</dd>
		<?php endif; ?>
		<?php if ($this->contact->country && $this->params->get('show_country')) : ?>
		<dd>
			<i class="fa fa-globe"></i>
			<span class="contact-country" itemprop="addressCountry">
				<?php echo $this->contact->country .'<br/>'; ?>
			</span>
		</dd>
		<?php endif; ?>
	<?php endif; ?>

	<?php if ($this->contact->email_to && $this->params->get('show_email')) : ?>
		<dd>
			<i class="fa fa-envelope"></i>
			<span class="contact-emailto">
				<?php echo $this->contact->email_to; ?>
			</span>
		</dd>
	<?php endif; ?>

	<?php if ($this->contact->telephone && $this->params->get('show_telephone')) : ?>
		<dd>
			<i class="fa fa-phone"></i>
			<span class="contact-telephone" itemprop="telephone">
				<?php echo nl2br($this->contact->telephone); ?>
			</span>
		</dd>
	<?php endif; ?>
	<?php if ($this->contact->fax && $this->params->get('show_fax')) : ?>
		<dd>
			<i class="fa fa-fax"></i>
			<span class="contact-fax" itemprop="faxNumber">
			<?php echo nl2br($this->contact->fax); ?>
			</span>
		</dd>
	<?php endif; ?>
	<?php if ($this->contact->mobile && $this->params->get('show_mobile')) :?>
		<dd>
			<i class="fa fa-mobile"></i>
			<span class="contact-mobile" itemprop="telephone">
				<?php echo nl2br($this->contact->mobile); ?>
			</span>
		</dd>
	<?php endif; ?>
	<?php if ($this->contact->webpage && $this->params->get('show_webpage')) : ?>
		<dd>
			<i class="fa fa-link fa-rotate-90"></i>
			<span class="contact-webpage">
				<a href="<?php echo $this->contact->webpage; ?>" target="_blank" itemprop="url">
				<?php echo $this->contact->webpage; ?></a>
			</span>
		</dd>
	<?php endif; ?>

	<?php if ($this->params->get('allow_vcard')) :	?>
		<dd>
			<?php echo JText::_('COM_CONTACT_DOWNLOAD_INFORMATION_AS');?>
			<a href="<?php echo JRoute::_('index.php?option=com_contact&amp;view=contact&amp;id='.$this->contact->id . '&amp;format=vcf'); ?>">
			<?php echo JText::_('COM_CONTACT_VCARD');?></a>
		</dd>
	<?php endif; ?>
</dl>