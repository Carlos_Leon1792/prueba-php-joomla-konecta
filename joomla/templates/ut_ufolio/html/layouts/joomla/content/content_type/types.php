<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$item = $displayData['item'];
$exfield = $displayData['exfield'];
$id = $displayData['id'];
//Valor of 'id' in modules $module->id, in categorys and Item $this->item->id
?>
<div class="type-item">
	<?php
	// Video
	$video_id = $exfield->get('video_id');
	$yt = $exfield->get('video_provider') == 'youtube';
	$vim = $exfield->get('video_provider') == 'vimeo';
	if ($exfield->get('type')=='video') :?>
		<div class="video-embed">
			<?php if($yt){ echo '<iframe src="https://www.youtube.com/embed/'.$video_id.'?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen></iframe>'; } ?>
			<?php if($vim){ echo '<iframe src="https://player.vimeo.com/video/'.$video_id.'" allowfullscreen></iframe>'; } ?>
		</div>
	<?php endif; ?>
	<?php
	//Audio
	if ($exfield->get('type')=='audio') :?>
		<div class="audio-embed">
			<?php 
			$audio_embed = $exfield->get('audio_embed');
			$search = array('/width="100%"/', '/frameborder="no"/', '/scrolling="no"/');
			$replace = array('');
			echo preg_replace($search, $replace, $audio_embed);?>
		</div>
	<?php endif; ?>
	<?php
	//Gallery   
	if ($exfield->get('type')=='gallery') {
		//Decode Extra Images
		$gallery = $exfield->get('image_gallery');
		?>
		<div id="gallery-<?php echo $this->escape($id);?>" class="type-gallery carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				<?php if ($gallery):?>
					<?php foreach ($gallery as $item => $value) :?>
						<div class="item<?php echo ($item=='image_gallery0') ? ' active': ''; ?>" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
							<img src="<?php echo $value->image;?>" alt="<?php echo $value->image_alt;?>">
							<meta itemprop="url" content="<?php echo JURI::base().$value->image;?>">
							<meta itemprop="width" content="auto">
							<meta itemprop="height" content="auto">
						</div>
					<?php endforeach; ?>
				<?php endif;?>
				<?php if ($exfield->get('indicators')):?>
					<ol class="carousel-indicators">
					<?php foreach ($gallery as $item => $value) :?>
						<li data-target="#gallery-<?php echo $this->escape($id);?>" data-slide-to="<?php echo trim($item,'image_gallery');?>" class="<?php echo ($item==='image_gallery0') ? ' active': ''; ?>"></li>
					<?php endforeach;?>
					</ol>
				<?php endif; ?>
				<?php if ($exfield->get('controls')) :?>
					<a class="left carousel-control" href="#gallery-<?php echo $this->escape($id);?>" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					</a>
					<a class="right carousel-control" href="#gallery-<?php echo $this->escape($id)?>" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					</a>
				<?php endif; ?>
			</div>
		</div>
	<?php };?>
	<?php
	//Link Type
	if ($exfield->get('type')=='link') :?>
		<div class="type-link">
			<a href="<?php echo $exfield->get('link_url');?>" target="<?php echo $exfield->get('link_target');?>"><?php echo $exfield->get('link_text');?></a>
		</div>
	<?php endif; ?>
	<?php
	//Quote Type
	if ($exfield->get('type')=='quote') :?>
		<div class="type-quote">
			<blockquote>
				<p><?php echo $exfield->get('quote_text');?></p>
				<footer><?php echo $exfield->get('quote_author');?></footer>
			</blockquote>
		</div>
	<?php endif; ?>
</div>
