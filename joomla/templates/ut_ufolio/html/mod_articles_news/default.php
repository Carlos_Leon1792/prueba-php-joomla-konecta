<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="news<?php echo $moduleclass_sfx; ?> clearfix">
	<?php if ($module->title):?>
		<div class="module-title">
			<h2><?php echo $module->title;?></h2>
		</div>
	<?php endif; ?>
	<div class="news-ct container">
		<?php foreach ($list as $item) : ?>
			<?php require JModuleHelper::getLayoutPath('mod_articles_news', '_item'); ?>
		<?php endforeach; ?>
	</div>
</div>
