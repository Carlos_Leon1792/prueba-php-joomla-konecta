<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="news-vertical<?php echo $moduleclass_sfx; ?> clearfix">
	<div class="news-ct">
		<?php foreach ($list as $item) : ?>
			<?php
			$images = json_decode($item->images);
			$item_heading = $params->get('item_heading', 'h4');
			//Extra Fields
			$exfield = new JRegistry($item->attribs);
			?>
			<div class="news-item">
				<?php if ($exfield->get('type')=='standard' || $exfield->get('type')==''):?>
					<?php if ($images->image_intro) :?>
						<div class="news-image">
							<a href="<?php echo $item->link; ?>">
									<img src="<?php echo $images->image_intro;?>" alt="<?php echo $ite->title;?>" class="img-responsive">
							</a>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<?php echo JLayoutHelper::render('joomla.content.content_type.types_mod', array('item' => $item, 'exfield'=> $exfield, 'id'=>$module->id)); ?>
				<?php endif;?>

				<?php if ($params->get('item_title')) : ?>
					<<?php echo $item_heading; ?> class="newsflash-title<?php echo $params->get('moduleclass_sfx'); ?>">
					<?php if ($params->get('link_titles') && $item->link != '') : ?>
						<a href="<?php echo $item->link; ?>">
							<?php echo $item->title; ?>
						</a>
					<?php else : ?>
						<?php echo $item->title; ?>
					<?php endif; ?>
					</<?php echo $item_heading; ?>>
				<?php endif; ?>

				<?php echo JHTML::_('string.abridge', $item->introtext, $length=70, $intro=60); ?>

				<?php if (isset($item->link) && $item->readmore != 0 && $params->get('readmore')) : ?>
					<?php echo '<a class="readmore" href="' . $item->link . '">' . $item->linkText . '</a>'; ?>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
</div>
