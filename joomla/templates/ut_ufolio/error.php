<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.jud_factory
 *
 * @copyright   Copyright (C) 2014 - 2016 Unidisenos Multimedia solutios, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * Author: Unidisenos Multimedia Solutions
 * Website:  http://www.unidisenos.com
 * This file can not be redistributed in any form.
 * ------------------------------------------------------------------------
 */

defined('_JEXEC') or die;

if (!isset($this->error))
{
	$this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
	$this->debug = false;
}
//get language and direction
$doc = JFactory::getDocument();
$app             = JFactory::getApplication();
$this->language = $doc->language;
$this->direction = $doc->direction;
$theme = JFactory::getApplication()->getTemplate(true)->params->get('theme', '');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<title><?php echo $this->error->getCode(); ?> - <?php echo $this->title; ?></title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/error.css" type="text/css" />
	<?php if($theme && is_file(T3_TEMPLATE_PATH . '/css/themes/' . $theme . '/error.css')):?>
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/themes/<?php echo $theme ?>/error.css" type="text/css" />
	<?php endif; ?>
	<?php 
	if ($this->direction == 'rtl') : ?>
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/error_rtl.css" type="text/css" />
	<?php endif; ?>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,900,300,700' rel='stylesheet' type='text/css'>
</head>
 
<body class="page-error">
    <div class="error-page-wrap">
        <div class="inner">
			<div class="error-code"><?php echo $this->error->getCode(); ?></div>
	        <div class="error-message">
		        <?php echo $this->error->getMessage(); ?>
		    </div>
		    <div class="error-message-option">
		        <?php echo JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?>
			</div>
			<a href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>" class="btn btn-search"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a>
		    <div class="error-search">
		    	<?php if (JModuleHelper::getModule('search')) : ?>
		        <?php echo $doc->getBuffer('module', 'search'); ?>
		        <?php endif; ?>
		    </div>
        </div>
	</div>
</body>
</html>