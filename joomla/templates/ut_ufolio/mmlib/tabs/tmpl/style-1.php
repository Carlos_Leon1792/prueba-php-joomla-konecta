<?php
/**
* @package    Joomla.site
* @subpackage   mod_ut_multimodule
* @author   Unitemplates http://www.unitemplates.com
* @copyright  Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license    GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";
//Tabs variables
$subtitle = $helper->get('tabs_subtitle');
$intro = $helper->get('tabs_intro');
$count = $helper->count('tab_name');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'tabs-'. $module->id;} ?>" class="tabs" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="tabs-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="tabs-1 <?php if ($add_container){echo 'container';}?> clearfix">
			<?php if ($module->showtitle) { echo '<h3 class="tabs-title">'.$module->title.'</h3>'; }?>
			<?php if ($subtitle) { echo '<h4 class="tabs-subtitle">'.$subtitle.'</h4>'; }?>
			<?php if ($intro) { echo '<p class="tabs-intro">'.$intro.'</p>'; }?>

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
			<?php for ($i=0; $i < $count; $i++) :?>
				<li role="presentation" class="tab-name <?php if($i<1) echo "active";?> col-xs-12 col-sm-6 col-md-<?php echo $helper->get('tabs_number');?>"><a class="<?php echo $helper->get('tab_color' , $i);?>" href="#tab-<?php echo $module->id.'-'.$i;?>" aria-controls="tab-<?php echo $module->id.'-'.$i;?>" role="tab" data-toggle="tab"><?php echo $helper->get('tab_name' , $i); ?></a></li>
			<?php endfor; ?>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content clearfix">
				<?php for ($i=0; $i < $count; $i++) : ?>
				<div role="tabpanel" class="tab-pane <?php if($i<1) echo "active";?>" id="tab-<?php echo $module->id.'-'.$i;?>">
					<div class="tab-image col-xs-12 col-sm-6 pull-<?php echo $helper->get('tab_image_float' , $i);?>">
						<img src="<?php echo $helper->get('tab_image' , $i).'" alt="'.$helper->get('tab_image' , $i); ?>">
					</div>
					<div class="tab-item col-xs-12 col-sm-6">
						<?php if($helper->get('tab_intro' , $i)){echo '<p class="tab-intro">' .$helper->get('tab_intro' , $i). '</p>'; }?>
						<?php if($helper->get('tab_list' , $i)){echo '<ul class="tab-list">' .$helper->get('tab_list' , $i). '</ul>'; }?>
						<?php if($helper->get('tab_desc' , $i)){echo '<p class="tab-desc">' .$helper->get('tab_desc' , $i). '</p>'; }?>
					</div>
				</div>
				<?php endfor; ?>
			</div>
		</div>
	</div>
</div>
