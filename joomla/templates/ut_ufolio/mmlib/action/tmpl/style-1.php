<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Action variables
$intro = $helper->get('action_intro');
$count = $helper->count('action_link_url');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'action-'. $module->id;} ?>" class="action" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="action-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="action-1 <?php if ($add_container){echo 'container';}?> clearfix">
			<?php if ($module->showtitle) { echo '<h3 class="action-title">' .$module->title. '</h3>'; }?>
			<p class="action-intro"><?php echo $intro;?></p>
			<nav class="action-links">
			<?php for ($i=0; $i < $count; $i++) :?>
				<?php if ($helper->get('action_link_url', $i)) {
					echo '<a class="btn '.$helper->get('action_btn_style', $i).' '.$helper->get('action_btn_size', $i).'" href="'.$helper->get('action_link_url', $i).'" target="'.$helper->get('action_link_target' , $i).'">'.$helper->get('action_link_name', $i).'</a>';
				}?>
			<?php endfor; ?>
			</nav>
		</div>
	</div>
</div>