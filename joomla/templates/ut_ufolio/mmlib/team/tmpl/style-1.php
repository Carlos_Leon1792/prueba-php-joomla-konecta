<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Team variables
$subtitle = $helper->get('team_subtitle');
$intro = $helper->get('team_intro');
$count = $helper->count('team_name');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'team-'. $module->id;} ?>" class="team" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="team-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="team-1 <?php if ($add_container){echo 'container';}?> clearfix">
			<div class="team-header">
				<?php if ($module->showtitle) { echo '<h3 class="team-title">'.$module->title.'</h3>'; }?>
				<?php if ($subtitle) { echo '<h4 class="team-subtitle">'.$subtitle.'</h4>'; }?>
			</div>
			<?php if ($intro) { echo '<p class="team-intro">'.$intro.'</p>'; }?>

			<div class="team-row clearfix">
				<?php for ($i=0; $i < $count; $i++) : ?>
					<div class="team-item col-md-<?php echo $helper->get('columns'); ?> col-sm-6">
						<div class="team-image"><img src="<?php echo $helper->get('team_image' , $i).'" alt="'.$helper->get('team_name' , $i); ?>"></div>
						<h4 class="team-name"><?php echo ($helper->get('team_name' , $i));?></h4>
						<?php if ($helper->get('team_position' , $i)) { echo	'<p class="team-position">'.$helper->get('team_position' , $i).'</p>'; }?>
						<?php if ($helper->get('team_experience' , $i)) { echo '<p class="team-experience">'.$helper->get('team_experience' , $i).'</p>';}?>
						<div class="team-links">
							<?php if ($helper->get('team_twitter' , $i)) { echo '<a class="btn-twitter '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_twitter' , $i).'" target="_blank"><i class="fa fa-twitter"></i></a>'; }?>
							<?php if ($helper->get('team_facebook' , $i)) { echo '<a class="btn-facebook '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_facebook' , $i).'" target="_blank"><i class="fa fa-facebook"></i></a>'; }?>
							<?php if ($helper->get('team_gplus' , $i)) { echo '<a class="btn-google '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_gplus' , $i).'" target="_blank"><i class="fa fa-google-plus"></i></a>'; }?>
							<?php if ($helper->get('team_youtube' , $i)) { echo '<a class="btn-youtube '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_youtube' , $i).'" target="_blank"><i class="fa fa-youtube-square"></i></a>'; }?>
							<?php if ($helper->get('team_instagram' , $i)) { echo '<a class="btn-instagram '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_instagram' , $i).'" target="_blank"><i class="fa fa-instagram"></i></a>'; }?>
							<?php if ($helper->get('team_soc_url' , $i)) { echo '<a class="btn-custom '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_soc_url' , $i).'" target="_blank"><i class="'.$helper->get('team_soc_icon' , $i).'"></i></a>'; }?>
						</div>
					</div>
				<?php endfor;?>
			</div>
		</div>
	</div>
</div>