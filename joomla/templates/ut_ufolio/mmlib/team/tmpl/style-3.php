<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

$mod_url = 'modules/mod_ut_multimodule/assets/';
$doc = JFactory::getDocument();
$doc->addStylesheet($mod_url.'owl/owl.carousel.css');
$doc->addStylesheet($mod_url.'owl/owl.theme.default.min.css');
$doc->addScript($mod_url.'owl/owl.carousel.min.js');
// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Team Variables
$intro = $helper->get('team_intro');
$count = $helper->count('team_name');

 ?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'team-'. $module->id;} ?>" class="team" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="team-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="team-3 <?php if ($add_container){echo 'container';}?>">
			<?php if ($module->showtitle) { echo '<h3 class="team-title">'.$module->title.'</h3>';}?>
			<?php if ($intro) { echo '<p class="team-intro">'.$intro.'</p>';}?>
		  	<!-- testimony's -->
			<div class="owl-team-3">
			<?php for ($i=0; $i < $count; $i++) : ?>
				<div class="item col-xs-12">
					<div class="team-image"><img src="<?php echo $helper->get('team_image' , $i).'" alt="'.$helper->get('team_name' , $i); ?>"></div>
					<h4 class="team-name"><?php echo ($helper->get('team_name' , $i));?></h4>
					<?php if ($helper->get('team_position' , $i)) { echo	'<p class="team-position">'.$helper->get('team_position' , $i).'</p>'; }?>
					<div class="team-links">
						<?php if ($helper->get('team_twitter' , $i)) { echo '<a class="btn-twitter '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_twitter' , $i).'" target="_blank"><i class="fa fa-twitter"></i></a>'; }?>
						<?php if ($helper->get('team_facebook' , $i)) { echo '<a class="btn-facebook '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_facebook' , $i).'" target="_blank"><i class="fa fa-facebook"></i></a>'; }?>
						<?php if ($helper->get('team_gplus' , $i)) { echo '<a class="btn-google '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_gplus' , $i).'" target="_blank"><i class="fa fa-google-plus"></i></a>'; }?>
						<?php if ($helper->get('team_youtube' , $i)) { echo '<a class="btn-youtube '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_youtube' , $i).'" target="_blank"><i class="fa fa-youtube-square"></i></a>'; }?>
						<?php if ($helper->get('team_instagram' , $i)) { echo '<a class="btn-instagram '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_instagram' , $i).'" target="_blank"><i class="fa fa-instagram"></i></a>'; }?>
						<?php if ($helper->get('team_soc_url' , $i)) { echo '<a class="btn-custom '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_soc_url' , $i).'" target="_blank"><i class="'.$helper->get('team_soc_icon' , $i).'"></i></a>'; }?>
					</div>
					<div class="magnifier">
						<h4 class="team-name"><?php echo ($helper->get('team_name' , $i));?></h4>
						<?php if ($helper->get('team_position' , $i)) { echo	'<p class="team-position">'.$helper->get('team_position' , $i).'</p>'; }?>
						<?php if ($helper->get('team_experience' , $i)) { echo '<p class="team-experience">'.$helper->get('team_experience' , $i).'</p>';}?>
						<div class="team-links">
							<?php if ($helper->get('team_twitter' , $i)) { echo '<a class="btn-twitter '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_twitter' , $i).'" target="_blank"><i class="fa fa-twitter"></i></a>'; }?>
							<?php if ($helper->get('team_facebook' , $i)) { echo '<a class="btn-facebook '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_facebook' , $i).'" target="_blank"><i class="fa fa-facebook"></i></a>'; }?>
							<?php if ($helper->get('team_gplus' , $i)) { echo '<a class="btn-google '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_gplus' , $i).'" target="_blank"><i class="fa fa-google-plus"></i></a>'; }?>
							<?php if ($helper->get('team_youtube' , $i)) { echo '<a class="btn-youtube '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_youtube' , $i).'" target="_blank"><i class="fa fa-youtube-square"></i></a>'; }?>
							<?php if ($helper->get('team_instagram' , $i)) { echo '<a class="btn-instagram '.$helper->get('icon_color').' '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_instagram' , $i).'" target="_blank"><i class="fa fa-instagram"></i></a>'; }?>
							<?php if ($helper->get('team_soc_url' , $i)) { echo '<a class="btn-custom '.$helper->get('icon_style').' '.$helper->get('icon_size').'" href="'.$helper->get('team_soc_url' , $i).'" target="_blank"><i class="'.$helper->get('team_soc_icon' , $i).'"></i></a>'; }?>
						</div>
					</div>
				</div>
			<?php endfor; ?>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		jQuery('.owl-team-3').owlCarousel({
		    loop:true,
		    dots:true,
		    nav:false,
		    margin:15,
		    responsive:{
		        0:{
		            items:1
		        },
		        768:{
		            items:2
		        },
		        992:{
		            items:4
		        }
		    }
		})
	});
</script>