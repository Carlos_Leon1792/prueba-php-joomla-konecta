<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

$mod_url = 'modules/mod_ut_multimodule/assets/';
$doc = JFactory::getDocument();
$doc->addStylesheet($mod_url.'owl/owl.carousel.css');
$doc->addStylesheet($mod_url.'owl/owl.theme.default.min.css');
$doc->addScript($mod_url.'owl/owl.carousel.min.js');
// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Team Variables
$intro = $helper->get('team_intro');
$count = $helper->count('team_name');

 ?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'team-'. $module->id;} ?>" class="team" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="team-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="team-4 <?php if ($add_container){echo 'container';}?>">
			<?php if ($module->showtitle) { echo '<h3 class="team-title">'.$module->title.'</h3>';}?>
			<?php if ($intro) { echo '<p class="team-intro">'.$intro.'</p>';}?>
		  	<!-- testimony's -->
			<div class="owl-team-4">
			<?php for ($i=0; $i < $count; $i++) : ?>
				<?php //variables internos
					$twitter = $helper->get('team_twitter', $i);
					$facebook = $helper->get('team_facebook', $i);
					$gplus = $helper->get('team_gplus', $i);
					$youtube = $helper->get('team_youtube', $i);
					$instagram = $helper->get('team_instagram', $i);
					$soc_url = $helper->get('team_soc_url', $i);
					$icon_style = $helper->get('icon_style');
					$icon_size = $helper->get('icon_size');
					$icon_color = $helper->get('icon_color');
				?>
				<div class="item col-xs-12" data-hash="item<?php echo$i;?>">
					<div class="team-image"><img src="<?php echo $helper->get('team_image' , $i).'" alt="'.$helper->get('team_name' , $i); ?>"></div>
					<div class="magnifier">
						<h4 class="team-name"><?php echo ($helper->get('team_name' , $i));?></h4>
						<?php if ($helper->get('team_position' , $i)) { echo	'<p class="team-position">'.$helper->get('team_position' , $i).'</p>'; }?>
						<?php if ($helper->get('team_experience' , $i)) { echo '<p class="team-experience">'.$helper->get('team_experience' , $i).'</p>';}?>
						
						<?php if ($twitter || $facebook || $gplus || $youtube || $instagram || $soc_url) :?>
						<div class="team-links">
							<?php if ($twitter) { echo '<a class="btn-twitter '.$icon_color.' '.$icon_style.' '.$icon_size.'" href="'.$helper->get('team_twitter' , $i).'" target="_blank"><i class="fa fa-twitter"></i></a>'; }?>
							<?php if ($facebook) { echo '<a class="btn-facebook '.$icon_color.' '.$icon_style.' '.$icon_size.'" href="'.$facebook.'" target="_blank"><i class="fa fa-facebook"></i></a>'; }?>
							<?php if ($gplus) { echo '<a class="btn-google '.$icon_color.' '.$icon_style.' '.$icon_size.'" href="'.$gplus.'" target="_blank"><i class="fa fa-google-plus"></i></a>'; }?>
							<?php if ($youtube) { echo '<a class="btn-youtube '.$icon_color.' '.$icon_style.' '.$icon_size.'" href="'.$youtube.'" target="_blank"><i class="fa fa-youtube-square"></i></a>'; }?>
							<?php if ($instagram) { echo '<a class="btn-instagram '.$icon_color.' '.$icon_style.' '.$icon_size.'" href="'.$instagram.'" target="_blank"><i class="fa fa-instagram"></i></a>'; }?>
							<?php if ($soc_url) { echo '<a class="btn-custom '.$icon_style.' '.$icon_size.'" href="'.$soc_url.'" target="_blank"><i class="'.$helper->get('team_soc_icon' , $i).'"></i></a>'; }?>
						</div>
						<?php endif;?>
					</div>
				</div>
			<?php endfor; ?>
			</div>
			<div class="team-owl-hash clearfix">
			<?php for ($j=0; $j < $count; $j++) :?>
				<div class="team-thumb col-xs-<?php echo $helper->get('columns');?>">
					<a href="#item<?php echo $j;?>">
						<img src="<?php echo $helper->get('team_image', $j);?>">
					</a>
				</div>
			<?php endfor;?>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		jQuery('.owl-team-4').owlCarousel({
		    items:1	,
		    loop:false,
		    dots:false,
		    center:true,
		    nav:false,
			smartSpeed:500,
		    margin:15,
		    URLhashListener:true,
	        autoplayHoverPause:true,
	        startPosition: 'URLHash'
		})
	});
</script>