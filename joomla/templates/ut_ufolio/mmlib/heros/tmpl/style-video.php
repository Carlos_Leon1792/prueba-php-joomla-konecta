<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Hero variables
$intro = $helper->get('hero_intro');
$subtitle = $helper->get('hero_subtitle');
$yt = $helper->get('hero_yt_video_id');
$vim = $helper->get('hero_vim_video_id');
$count = $helper->count('hero_link_url');

?>

<div class="hero-video">
	<div class="hero-video-wrapper">
		<?php if ($yt) { echo '<iframe src="https://www.youtube.com/embed/'.$yt.'?rel=0&amp;playlist='.$yt.'&amp;controls=0&amp;autoplay=1&amp;loop=1&amp;html5=1&amp;showinfo=0" allowfullscreen></iframe>';}?>
		<?php if ($vim) { echo '<iframe allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" src="//player.vimeo.com/video/'.$vim.'?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;loop=1"></iframe>';}?>
		<div class="hero-content <?php if ($add_container){echo 'container';}?>">
			<?php if ($module->showtitle){ echo '<h2 class="hero-title">'.$module->title.'</h2>'; }?>
			<?php if ($subtitle){echo '<h3 class="hero-subtitle">'.$subtitle.'</h3>';}?>
			<?php if ($intro){ echo '<p class="hero-intro">'.$intro.'</p>'; }?>
			<nav class="hero-links">
				<?php for ($i=0; $i < $count; $i++) :?>
				<?php if ($helper->get('hero_link_url', $i)) {
					echo '<a class="btn '.$helper->get('hero_btn_style', $i).' '.$helper->get('hero_btn_size', $i).'" href="'.$helper->get('hero_link_url', $i).'" target="'.$helper->get('hero_link_target' , $i).'">'.$helper->get('hero_link_name', $i).'</a>';
				}?>
				<?php endfor; ?>
			</nav>
		</div>
	</div>
</div>