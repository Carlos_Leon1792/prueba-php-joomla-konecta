<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Hero variables
$intro = $helper->get('hero_intro');
$count = $helper->count('hero_link_url');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'hero-'. $module->id;} ?>" class="hero" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="hero-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="hero-2 <?php if ($add_container){echo 'container';}?> clearfix">
			<div class="hero-content col-xs-12">
				<?php if ($module->showtitle) { echo '<h3 class="hero-title">'.$module->title.'</h3>'; }?>
				<p class="hero-intro"><?php echo $intro;?></p>
				<?php if ($helper->get('hero_link_name')) :?>
				<nav class="hero-links">
				<?php for ($i=0; $i < $count; $i++) :?>
				<?php if ($helper->get('hero_link_url', $i)) {
					echo '<a class="btn '.$helper->get('hero_btn_style', $i).' '.$helper->get('hero_btn_size', $i).'" href="'.$helper->get('hero_link_url', $i).'"target="'.$helper->get('hero_link_target' , $i).'">'.$helper->get('hero_link_name', $i).'</a>';
				}?>
				<?php endfor; ?>
				</nav>
				<?php endif;?>
			</div>
			<?php if ($helper->get('hero_image')) { echo '<div class="col-xs-12"><div class="hero-image"><img src="'.$helper->get('hero_image').'" class="img-responsive"></div></div>';} ?>
		</div>
	</div>
</div>