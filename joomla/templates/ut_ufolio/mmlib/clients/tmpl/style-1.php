<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Clients Variables
$subtitle = $helper->get('clients_subtitle');
$intro = $helper->get('clients_intro');
$count = $helper->count('clients_logo');

 ?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'clients-'. $module->id;} ?>" class="clients" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="clients-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="clients-1 <?php if ($add_container){echo 'container';}?> clearfix">
			<?php if (($module->showtitle) || $helper->get('clients_intro')){?>
			<div class="clients-header <?php echo 'text-'.$helper->get('clients_text_align');?>">
				<?php if ($module->showtitle) { echo '<h3 class="clients-title">'.$module->title.'</h3>'; }?>
				<?php if ($subtitle) { echo '<h4 class="clients-subtitle">'.$subtitle.'</h4>'; }?>
			</div>
			<?php if ($intro) { echo '<p class="clients-intro text-'.$helper->get('clients_text_align').'">'.$intro.'</p>'; }?>
			<?php }?>
			<div class="clients-row clearfix">
			<?php for ($i=0; $i < $count; $i++) :?>
				<div class="clients-logo col-xs-6 col-sm-<?php echo $helper->get('columns');?>">
				<a <?php echo 'href="'.$helper->get('clients_link' , $i).'" title="'.$helper->get('clients_name' , $i).'" target="_blank"';?>>
					<img src="<?php echo $helper->get('clients_logo' , $i);?>" alt="<?php echo $helper->get('clients_name' , $i);?>" class="img-responsive">
				</a>
				</div>
			<?php endfor;?>
			</div>
		</div>
	</div>
</div>