<?php
/**
* @package      Joomla.site
* @subpackage   mod_ut_multimodule
* @author       Unitemplates http://www.unitemplates.com
* @copyright    Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license      GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Skill Variables
$subtitle = $helper->get('skill_subtitle');
$intro = $helper->get('skill_intro');
$count = $helper->count('circle_percent');
?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'skill-'. $module->id;} ?>" class="skill" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
    <div class="skill-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
        <div class="skill-4 <?php if ($add_container){echo 'container';}?> clearfix">
            <div class="skill-header">
                <?php if ($module->showtitle) { echo '<h3 class="skill-title">'.$module->title.'</h3>'; }?>
                <?php if ($subtitle) { echo '<h4 class="skill-subtitle">'.$subtitle.'</h4>'; }?>
            </div>
            <?php if ($intro) { echo '<p class="skill-intro">'.$intro.'</p>';}?>
            <div class="skill-row clearfix">
                <?php for ($i=0; $i < $count; $i++) : ?>
                <div class="skill-container col-sm-6 col-md-<?php echo $helper->get('columns');?>">
                    <div id="circle-<?php echo $module->id.'-'.$i; ?>" 
                    <?php if ($helper->get('circle_startdegree')) :?>
                        data-startdegree="<?php echo $helper->get('circle_startdegree');?>"
                    <?php endif; ?>
                    <?php if ($helper->get('circle_type')) :?>
                        data-type="<?php echo $helper->get('circle_type');?>"
                    <?php endif; ?>
                    <?php if ($helper->get('circle_fill')) :?>
                        data-fill="<?php echo $helper->get('circle_fill');?>"
                    <?php endif; ?>
                    <?php if ($helper->get('circle_border')) :?>
                        data-border="<?php echo $helper->get('circle_border');?>"
                    <?php endif;?>
                    <?php if ($helper->get('circle_bordersize')) :?>
                        data-bordersize="<?php echo $helper->get('circle_bordersize');?>"
                    <?php endif;?>
                        data-dimension="<?php echo $helper->get('circle_dimension');?>"
                        data-width="<?php echo $helper->get('circle_width');?>"
                        data-fontsize="<?php echo $helper->get('circle_fontsize');?>"
                        data-text="<?php if ($helper->get('circle_text' , $i)){echo $helper->get('circle_text', $i);}else{echo $helper->get('circle_percent', $i).'%';}?>"
                    <?php if ($helper->get('circle_info', $i)) :?>
                        data-info="<?php echo $helper->get('circle_info' , $i);?>"
                    <?php endif; ?>
                        data-percent="<?php echo $helper->get('circle_percent' , $i);?>"
                        data-fgcolor="<?php echo $helper->get('circle_fgcolor' , $i);?>"
                        data-bgcolor="<?php echo $helper->get('circle_bgcolor' , $i);?>">
                    </div>
                </div>
                <?php endfor; ?>
                <script>
                    jQuery( document ).ready(function() {
                        <?php for ($i=0; $i < $count; $i++) :?>
                        jQuery('#circle-<?php echo $module->id.'-'.$i;?>').circliful();
                        <?php endfor; ?>
                    });
                </script>
            </div>
        </div>
    </div>
</div>
