<?php
/**
* @package      Joomla.site
* @subpackage   mod_ut_multimodule
* @author       Unitemplates http://www.unitemplates.com
* @copyright    Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license      GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Skill Variables
$subtitle = $helper->get('skill_subtitle');
$intro = $helper->get('skill_intro');
$count = $helper->count('skill_name');
?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'skill-'. $module->id;} ?>" class="skill" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="skill-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="skill-2 <?php if ($add_container){echo 'container';}?> clearfix">
            <div class="skill-header">
                <?php if ($module->showtitle) { echo '<h3 class="skill-title">'.$module->title.'</h3>'; }?>
                <?php if ($subtitle) { echo '<h4 class="skill-subtitle">'.$subtitle.'</h4>'; }?>
            </div>
            <?php if ($intro) { echo '<p class="skill-intro">'.$intro.'</p>';}?>
            <div class="skill-row clearfix">
                <?php for ($i=0; $i < $count; $i++) : ?>
                <div class="skill-container col-sm-<?php echo $helper->get('columns');?>">
                    <div class="skillbar" data-percent="<?php echo $helper->get('skill_percent', $i);?>%">
                        <div class="count-bar <?php echo $helper->get('skill_color', $i);?>">
                            <div class="title"><?php echo $helper->get('skill_name' , $i);?></div>
                            <div class="count"></div>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</div>
