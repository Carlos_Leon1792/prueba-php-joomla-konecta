<?php
/**
* @package      Joomla.site
* @subpackage   mod_ut_multimodule
* @author       Unitemplates http://www.unitemplates.com
* @copyright    Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license      GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Skill Variables
$subtitle = $helper->get('skill_subtitle');
$intro = $helper->get('skill_intro');
$count = $helper->count('count_value');
?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'skill-'. $module->id;} ?>" class="skill" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
    <div class="skill-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
        <div class="skill-3 <?php if ($add_container){echo 'container';}?> clearfix">
            <div class="skill-header">
                <?php if ($module->showtitle) { echo '<h3 class="skill-title">'.$module->title.'</h3>'; }?>
                <?php if ($subtitle) { echo '<h4 class="skill-subtitle">'.$subtitle.'</h4>'; }?>
            </div>
            <?php if ($intro) { echo '<p class="skill-intro">'.$intro.'</p>';}?>
            <div class="skill-row clearfix">
                <?php for ($i=0; $i < $count; $i++) : ?>
                <div class="skill-container col-sm-6 col-md-<?php echo $helper->get('columns');?>">
                    <?php if ($helper->get('count_image', $i) || $helper->get('count_icon', $i)) :?>
                    <div class="count-icon">
                        <?php if ($helper->get('count_image', $i)) { echo '<img src="'.$helper->get('count_image' , $i).'">';}?>
                        <?php if ($helper->get('count_icon', $i)) { echo '<i class="'.$helper->get('count_icon' , $i).'"></i>';}?>
                    </div>
                    <?php endif; ?>
                    <div class="count-ctrl">
                    <?php if ($helper->get('count_prefix' , $i)) { echo '<span>'.$helper->get('count_prefix' , $i).'</span>';}?>
                    <span class="count-count" 
                    data-decimals="<?php echo $helper->get('count_decimals', $i);?>" 
                    data-decimal-delimiter="<?php echo $helper->get('count_decimal_delimiter', $i);?>" 
                    data-thousand-delimiter="<?php echo $helper->get('count_thousand_delimiter', $i);?>" 
                    data-value="<?php echo $helper->get('count_value', $i);?>" 
                    data-duration="<?php echo $helper->get('count_duration', $i);?>"></span>
                    <?php if ($helper->get('count_postfix' , $i)) { echo '<span>'.$helper->get('count_postfix' , $i).'</span>';}?>
                    </div>
                    <?php if ($helper->get('count_name')) :?>
                        <h5 class="count-name"><?php echo $helper->get('count_name' , $i);?></h5>
                    <?php endif; ?>
                    <?php if ($helper->get('count_desc', $i)) :?>
                        <p class="count-desc"><?php echo $helper->get('count_desc', $i); ?></p>
                    <?php endif; ?>
                </div>
                <?php endfor; ?>
                <script type="text/javascript">
                    jQuery(function() {
                      jQuery(".count-count").countimator();
                    });
                </script>
            </div>
        </div>
    </div>
</div>
