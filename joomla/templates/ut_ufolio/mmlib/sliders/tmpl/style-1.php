<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$doc = JFactory::getDocument();
//Add animate.css modified by leoalv
if($helper->get('slider_load_animatecss')):
	$doc->addStyleSheet('templates/'.T3_TEMPLATE.'/mmlib/sliders/js/animate/animate.css');
endif;
//$doc->addScript('templates/'.T3_TEMPLATE.'/mmlib/sliders/js/animate/animate.js');
$add_container = $params->get('add_container')==1;
$custom_id = $params->get('custom_id');

//Slider variables
$count = $helper->count('slider_image');
$indicators = $helper->get('slider_indicators');
$controls = $helper->get('slider_controls');
$autoplay = $helper->get('slider_autoplay');
$interval = $helper->get('slider_interval');
$animatecss = $helper->get('slider_load-animatecss');

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";
?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'slider-'. $module->id;} ?>" class="sliders">
	<div class="slider-1">
		<div id="ut-carousel-<?php echo $module->id; ?>" class="carousel slide" data-ride="carousel" data-interval="<?php if($autoplay){echo $interval;}else{echo 'false';}?>">
		  	<!-- Indicators -->
		  	<?php if ($indicators) :?>
		    <ol class="carousel-indicators <?php echo $helper->get('slider_indicators_position');?>">
		    <?php for ( $i = 0; $i < $count; $i++) :?>
		          <li data-target="#ut-carousel-<?php echo $module->id ?>" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo 'active' ?>"></li>
		        <?php endfor; ?>
		  	</ol>
		  	<?php endif; ?>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php for ($i=0; $i < $count; $i++) :?>
				<div class="item animated <?php echo $helper->get('slider_image_effect', $i);?> <?php if ($i<1) echo "active";?>">
					<img src="<?php echo $helper->get('slider_image' , $i);?>" alt="Slide Image">
					<div class="item-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
						<div class="item-elements <?php if($add_container){echo'container';}?> <?php echo 'align-'.$helper->get('slider_align_content', $i);?> <?php echo 'distribute-'.$helper->get('slider_distribute_content', $i);?>">
							<?php for ($j=1; $j < 4; $j++) :?>
								<?php if($helper->get('slider_section'.$j.'', $i)):?>
								<div class="element-section animated 
								<?php echo $helper->get('slider_section'.$j.'_width', $i);
								echo ' '.$helper->get('slider_section'.$j.'_effect', $i);
								echo ' delay-'.$helper->get('slider_section'.$j.'_delay', $i).'s';
								echo ' duration-'.$helper->get('slider_section'.$j.'_duration', $i).'s';
								echo ' '.$helper->get('slider_section'.$j.'_text_align', $i);
								echo ' '.$helper->get('slider_section'.$j.'_align', $i);
								echo ' '.$helper->get('slider_section'.$j.'_hide', $i);
								?>">
									<?php echo $helper->get('slider_section'.$j.'', $i);?>
								</div>
								<?php endif;?>
							<?php endfor;?>
						</div>
					</div>					
				</div>
				<?php endfor; ?>
			</div>


			<!-- Controls -->
			<?php if ($controls) {?>
			<a class="left carousel-control <?php echo $helper->get('slider_controls_position')?> " href="#ut-carousel-<?php echo $module->id; ?>" role="button" data-slide="prev">
				<span class="fa fa-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control <?php echo $helper->get('slider_controls_position')?>" href="#ut-carousel-<?php echo $module->id; ?>" role="button" data-slide="next">
			    <span class="fa fa-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			</a>
			<?php }?>
		</div>
	</div>
</div>