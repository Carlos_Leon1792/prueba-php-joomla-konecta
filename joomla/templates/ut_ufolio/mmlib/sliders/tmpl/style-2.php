<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
//load animate.css
$assets_url = 'modules/mod_ut_multimodule/assets/';
$doc->addStylesheet($assets_url.'pogo/pogo-slider.min.css');
$doc->addScript($assets_url.'pogo/jquery.pogo-slider.min.js');

//Slider variables
$count = $helper->count('pogo_slider_img');
$targetsize = $helper->get('pogo_preserveTargetSize');
?>
<style type="text/css">
	@media screen and (max-width: 479px){.content-container{<?php if ($targetsize){echo 'display: block;';} else {echo 'display: none;';}?>}}
	.content-container .caption-container{margin-top:<?php echo $helper->get('pogo_caption_position');?>!important;}
	.content-container .content-image-container{margin-top:<?php echo $helper->get('pogo_content_img_position');?>!important;text-align: center;}
	<?php if ($helper->get('pogo_position_progress')){ echo '.pogoSlider-progressBar{bottom: 0;top: inherit;}';}?>
	<?php if ($targetsize){echo '@media(max-width: 479px){.caption-container p{font-size:10px;}}';}?>
</style>
<div class="sliders">
	<div class="slider-2">
		<div class="pogoSlider" id="ut-mm-pogo-slider-<?php echo $module->id;?>">
		<?php for ($i=0; $i < $count ; $i++) :?>
			<div class="pogoSlider-slide" data-transition="<?php echo $helper->get('pogo_slideTransition', $i);?>" data-duration="<?php echo $helper->get('pogo_slideTransitionDuration' , $i);?>" style="background-image:url(<?php echo $helper->get('pogo_slider_img', $i);?>);">
				<div class="content-container">	
					<div class="pogoSlider-slide-element caption-container <?php if ($helper->get('pogo_content_img', $i)){ echo 'w50';} else {echo 'w100';} ?> <?php echo $helper->get('pogo_caption_text_align' , $i);?> <?php echo $helper->get('pogo_caption_float' , $i);?> <?php if($helper->get('pogo_caption_in' , $i) || $helper->get('pogo_caption_out' , $i)) { echo '" data-in="'.$helper->get('pogo_caption_in' , $i). '" data-out="'.$helper->get('pogo_caption_out' , $i); }?>">
						<?php echo $helper->get('pogo_caption_content', $i);?>
					</div>

					<?php if ($helper->get('pogo_content_img' , $i)) :?>
					<div class="content-image-container w50 hidden-xs pogoSlider-slide-element" data-in="<?php echo $helper->get('pogo_content_img_in', $i);?>" data-out="<?php echo $helper->get('pogo_content_img_out', $i);?>">
						<img src="<?php echo $helper->get('pogo_content_img', $i);?>">
					</div>
					<?php endif;  ?>
				</div>
			</div>
		<?php endfor;?>
		</div><!-- .pogoSlider -->
	</div>
	<script>
		jQuery(document).ready(function ($) {

			$('#ut-mm-pogo-slider-<?php echo $module->id;?>').pogoSlider({
				autoplay: <?php echo $helper->get('pogo_autoplay');?>,
				autoplayTimeout: <?php echo $helper->get('pogo_autoplayTimeout');?>,
				displayProgess: <?php echo $helper->get('pogo_displayProgress');?>,
				generateButtons: <?php echo $helper->get('pogo_generateButtons');?>,
				buttonPosition: '<?php echo $helper->get('pogo_buttonPosition');?>',
				generateNav: <?php echo $helper->get('pogo_generateNav');?>,
				navPosition: '<?php echo $helper->get('pogo_navPosition');?>',
				preserveTargetSize: <?php if ($targetsize){ echo $targetsize;} else {echo 'false';}?>,
				targetWidth: <?php echo $helper->get('pogo_targetWidth');?>,
				targetHeight: <?php echo $helper->get('pogo_targetHeight');?>,
				pauseOnHover:<?php echo $helper->get('pogo_pauseOnHover');?>,
				responsive: true
			}).data('plugin_pogoSlider');
		});
	</script>
</div>