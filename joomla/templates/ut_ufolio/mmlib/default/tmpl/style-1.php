<?php
/**
* @package    Joomla.site
* @subpackage   mod_ut_multimodule
* @author   Unitemplates http://www.unitemplates.com
* @copyright  Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license    GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";
//Default variables
$subtitle = $helper->get('default_subtitle');
$intro = $helper->get('default_intro');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'default-'. $module->id;} ?>" class="default" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
  <div class="default-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
    <div class="default-1 <?php if ($add_container){echo 'container';}?>">
    	<div class="default-header">
			<?php if ($module->showtitle) { echo '<h3 class="default-title">'.$module->title.'</h3>'; }?>
			<?php if ($subtitle) { echo '<h4 class="default-subtitle">'.$subtitle.'</h4>'; }?>
		</div>
      <?php if ($intro) { echo '<p class="default-intro">'.$intro.'</p>'; }?>
    </div>
  </div>
</div>
