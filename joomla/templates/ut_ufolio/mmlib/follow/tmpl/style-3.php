<?php
/**
* @package    Joomla.site
* @subpackage   mod_ut_multimodule
* @author   Unitemplates http://www.unitemplates.com
* @copyright  Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license    GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

//Clients Variables
$count = $helper->count('follow_soc_icon');

 ?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'follow-'. $module->id;} ?>" class="follow">
	<div class="follow-ct <?php echo $params->get('bg_color');?>">
		<div class="follow-3 <?php if ($add_container){echo 'container';}?> clearfix">
			<?php if($module->showtitle) {?>
			<div class="follow-header">
				<h3 class="follow-title"><?php echo $module->title;?></h3>
			</div>
			<?php } ?>
			
			<div class="follow-row clearfix" itemscope itemtype="http://schema.org/Organization">
				<?php for ($i=0; $i < $count; $i++) :?>
					<div class="follow-item col-xs-6 col-sm-<?php echo $helper->get('columns');?>">
						<a class="btn-<?php echo $helper->get('follow_soc_name' , $i);?> <?php echo $helper->get('icon_color');?>" href="<?php echo $helper->get('follow_soc_url' , $i);?>" target="_blank" itemprop="sameAs">
							<i class="<?php echo $helper->get('follow_soc_icon' , $i);?>"></i><span><?php echo $helper->get('follow_soc_name' , $i);?></span>
						</a>
					</div>
				<?php endfor; ?>
			</div>
		</div>
	</div>
</div>