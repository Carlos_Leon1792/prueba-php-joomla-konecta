<?php
/**
* @package    Joomla.site
* @subpackage   mod_ut_multimodule
* @author   Unitemplates http://www.unitemplates.com
* @copyright  Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license    GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
//Clients Variables
$count = $helper->count('follow_soc_icon');

 ?>
<div class="follow">
	<div class="follow-ct">
		<div class="follow-1 clearfix">
			<?php if($module->showtitle) {?>
			<div class="follow-header">
				<h3 class="follow-title"><?php echo $module->title;?></h3>
			</div>
			<?php } ?>
			
			<div class="follow-row flearfix">
				<?php for ($i=0; $i < $count; $i++) :?>
					<a class="btn-<?php echo $helper->get('follow_soc_name' , $i);?> <?php echo $helper->get('icon_color');?> <?php echo $helper->get('icon_style');?> <?php echo $helper->get('icon_size');?>" href="<?php echo $helper->get('follow_soc_url' , $i);?>" target="_blank"><i class="<?php echo $helper->get('follow_soc_icon' , $i);?>"></i></a>
				<?php endfor; ?>
			</div>
		</div>
	</div>
</div>