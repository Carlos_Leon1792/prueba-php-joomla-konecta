<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Testimonial variables
$count = $helper->count('testimonio');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'testimonial-'. $module->id;} ?>" class="testimonial" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="testimonial-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="testimonial-6 <?php if ($add_container){echo 'container';}?> clearfix">

				<!-- Tab panes -->
				<div class="tab-content">
			    	<?php for ($i=0; $i < $count; $i++) : ?>
			    	<div role="tabpanel" class="tab-pane fade <?php if($i==1) echo "in active";?>" id="testimonial-<?php echo $module->id.'-'.$i;?>">
						<div class="testimonial-item">
							<div class="testimonial-name">
								<h4 class="name"><?php echo ($helper->get('authorname' , $i));?></h4>
								<?php if ($helper->get('author_position' , $i)) { echo	'<h5 class="position">'.$helper->get('author_position' , $i).'</h5>'; }?>
							</div>
							<?php if ($helper->get('testimonio' , $i)) { echo '<p class="testimonio">'.$helper->get('testimonio' , $i).'</p>';}?>
						</div>
					</div>
					<?php endfor; ?>
				</div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
				<?php for ($i=0; $i < $count; $i++) :?>
				  <li role="presentation" class="testimonial-image <?php if($i==1) echo "active";?>"><a href="#testimonial-<?php echo $module->id.'-'.$i;?>" aria-controls="testimonial-<?php echo $module->id.'-'.$i;?>" role="tab" data-toggle="tab"><img src="<?php echo $helper->get('authorimage' , $i).'" alt="'.$helper->get('authorname' , $i); ?>"></a></li>
				<?php endfor; ?>
				</ul>
		</div>
	</div>
</div>