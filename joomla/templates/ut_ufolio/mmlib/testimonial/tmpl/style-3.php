<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

$mod_url = 'modules/mod_ut_multimodule/assets/';
$doc = JFactory::getDocument();
$doc->addStylesheet($mod_url.'owl/owl.carousel.css');
$doc->addStylesheet($mod_url.'owl/owl.theme.default.min.css');
$doc->addScript($mod_url.'owl/owl.carousel.min.js');
// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

// Testimonial Variables
$subtitle = $helper->get('testimonial_subtitle');
$count = $helper->count('testimonio');

 ?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'testimonial-'. $module->id;} ?>" class="testimonial" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="testimonial-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="testimonial-3 <?php if ($add_container){echo 'container';}?>">
			<div class="testimonial-header">
				<?php if ($module->showtitle) { echo '<h3 class="testimonial-title">'.$module->title.'</h3>';}?>
				<?php if ($subtitle) {echo  '<h4 class="testimonial-subtitle">' .$subtitle.'</h4>'; }?>
			</div>
			<?php if ($helper->get('testimonial_intro')) { echo '<p class="testimonial-intro">'.$helper->get('testimonial_intro').'</p>';}?>
		  	<!-- testimony's -->
			<div class="owl-testimonial-3">
			<?php for ($i=0; $i < $count; $i++) : ?>
				<div class="item col-xs-12">
					<?php if ($helper->get('authorimage' ,  $i)) {?>
						<div class="author-image"><img src="<?php echo ($helper->get('authorimage' ,  $i)); ?>" alt="<?php echo ($helper->get('authorname' ,  $i));  ?>"></div>
					<?php } ?>
					<?php if ($helper->get('authorname' ,  $i)) {?>
					<h4 class="author-name"><?php echo ($helper->get('authorname' ,  $i));  ?></h4>
					<?php } ?>
					<?php if ($helper->get('author_position' ,  $i)) { ?>
					<h5 class="author-position"><?php echo ($helper->get('author_position' ,  $i));  ?></h5>
					<?php } ?>
					<i class="fa fa-newspaper-o"></i>
					<?php if ($helper->get('testimonio' ,  $i)) { ?>
					<p class="testimonio"></i><?php echo ($helper->get('testimonio' ,  $i));  ?></p>
					<?php } ?>
				</div>
			<?php endfor; ?>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		jQuery('.owl-testimonial-3').owlCarousel({
		    loop:true,
		    dots:true,
		    nav:false,
		    margin:15,
		    responsive:{
		        0:{
		            items:1
		        },
		        768:{
		            items:2
		        },
		        1000:{
		            items:2
		        }
		    }
		})
	});
</script>