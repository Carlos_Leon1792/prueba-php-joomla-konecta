<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

// Testimonial Variables
$count = $helper->count('testimonio');

 ?>
<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'testimonial-'. $module->id;} ?>" class="testimonial" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="testimonial-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div id="testimonial-slider-<?php echo $module->id; ?>" class="carousel slide testimonial-1 <?php if ($add_container){echo 'container';}?>" data-ride="carousel">
		  	<!-- testimony's -->
			<div class="carousel-inner" role="listbox">
			<?php for ($i=0; $i<$count; $i++) :?>
				<div class="item <?php if($i<1) echo "active"; ?>">
					<?php if ($helper->get('testimonio' , $i)) { ?>
					<p class="testimonio"><i class="fa fa-quote-left"></i><?php echo ($helper->get('testimonio' , $i));  ?></p>
					<?php } ?>
					<?php if ($helper->get('authorimage' , $i)) {?>
						<div class="author-image"><img src="<?php echo ($helper->get('authorimage' , $i)); ?>" alt="Author"></div>
					<?php } ?>
					<?php if ($helper->get('authorname' , $i)) {?>
					<div class="author-name"><?php echo ($helper->get('authorname' , $i));  ?></div>
					<?php } ?>
					<?php if ($helper->get('author_position' , $i)) { ?>
					<div class="author-position"><?php echo ($helper->get('author_position' , $i));  ?></div>
					<?php } ?>
				</div>
			<?php endfor;?>
			</div>
		    <!-- indicators -->
		    <ol class="carousel-indicators">
			    <?php for ( $i = 0; $i < $count; $i++) :?>
		          <li data-target="#testimonial-slider-<?php echo $module->id ?>" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo 'active' ?>"></li>
		        <?php endfor; ?>
		  	</ol>

		</div>
	</div>
</div>