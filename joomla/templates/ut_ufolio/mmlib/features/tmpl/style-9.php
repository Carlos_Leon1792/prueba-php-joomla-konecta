<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

$mod_url = 'modules/mod_ut_multimodule/assets/';
$doc = JFactory::getDocument();
$doc->addStylesheet($mod_url.'owl/owl.carousel.css');
$doc->addStylesheet($mod_url.'owl/owl.theme.default.min.css');
$doc->addScript($mod_url.'owl/owl.carousel.min.js');
// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Featured variables
$subtitle = $helper->get('feature_subtitle');
$intro = $helper->get('feature_intro');
$count = $helper->count('feature_name');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'feature-'. $module->id;} ?>" class="feature" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="feature-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="feature-9 <?php if ($add_container){echo 'container';}?> clearfix">
			<?php if ($module->showtitle) { echo '<h3 class="feature-title">'.$module->title.'</h3>'; }?>
			<?php if ($subtitle) { echo '<h4 class="feature-subtitle">'.$subtitle.'</h4>'; }?>
			<?php if ($intro) { echo '<p class="feature-intro">'.$intro.'</p>'; }?>
			<div class="owl-feature-9">
				<?php for ($i=0; $i<$count ; $i++) :?>
					<div class="item col-xs-12">
						<div class="feature-item-ct">
							<?php if ($helper->get('feature_image') || $helper->get('feature_icon')) :?>
							<div class="feature-icon">
								<?php if ($helper->get('feature_image' , $i)) { echo '<img src="'.$helper->get('feature_image' , $i).'">'; } ?>
								<?php if ($helper->get('feature_icon' , $i)) { echo '<i class="'.$helper->get('feature_icon' , $i).'"></i>'; } ?>
							</div>
							<?php endif; ?>

							<?php if ($helper->get('feature_name')) :?>
								<h4 class="feature-name"><?php echo $helper->get('feature_name' , $i); ?></h4>
							<?php endif; ?>

							<?php if ($helper->get('feature_desc')) :?>
								<p class="feature-desc"><?php echo $helper->get('feature_desc' , $i); ?></p>
							<?php endif; ?>

							<?php if ($helper->get('feature_link_name' , $i)) {
								echo '<a href="'.$helper->get('feature_link_url' , $i).'" class="btn btn-primary">'.$helper->get('feature_link_name', $i).'</a>' ;
							} ?>
						</div>
					</div>
				<?php endfor; ?>
			</div>
			<script>
				jQuery(document).ready(function(){
					jQuery('.owl-feature-9').owlCarousel({
					    loop:true,
					    dots:true,
					    nav:false,
					    autoplay:true,
					    margin:15,
					    responsive:{
					        0:{
					            items:1
					        },
					        768:{
					            items:2
					        },
					        1000:{
					            items:<?php echo 12 / $helper->get('columns'); ?>
					        }
					    }
					})
				});
			</script>
		</div>
	</div>
</div>