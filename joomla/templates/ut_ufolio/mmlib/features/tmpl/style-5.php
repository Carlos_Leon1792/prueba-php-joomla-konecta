<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Featured variables
$subtitle = $helper->get('feature_subtitle');
$intro = $helper->get('feature_intro');
$count = $helper->count('feature_name');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'feature-'. $module->id;} ?>" class="feature" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="feature-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="feature-5 <?php if ($add_container){echo 'container';}?>">
			<?php if ($module->showtitle) { echo '<h3 class="feature-title">'.$module->title.'</h3>'; }?>
			<?php if ($subtitle) { echo '<h4 class="feature-subtitle">'.$subtitle.'</h4>'; }?>
			<?php if ($intro) { echo '<p class="feature-intro">'.$intro.'</p>'; }?>

			<div class="feature-row clearfix">
				<?php for ($i=0; $i < $count; $i++) : ?>
					<div class="feature-item col-sm-<?php echo $helper->get('columns');?>">
						<div class="feature-item-ct">
							<div class="feature-icon">
								<?php if ($helper->get('feature_image' , $i)) { echo '<img src="'.$helper->get('feature_image' , $i).'">';} ?>
								<?php if ($helper->get('feature_icon' , $i)) { echo '<i class="'.$helper->get('feature_icon' , $i).'"></i>';} ?>
							</div>
							<h4 class="feature-name"><?php echo $helper->get('feature_name' , $i); ?></h4>
						</div>
					</div>
				<?php endfor; ?>
			</div>
		</div>
	</div>
</div>