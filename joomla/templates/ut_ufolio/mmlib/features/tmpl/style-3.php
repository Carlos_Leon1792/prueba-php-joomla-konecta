<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Featured variables
$intro = $helper->get('feature_intro');
$subtitle = $helper->get('feature_subtitle');
$count = $helper->count('feature_name');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'feature-'. $module->id;} ?>" class="feature" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="feature-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="feature-3 <?php if ($add_container){echo 'container';}?>">
			<div class="feature-header">
				<?php if ($module->showtitle) { echo '<h3 class="feature-title">'.$module->title.'</h3>'; }?>
				<?php if ($subtitle) { echo '<h4 class="feature-subtitle">'.$subtitle.'</h4>'; }?>
			</div>			
			<?php if ($intro) { echo '<p class="feature-intro">'.$intro.'</p>'; }?>
			<div class="feature-row clearfix">
				<?php if ($helper->get('feature_big_image')) :?>
				<div class="feature-image col-sm-4 hidden-xs"><img src="<?php echo $helper->get('feature_big_image'); ?>" alt="<?php echo $module->title;?>"></div>
				<?php endif; ?>
				
				<?php for ($i=0; $i<$count ; $i++) : ?>
					<div class="feature-item col-sm-4">
						<div class="feature-item-ct">
							<div class="feature-icon">
								<?php if ($helper->get('feature_image' , $i)) { echo '<img src="'.$helper->get('feature_image' , $i).'">';} ?>
								<?php if ($helper->get('feature_icon' , $i)) { echo '<i class="'.$helper->get('feature_icon' , $i).'"></i>';} ?>
							</div>
							<h4 class="feature-name"><?php echo $helper->get('feature_name' , $i); ?></h4>
							<p class="feature-desc"><?php echo $helper->get('feature_desc' , $i); ?></p>
							<?php if ($helper->get('feature_link_name' , $i)) {
								echo '<a href="'.$helper->get('feature_link_url' , $i).'" class="btn btn-primary">'.$helper->get('feature_link_name' , $i).'</a>' ;
							} ?>
						</div>
					</div>
				<?php endfor; ?>				
			</div>
		</div>
	</div>
</div>