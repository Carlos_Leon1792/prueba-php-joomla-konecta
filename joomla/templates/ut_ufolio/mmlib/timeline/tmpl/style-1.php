<?php
/**
* @package    Joomla.site
* @subpackage   mod_ut_multimodule
* @author   Unitemplates http://www.unitemplates.com
* @copyright  Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license    GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";
//Timeline variables
$subtitle = $helper->get('timeline_subtitle');
$intro = $helper->get('timeline_intro');
$count = $helper->count('timeline_date');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'timeline-'. $module->id;} ?>" class="timeline" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
  <div class="timeline-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
    <div class="timeline-1 <?php if ($add_container){echo 'container';}?>">
    	<div class="timeline-header">
			<?php if ($module->showtitle) { echo '<h3 class="timeline-title">'.$module->title.'</h3>'; }?>
			<?php if ($subtitle) { echo '<h4 class="timeline-subtitle">'.$subtitle.'</h4>'; }?>
		</div>
		<?php if ($intro) { echo '<p class="timeline-intro">'.$intro.'</p>'; }?>
		<div class="timeline-row">
			<div class="timeline-list">
				<?php for ($i=0; $i < $count; $i++) :?>
					<div class="row">
						<time class="timeline-date col-sm-6 <?php if($i % 2 == 1){echo'pull-left';}else{echo'pull-right';};?>" datetime="<?php echo $helper->get('timeline_date' , $i);?>"><?php echo $helper->get('timeline_date' , $i);?></time>
						<div class="timeline-item col-sm-6 <?php if ($i % 2 == 1){echo 'pull-right';}else{echo'pull-left';}?>">
							<div class="timeline-content">
								<?php if ($helper->get('timeline_image') || $helper->get('timeline_icon')) :?>
									<div class="timeline-figure">
										<?php if ($helper->get('timeline_image' , $i)) :?>
											<img src="<?php echo $helper->get('timeline_image' , $i);?>">
										<?php endif; ?>
										<?php if ($helper->get('timeline_icon' , $i)) :?>
											<i class="<?php echo $helper->get('timeline_icon' , $i);?>"></i>
										<?php endif; ?>
									</div>
								<?php endif; ?>
								<?php if ($helper->get('timeline_name')) :?>
									<h4 class="timeline-name"><?php echo $helper->get('timeline_name' , $i);?></h4>
								<?php endif; ?>
								<?php if ($helper->get('timeline_desc' , $i)) :?>
									<p class="timeline-desc"><?php echo $helper->get('timeline_desc' , $i);?></p>
								<?php endif; ?>
								<?php if ($helper->get('timeline_btn_link' , $i)) :?>
									<a class="btn <?php echo $helper->get('timeline_btn_style', $i);?>" href="<?php echo $helper->get('timeline_btn_link' , $i);?>"><?php echo $helper->get('timeline_btn_name' , $i);?></a>
								<?php endif;?>
							</div>
						</div>
					</div>
				<?php endfor; ?>
			</div>
		</div>
    </div>
  </div>
</div>
