<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @author		Unitemplates http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/

defined('_JEXEC') or die;
$custom_id = $params->get('custom_id');
$add_container = $params->get('add_container')==1;

// RGBA Custom bg 
$bg_custom = $params->get('bg_custom');
$opacity = $params->get ('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";

//Featured variables
$intro = $helper->get('pricing_intro');
$subtitle = $helper->get('pricing_subtitle');
$count = $helper->count('pricing_plan_name');
?>

<div id="<?php if($custom_id) {echo $custom_id;} else{echo 'pricing-'. $module->id;} ?>" class="pricing" <?php if ($params->get('bg_image')) { echo 'style="background-image:url('.$params->get('bg_image').')"';} ?>>
	<div class="pricing-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="pricing-1 <?php if ($add_container){echo 'container';}?>">
			<?php if ($module->showtitle) { echo '<h3 class="pricing-title">'.$module->title.'</h3>'; }?>
			<?php if ($subtitle) { echo '<h4 class="pricing-subtitle">'.$subtitle.'</h4>'; }?>
			<?php if ($intro) { echo '<p class="pricing-intro">'.$intro.'</p>'; }?>
			
			<div class="pricing-row clearfix">
				<?php for ($i=0; $i<$count; $i++) : ?>
					<div class="pricing-item col-sm-<?php echo $helper->get('columns'); ?> <?php if($helper->get('pricing_plan_featured', $i)){echo'plan-featured';}?>">
						<div class="pricing-item-ct">
							<div class="plan-name">
								<h3 class="name"><?php echo $helper->get('pricing_plan_name' , $i); ?></h3>
							</div>
							<div class="plan-price">
								<h4 class="price"><?php echo $helper->get('pricing_plan_price' , $i); ?></h4>

								<?php if($helper->get('pricing_plan_valid', $i)):?>
								<p class="valid"><?php echo $helper->get('pricing_plan_valid', $i);?></p>
								<?php endif; ?>
							</div>						

							<?php if($helper->get('pricing_plan_info', $i)) :?>
							<div class="plan-info">
								<ul><?php echo $helper->get('pricing_plan_info', $i);?></ul>
							</div>
							<?php endif;?>

							<?php if ($helper->get('pricing_plan_link_name' , $i)):?>
								<div class="plan-link">
									<a href="<?php echo $helper->get('pricing_plan_link_url' , $i);?>" class="btn btn-<?php if($helper->get('pricing_plan_featured', $i)){echo'primary';}else{echo 'inverse';}?> btn-block btn-lg"><?php echo $helper->get('pricing_plan_link_name' , $i);?></a>
								</div>
							<?php endif;?>
						</div>
					</div>
				<?php endfor ?>
			</div>
		</div>
	</div>
</div>