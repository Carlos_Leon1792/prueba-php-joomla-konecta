<?php
// add css and jquery
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::root(true) . '/modules/mod_ut_multimodule/admin/assets/admin.css');
$doc->addScript(JUri::root(true) . '/modules/mod_ut_multimodule/admin/assets/script.js');
// add jquery-IU
$doc->addStyleSheet(JUri::root(true) . '/modules/mod_ut_multimodule/admin/assets/jquery-ui/jquery-ui.min.css');
$doc->addScript(JUri::root(true) . '/modules/mod_ut_multimodule/admin/assets/jquery-ui/jquery-ui.min.js');

$fields = $displayData['fields'];
$group_types = $displayData['group_types'];

?>

<div class="mm-admin joomla<?php echo substr(JVERSION, 0, 1) ?>">

	<div class="control-group multimodule-header ">

		<div class="control-label">
			<label id="multimodule-type-lbl" for="multimodule-type" class="hastip" title="<?php echo JText::_('UT_MM_TYPE_DESC') ?>"><?php echo JText::_('UT_MM_TYPE_LABEL') ?></label>
		</div>

		<div class="controls">
			<select id="multimodule-type" name="multimodule-type" class="required">
				<option value="" selected="selected"><?php echo JText::_('UT_MM_LAYOUT_DEFAULT') ?></option>
				<?php foreach ($group_types as $tpl => $types) : ?>
					<optgroup id="multimodule-type-<?php echo $tpl ?>" label="<?php if ($tpl=='_'): ?>---From Module---<?php else: ?>---From <?php echo $tpl ?> Template---<?php endif ?>">
						<?php foreach ($types as $type => $title): ?>
							<option value="<?php echo $tpl ?>:<?php echo $type ?>"><?php echo $title ?></option>
						<?php endforeach ?>
					</optgroup>
				<?php endforeach ?>
			</select>
		</div>

	</div>

	<?php foreach ($fields as $type => $fieldsets) : ?>
	<div id="multimodule-<?php echo $type ?>" class="multimodule-layout-config hide">
		<?php echo $fieldsets ?>
	</div>
	<?php endforeach ?>

</div>

<div id="mm-dialog-confirm" class="hide" title="<?php echo JText::_('UT_MM_CONFIRM_DELETE_TITLE') ?>">
	<?php echo JText::_('UT_MM_CONFIRM_DELETE_MSG') ?>
</div>

<script>
	MultimoduleInit(jQuery);
</script>
