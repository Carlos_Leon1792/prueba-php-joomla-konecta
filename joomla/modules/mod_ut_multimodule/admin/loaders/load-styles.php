<?php
$fieldsets = $displayData['fieldsets'];
$fieldsets_html = $displayData['fieldsets_html'];
$description = $displayData['description'];
$layouts = $displayData['layouts'];
$type = $displayData['type'];
$sampledata = $displayData['sample-data'];
?>


<input name="multimodule-sample-data" type="hidden" value="<?php echo htmlspecialchars($sampledata, ENT_COMPAT, 'UTF-8') ?>" data-ignoresave="1" />


<!-- Layout field -->
<div class="control-group multimodule-subheader">
	<div class="control-label"><label id="multimodule-layout-<?php echo $type ?>-lbl" for="multimodule-layout-<?php echo $type ?>" class="hasTip" title="<?php echo JText::_('UT_MM_LAYOUT_DESC') ?>"><?php echo JText::_('UT_MM_LAYOUT_LABEL') ?></label></div>
	<div class="controls">
		<select id="multimodule-layout-<?php echo $type ?>" class="multimodule-layouts" name="multimodule-layout-<?php echo $type ?>">
			<?php foreach ($layouts as $layout): ?>
				<option value="<?php echo $layout ?>"><?php echo $layout ?></option>
			<?php endforeach ?>
		</select>
	</div>
</div>

<?php if ($description): ?>
<p class="multimodule-layout-desc"><?php echo $description ?></p>
<?php endif ?>

<?php
if (!is_array($fieldsets)) return;
foreach ($fieldsets as $name => $fieldset) :
	$multiple = isset($fieldset->multiple) ? $fieldset->multiple : false;
	$support_layouts = isset($fieldset->layouts) ? ' data-layouts="' . $fieldset->layouts . '"' : '';
?>

<div class="multimodule-group clearfix <?php if ($multiple): ?>multimodule-multiple<?php endif ?>"<?php echo $support_layouts ?>>

	<div class="multimodule-group-header clearfix">
		<h3 class="fieldset-title"><?php echo JText::_($fieldset->label) ?></h3>
		<p class="fieldset-desc"><?php echo JText::_($fieldset->description) ?></p>
	</div>

	<div class="multimodule-row clearfix">
		<?php echo $fieldsets_html [$name] ?>
	</div>

	<?php if ($multiple): ?>
	<div class="multimodule-row-actions clearfix">
		<div class="btn btn-primary multimodule-btn-add"><?php echo JText::_('UT_MM_BTN_ADD') ?></div>
	</div>

	<div class="btn btn-danger multimodule-btn-del"><?php echo JText::_('UT_MM_BTN_DEL') ?></div>
	<?php endif ?>

</div>

<?php endforeach ?>