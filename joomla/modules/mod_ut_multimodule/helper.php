<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class ModMultimoduleHelper
{
	protected $data = null;
	protected $layout = '';
	protected $type = '';
	protected $template = '';
	protected $group = '';
	protected $keys = array();

	public function __construct($params)
	{
		$config = json_decode($this->decode($params->get('multimodule-config')), true);
		$type = '';
		if (isset($config[':type'])) {
			$type = $config[':type'];
		} else if (isset($config[':layout'])) {
			$type = $config[':layout'];
		}

		$tmp = explode(':', $type, 2);
		$this->type = count($tmp) > 1 ? $tmp[1] : $tmp[0];
		$this->template = count($tmp) > 1 && $tmp[0] != '_' ? $tmp[0] : '';

		$this->data = isset($config[$this->type]) ? $config[$this->type] : array();

		// detect group key
		$this->keys = array_keys($this->data);

		// detect layout
		$this->layout = $this->get('multimodule-layout-' . $this->type, 0, 'default');
	}

	public function decode($str)
	{
		return str_replace(array('((', '))'), array('<', '>'), $str);
	}

	public function get($name, $i = 0, $default = null)
	{
		$value = (array)$this->findParam($name);
		$val = isset($value[$i]) ? $value[$i] : $default;
		if (preg_match ('/^(.*)\|filter:(.*)$/', $val, $match) && method_exists($this, 'filter'.$match[2])) {
			$func = 'filter'.$match[2];
			$val = $this->$func ($match[1]);
		}
		return $val;
	}

	public function count($name)
	{
		$value = (array)$this->findParam($name);
		return count($value);
	}

	public function findParam($name)
	{
		$pattern = '/\[' . preg_quote($name) . '\]/';
		foreach ($this->keys as $key) {
			if ($key == $name || preg_match($pattern, $key)) return $this->data[$key];
		}
		return null;
	}

	public function find($filename, $get_url = false, $return_default = false)
	{
		$template = JFactory::getApplication()->getTemplate();

		// Build the template and base path for the layout
		$paths = array();
		$paths[JUri::base(true) . '/modules/mod_ut_multimodule/mmlib/'] = JPATH_BASE . '/modules/mod_ut_multimodule/mmlib/';
		// current template
		$paths[JUri::base(true) . '/templates/' . $template . '/mmlib/'] = JPATH_THEMES . '/' . $template . '/mmlib/';
		// config template
		$paths[JUri::base(true) . '/templates/' . $this->template . '/mmlib/'] = JPATH_THEMES . '/' . $this->template . '/mmlib/';
		// for T3: local folder
		$paths[JUri::base(true) . '/' . $template . '/local/mmlib/'] = JPATH_THEMES . '/' . $template . '/local/mmlib/';

		foreach ($paths as $uri => $path) {
			if (is_file($path . $this->type . '/' . $filename))
				return ($get_url ? $uri : $path) . $this->type . '/' . $filename;
		}

		// return default
		if ($return_default) {
			$default = $get_url ? JUri::base(true) . '/templates/' . $this->template . '/mmlib/' : JPATH_THEMES . '/' . $this->template . '/mmlib/';
			return $default . $this->type . '/' . $filename;
		}
		
		return null;
	}

	public function getLayout()
	{
		$layout = $this->find('tmpl/' . $this->layout . '.php');
		if (!$layout) $layout = $this->find('tmpl/style-1.php');
		return $layout;
	}

	public function addAssets()
	{
		$doc = JFactory::getDocument();

		if (($css = $this->find('css/style.css', true, true))) {
			$doc->addStyleSheet($css);
		}
		if (($js = $this->find('js/script.js', true))) {
			$doc->addScript($js);
		}
	}

}
