<?php
/**
* @package 		Joomla.site
* @subpackage 	mod_ut_multimodule
* @version		2.0.0
* @created		Jan 2015
* @author		Unitemplates
* @email		info@unitemplates.com
* @website		http://www.unitemplates.com
* @copyright	Copyright (C) 2014 - 2015 Unitemplates. All rights reserved.
* @license		GNU General Public License version 2 or later.
*/
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$helper = new ModMultimoduleHelper ($params);


$class_sfx	= htmlspecialchars($params->get('moduleclass_sfx'));

$helper->addAssets();

$layout_path = $helper->getLayout();
if ($layout_path)
{
	require $layout_path;
}
