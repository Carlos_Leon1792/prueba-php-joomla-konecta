<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_ut_contact_pro
 * @copyright   Copyright (C) 2016 www.Unitemplates.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

if ($fixed_url) {
  $url = $fixed_url_address;
}
else {
  if (!$exact_url) {
    $url = JURI::current();
  }
  else {
    if (!$disable_https) {
      $url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    }
    else {
      $url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    }
  }
}

$url = htmlentities($url, ENT_COMPAT, "UTF-8").'#ut-ctc-'.$module->id;

$myError = '';
$CORRECT_ANTISPAM_ANSWER = '';
$CORRECT_NAME = '';
$CORRECT_EMAIL = '';
$CORRECT_SUBJECT = '';
$CORRECT_MESSAGE = '';

if (isset($_POST["ut_email"])) {
  $CORRECT_NAME = htmlentities($_POST["ut_name"], ENT_COMPAT, "UTF-8");
  $CORRECT_SUBJECT = htmlentities($_POST["ut_subject"], ENT_COMPAT, "UTF-8");
  $CORRECT_MESSAGE = htmlentities($_POST["ut_message"], ENT_COMPAT, "UTF-8");
  // check anti-spam
  if ($enable_anti_spam) {
    if (strtolower($_POST["ut_anti_spam_answer"]) != strtolower($anti_spam_a)) {
      $myError = '<span class="error-color">' . $wrongantispamanswer . '</span>';
    }
    else {
      $CORRECT_ANTISPAM_ANSWER = htmlentities($_POST["ut_anti_spam_answer"], ENT_COMPAT, "UTF-8");
    }
  }
  // check email
  if ($_POST["ut_email"] === "") {
    $myError = '<span class="error-color">' . $no_email . '</span>';
  }
  if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", strtolower($_POST["ut_email"]))) {
    $myError = '<span class="error-color">' . $invalid_email . '</span>';
  }
  else {
    $CORRECT_EMAIL = htmlentities($_POST["ut_email"], ENT_COMPAT, "UTF-8");
  }

  if ($myError == '') {
    $mySubject = $_POST["ut_subject"];
    $myMessage = 'You received a message from '. $_POST["ut_name"] ."\n\n". $_POST["ut_email"] ."\n\n". $_POST["ut_message"];

    $mailSender = JFactory::getMailer();
    $mailSender->addRecipient($recipient);

    $mailSender->setSender(array($fromEmail,$fromName));
    $mailSender->addReplyTo($_POST["ut_email"], $_POST["ut_name"]);

    $mailSender->setSubject($mySubject);
    $mailSender->setBody($myMessage);

    if ($mailSender->Send() !== true) {
      $myReplacement = '<span class="error-color">' . $error_text . '</span>';
      print $myReplacement;
    }
    else {
      $myReplacement = '<div class="jumbotron bg-success"><span class="success-color">' . $success_text . '</span></div>';
      print $myReplacement;
      return true;
    }

  }
} // end if posted

// check recipient
if ($recipient === "") {
  $myReplacement = '<span class="error-color">No recipient specified</span>';
  print $myReplacement;
  return true;
}
?>
<div class="form-horizontal">
  <form id="ctc-form" action="<?php echo $url;?>" method="post">
  
    <?php if ($myError != '') {print $myError;}?>

    <div class="row">
      <div class="col-sm-4">
        <input class="inputbox" placeholder="<?php echo $name_text;?>" type="text" name="ut_name" value="<?php echo $CORRECT_NAME;?>"/>
      </div>
      <div class="col-sm-4">
        <input class="inputbox" placeholder="<?php echo$email_text;?>" type="text" name="ut_email" value="<?php echo $CORRECT_EMAIL;?>"/>
      </div>
      <div class="col-sm-4">
        <input class="inputbox" placeholder="<?php echo$subject_text;?>" type="text" name="ut_subject" value="<?php echo $CORRECT_SUBJECT;?>"/>
      </div>
      <div class="col-sm-12">
        <textarea class="textarea" placeholder="<?php echo $msg_text;?>" name="ut_message" rows="3"><?php echo $CORRECT_MESSAGE;?></textarea>
      </div>
      <div class="col-sm-6">
        <?php if ($enable_anti_spam):?>
          <input class="inputbox" placeholder="<?php echo$anti_spam_q;?>" type="text" name="ut_anti_spam_answer" value="<?php echo $CORRECT_ANTISPAM_ANSWER;?>"/>
        <?php endif;?>
      </div>
      <div class="col-sm-6">
        <input class="btn btn-primary button" type="submit" value="<?php echo $button_text;?>"/>
      </div>
    </div>
  </form>
</div>