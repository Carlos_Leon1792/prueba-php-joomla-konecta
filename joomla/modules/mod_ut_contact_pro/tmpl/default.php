<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_ut_contact_pro
 * @copyright   Copyright (C) 2016 www.Unitemplates.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');
?>
<div id="ut-ctc-<?php echo $uniqid;?>" class="ut-ctc <?php echo $moduleclass_sfx;?>">
	<div class="ctc-ct">
		<div class="ctc-default <?php if($add_container) {echo $add_container;}?>">
			<?php if ($module->showtitle) { echo '<h3 class="ctc-title">' . $module->title . '</h3>'; }?>
			<?php if ($intro) { echo '<p class="ctc-intro">' . $intro . '</p>'; } ?>
			<?php require JModuleHelper::getLayoutPath('mod_ut_contact_pro', '_form-vertical'); ?>
		</div>
	</div>
</div>