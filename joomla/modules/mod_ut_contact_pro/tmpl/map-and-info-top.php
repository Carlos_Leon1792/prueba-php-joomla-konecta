<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_ut_contact_pro
 * @copyright   Copyright (C) 2016 www.Unitemplates.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');
?>
<div id="ut-ctc-<?php echo $uniqid;?>" class="ut-ctc <?php echo $moduleclass_sfx;?>" <?php if ($bg_image) {echo 'style="background-image: url(' .$bg_image . ');"';} ?>>
	<div class="ctc-ct <?php echo $params->get('bg_color');?>" <?php if ($bg_custom) { echo 'style="background-color:rgba'.$rgba.';"';} ?>>
		<div class="ctc-map-info <?php if($add_container) {echo $add_container;}?>">
			<?php if ($module->showtitle) { echo '<h3 class="ctc-title">' . $module->title . '</h3>'; }?>
			<?php if ($intro) { echo '<p class="ctc-intro">' . $intro . '</p>'; } ?>
			<div class="ctc-top">
				<?php require JModuleHelper::getLayoutPath('mod_ut_contact_pro', '_info-horizontal'); ?>
			</div>
			<div class="ctc-bottom">
				<?php require JModuleHelper::getLayoutPath('mod_ut_contact_pro', '_map'); ?>
			</div>
		</div>
	</div>
</div>