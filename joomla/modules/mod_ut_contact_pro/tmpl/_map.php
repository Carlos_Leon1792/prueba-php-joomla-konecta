<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_ut_contact_pro
 * @copyright   Copyright (C) 2016 www.Unitemplates.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');
?>

<?php if($google_map):?>
	<?php $document->addScript($mapAPI);?>
	<div class="google-map">
		<div id="map-canvas<?php echo $uniqid; ?>" class="map-canvas" style="width:100%;height:<?php echo $map_height;?>"></div>
	</div>
	<script>
		//API demos Used(synchronous loading, info window,)
		var myLatlng<?php echo $uniqid; ?> = new google.maps.LatLng(<?php echo $params->get('coordinates');?>);
		var mapOptions<?php echo $uniqid; ?> = {
			scrollwheel: <?php echo $zoom_mouse;?>,
			zoom: <?php echo $zoom;?>,
			center: myLatlng<?php echo $uniqid; ?>,
			disableDefaultUI: <?php echo $show_control;?>,
			mapTypeId: google.maps.MapTypeId.<?php echo $maptype;?>,
			mapTypeControl: <?php echo $show_control;?>,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.<?php echo $control_float;?>_TOP
			},
			panControl: <?php echo $show_control;?>,
			panControlOptions: {
				position: google.maps.ControlPosition.<?php echo $control_float;?>_CENTER
			},
			zoomControl: <?php echo $show_control;?>,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.<?php echo $control_float;?>_CENTER
			},
			scaleControl: <?php echo $show_control;?>,
			streetViewControl: <?php echo $show_control;?>,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.<?php echo $control_float;?>_CENTER
			}
		};

		var map<?php echo $uniqid; ?> = new google.maps.Map(document.getElementById('map-canvas<?php echo $uniqid; ?>'), mapOptions<?php echo $uniqid; ?>);

		<?php if ($params->get('show_marker' , 1)) {?>
			<?php if($params->get('marker_info')) {?>
			//Info Window
			var contentString<?php echo $uniqid; ?> = '<div id="content">'+
				'<div id="siteNotice"></div>'+
				'<h1 id="firstHeading" class="firstHeading"><?php echo $params->get('marker_title');?></h1>'+
				'<div id="bodyContent">'+
					'<p><?php echo $params->get('marker_info');?></p>'+
				'</div>'+
			'</div>';
			var infowindow<?php echo $uniqid; ?> = new google.maps.InfoWindow({
				content: contentString<?php echo $uniqid; ?>,
				maxWidth: 300
			});
			<?php }?>

			//Marker
			var marker<?php echo $uniqid; ?> = new google.maps.Marker({
				position: myLatlng<?php echo $uniqid; ?>,
				map: map<?php echo $uniqid; ?>,
				title: <?php echo "'".$params->get('marker_title')."'";?>
			});

			//Event for open Info Window
			google.maps.event.addListener(marker<?php echo $uniqid; ?>, 'click', function() {
				infowindow<?php echo $uniqid; ?>.open(map<?php echo $uniqid; ?>,marker<?php echo $uniqid; ?>);
			});
		<?php }?>
	</script>
<?php elseif($image_map):?>
	<div class="ctc-image-map">
		<img class="img-responsive" src="<?php echo $image_map;?>">
	</div>
<?php endif;?>