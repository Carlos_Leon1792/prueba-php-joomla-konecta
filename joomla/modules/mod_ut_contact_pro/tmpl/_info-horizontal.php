<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_ut_contact_pro
 * @copyright   Copyright (C) 2016 www.Unitemplates.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

// Get info
$info = $params->get('info_list');
?>
<div class="info-horizontal">
	<?php
	if($info){
		echo "<dl>";
		foreach ($info as $item => $value) {
			if (($value->icon) || ($value->name)) {echo '<dt>';}
			if ($value->icon){echo '<i class="text-primary '.$value->icon . '"></i>';}
			if ($value->name){echo $value->name;}
			if (($value->icon) || ($value->name)) {echo '</dt>';}
			if ($value->detail){echo '<dd>' . $value->detail . '</dd>';}
		}
		echo "</dl>";
	}?>
</div>