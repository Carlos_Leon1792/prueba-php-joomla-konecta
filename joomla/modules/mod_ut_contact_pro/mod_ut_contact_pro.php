<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_ut_contact_pro
 * @copyright   Copyright (C) 2016 www.Unitemplates.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
$uniqid = $module->id;
$document 			= JFactory::getDocument();
$document->addStylesheet(JURI::base(true) . '/modules/mod_ut_contact_pro/assets/css/ut_ctc.css');

// FORM Email Parameters
$recipient = $params->get('email_recipient');
$fromName = @$params->get('from_name');
$fromEmail = @$params->get('from_email');

// FORM Text Parameters
$name_text = JText::_('NAME_TEXT');
$email_text = JText::_('EMAIL_TEXT');
$subject_text = JText::_('SUBJECT_TEXT');
$msg_text = JText::_('MESSAGE_TEXT');
$button_text = JText::_('BUTTON_TEXT');
$no_email = JText::_('NO_EMAIL');
$invalid_email = JText::_('INVALID_EMAIL');
$wrongantispamanswer = JText::_('WRONG_ANTISPAM');
$error_text = $params->get('error_text');
$success_text = $params->get('success_text');
$intro				= $params->get('intro');

// FORM URL Parameters
$exact_url = $params->get('exact_url', true);
$disable_https = $params->get('disable_https', true);
$fixed_url = $params->get('fixed_url', true);
$fixed_url_address = $params->get('fixed_url_address', '');

// FORM Anti-spam Parameters
$enable_anti_spam = $params->get('enable_anti_spam', true);
$anti_spam_q = $params->get('anti_spam_q');
$anti_spam_a = $params->get('anti_spam_a');

//INFO TAB
$intro				=$params->get('intro');

//Map tab
$mapKey				=$params->get('map_key');
$mapAPI				='https://maps.googleapis.com/maps/api/js?key='.$mapKey;
$image_map			=$params->get('image_map');
$google_map			=$params->get('google_map');
$coordinates		=$params->get('coordinates');
$map_height			=$params->get('map_height');
$maptype			=$params->get('maptype');
$zoom				=$params->get('zoom');
$zoom_mouse			=$params->get('zoom_mouse');
$show_control		=$params->get('show_control');
$control_float		=$params->get('control_float');

//Style tab
$add_container		=$params->get('add_container');
$float_info			=$params->get('float_info');
$bg_image			=$params->get('bg_image');
$bg_color			=$params->get('bg_color');
//RGBA
$bg_custom			=$params->get('bg_custom');
$opacity			=$params->get('opacity');
list($r, $g, $b) = sscanf($bg_custom, "#%02x%02x%02x");
$rgba = "($r, $g, $b, $opacity)";


// Module Class Suffix Parameter
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_ut_contact_pro', $params->get('layout', 'default'));