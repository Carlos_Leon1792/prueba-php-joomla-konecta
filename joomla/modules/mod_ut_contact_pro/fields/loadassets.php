<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_ut_contact_pro
 * @copyright   Copyright (C) 2016 www.Unitemplates.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die( 'Restricted access' );


jimport('joomla.form.formfield');
 
class JFormFieldLoadassets extends JFormField {
 
	protected $type = 'loadassets';
 
	public function getLabel() {}
 
	public function getInput() {
		$doc = JFactory::getDocument();
		$doc->addStyleSheet(JUri::root(true) . '/modules/mod_ut_contact_pro/assets/css/admin.css');
	}
}