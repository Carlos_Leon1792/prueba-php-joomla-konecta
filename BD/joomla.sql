-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-03-2020 a las 13:22:12
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `joomla`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_action_logs`
--

CREATE TABLE `lbazs_action_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_language_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `ip_address` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.0.0.0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_action_logs`
--

INSERT INTO `lbazs_action_logs` (`id`, `message_language_key`, `message`, `log_date`, `extension`, `user_id`, `item_id`, `ip_address`) VALUES
(1, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2020-03-24 23:43:39', 'com_users', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(2, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_LANGUAGE\",\"id\":10000,\"name\":\"Spanish (espa\\u00f1ol)\",\"extension_name\":\"Spanish (espa\\u00f1ol)\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-24 23:44:39', 'com_installer', 66, 10000, 'COM_ACTIONLOGS_DISABLED'),
(3, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_LANGUAGE\",\"id\":10001,\"name\":\"Spanish (espa\\u00f1ol)\",\"extension_name\":\"Spanish (espa\\u00f1ol)\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-24 23:44:39', 'com_installer', 66, 10001, 'COM_ACTIONLOGS_DISABLED'),
(4, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PACKAGE\",\"id\":10002,\"name\":\"Spanish (es-ES) Language Pack\",\"extension_name\":\"Spanish (es-ES) Language Pack\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-24 23:44:39', 'com_installer', 66, 10002, 'COM_ACTIONLOGS_DISABLED'),
(5, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_OUT', '{\"action\":\"logout\",\"id\":\"66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2020-03-24 23:45:52', 'com_users', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(6, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2020-03-24 23:46:05', 'com_users', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(7, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2020-03-25 00:31:00', 'com_users', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(8, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10004,\"name\":\"Widgetkit\",\"extension_name\":\"Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:31:16', 'com_installer', 66, 10004, 'COM_ACTIONLOGS_DISABLED'),
(9, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10005,\"name\":\"Widgetkit Twitter\",\"extension_name\":\"Widgetkit Twitter\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:31:16', 'com_installer', 66, 10005, 'COM_ACTIONLOGS_DISABLED'),
(10, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10006,\"name\":\"System - Widgetkit\",\"extension_name\":\"System - Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:31:16', 'com_installer', 66, 10006, 'COM_ACTIONLOGS_DISABLED'),
(11, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10007,\"name\":\"Content - Widgetkit\",\"extension_name\":\"Content - Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:31:16', 'com_installer', 66, 10007, 'COM_ACTIONLOGS_DISABLED'),
(12, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10008,\"name\":\"System - Widgetkit ZOO\",\"extension_name\":\"System - Widgetkit ZOO\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:31:16', 'com_installer', 66, 10008, 'COM_ACTIONLOGS_DISABLED'),
(13, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10009,\"name\":\"System - Widgetkit Joomla\",\"extension_name\":\"System - Widgetkit Joomla\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:31:16', 'com_installer', 66, 10009, 'COM_ACTIONLOGS_DISABLED'),
(14, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT\",\"id\":10003,\"name\":\"Widgetkit\",\"extension_name\":\"Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:31:16', 'com_installer', 66, 10003, 'COM_ACTIONLOGS_DISABLED'),
(15, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PACKAGE\",\"id\":false,\"name\":\"Widgetkit\",\"extension_name\":\"Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:33:21', 'com_installer', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(16, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__extensions\"}', '2020-03-25 00:34:16', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(17, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10007,\"title\":\"Content - Widgetkit\",\"itemlink\":\"index.php?option=com_plugins&task=plugin.edit&extension_id=10007\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:34:16', 'com_plugins.plugin', 66, 10007, 'COM_ACTIONLOGS_DISABLED'),
(18, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__extensions\"}', '2020-03-25 00:34:17', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(19, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10009,\"title\":\"System - Widgetkit Joomla\",\"itemlink\":\"index.php?option=com_plugins&task=plugin.edit&extension_id=10009\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:34:17', 'com_plugins.plugin', 66, 10009, 'COM_ACTIONLOGS_DISABLED'),
(20, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__extensions\"}', '2020-03-25 00:34:18', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(21, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10006,\"title\":\"System - Widgetkit\",\"itemlink\":\"index.php?option=com_plugins&task=plugin.edit&extension_id=10006\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:34:18', 'com_plugins.plugin', 66, 10006, 'COM_ACTIONLOGS_DISABLED'),
(22, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__extensions\"}', '2020-03-25 00:34:18', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(23, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10008,\"title\":\"System - Widgetkit ZOO\",\"itemlink\":\"index.php?option=com_plugins&task=plugin.edit&extension_id=10008\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:34:18', 'com_plugins.plugin', 66, 10008, 'COM_ACTIONLOGS_DISABLED'),
(24, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PACKAGE\",\"id\":false,\"name\":\"Widgetkit\",\"extension_name\":\"Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:34:38', 'com_installer', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(25, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT\",\"id\":\"10003\",\"name\":\"com_widgetkit\",\"extension_name\":\"com_widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:35:46', 'com_installer', 66, 10003, 'COM_ACTIONLOGS_DISABLED'),
(26, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":\"10008\",\"name\":\"System - Widgetkit ZOO\",\"extension_name\":\"System - Widgetkit ZOO\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:37:13', 'com_installer', 66, 10008, 'COM_ACTIONLOGS_DISABLED'),
(27, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10010,\"name\":\"System - Widgetkit K2\",\"extension_name\":\"System - Widgetkit K2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:37:23', 'com_installer', 66, 10010, 'COM_ACTIONLOGS_DISABLED'),
(28, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10011,\"name\":\"Installer - YOOtheme\",\"extension_name\":\"Installer - YOOtheme\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:37:31', 'com_installer', 66, 10011, 'COM_ACTIONLOGS_DISABLED'),
(29, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10012,\"name\":\"Editors-XTD - Widgetkit\",\"extension_name\":\"Editors-XTD - Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:37:42', 'com_installer', 66, 10012, 'COM_ACTIONLOGS_DISABLED'),
(30, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10013,\"name\":\"Content - Widgetkit\",\"extension_name\":\"Content - Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:37:49', 'com_installer', 66, 10013, 'COM_ACTIONLOGS_DISABLED'),
(31, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":\"10004\",\"name\":\"Widgetkit\",\"extension_name\":\"Widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:37:58', 'com_installer', 66, 10004, 'COM_ACTIONLOGS_DISABLED'),
(32, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT\",\"id\":\"10003\",\"name\":\"com_widgetkit\",\"extension_name\":\"com_widgetkit\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:38:08', 'com_installer', 66, 10003, 'COM_ACTIONLOGS_DISABLED'),
(33, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_LIBRARY\",\"id\":10015,\"name\":\"Fox Contact\",\"extension_name\":\"Fox Contact\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:40:34', 'com_installer', 66, 10015, 'COM_ACTIONLOGS_DISABLED'),
(34, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10016,\"name\":\"MOD_FOXCONTACT\",\"extension_name\":\"MOD_FOXCONTACT\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:40:34', 'com_installer', 66, 10016, 'COM_ACTIONLOGS_DISABLED'),
(35, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10017,\"name\":\"plg_content_foxcontact\",\"extension_name\":\"plg_content_foxcontact\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:40:34', 'com_installer', 66, 10017, 'COM_ACTIONLOGS_DISABLED'),
(36, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT\",\"id\":10014,\"name\":\"COM_FOXCONTACT\",\"extension_name\":\"COM_FOXCONTACT\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:40:34', 'com_installer', 66, 10014, 'COM_ACTIONLOGS_DISABLED'),
(37, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10019,\"name\":\"Unite Revolution Slider 2\",\"extension_name\":\"Unite Revolution Slider 2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:41:16', 'com_installer', 66, 10019, 'COM_ACTIONLOGS_DISABLED'),
(38, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT\",\"id\":10018,\"name\":\"com_uniterevolution2\",\"extension_name\":\"com_uniterevolution2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:41:16', 'com_installer', 66, 10018, 'COM_ACTIONLOGS_DISABLED'),
(39, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10021,\"name\":\"plg_editors_jce\",\"extension_name\":\"plg_editors_jce\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:45:31', 'com_installer', 66, 10021, 'COM_ACTIONLOGS_DISABLED'),
(40, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10022,\"name\":\"plg_quickicon_jcefilebrowser\",\"extension_name\":\"plg_quickicon_jcefilebrowser\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:45:31', 'com_installer', 66, 10022, 'COM_ACTIONLOGS_DISABLED'),
(41, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__extensions\"}', '2020-03-25 00:45:31', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(42, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT\",\"id\":10020,\"name\":\"JCE\",\"extension_name\":\"JCE\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 00:45:31', 'com_installer', 66, 10020, 'COM_ACTIONLOGS_DISABLED'),
(43, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2020-03-25 01:17:00', 'com_users', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(44, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_TEMPLATE\",\"id\":10023,\"name\":\"ut_ufolio\",\"extension_name\":\"ut_ufolio\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:18:53', 'com_installer', 66, 10023, 'COM_ACTIONLOGS_DISABLED'),
(45, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10024,\"name\":\"T3 Framework\",\"extension_name\":\"T3 Framework\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:19:30', 'com_installer', 66, 10024, 'COM_ACTIONLOGS_DISABLED'),
(46, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT\",\"id\":10025,\"name\":\"SP Simple Portfolio\",\"extension_name\":\"SP Simple Portfolio\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:22:29', 'com_installer', 66, 10025, 'COM_ACTIONLOGS_DISABLED'),
(47, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10026,\"name\":\"MOD_ITPSHARE\",\"extension_name\":\"MOD_ITPSHARE\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:22:55', 'com_installer', 66, 10026, 'COM_ACTIONLOGS_DISABLED'),
(48, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10027,\"name\":\"S5 MailChimp Signup\",\"extension_name\":\"S5 MailChimp Signup\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:23:05', 'com_installer', 66, 10027, 'COM_ACTIONLOGS_DISABLED'),
(49, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10028,\"name\":\"SP Simple Portfolio Module\",\"extension_name\":\"SP Simple Portfolio Module\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:23:13', 'com_installer', 66, 10028, 'COM_ACTIONLOGS_DISABLED'),
(50, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10029,\"name\":\"UT Contact Pro\",\"extension_name\":\"UT Contact Pro\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:23:26', 'com_installer', 66, 10029, 'COM_ACTIONLOGS_DISABLED'),
(51, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":10030,\"name\":\"UT Multi Module\",\"extension_name\":\"UT Multi Module\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:23:35', 'com_installer', 66, 10030, 'COM_ACTIONLOGS_DISABLED'),
(52, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":\"10024\",\"name\":\"T3 Framework\",\"extension_name\":\"T3 Framework\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:23:50', 'com_installer', 66, 10024, 'COM_ACTIONLOGS_DISABLED'),
(53, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":1,\"title\":\"Inicio\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=1\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:35:15', 'com_content.article', 66, 1, 'COM_ACTIONLOGS_DISABLED'),
(54, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:35:30', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(55, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:35:37', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(56, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 01:35:37', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(57, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":3,\"title\":\"Cont\\u00e1ctenos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=3\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:35:46', 'com_content.article', 66, 3, 'COM_ACTIONLOGS_DISABLED'),
(58, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":4,\"title\":\"Encuestas\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=4\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:35:58', 'com_content.article', 66, 4, 'COM_ACTIONLOGS_DISABLED'),
(59, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_CATEGORY\",\"id\":8,\"title\":\"Comunicaciones\",\"itemlink\":\"index.php?option=com_categories&task=category.edit&id=8\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:36:56', 'com_categories.category', 66, 8, 'COM_ACTIONLOGS_DISABLED'),
(60, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_CATEGORY\",\"id\":9,\"title\":\"Equipo de desarrollo\",\"itemlink\":\"index.php?option=com_categories&task=category.edit&id=9\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:37:10', 'com_categories.category', 66, 9, 'COM_ACTIONLOGS_DISABLED'),
(61, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_CATEGORY\",\"id\":10,\"title\":\"Equipo comercial\",\"itemlink\":\"index.php?option=com_categories&task=category.edit&id=10\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:37:22', 'com_categories.category', 66, 10, 'COM_ACTIONLOGS_DISABLED'),
(62, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":113,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=113\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:39:14', 'com_menus.item', 66, 113, 'COM_ACTIONLOGS_DISABLED'),
(63, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":101,\"title\":\"Home\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=101\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:39:35', 'com_menus.item', 66, 101, 'COM_ACTIONLOGS_DISABLED'),
(64, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__menu\"}', '2020-03-25 01:39:35', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(65, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":114,\"title\":\"Cont\\u00e1ctenos\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=114\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:40:04', 'com_menus.item', 66, 114, 'COM_ACTIONLOGS_DISABLED'),
(66, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":101,\"title\":\"Inicio\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=101\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:40:17', 'com_menus.item', 66, 101, 'COM_ACTIONLOGS_DISABLED'),
(67, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__menu\"}', '2020-03-25 01:40:17', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(68, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":115,\"title\":\"Encuesta\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=115\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:41:03', 'com_menus.item', 66, 115, 'COM_ACTIONLOGS_DISABLED'),
(69, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:42:39', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(70, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:42:46', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(71, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:42:46', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(72, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":1,\"title\":\"Main Menu\",\"extension_name\":\"Main Menu\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=1\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:43:57', 'com_modules.module', 66, 1, 'COM_ACTIONLOGS_DISABLED'),
(73, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:43:57', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(74, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":1,\"title\":\"Main Menu\",\"extension_name\":\"Main Menu\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=1\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:46:03', 'com_modules.module', 66, 1, 'COM_ACTIONLOGS_DISABLED'),
(75, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:46:03', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(76, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":1,\"title\":\"Main Menu\",\"extension_name\":\"Main Menu\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=1\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:46:17', 'com_modules.module', 66, 1, 'COM_ACTIONLOGS_DISABLED'),
(77, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:46:17', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(78, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:46:26', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(79, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":16,\"title\":\"Login Form\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=16\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:46:26', 'com_modules.module', 66, 16, 'COM_ACTIONLOGS_DISABLED'),
(80, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:46:40', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(81, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":17,\"title\":\"Breadcrumbs\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=17\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:46:40', 'com_modules.module', 66, 17, 'COM_ACTIONLOGS_DISABLED'),
(82, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:47:42', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(83, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:47:42', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(84, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:48:02', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(85, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:48:02', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(86, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:48:25', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(87, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:48:25', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(88, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:48:47', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(89, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:48:47', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(90, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:49:58', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(91, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:49:58', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(92, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:50:17', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(93, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:50:17', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(94, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":3,\"title\":\"Cont\\u00e1ctenos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=3\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:51:04', 'com_content.article', 66, 3, 'COM_ACTIONLOGS_DISABLED'),
(95, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 01:51:04', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(96, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":3,\"title\":\"Cont\\u00e1ctenos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=3\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:52:33', 'com_content.article', 66, 3, 'COM_ACTIONLOGS_DISABLED'),
(97, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 01:52:33', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(98, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:52:43', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(99, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 01:52:43', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(100, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":3,\"title\":\"Cont\\u00e1ctenos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=3\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:53:56', 'com_content.article', 66, 3, 'COM_ACTIONLOGS_DISABLED'),
(101, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 01:53:56', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(102, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MEDIA\",\"id\":0,\"title\":\"3d.jpeg\",\"itemlink\":\"index.php?option=com_media&task=file.edit&id=0\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:58:11', 'com_media.file', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(103, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MEDIA\",\"id\":0,\"title\":\"android.jpeg\",\"itemlink\":\"index.php?option=com_media&task=file.edit&id=0\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:58:11', 'com_media.file', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(104, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MEDIA\",\"id\":0,\"title\":\"whatssap.jpeg\",\"itemlink\":\"index.php?option=com_media&task=file.edit&id=0\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 01:58:11', 'com_media.file', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(105, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":5,\"title\":\"Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=5\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:01:34', 'com_content.article', 66, 5, 'COM_ACTIONLOGS_DISABLED'),
(106, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:01:42', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(107, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 02:01:42', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(108, 'PLG_ACTIONLOG_JOOMLA_APPLICATION_CONFIG_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_APPLICATION_CONFIG\",\"extension_name\":\"com_config.application\",\"itemlink\":\"index.php?option=com_config\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:02:15', 'com_config.application', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(109, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":5,\"title\":\"Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=5\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:02:39', 'com_content.article', 66, 5, 'COM_ACTIONLOGS_DISABLED'),
(110, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:02:39', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(111, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":5,\"title\":\"Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=5\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:03:19', 'com_content.article', 66, 5, 'COM_ACTIONLOGS_DISABLED'),
(112, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:03:19', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(113, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":5,\"title\":\"Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=5\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:03:44', 'com_content.article', 66, 5, 'COM_ACTIONLOGS_DISABLED'),
(114, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:03:44', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(115, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MEDIA\",\"id\":0,\"title\":\"ventas.jpg\",\"itemlink\":\"index.php?option=com_media&task=file.edit&id=0\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:07:22', 'com_media.file', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(116, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":6,\"title\":\"Prochile busca aumentar comercio con Colombia\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=6\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:08:01', 'com_content.article', 66, 6, 'COM_ACTIONLOGS_DISABLED'),
(117, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MEDIA\",\"id\":0,\"title\":\"comunicaciones.jpeg\",\"itemlink\":\"index.php?option=com_media&task=file.edit&id=0\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:09:59', 'com_media.file', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(118, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":7,\"title\":\"Vicepresidenta habla de estudio que ubica al pa\\u00eds como el m\\u00e1s corrupto\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=7\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:10:51', 'com_content.article', 66, 7, 'COM_ACTIONLOGS_DISABLED'),
(119, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":6,\"title\":\"Prochile busca aumentar comercio con Colombia\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=6\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:11:34', 'com_content.article', 66, 6, 'COM_ACTIONLOGS_DISABLED'),
(120, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:11:34', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(121, 'PLG_ACTIONLOG_JOOMLA_APPLICATION_CONFIG_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_APPLICATION_CONFIG\",\"extension_name\":\"com_config.application\",\"itemlink\":\"index.php?option=com_config\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:12:00', 'com_config.application', 66, 0, 'COM_ACTIONLOGS_DISABLED');
INSERT INTO `lbazs_action_logs` (`id`, `message_language_key`, `message`, `log_date`, `extension`, `user_id`, `item_id`, `ip_address`) VALUES
(122, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:14:18', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(123, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:14:18', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(124, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:14:40', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(125, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:14:40', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(126, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:15:15', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(127, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:15:15', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(128, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:15:29', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(129, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:15:29', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(130, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":100,\"title\":\"Noticias desarrollo\",\"extension_name\":\"Noticias desarrollo\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=100\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:16:07', 'com_modules.module', 66, 100, 'COM_ACTIONLOGS_DISABLED'),
(131, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":101,\"title\":\"Noticias Comercial\",\"extension_name\":\"Noticias Comercial\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=101\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:16:29', 'com_modules.module', 66, 101, 'COM_ACTIONLOGS_DISABLED'),
(132, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":102,\"title\":\"Noticias comunicaciones\",\"extension_name\":\"Noticias comunicaciones\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=102\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:16:54', 'com_modules.module', 66, 102, 'COM_ACTIONLOGS_DISABLED'),
(133, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":101,\"title\":\"Noticias Comercial\",\"extension_name\":\"Noticias Comercial\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=101\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:17:14', 'com_modules.module', 66, 101, 'COM_ACTIONLOGS_DISABLED'),
(134, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 02:17:14', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(135, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":3,\"title\":\"Cont\\u00e1ctenos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=3\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:17:34', 'com_content.article', 66, 3, 'COM_ACTIONLOGS_DISABLED'),
(136, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:17:34', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(137, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:18:33', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(138, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:18:33', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(139, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:19:33', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(140, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:19:33', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(141, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:20:11', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(142, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:20:11', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(143, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:21:35', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(144, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:21:35', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(145, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":116,\"title\":\"Encuesta\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=116\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:27:51', 'com_menus.item', 66, 116, 'COM_ACTIONLOGS_DISABLED'),
(146, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":116,\"title\":\"Encuesta\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=116\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:28:19', 'com_menus.item', 66, 116, 'COM_ACTIONLOGS_DISABLED'),
(147, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":103,\"title\":\"Encuesta\",\"extension_name\":\"Encuesta\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=103\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:33:09', 'com_modules.module', 66, 103, 'COM_ACTIONLOGS_DISABLED'),
(148, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":2,\"title\":\"Contenidos\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=2\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:33:15', 'com_content.article', 66, 2, 'COM_ACTIONLOGS_DISABLED'),
(149, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:33:15', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(150, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":4,\"title\":\"Encuestas\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=4\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:33:37', 'com_content.article', 66, 4, 'COM_ACTIONLOGS_DISABLED'),
(151, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:33:37', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(152, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MEDIA\",\"id\":0,\"title\":\"banner1.jpg\",\"itemlink\":\"index.php?option=com_media&task=file.edit&id=0\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:42:25', 'com_media.file', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(153, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":1,\"title\":\"Inicio\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=1\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:42:48', 'com_content.article', 66, 1, 'COM_ACTIONLOGS_DISABLED'),
(154, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:42:48', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(155, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MEDIA\",\"id\":0,\"title\":\"bannerguia-01.png\",\"itemlink\":\"index.php?option=com_media&task=file.edit&id=0\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:46:16', 'com_media.file', 66, 0, 'COM_ACTIONLOGS_DISABLED'),
(156, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":1,\"title\":\"Inicio\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=1\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:48:16', 'com_content.article', 66, 1, 'COM_ACTIONLOGS_DISABLED'),
(157, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:48:16', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(158, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":1,\"title\":\"Inicio\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=1\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:48:49', 'com_content.article', 66, 1, 'COM_ACTIONLOGS_DISABLED'),
(159, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__content\"}', '2020-03-25 02:48:49', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(160, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":103,\"title\":\"Encuesta\",\"extension_name\":\"Encuesta\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=103\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:49:47', 'com_modules.module', 66, 103, 'COM_ACTIONLOGS_DISABLED'),
(161, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 02:49:47', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(162, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:50:16', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(163, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 02:50:16', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED'),
(164, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Contacto\",\"extension_name\":\"Contacto\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\"}', '2020-03-25 02:51:54', 'com_modules.module', 66, 99, 'COM_ACTIONLOGS_DISABLED'),
(165, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"66\",\"title\":\"admin\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"userid\":\"66\",\"username\":\"admin\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=66\",\"table\":\"#__modules\"}', '2020-03-25 02:51:54', 'com_checkin', 66, 66, 'COM_ACTIONLOGS_DISABLED');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_action_logs_extensions`
--

CREATE TABLE `lbazs_action_logs_extensions` (
  `id` int(10) UNSIGNED NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_action_logs_extensions`
--

INSERT INTO `lbazs_action_logs_extensions` (`id`, `extension`) VALUES
(1, 'com_banners'),
(2, 'com_cache'),
(3, 'com_categories'),
(4, 'com_config'),
(5, 'com_contact'),
(6, 'com_content'),
(7, 'com_installer'),
(8, 'com_media'),
(9, 'com_menus'),
(10, 'com_messages'),
(11, 'com_modules'),
(12, 'com_newsfeeds'),
(13, 'com_plugins'),
(14, 'com_redirect'),
(15, 'com_tags'),
(16, 'com_templates'),
(17, 'com_users'),
(18, 'com_checkin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_action_logs_users`
--

CREATE TABLE `lbazs_action_logs_users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `notify` tinyint(1) UNSIGNED NOT NULL,
  `extensions` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_action_log_config`
--

CREATE TABLE `lbazs_action_log_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `id_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_action_log_config`
--

INSERT INTO `lbazs_action_log_config` (`id`, `type_title`, `type_alias`, `id_holder`, `title_holder`, `table_name`, `text_prefix`) VALUES
(1, 'article', 'com_content.article', 'id', 'title', '#__content', 'PLG_ACTIONLOG_JOOMLA'),
(2, 'article', 'com_content.form', 'id', 'title', '#__content', 'PLG_ACTIONLOG_JOOMLA'),
(3, 'banner', 'com_banners.banner', 'id', 'name', '#__banners', 'PLG_ACTIONLOG_JOOMLA'),
(4, 'user_note', 'com_users.note', 'id', 'subject', '#__user_notes', 'PLG_ACTIONLOG_JOOMLA'),
(5, 'media', 'com_media.file', '', 'name', '', 'PLG_ACTIONLOG_JOOMLA'),
(6, 'category', 'com_categories.category', 'id', 'title', '#__categories', 'PLG_ACTIONLOG_JOOMLA'),
(7, 'menu', 'com_menus.menu', 'id', 'title', '#__menu_types', 'PLG_ACTIONLOG_JOOMLA'),
(8, 'menu_item', 'com_menus.item', 'id', 'title', '#__menu', 'PLG_ACTIONLOG_JOOMLA'),
(9, 'newsfeed', 'com_newsfeeds.newsfeed', 'id', 'name', '#__newsfeeds', 'PLG_ACTIONLOG_JOOMLA'),
(10, 'link', 'com_redirect.link', 'id', 'old_url', '#__redirect_links', 'PLG_ACTIONLOG_JOOMLA'),
(11, 'tag', 'com_tags.tag', 'id', 'title', '#__tags', 'PLG_ACTIONLOG_JOOMLA'),
(12, 'style', 'com_templates.style', 'id', 'title', '#__template_styles', 'PLG_ACTIONLOG_JOOMLA'),
(13, 'plugin', 'com_plugins.plugin', 'extension_id', 'name', '#__extensions', 'PLG_ACTIONLOG_JOOMLA'),
(14, 'component_config', 'com_config.component', 'extension_id', 'name', '', 'PLG_ACTIONLOG_JOOMLA'),
(15, 'contact', 'com_contact.contact', 'id', 'name', '#__contact_details', 'PLG_ACTIONLOG_JOOMLA'),
(16, 'module', 'com_modules.module', 'id', 'title', '#__modules', 'PLG_ACTIONLOG_JOOMLA'),
(17, 'access_level', 'com_users.level', 'id', 'title', '#__viewlevels', 'PLG_ACTIONLOG_JOOMLA'),
(18, 'banner_client', 'com_banners.client', 'id', 'name', '#__banner_clients', 'PLG_ACTIONLOG_JOOMLA'),
(19, 'application_config', 'com_config.application', '', 'name', '', 'PLG_ACTIONLOG_JOOMLA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_assets`
--

CREATE TABLE `lbazs_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_assets`
--

INSERT INTO `lbazs_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 173, 0, 'root.1', 'Root Asset', '{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(8, 1, 17, 40, 1, 'com_content', 'com_content', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1}}'),
(9, 1, 41, 42, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 43, 44, 1, 'com_installer', 'com_installer', '{\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),
(11, 1, 45, 48, 1, 'com_languages', 'com_languages', '{\"core.admin\":{\"7\":1}}'),
(12, 1, 49, 50, 1, 'com_login', 'com_login', '{}'),
(13, 1, 51, 52, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 53, 54, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 55, 56, 1, 'com_media', 'com_media', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),
(16, 1, 57, 60, 1, 'com_menus', 'com_menus', '{\"core.admin\":{\"7\":1}}'),
(17, 1, 61, 62, 1, 'com_messages', 'com_messages', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(18, 1, 63, 128, 1, 'com_modules', 'com_modules', '{\"core.admin\":{\"7\":1}}'),
(19, 1, 129, 132, 1, 'com_newsfeeds', 'com_newsfeeds', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(20, 1, 133, 134, 1, 'com_plugins', 'com_plugins', '{\"core.admin\":{\"7\":1}}'),
(21, 1, 135, 136, 1, 'com_redirect', 'com_redirect', '{\"core.admin\":{\"7\":1}}'),
(22, 1, 137, 138, 1, 'com_search', 'com_search', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(23, 1, 139, 140, 1, 'com_templates', 'com_templates', '{\"core.admin\":{\"7\":1}}'),
(24, 1, 141, 144, 1, 'com_users', 'com_users', '{\"core.admin\":{\"7\":1}}'),
(26, 1, 145, 146, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 27, 2, 'com_content.category.2', 'Uncategorised', '{}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{}'),
(30, 19, 130, 131, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{}'),
(32, 24, 142, 143, 2, 'com_users.category.7', 'Uncategorised', '{}'),
(33, 1, 147, 148, 1, 'com_finder', 'com_finder', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(34, 1, 149, 150, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{}'),
(35, 1, 151, 152, 1, 'com_tags', 'com_tags', '{}'),
(36, 1, 153, 154, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 155, 156, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 157, 158, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 64, 65, 2, 'com_modules.module.1', 'Main Menu', '{}'),
(40, 18, 66, 67, 2, 'com_modules.module.2', 'Login', '{}'),
(41, 18, 68, 69, 2, 'com_modules.module.3', 'Popular Articles', '{}'),
(42, 18, 70, 71, 2, 'com_modules.module.4', 'Recently Added Articles', '{}'),
(43, 18, 72, 73, 2, 'com_modules.module.8', 'Toolbar', '{}'),
(44, 18, 74, 75, 2, 'com_modules.module.9', 'Quick Icons', '{}'),
(45, 18, 76, 77, 2, 'com_modules.module.10', 'Logged-in Users', '{}'),
(46, 18, 78, 79, 2, 'com_modules.module.12', 'Admin Menu', '{}'),
(47, 18, 80, 81, 2, 'com_modules.module.13', 'Admin Submenu', '{}'),
(48, 18, 82, 83, 2, 'com_modules.module.14', 'User Status', '{}'),
(49, 18, 84, 85, 2, 'com_modules.module.15', 'Title', '{}'),
(50, 18, 86, 87, 2, 'com_modules.module.16', 'Login Form', '{}'),
(51, 18, 88, 89, 2, 'com_modules.module.17', 'Breadcrumbs', '{}'),
(52, 18, 90, 91, 2, 'com_modules.module.79', 'Multilanguage status', '{}'),
(53, 18, 92, 93, 2, 'com_modules.module.86', 'Joomla Version', '{}'),
(54, 16, 58, 59, 2, 'com_menus.menu.1', 'Main Menu', '{}'),
(55, 18, 94, 95, 2, 'com_modules.module.87', 'Sample Data', '{}'),
(56, 1, 159, 160, 1, 'com_privacy', 'com_privacy', '{}'),
(57, 1, 161, 162, 1, 'com_actionlogs', 'com_actionlogs', '{}'),
(58, 18, 96, 97, 2, 'com_modules.module.88', 'Latest Actions', '{}'),
(59, 18, 98, 99, 2, 'com_modules.module.89', 'Privacy Dashboard', '{}'),
(60, 11, 46, 47, 2, 'com_languages.language.2', 'Spanish (español)', '{}'),
(61, 18, 100, 101, 2, 'com_modules.module.90', 'Widgetkit', '{}'),
(62, 18, 102, 103, 2, 'com_modules.module.91', 'Widgetkit Twitter', '{}'),
(63, 1, 163, 164, 1, 'com_widgetkit', 'Widgetkit', '{}'),
(64, 18, 104, 105, 2, 'com_modules.module.92', 'Fox Contact', '{}'),
(65, 1, 165, 166, 1, 'com_foxcontact', 'COM_FOXCONTACT', '{}'),
(66, 18, 106, 107, 2, 'com_modules.module.93', 'Unite Revolution Slider 2', '{}'),
(67, 1, 167, 168, 1, 'com_uniterevolution2', 'com_uniterevolution2', '{\"revolution2.slidersetting\":{\"7\":1},\"revolution2.slideroperations\":{\"7\":1},\"revolution2.slideoperations\":{\"7\":1},\"revolution2.editslide\":{\"7\":1} }'),
(68, 1, 169, 170, 1, 'com_jce', 'JCE', '{}'),
(69, 1, 171, 172, 1, 'com_spsimpleportfolio', 'SP Simple Portfolio', '{}'),
(70, 18, 108, 109, 2, 'com_modules.module.94', 'ITPShare', '{}'),
(71, 18, 110, 111, 2, 'com_modules.module.95', 'S5 MailChimp Signup', '{}'),
(72, 18, 112, 113, 2, 'com_modules.module.96', 'SP Simple Portfolio Module', '{}'),
(73, 18, 114, 115, 2, 'com_modules.module.97', 'UT Contact Pro', '{}'),
(74, 18, 116, 117, 2, 'com_modules.module.98', 'UT Multi Module', '{}'),
(75, 27, 19, 20, 3, 'com_content.article.1', 'Inicio', '{}'),
(76, 27, 21, 22, 3, 'com_content.article.2', 'Contenidos', '{}'),
(77, 27, 23, 24, 3, 'com_content.article.3', 'Contáctenos', '{}'),
(78, 27, 25, 26, 3, 'com_content.article.4', 'Encuestas', '{}'),
(79, 8, 28, 31, 2, 'com_content.category.8', 'Comunicaciones', '{}'),
(80, 8, 32, 35, 2, 'com_content.category.9', 'Equipo de desarrollo', '{}'),
(81, 8, 36, 39, 2, 'com_content.category.10', 'Equipo comercial', '{}'),
(82, 18, 118, 119, 2, 'com_modules.module.99', 'Contacto', '{}'),
(83, 80, 33, 34, 3, 'com_content.article.5', 'Google presenta su versión preliminar Android 11 para desarrolladores', '{}'),
(84, 81, 37, 38, 3, 'com_content.article.6', 'Prochile busca aumentar comercio con Colombia', '{}'),
(85, 79, 29, 30, 3, 'com_content.article.7', 'Vicepresidenta habla de estudio que ubica al país como el más corrupto', '{}'),
(86, 18, 120, 121, 2, 'com_modules.module.100', 'Noticias desarrollo', '{}'),
(87, 18, 122, 123, 2, 'com_modules.module.101', 'Noticias Comercial', '{}'),
(88, 18, 124, 125, 2, 'com_modules.module.102', 'Noticias comunicaciones', '{}'),
(89, 18, 126, 127, 2, 'com_modules.module.103', 'Encuesta', '{}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_associations`
--

CREATE TABLE `lbazs_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_banners`
--

CREATE TABLE `lbazs_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT 0,
  `type` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT 0,
  `impmade` int(11) NOT NULL DEFAULT 0,
  `clicks` int(11) NOT NULL DEFAULT 0,
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT 0,
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT -1,
  `track_clicks` tinyint(4) NOT NULL DEFAULT -1,
  `track_impressions` tinyint(4) NOT NULL DEFAULT -1,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_banner_clients`
--

CREATE TABLE `lbazs_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT 0,
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT -1,
  `track_clicks` tinyint(4) NOT NULL DEFAULT -1,
  `track_impressions` tinyint(4) NOT NULL DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_banner_tracks`
--

CREATE TABLE `lbazs_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_categories`
--

CREATE TABLE `lbazs_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lft` int(11) NOT NULL DEFAULT 0,
  `rgt` int(11) NOT NULL DEFAULT 0,
  `level` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_categories`
--

INSERT INTO `lbazs_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 17, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 66, '2020-03-24 23:42:44', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-24 23:42:44', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-24 23:42:44', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-24 23:42:44', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-24 23:42:44', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-24 23:42:44', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 79, 1, 11, 12, 1, 'comunicaciones', 'com_content', 'Comunicaciones', 'comunicaciones', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-25 01:36:56', 0, '2020-03-25 01:36:56', 0, '*', 1),
(9, 80, 1, 13, 14, 1, 'equipo-de-desarrollo', 'com_content', 'Equipo de desarrollo', 'equipo-de-desarrollo', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-25 01:37:10', 0, '2020-03-25 01:37:10', 0, '*', 1),
(10, 81, 1, 15, 16, 1, 'equipo-comercial', 'com_content', 'Equipo comercial', 'equipo-comercial', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 66, '2020-03-25 01:37:22', 0, '2020-03-25 01:37:22', 0, '*', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_contact_details`
--

CREATE TABLE `lbazs_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `catid` int(11) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Set if contact is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_content`
--

CREATE TABLE `lbazs_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_content`
--

INSERT INTO `lbazs_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`, `note`) VALUES
(1, 75, 'Inicio', 'inicio', '<p><img src=\"images/bannerguia-01.png\" alt=\"\" width=\"100%\" height=\"100%\" /></p>\r\n<p> </p>\r\n<table style=\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 305px; text-align: center;\">{loadposition desarrollo}</td>\r\n<td style=\"width: 285px; text-align: center;\">{loadposition comercial}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p style=\"text-align: center;\"> </p>\r\n<table style=\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 305px; text-align: center;\">{loadposition encuesta}</td>\r\n<td style=\"width: 285px; text-align: center;\">{loadposition contacto}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p style=\"text-align: center;\"><br /><br /></p>', '', 1, 2, '2020-03-25 01:35:15', 66, '', '2020-03-25 02:48:49', 66, 0, '0000-00-00 00:00:00', '2020-03-25 01:35:15', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"show_icon\":\"0\",\"type\":\"standard\",\"video_provider\":\"youtube\",\"video_id\":\"\",\"audio_embed\":\"\",\"image_gallery\":[],\"controls\":\"1\",\"indicators\":\"0\",\"link_text\":\"\",\"link_url\":\"\",\"link_target\":\"_blank\",\"quote_text\":\"\",\"quote_author\":\"\"}', 4, 4, '', '', 1, 16, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(2, 76, 'Contenidos', 'contenidos', '<table style=\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 305px; text-align: center;\">{loadposition desarrollo}</td>\r\n<td style=\"width: 285px; text-align: center;\">{loadposition comercial}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<table style=\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 305px; text-align: center;\"> {loadposition comunicaciones}</td>\r\n<td style=\"width: 285px; text-align: center;\"> {loadposition comunicaciones}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>', '', 1, 2, '2020-03-25 01:35:30', 66, '', '2020-03-25 02:33:15', 66, 66, '2020-03-25 02:35:15', '2020-03-25 01:35:30', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"show_icon\":\"0\",\"type\":\"standard\",\"video_provider\":\"youtube\",\"video_id\":\"\",\"audio_embed\":\"\",\"image_gallery\":[],\"controls\":\"1\",\"indicators\":\"0\",\"link_text\":\"\",\"link_url\":\"\",\"link_target\":\"_blank\",\"quote_text\":\"\",\"quote_author\":\"\"}', 11, 3, '', '', 1, 14, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(3, 77, 'Contáctenos', 'contactenos', '<p>{loadposition contacto}</p>', '', 1, 2, '2020-03-25 01:35:46', 66, '', '2020-03-25 02:17:34', 66, 66, '2020-03-25 02:35:17', '2020-03-25 01:35:46', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"show_icon\":\"0\",\"type\":\"standard\",\"video_provider\":\"youtube\",\"video_id\":\"\",\"audio_embed\":\"\",\"image_gallery\":[],\"controls\":\"1\",\"indicators\":\"0\",\"link_text\":\"\",\"link_url\":\"\",\"link_target\":\"_blank\",\"quote_text\":\"\",\"quote_author\":\"\"}', 5, 2, '', '', 1, 15, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(4, 78, 'Encuestas', 'encuestas', '<p>{loadposition encuesta}</p>', '', 1, 2, '2020-03-25 01:35:58', 66, '', '2020-03-25 02:33:37', 66, 66, '2020-03-25 02:33:37', '2020-03-25 01:35:58', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"show_icon\":\"0\",\"type\":\"standard\",\"video_provider\":\"youtube\",\"video_id\":\"\",\"audio_embed\":\"\",\"image_gallery\":[],\"controls\":\"1\",\"indicators\":\"0\",\"link_text\":\"\",\"link_url\":\"\",\"link_target\":\"_blank\",\"quote_text\":\"\",\"quote_author\":\"\"}', 2, 1, '', '', 1, 3, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(5, 83, 'Google presenta su versión preliminar Android 11 para desarrolladores', 'google-presenta-su-version-preliminar-android-11-para-desarrolladores', '<h2 style=\"text-align: center;\">Google presenta su versión preliminar Android 11 para desarrolladores</h2>\r\n<p class=\"MsoNormal\" style=\"text-align: center;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\"> <img src=\"images/android.jpeg\" alt=\"\" /></span></p>\r\n<p style=\"text-align: center;\">La próxima versión del sistema contempla terminales 5G, pantallas curvas y más privacidad.</p>\r\n', '\r\n<h1 class=\"MsoNormal\" style=\"text-align: center;\">Google presenta su versión preliminar Android 11 para desarrolladores</h1>\r\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/tyx05coXixw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>', 1, 9, '2020-03-25 02:01:34', 66, '', '2020-03-25 02:03:44', 66, 66, '2020-03-25 02:04:26', '2020-03-25 02:01:34', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"show_icon\":\"0\",\"type\":\"standard\",\"video_provider\":\"youtube\",\"video_id\":\"\",\"audio_embed\":\"\",\"image_gallery\":[],\"controls\":\"1\",\"indicators\":\"0\",\"link_text\":\"\",\"link_url\":\"\",\"link_target\":\"_blank\",\"quote_text\":\"\",\"quote_author\":\"\"}', 4, 0, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(6, 84, 'Prochile busca aumentar comercio con Colombia', 'prochile-busca-aumentar-comercio-con-colombia', '<h2 style=\"text-align: center;\">Prochile busca aumentar comercio con Colombia</h2>\r\n<p class=\"MsoNormal\" style=\"text-align: center;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\"> <img src=\"images/ventas.jpg\" alt=\"\" /></span></p>\r\n<p style=\"text-align: center;\">Al cierre de 2019, las exportaciones de Chile a Colombia sumaron US$643 millones. De esta cifra se destacan las ventas de salmón y de vino.</p>\r\n', '\r\n<h2 style=\"text-align: center;\">Prochile busca aumentar comercio con Colombia</h2>\r\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/FyqhwEEI5Ww\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>', 1, 10, '2020-03-25 02:08:01', 66, '', '2020-03-25 02:11:34', 66, 0, '0000-00-00 00:00:00', '2020-03-25 02:08:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"show_icon\":\"0\",\"type\":\"standard\",\"video_provider\":\"youtube\",\"video_id\":\"\",\"audio_embed\":\"\",\"image_gallery\":[],\"controls\":\"1\",\"indicators\":\"0\",\"link_text\":\"\",\"link_url\":\"\",\"link_target\":\"_blank\",\"quote_text\":\"\",\"quote_author\":\"\"}', 2, 0, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(7, 85, 'Vicepresidenta habla de estudio que ubica al país como el más corrupto', 'vicepresidenta-habla-de-estudio-que-ubica-al-pais-como-el-mas-corrupto', '<h2 style=\"text-align: center;\">Vicepresidenta habla de estudio que ubica al país como el más corrupto</h2>\r\n<p class=\"MsoNormal\" style=\"text-align: center;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\"> <img src=\"images/comunicaciones.jpeg\" alt=\"\" /></span></p>\r\n<p style=\"text-align: center;\">Según un informe de U.S. News, el mundo percibe a Colombia como el lugar con mayor corrupción.</p>\r\n', '\r\n<h2 style=\"text-align: center;\">Vicepresidenta habla de estudio que ubica al país como el más corrupto</h2>\r\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/2SLPFsdwd0I\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>', 1, 8, '2020-03-25 02:10:51', 66, '', '2020-03-25 02:10:51', 0, 0, '0000-00-00 00:00:00', '2020-03-25 02:10:51', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"show_icon\":\"0\",\"type\":\"standard\",\"video_provider\":\"youtube\",\"video_id\":\"\",\"audio_embed\":\"\",\"image_gallery\":[],\"controls\":\"1\",\"indicators\":\"0\",\"link_text\":\"\",\"link_url\":\"\",\"link_target\":\"_blank\",\"quote_text\":\"\",\"quote_author\":\"\"}', 1, 0, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_contentitem_tag_map`
--

CREATE TABLE `lbazs_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_content_frontpage`
--

CREATE TABLE `lbazs_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT 0,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_content_rating`
--

CREATE TABLE `lbazs_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT 0,
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_content_types`
--

CREATE TABLE `lbazs_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_content_types`
--

INSERT INTO `lbazs_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\", \"note\":\"note\"}, \"special\":{\"fulltext\":\"fulltext\"}}', 'ContentHelperRoute::getArticleRoute', '{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"ordering\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(2, 'Contact', 'com_contact.contact', '{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}', 'ContactHelperRoute::getContactRoute', '{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(4, 'User', 'com_users.user', '{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContentHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(6, 'Contact Category', 'com_contact.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContactHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(8, 'Tag', 'com_tags.tag', '{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}', 'TagsHelperRoute::getTagRoute', '{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(9, 'Banner', 'com_banners.banner', '{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(11, 'Banner Client', 'com_banners.client', '{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),
(12, 'User Notes', 'com_users.note', '{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(13, 'User Notes Category', 'com_users.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_core_log_searches`
--

CREATE TABLE `lbazs_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_extensions`
--

CREATE TABLE `lbazs_extensions` (
  `extension_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT 0 COMMENT 'Parent package ID for extensions installed as a package.',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `protected` tinyint(3) NOT NULL DEFAULT 0,
  `manifest_cache` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT 0,
  `state` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_extensions`
--

INSERT INTO `lbazs_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 0, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 0, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}', '{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 0, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 0, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 0, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{\"contact_layout\":\"_:default\",\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_tags\":\"1\",\"show_info\":\"1\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_image\":\"1\",\"show_misc\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"articles_display_num\":\"10\",\"show_profile\":\"0\",\"show_user_custom_fields\":[\"-1\"],\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"0\",\"maxLevel\":\"-1\",\"show_subcat_desc\":\"1\",\"show_empty_categories\":\"0\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_subcat_desc_cat\":\"1\",\"show_empty_categories_cat\":\"0\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"0\",\"show_pagination_limit\":\"0\",\"show_headings\":\"1\",\"show_image_heading\":\"0\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"initial_sort\":\"ordering\",\"captcha\":\"\",\"show_email_form\":\"1\",\"show_email_copy\":\"0\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_feed_link\":\"1\",\"sef_advanced\":0,\"sef_ids\":0,\"custom_fields_enable\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 0, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 0, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}', '{\"show_jed_info\":\"1\",\"cachetimeout\":\"6\",\"minimum_stability\":\"4\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 0, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"administrator\":\"en-GB\",\"site\":\"es-ES\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 0, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 0, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 0, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}', '{\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 0, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 0, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 0, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 0, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 0, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}', '{\"enabled\":\"0\",\"search_phrases\":\"1\",\"search_areas\":\"1\",\"show_date\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 0, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"template_positions_display\":\"0\",\"upload_limit\":\"10\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css,scss,sass\",\"font_formats\":\"woff,ttf,otf\",\"compressed_formats\":\"zip\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 0, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"1\",\"show_item_navigation\":\"1\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_icons\":\"1\",\"show_print_icon\":\"1\",\"show_email_icon\":\"0\",\"show_hits\":\"1\",\"show_noauth\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 0, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}', '{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"9\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 0, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 0, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}', '{\"allowUserRegistration\":\"0\",\"new_usertype\":\"2\",\"guest_usergroup\":\"9\",\"sendpassword\":\"0\",\"useractivation\":\"2\",\"mail_to_admin\":\"1\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 0, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{\"enabled\":\"0\",\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_autosuggest\":\"1\",\"show_suggested_query\":\"1\",\"show_explained_query\":\"1\",\"show_advanced\":\"1\",\"show_advanced_tips\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"sort_order\":\"relevance\",\"sort_direction\":\"desc\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stem\":\"1\",\"stemmer\":\"snowball\",\"enable_logging\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 0, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}', '{\"updatesource\":\"default\",\"customurl\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 0, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_description\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 0, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 0, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 0, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(33, 0, 'com_fields', 'component', 'com_fields', '', 1, 1, 1, 0, '{\"name\":\"com_fields\",\"type\":\"component\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(34, 0, 'com_associations', 'component', 'com_associations', '', 1, 1, 1, 0, '{\"name\":\"com_associations\",\"type\":\"component\",\"creationDate\":\"January 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_ASSOCIATIONS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(35, 0, 'com_privacy', 'component', 'com_privacy', '', 1, 1, 1, 1, '{\"name\":\"com_privacy\",\"type\":\"component\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"COM_PRIVACY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"privacy\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(36, 0, 'com_actionlogs', 'component', 'com_actionlogs', '', 1, 1, 1, 1, '{\"name\":\"com_actionlogs\",\"type\":\"component\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"COM_ACTIONLOGS_XML_DESCRIPTION\",\"group\":\"\"}', '{\"ip_logging\":0,\"csv_delimiter\":\",\",\"loggable_extensions\":[\"com_banners\",\"com_cache\",\"com_categories\",\"com_checkin\",\"com_config\",\"com_contact\",\"com_content\",\"com_installer\",\"com_media\",\"com_menus\",\"com_messages\",\"com_modules\",\"com_newsfeeds\",\"com_plugins\",\"com_redirect\",\"com_tags\",\"com_templates\",\"com_users\"]}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 0, 'LIB_PHPUTF8', 'library', 'phputf8', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPUTF8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 0, 'LIB_JOOMLA', 'library', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"LIB_JOOMLA\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"mediaversion\":\"89cb797c08d653f58b3cc32ef5f51f4d\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 0, 'LIB_IDNA', 'library', 'idna_convert', '', 0, 1, 1, 1, '{\"name\":\"LIB_IDNA\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 0, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 0, 'LIB_PHPASS', 'library', 'phpass', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPASS\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 0, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 0, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 0, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 0, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 0, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 0, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 0, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 0, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 0, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 0, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 0, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 0, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 0, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 0, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 0, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 0, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 0, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 0, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 0, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 0, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 0, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 0, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 0, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 0, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 0, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 0, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 0, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 0, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 0, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 0, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 0, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 0, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 0, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 0, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 0, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 0, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}', '{\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 0, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_version\"}', '{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 0, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}', '{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 0, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}', '{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 0, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}', '{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(318, 0, 'mod_sampledata', 'module', 'mod_sampledata', '', 1, 1, 1, 0, '{\"name\":\"mod_sampledata\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"MOD_SAMPLEDATA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_sampledata\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(319, 0, 'mod_latestactions', 'module', 'mod_latestactions', '', 1, 1, 1, 0, '{\"name\":\"mod_latestactions\",\"type\":\"module\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"MOD_LATESTACTIONS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latestactions\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(320, 0, 'mod_privacy_dashboard', 'module', 'mod_privacy_dashboard', '', 1, 1, 1, 0, '{\"name\":\"mod_privacy_dashboard\",\"type\":\"module\",\"creationDate\":\"June 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"MOD_PRIVACY_DASHBOARD_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_privacy_dashboard\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `lbazs_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(400, 0, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}', '{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 0, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 0, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}', '{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 0, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 0, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}', '{\"mode\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 0, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}', '{\"style\":\"xhtml\"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 0, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 0, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}', '{\"position\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 0, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 0, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 - 2017 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.40.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}', '{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 0, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 0, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2019\",\"author\":\"Tiny Technologies, Inc\",\"copyright\":\"Tiny Technologies, Inc\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"https:\\/\\/www.tiny.cloud\",\"version\":\"4.5.11\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}', '{\"configuration\":{\"toolbars\":{\"2\":{\"toolbar1\":[\"bold\",\"underline\",\"strikethrough\",\"|\",\"undo\",\"redo\",\"|\",\"bullist\",\"numlist\",\"|\",\"pastetext\"]},\"1\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"formatselect\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"code\",\"|\",\"hr\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"pastetext\",\"preview\"]},\"0\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"styleselect\",\"|\",\"formatselect\",\"fontselect\",\"fontsizeselect\",\"|\",\"searchreplace\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"image\",\"|\",\"code\",\"|\",\"forecolor\",\"backcolor\",\"|\",\"fullscreen\",\"|\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"emoticons\",\"media\",\"hr\",\"ltr\",\"rtl\",\"|\",\"cut\",\"copy\",\"paste\",\"pastetext\",\"|\",\"visualchars\",\"visualblocks\",\"nonbreaking\",\"blockquote\",\"template\",\"|\",\"print\",\"preview\",\"codesample\",\"insertdatetime\",\"removeformat\"]}},\"setoptions\":{\"2\":{\"access\":[\"1\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"1\":{\"access\":[\"6\",\"2\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"0\":{\"access\":[\"7\",\"4\",\"8\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}}},\"sets_amount\":3,\"html_height\":\"550\",\"html_width\":\"750\",\"extended_elements\":\",@[data-lightbox],@[data-spotlight]\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 0, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 0, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 0, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 0, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 0, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 0, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 0, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 0, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 0, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 0, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}', '{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 0, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}', '{\"browsercache\":\"0\",\"cachetime\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 0, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}', '{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 0, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 0, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(428, 0, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 0, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 0, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(431, 0, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}', '{\"autowebpage\":\"\",\"category\":\"4\",\"autopublish\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 0, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"autoregister\":\"1\",\"mail_to_user\":\"1\",\"forceLogout\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 0, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}', '{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 0, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 0, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 0, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 0, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 0, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 0, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 0, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"highlight\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 0, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 0, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 0, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 0, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 0, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 0, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 0, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 0, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cookie\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 0, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 0, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(452, 0, 'plg_system_updatenotification', 'plugin', 'updatenotification', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_updatenotification\",\"type\":\"plugin\",\"creationDate\":\"May 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"updatenotification\"}', '{\"lastrun\":1585138907}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(453, 0, 'plg_editors-xtd_module', 'plugin', 'module', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_module\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_MODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"module\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(454, 0, 'plg_system_stats', 'plugin', 'stats', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_stats\",\"type\":\"plugin\",\"creationDate\":\"November 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 0, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 0, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 0, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(458, 0, 'plg_quickicon_phpversioncheck', 'plugin', 'phpversioncheck', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_phpversioncheck\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_QUICKICON_PHPVERSIONCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpversioncheck\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(459, 0, 'plg_editors-xtd_menu', 'plugin', 'menu', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_menu\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(460, 0, 'plg_editors-xtd_contact', 'plugin', 'contact', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_contact\",\"type\":\"plugin\",\"creationDate\":\"October 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(461, 0, 'plg_system_fields', 'plugin', 'fields', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_fields\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_SYSTEM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(462, 0, 'plg_fields_calendar', 'plugin', 'calendar', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_calendar\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CALENDAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"calendar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(463, 0, 'plg_fields_checkboxes', 'plugin', 'checkboxes', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_checkboxes\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CHECKBOXES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"checkboxes\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(464, 0, 'plg_fields_color', 'plugin', 'color', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_color\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_COLOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"color\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(465, 0, 'plg_fields_editor', 'plugin', 'editor', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_editor\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_EDITOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"editor\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(466, 0, 'plg_fields_imagelist', 'plugin', 'imagelist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_imagelist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_IMAGELIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"imagelist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(467, 0, 'plg_fields_integer', 'plugin', 'integer', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_integer\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_INTEGER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"integer\"}', '{\"multiple\":\"0\",\"first\":\"1\",\"last\":\"100\",\"step\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(468, 0, 'plg_fields_list', 'plugin', 'list', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_list\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_LIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"list\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(469, 0, 'plg_fields_media', 'plugin', 'media', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_media\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(470, 0, 'plg_fields_radio', 'plugin', 'radio', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_radio\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_RADIO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"radio\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(471, 0, 'plg_fields_sql', 'plugin', 'sql', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_sql\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_SQL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sql\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(472, 0, 'plg_fields_text', 'plugin', 'text', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_text\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"text\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(473, 0, 'plg_fields_textarea', 'plugin', 'textarea', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_textarea\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXTAREA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"textarea\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(474, 0, 'plg_fields_url', 'plugin', 'url', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_url\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_URL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"url\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(475, 0, 'plg_fields_user', 'plugin', 'user', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_user\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(476, 0, 'plg_fields_usergrouplist', 'plugin', 'usergrouplist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_usergrouplist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USERGROUPLIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"usergrouplist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(477, 0, 'plg_content_fields', 'plugin', 'fields', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_CONTENT_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(478, 0, 'plg_editors-xtd_fields', 'plugin', 'fields', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(479, 0, 'plg_sampledata_blog', 'plugin', 'blog', 'sampledata', 0, 1, 1, 0, '{\"name\":\"plg_sampledata_blog\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"PLG_SAMPLEDATA_BLOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"blog\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(480, 0, 'plg_system_sessiongc', 'plugin', 'sessiongc', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sessiongc\",\"type\":\"plugin\",\"creationDate\":\"February 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.6\",\"description\":\"PLG_SYSTEM_SESSIONGC_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sessiongc\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(481, 0, 'plg_fields_repeatable', 'plugin', 'repeatable', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_repeatable\",\"type\":\"plugin\",\"creationDate\":\"April 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_FIELDS_REPEATABLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"repeatable\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(482, 0, 'plg_content_confirmconsent', 'plugin', 'confirmconsent', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_confirmconsent\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_CONTENT_CONFIRMCONSENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"confirmconsent\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(483, 0, 'PLG_SYSTEM_ACTIONLOGS', 'plugin', 'actionlogs', 'system', 0, 1, 1, 0, '{\"name\":\"PLG_SYSTEM_ACTIONLOGS\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_SYSTEM_ACTIONLOGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"actionlogs\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(484, 0, 'PLG_ACTIONLOG_JOOMLA', 'plugin', 'joomla', 'actionlog', 0, 1, 1, 0, '{\"name\":\"PLG_ACTIONLOG_JOOMLA\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_ACTIONLOG_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(485, 0, 'plg_system_privacyconsent', 'plugin', 'privacyconsent', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_privacyconsent\",\"type\":\"plugin\",\"creationDate\":\"April 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_SYSTEM_PRIVACYCONSENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"privacyconsent\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(486, 0, 'plg_system_logrotation', 'plugin', 'logrotation', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_logrotation\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_SYSTEM_LOGROTATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logrotation\"}', '{\"lastrun\":1585093406}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `lbazs_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(487, 0, 'plg_privacy_user', 'plugin', 'user', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_user\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(488, 0, 'plg_quickicon_privacycheck', 'plugin', 'privacycheck', 'quickicon', 0, 1, 1, 0, '{\"name\":\"plg_quickicon_privacycheck\",\"type\":\"plugin\",\"creationDate\":\"June 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_QUICKICON_PRIVACYCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"privacycheck\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(489, 0, 'plg_user_terms', 'plugin', 'terms', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_terms\",\"type\":\"plugin\",\"creationDate\":\"June 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_USER_TERMS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"terms\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(490, 0, 'plg_privacy_contact', 'plugin', 'contact', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_contact\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(491, 0, 'plg_privacy_content', 'plugin', 'content', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_content\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(492, 0, 'plg_privacy_message', 'plugin', 'message', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_message\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_MESSAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"message\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(493, 0, 'plg_privacy_actionlogs', 'plugin', 'actionlogs', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_actionlogs\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_ACTIONLOGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"actionlogs\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(494, 0, 'plg_captcha_recaptcha_invisible', 'plugin', 'recaptcha_invisible', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha_invisible\",\"type\":\"plugin\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_INVISIBLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha_invisible\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(495, 0, 'plg_privacy_consents', 'plugin', 'consents', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_consents\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_CONSENTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"consents\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 0, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 0, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"showSiteName\":\"0\",\"colourChoice\":\"0\",\"boldText\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 0, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{\"name\":\"protostar\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 0, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 802, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"March 2020\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.16\",\"description\":\"en-GB site language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 802, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"March 2020\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.16\",\"description\":\"en-GB administrator language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 0, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"March 2020\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2020 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.16\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 0, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"March 2020\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.16.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 10002, 'Spanishespaol', 'language', 'es-ES', '', 0, 1, 0, 0, '{\"name\":\"Spanish (espa\\u00f1ol)\",\"type\":\"language\",\"creationDate\":\"11\\/03\\/2020\",\"author\":\"Spanish Translation Team [es-ES]\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"es.es.translation.team@gmail.com\",\"authorUrl\":\"http:\\/\\/joomlaes.org\",\"version\":\"3.9.16.1\",\"description\":\"es-ES - Site language\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 10002, 'Spanishespaol', 'language', 'es-ES', '', 1, 1, 0, 0, '{\"name\":\"Spanish (espa\\u00f1ol)\",\"type\":\"language\",\"creationDate\":\"11\\/03\\/2020\",\"author\":\"Spanish Translation Team [es-ES]\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"es.es.translation.team@gmail.com\",\"authorUrl\":\"http:\\/\\/joomlaes.org\",\"version\":\"3.9.16.1\",\"description\":\"es-ES - Administration language\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 0, 'Spanish (es-ES) Language Pack', 'package', 'pkg_es-ES', '', 0, 1, 1, 0, '{\"name\":\"Spanish (es-ES) Language Pack\",\"type\":\"package\",\"creationDate\":\"11\\/03\\/2020\",\"author\":\"Spanish Translation Team [es-ES]\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"es.es.translation.team@gmail.com\",\"authorUrl\":\"http:\\/\\/joomlaes.org\",\"version\":\"3.9.16.1\",\"description\":\"<div style=\\\"text-align:left;\\\"><h2>Successfully installed the spanish language pack for Joomla! 3.9.16.1<\\/h2><p><\\/p><p>Please report any bugs or issues at the Spanish Translation Team using the mail: es.es.translation.team@gmail.com<\\/p><p><\\/p><p>Translated by: The Spanish Translation Team [es-ES]<\\/p><h2>El paquete en espa\\u00f1ol para Joomla! 3.9.16.1 se ha instalado correctamente.<\\/h2><p><\\/p><p>Por favor, reporte cualquier bug o asunto relacionado a nuestra direcci\\u00f3n de correo electr\\u00f3nico: es.es.translation.team@gmail.com<\\/p><p><\\/p><p>Traducci\\u00f3n: Spanish Translation Team [es-ES]<\\/p><\\/div>\",\"group\":\"\",\"filename\":\"pkg_es-ES\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 0, 'com_widgetkit', 'component', 'com_widgetkit', '', 1, 1, 0, 0, '{\"name\":\"com_widgetkit\",\"type\":\"component\",\"creationDate\":\"January 2016\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/yootheme.com\",\"version\":\"2.5.3\",\"description\":\"<h1>|| Downloaded from || JARtheme.COM ||<\\/h1>\",\"group\":\"\",\"filename\":\"widgetkit\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 0, 'Widgetkit', 'module', 'mod_widgetkit', '', 0, 1, 0, 0, '{\"name\":\"Widgetkit\",\"type\":\"module\",\"creationDate\":\"January 2016\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/yootheme.com\",\"version\":\"2.5.3\",\"description\":\"<h1>|| Downloaded from || JARtheme.COM ||<\\/h1>\",\"group\":\"\",\"filename\":\"mod_widgetkit\"}', '{\"widget_id\":\"\",\"moduleclass_sfx\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 0, 'Widgetkit Twitter', 'module', 'mod_widgetkit_twitter', '', 0, 1, 0, 0, '{\"name\":\"Widgetkit Twitter\",\"type\":\"module\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Twitter module for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\",\"filename\":\"mod_widgetkit_twitter\"}', '{\"style_\":\"list\",\"from_user\":\"\",\"to_user\":\"\",\"ref_user\":\"\",\"hashtag\":\"\",\"word\":\"\",\"nots\":\"\",\"limit\":\"5\",\"image_size\":\"48\",\"show_image\":\"1\",\"show_author\":\"1\",\"show_date\":\"1\",\"consumer_key\":\"\",\"consumer_secret\":\"\",\"access_token\":\"\",\"access_token_secret\":\"\",\"moduleclass_sfx\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 0, 'System - Widgetkit', 'plugin', 'widgetkit_system', 'system', 0, 0, 1, 0, '{\"name\":\"System - Widgetkit\",\"type\":\"plugin\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Plugin for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\",\"filename\":\"widgetkit_system\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 0, 'Content - Widgetkit', 'plugin', 'widgetkit_content', 'content', 0, 0, 1, 0, '{\"name\":\"Content - Widgetkit\",\"type\":\"plugin\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Plugin for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\",\"filename\":\"widgetkit_content\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10008, 0, 'System - Widgetkit ZOO', 'plugin', 'widgetkit_zoo', 'system', 0, 0, 1, 0, '{\"name\":\"System - Widgetkit ZOO\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/yootheme.com\",\"version\":\"2.5.3\",\"description\":\"A widget toolkit by YOOtheme.\",\"group\":\"\",\"filename\":\"widgetkit_zoo\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10009, 0, 'System - Widgetkit Joomla', 'plugin', 'widgetkit_joomla', 'system', 0, 0, 1, 0, '{\"name\":\"System - Widgetkit Joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Joomla Content plugin for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\",\"filename\":\"widgetkit_joomla\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10010, 0, 'System - Widgetkit K2', 'plugin', 'widgetkit_k2', 'system', 0, 0, 1, 0, '{\"name\":\"System - Widgetkit K2\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/yootheme.com\",\"version\":\"2.5.3\",\"description\":\"A widget toolkit by YOOtheme.\",\"group\":\"\",\"filename\":\"widgetkit_k2\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10011, 0, 'Installer - YOOtheme', 'plugin', 'yootheme', 'installer', 0, 1, 1, 0, '{\"name\":\"Installer - YOOtheme\",\"type\":\"plugin\",\"creationDate\":\"December 2014\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/yootheme.com\",\"version\":\"1.0.0\",\"description\":\"PLG_INSTALLER_YOOTHEME_DESCRIPTION\",\"group\":\"\",\"filename\":\"yootheme\"}', '[]', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10012, 0, 'Editors-XTD - Widgetkit', 'plugin', 'widgetkit', 'editors-xtd', 0, 0, 1, 0, '{\"name\":\"Editors-XTD - Widgetkit\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/yootheme.com\",\"version\":\"2.5.3\",\"description\":\"A widget toolkit by YOOtheme.\",\"group\":\"\",\"filename\":\"widgetkit\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10013, 0, 'Content - Widgetkit', 'plugin', 'widgetkit', 'content', 0, 0, 1, 0, '{\"name\":\"Content - Widgetkit\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/yootheme.com\",\"version\":\"2.5.3\",\"description\":\"<h1>|| Downloaded from || JARtheme.COM ||<\\/h1>\",\"group\":\"\",\"filename\":\"widgetkit\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10014, 0, 'COM_FOXCONTACT', 'component', 'com_foxcontact', '', 1, 1, 0, 0, '{\"name\":\"COM_FOXCONTACT\",\"type\":\"component\",\"creationDate\":\"Feb 2018\",\"author\":\"Demis Palma\",\"copyright\":\"Copyright (c) 2010 - 2015 Demis Palma. All rights reserved.\",\"authorEmail\":\"demis@fox.ra.it\",\"authorUrl\":\"http:\\/\\/www.fox.ra.it\\/\",\"version\":\"3.8.2\",\"description\":\"COM_FOXCONTACT_DESCRIPTION\",\"group\":\"\",\"filename\":\"foxcontact\"}', '{\"use_dns\":\"1\",\"mail_sender_type\":\"Swift\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 0, 'Fox Contact', 'library', 'foxcontact', '', 0, 1, 1, 0, '{\"name\":\"Fox Contact\",\"type\":\"library\",\"creationDate\":\"Feb 2018\",\"author\":\"Demis Palma\",\"copyright\":\"Copyright (c) 2010 - 2015 Demis Palma. All rights reserved.\",\"authorEmail\":\"demis@fox.ra.it\",\"authorUrl\":\"http:\\/\\/www.fox.ra.it\\/\",\"version\":\"3.8.2\",\"description\":\"\",\"group\":\"\",\"filename\":\"foxcontact\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 0, 'MOD_FOXCONTACT', 'module', 'mod_foxcontact', '', 0, 1, 0, 0, '{\"name\":\"MOD_FOXCONTACT\",\"type\":\"module\",\"creationDate\":\"Feb 2018\",\"author\":\"Demis Palma\",\"copyright\":\"Copyright (c) 2010 - 2015 Demis Palma. All rights reserved.\",\"authorEmail\":\"demis@fox.ra.it\",\"authorUrl\":\"http:\\/\\/www.fox.ra.it\\/\",\"version\":\"3.8.2\",\"description\":\"MOD_FOXCONTACT_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_foxcontact\"}', '{\"design\":\"{}\",\"spam_check\":\"1\",\"spam_detected_textdisplay\":\"1\",\"spam_log\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 0, 'plg_content_foxcontact', 'plugin', 'foxcontact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_foxcontact\",\"type\":\"plugin\",\"creationDate\":\"Feb 2018\",\"author\":\"Demis Palma\",\"copyright\":\"Copyright (c) 2010 - 2015 Demis Palma. All rights reserved.\",\"authorEmail\":\"demis@fox.ra.it\",\"authorUrl\":\"http:\\/\\/www.fox.ra.it\\/\",\"version\":\"3.8.2\",\"description\":\"PLG_FOXCONTACT_DESCRIPTION\",\"group\":\"\",\"filename\":\"foxcontact\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 0, 'com_uniterevolution2', 'component', 'com_uniterevolution2', '', 1, 1, 0, 0, '{\"name\":\"com_uniterevolution2\",\"type\":\"component\",\"creationDate\":\"August 2012\",\"author\":\"Unite CMS \\/ Valiano\",\"copyright\":\"Copyright (C) 2012 UniteCMS.net, All rights reserved.\",\"authorEmail\":\"support@unitecms.net\",\"authorUrl\":\"http:\\/\\/www.unitecms.net\",\"version\":\"4.6.95\",\"description\":\"\\n\\t\\t\\t<h1 style=\\\"padding:10px; color:#00b2ff\\\">\\u062c\\u0648\\u0645\\u0644\\u0627 \\u0648\\u0628 \\u062f\\u06cc\\u0632\\u0627\\u06cc\\u0646<\\/h1>\\n\\t\\t\\t\\n\\t<p style=\\\"padding:10px; color:#39F\\\">---------------------------------------------------------------<\\/p>\\n<h2 style=\\\"padding:10px; color:#069\\\">\\u0645\\u0631\\u06a9\\u0632 \\u062f\\u0627\\u0646\\u0644\\u0648\\u062f \\u0631\\u0627\\u06cc\\u06af\\u0627\\u0646 \\u0627\\u0641\\u0632\\u0648\\u0646\\u0647 \\u0648 \\u0642\\u0627\\u0644\\u0628 \\u0647\\u0627\\u06cc \\u062c\\u0648\\u0645\\u0644\\u0627<\\/h2>\\n<h3 style=\\\"padding:10px; color: #033\\\">\\u062f\\u0627\\u0646\\u0644\\u0648\\u062f \\u0622\\u0645\\u0648\\u0632\\u0634 \\u0648 \\u0627\\u0645\\u06a9\\u0627\\u0646\\u0627\\u062a \\u062c\\u0648\\u0645\\u0644\\u0627 <a href=\\\"http:\\/\\/www.joomlawd.com\\\" target=\\\"_blank\\\">www.joomlawd.com <\\/a> <\\/h3>\\n<h3 style=\\\"padding:10px; color: #033\\\"> \\u0641\\u0631\\u0648\\u0634 \\u0641\\u0636\\u0627\\u06cc \\u0645\\u06cc\\u0632\\u0628\\u0627\\u0646\\u06cc<a href=\\\"http:\\/\\/www.magicgfx.ir\\/client\\/cart.php\\\" target=\\\"_blank\\\">www.magicgfx.ir <\\/a> <\\/h3>\\n\\t\\t\",\"group\":\"\",\"filename\":\"manifest\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 0, 'Unite Revolution Slider 2', 'module', 'mod_unite_revolution2', '', 0, 1, 0, 0, '{\"name\":\"Unite Revolution Slider 2\",\"type\":\"module\",\"creationDate\":\"October 2012\",\"author\":\"Unite CMS\",\"copyright\":\"Copyright (C) 2012 UniteCMS.net, All rights reserved.\",\"authorEmail\":\"support@unitecms.net\",\"authorUrl\":\"http:\\/\\/unitecms.net\",\"version\":\"4.6.93\",\"description\":\"\\n\\t\\t\\t<div style=\\\"font-weight:normal;\\\">\\n\\t\\t\\t<p><strong>Unite Revolution Slider (new edition)<\\/strong> module. Put the slider on any page. All the slider configuration located in Component.<\\/p>\\n\\t\\t\\t<p>\\n\\t\\t\\t\\tFor support please turn to <a href=\\\"http:\\/\\/unitecms.net\\/joomla-extensions\\/unite-revolution-slider-responsive\\\" target=\\\"_blank\\\">Unite Revolution Slider Page<\\/a>\\n\\t\\t\\t<\\/p>\\n\\t\\t\\t<small style=\\\"float:right\\\">ver. 4.6.93<\\/small>\\n\\t\\t\\t<\\/div>\\n     \\n\\t \",\"group\":\"\",\"filename\":\"mod_unite_revolution2\"}', '{\"include_jquery\":\"true\",\"js_load_type\":\"head\",\"no_conflict_mode\":\"false\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10020, 0, 'JCE', 'component', 'com_jce', '', 1, 1, 0, 0, '{\"name\":\"JCE\",\"type\":\"component\",\"creationDate\":\"12 December 2013\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2013 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"www.joomlacontenteditor.net\",\"version\":\"2.3.4.4\",\"description\":\"WF_ADMIN_DESC\",\"group\":\"\",\"filename\":\"jce\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10021, 0, 'plg_editors_jce', 'plugin', 'jce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_jce\",\"type\":\"plugin\",\"creationDate\":\"12 December 2013\",\"author\":\"Ryan Demmer\",\"copyright\":\"2006-2010 Ryan Demmer\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.3.4.4\",\"description\":\"WF_EDITOR_PLUGIN_DESC\",\"group\":\"\",\"filename\":\"jce\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10022, 0, 'plg_quickicon_jcefilebrowser', 'plugin', 'jcefilebrowser', 'quickicon', 0, 1, 1, 0, '{\"name\":\"plg_quickicon_jcefilebrowser\",\"type\":\"plugin\",\"creationDate\":\"12 December 2013\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2013 Ryan Demmer. All rights reserved\",\"authorEmail\":\"@@email@@\",\"authorUrl\":\"www.joomalcontenteditor.net\",\"version\":\"2.3.4.4\",\"description\":\"PLG_QUICKICON_JCEFILEBROWSER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jcefilebrowser\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10023, 0, 'ut_ufolio', 'template', 'ut_ufolio', '', 0, 1, 1, 0, '{\"name\":\"ut_ufolio\",\"type\":\"template\",\"creationDate\":\"Dec 2016\",\"author\":\"Unitemplates.com\",\"copyright\":\"Copyright (C), Unitemplates Professional Templates. Todos los derechos reservados.\",\"authorEmail\":\"info@unitemplates.com\",\"authorUrl\":\"http:\\/\\/www.unitemplates.com\",\"version\":\"1.2\",\"description\":\"\\n\\t\\t\\n\\t\\t<div align=\\\"center\\\">\\n\\t\\t\\t<div class=\\\"alert alert-success\\\" style=\\\"background-color:#DFF0D8;border-color:#D6E9C6;color: #b2b5c3;padding: 1px 0;\\\">\\n\\t\\t\\t\\t<a href=\\\"http:\\/\\/www.unitemplates.com\\/\\\" target=\\\"_blank\\\"><img src=\\\"http:\\/\\/www.unitemplates.com\\/images\\/logos\\/logo.png\\\" alt=\\\"Unitemplates\\\" width=\\\"160\\\" height=\\\"56\\\"><\\/a>\\n\\t\\t\\t\\t<h4><a href=\\\"http:\\/\\/www.unitemplates.com\\/\\\" target=\\\"_blank\\\" title=\\\"Unitemplates\\\">Home<\\/a> | <a href=\\\"http:\\/\\/demo.unitemplates.com\\/joomla\\/\\\" target=\\\"_blank\\\" title=\\\"Demo Joomla templates\\\">Demo<\\/a> | <a href=\\\"http:\\/\\/www.unitemplates.com\\/documentacion\\\" target=\\\"_blank\\\" title=\\\"Templates Documentation\\\">Documentacion<\\/a> | <a href=\\\"https:\\/\\/github.com\\/t3framework\\/t3\\/tags\\\" target=\\\"_blank\\\" title=\\\"Templates Documentation\\\">T3<\\/a><\\/h4>\\n\\t\\t\\t\\t<p> <\\/p>\\n\\t\\t\\t\\t<span style=\\\"color:#cc0000\\\">Note: Please install and activate the plugin T3, which lies in the Plugins folder of the downloaded file.<\\/span><p><\\/p>\\n\\t\\t\\t\\t<span style=\\\"color:#000000\\\">Nota: Por favor instala y activa el plugin de T3, que se encuentra en la carpeta Plugins del archivo descargado.<\\/span><p><\\/p>\\n\\t\\t\\t\\t<p>Copyright 2010 - 2015 <a href=\'http:\\/\\/www.unitemplates.com\\/\' title=\'Unitemplates!\'>Unitemplates.com<\\/a>.<\\/p>\\n\\t\\t\\t<\\/div>\\n\\t\\t\\t<style>table.adminform{width: 100%;}<\\/style>\\n\\t\\t<\\/div>\\n\\t\\t\\n\\t\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"sticky_nav\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10024, 0, 'T3 Framework', 'plugin', 't3', 'system', 0, 1, 1, 0, '{\"name\":\"T3 Framework\",\"type\":\"plugin\",\"creationDate\":\"November 30, 2016\",\"author\":\"JoomlArt.com\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"info@joomlart.com\",\"authorUrl\":\"http:\\/\\/www.t3-framework.org\",\"version\":\"2.6.2\",\"description\":\"\\n\\t\\n\\t<div align=\\\"center\\\">\\n\\t\\t<div class=\\\"alert alert-success\\\" style=\\\"background-color:#DFF0D8;border-color:#D6E9C6;color: #468847;padding: 1px 0;\\\">\\n\\t\\t\\t\\t<a href=\\\"http:\\/\\/t3-framework.org\\/\\\"><img src=\\\"http:\\/\\/joomlart.s3.amazonaws.com\\/images\\/jat3v3-documents\\/message-installation\\/logo.png\\\" alt=\\\"some_text\\\" width=\\\"300\\\" height=\\\"99\\\"><\\/a>\\n\\t\\t\\t\\t<h4><a href=\\\"http:\\/\\/t3-framework.org\\/\\\" title=\\\"\\\">Home<\\/a> | <a href=\\\"http:\\/\\/demo.t3-framework.org\\/\\\" title=\\\"\\\">Demo<\\/a> | <a href=\\\"http:\\/\\/t3-framework.org\\/documentation\\\" title=\\\"\\\">Document<\\/a> | <a href=\\\"https:\\/\\/github.com\\/t3framework\\/t3\\/blob\\/master\\/CHANGELOG.md\\\" title=\\\"\\\">Changelog<\\/a><\\/h4>\\n\\t\\t<p> <\\/p>\\n\\t\\t<p>Copyright 2004 - 2016 <a href=\'http:\\/\\/www.joomlart.com\\/\' title=\'Visit Joomlart.com!\'>JoomlArt.com<\\/a>.<\\/p>\\n\\t\\t<\\/div>\\n     <style>table.adminform{width: 100%;}<\\/style>\\n\\t <\\/div>\\n\\t\\t\\n\\t\",\"group\":\"\",\"filename\":\"t3\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10025, 0, 'SP Simple Portfolio', 'component', 'com_spsimpleportfolio', '', 1, 1, 0, 0, '{\"name\":\"SP Simple Portfolio\",\"type\":\"component\",\"creationDate\":\"December 2015\",\"author\":\"JoomShaper\",\"copyright\":\"Copyright (c) 2010- 2015 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"http:\\/\\/www.joomshaper.com\",\"version\":\"1.3\",\"description\":\"Simple Portfolio Component for Joomla 3.3+\",\"group\":\"\",\"filename\":\"spsimpleportfolio\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10026, 0, 'MOD_ITPSHARE', 'module', 'mod_itpshare', '', 0, 1, 0, 0, '{\"name\":\"MOD_ITPSHARE\",\"type\":\"module\",\"creationDate\":\"28 September, 2016\",\"author\":\"Todor Iliev\",\"copyright\":\"Copyright (C) 2016 Todor Iliev ( ITPrism.com )\",\"authorEmail\":\"todor@itprism.com\",\"authorUrl\":\"http:\\/\\/itprism.com\",\"version\":\"2.9\",\"description\":\"\\n    <p><a href=\\\"http:\\/\\/itprism.com\\/free-joomla-extensions\\/social-marketing-seo\\/socialmedia-multi-share\\\" target=\\\"_blank\\\">ITPShare<\\/a> is an extension that adds republish buttons, widgets or goodies on your site. There are social plugins of Twitter, Facebook, LinkedIn, StumbleUpon, Google Plus, Google Share, Thumblr, Pinterest, BufferApp,... It will help you to find more readers for your publications.<\\/p>\\n    <p><a href=\'http:\\/\\/itprism.com\\/help\\/30-itpshare-documentation-faq\' target=\'_blank\'>Read documentation<\\/a> that will help you to setup the extensions.<\\/p>\\n    <p><a href=\\\"http:\\/\\/itprism.com\\/\\\" target=\\\"_blank\\\">Subscribe for the newsletter<\\/a> to receive <strong>gifts<\\/strong>, <strong>discount codes<\\/strong>, information about <strong>updates<\\/strong> and information about <strong>new ITPrism extensions<\\/strong>.<\\/p>\\n    <p>Please, <a href=\\\"http:\\/\\/extensions.joomla.org\\/extensions\\/social-web\\/social-share\\/social-multi-share\\/15665\\\" target=\\\"_blank\\\">vote for the extension<\\/a> on Joomla! Extensions Directory<\\/p>\\n    \",\"group\":\"\",\"filename\":\"mod_itpshare\"}', '{\"loadCss\":\"1\",\"dynamicLocale\":\"0\",\"twitterButton\":\"0\",\"twitterName\":\"\",\"twitterRecommend\":\"\",\"twitterHashtag\":\"\",\"twitterSize\":\"small\",\"twitterCounter\":\"horizontal\",\"twitterLanguage\":\"en\",\"load_twitter_library\":\"1\",\"tumblrButton\":\"0\",\"tumblrType\":\"small\",\"loadTumblrJsLib\":\"1\",\"facebookLikeButton\":\"0\",\"facebookLikeAction\":\"like\",\"facebookLikeType\":\"button_count\",\"facebookLikeFaces\":\"0\",\"facebookLikeColor\":\"light\",\"facebookLikeFont\":\"\",\"facebookLikeWidth\":\"90\",\"fbLocale\":\"en_US\",\"facebookKidDirectedSite\":\"0\",\"facebookLikeAppId\":\"\",\"facebookLikeShare\":\"0\",\"facebookRootDiv\":\"1\",\"facebookLoadJsLib\":\"1\",\"stumbleButton\":\"0\",\"stumbleType\":\"1\",\"linkedInButton\":\"0\",\"linkedInType\":\"right\",\"linkedInLocale\":\"en_US\",\"load_linkedin_library\":\"1\",\"redditButton\":\"0\",\"redditType\":\"0\",\"redditBgColor\":\"\",\"redditBorderColor\":\"\",\"redditNewTab\":\"1\",\"plusButton\":\"0\",\"plusType\":\"medium\",\"plusAnnotation\":\"\",\"plusLocale\":\"en\",\"loadGoogleJsLib\":\"1\",\"pinterestButton\":\"0\",\"pinterestImages\":\"one\",\"pinterestType\":\"none\",\"pinterestColor\":\"gray\",\"pinterestLarge\":\"0\",\"loadPinterestJsLib\":\"1\",\"bufferButton\":\"0\",\"bufferType\":\"horizontal\",\"bufferTwitterName\":\"\",\"gsButton\":\"0\",\"gsType\":\"0\",\"gsAnnotation\":\"bubble\",\"gsLocale\":\"en\",\"shortener_service\":\"\",\"shortener_login\":\"\",\"shortener_api_key\":\"\",\"ebuttons1\":\"\",\"ebuttons2\":\"\",\"ebuttons3\":\"\",\"ebuttons4\":\"\",\"ebuttons5\":\"\",\"cache\":\"1\",\"cache_time\":\"900\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10027, 0, 'S5 MailChimp Signup', 'module', 'mod_s5_mailchimp_signup', '', 0, 1, 0, 0, '{\"name\":\"S5 MailChimp Signup\",\"type\":\"module\",\"creationDate\":\"September 2014\",\"author\":\"Shape5.com\",\"copyright\":\"\",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"The S5 MailChimp Signup module allows you to easily add a MailChimp Newsletter signup to your site so users can signup via the forum and be auto added to any list you specify in the admin of the module.\\n\\t\",\"group\":\"\",\"filename\":\"mod_s5_mailchimp_signup\"}', '{\"moduleclass_sfx\":\"\",\"mailchimp_pretext\":\"\",\"\":\"Instructions\",\"@spacer\":\"\",\"mailchimp_emailaddresstext\":\"Email Address...\",\"mailchimp_subscribe\":\"Join\",\"mailchimp_apikey\":\"\",\"mailchimp_listid\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10028, 0, 'SP Simple Portfolio Module', 'module', 'mod_spsimpleportfolio', '', 0, 1, 0, 0, '{\"name\":\"SP Simple Portfolio Module\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"JoomShaper\",\"copyright\":\"Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.2\",\"description\":\"Module to display latest item from SP Simple Portfolio\",\"group\":\"\",\"filename\":\"mod_spsimpleportfolio\"}', '{\"show_filter\":\"1\",\"category_id\":\"\",\"layout_type\":\"default\",\"columns\":\"3\",\"thumbnail_type\":\"masonry\",\"limit\":\"12\",\"cache\":\"1\",\"cache_time\":\"900\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10029, 0, 'UT Contact Pro', 'module', 'mod_ut_contact_pro', '', 0, 1, 0, 0, '{\"name\":\"UT Contact Pro\",\"type\":\"module\",\"creationDate\":\"Oct 2016\",\"author\":\"Unitemplates\",\"copyright\":\"Copyright (C) 2015 Unitemplates.com. All rights reserved.\",\"authorEmail\":\"info@unitemplates.com\",\"authorUrl\":\"http:\\/\\/www.unitemplates.com\\/\",\"version\":\"1.3\",\"description\":\"UT Contact Pro is a multi language contact module. You can combine maps, form, additional information, easy to configure and Anti-Spam.\",\"group\":\"\",\"filename\":\"mod_ut_contact_pro\"}', '{\"email_recipient\":\"email@email.com\",\"from_name\":\"Contact From Web\",\"from_email\":\"contact@yoursite.com\",\"success_text\":\"Thank you. Message been sent\",\"error_text\":\"Message not sent. Please try again.\",\"exact_url\":\"1\",\"disable_https\":\"1\",\"fixed_url\":\"0\",\"fixed_url_address\":\"\",\"info_list\":\"{\\\"info_list0\\\":{\\\"icon\\\":\\\"fa fa-home\\\",\\\"name\\\":\\\"Address\\\",\\\"detail\\\":\\\"Los Olivos Lima - Peru\\\"},\\\"info_list1\\\":{\\\"icon\\\":\\\"fa fa-phone\\\",\\\"name\\\":\\\"Phone\\\",\\\"detail\\\":\\\"51 000-0000\\\"},\\\"info_list2\\\":{\\\"icon\\\":\\\"fa fa-envelope\\\",\\\"name\\\":\\\"Mail\\\",\\\"detail\\\":\\\"info@localhost.xyz\\\"}}\",\"google_map\":\"0\",\"map_key\":\"\",\"coordinates\":\"\",\"map_height\":\"400px\",\"zoom\":\"15\",\"zoom_mouse\":\"false\",\"show_control\":\"true\",\"show_marker\":\"1\",\"add_container\":\"container\",\"opacity\":\"1\",\"enable_anti_spam\":\"1\",\"anti_spam_q\":\"3+4 = ?\",\"anti_spam_a\":\"7\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10030, 0, 'UT Multi Module', 'module', 'mod_ut_multimodule', '', 0, 1, 0, 0, '{\"name\":\"UT Multi Module\",\"type\":\"module\",\"creationDate\":\"Dec 2016\",\"author\":\"Unitemplates.com\",\"copyright\":\"Copyright (C) 2014 - 2016 Unitemplates. All rights reserved.\",\"authorEmail\":\"info@unitemplates.com\",\"authorUrl\":\"www.unitemplates.com\",\"version\":\"2.4.0\",\"description\":\"MOD_UT_MULTIMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_ut_multimodule\"}', '{\"add_container\":\"1\",\"opacity\":\"1\",\"multimodule-config\":\"1\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_fields`
--

CREATE TABLE `lbazs_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldparams` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `access` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_fields_categories`
--

CREATE TABLE `lbazs_fields_categories` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `category_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_fields_groups`
--

CREATE TABLE `lbazs_fields_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `access` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_fields_values`
--

CREATE TABLE `lbazs_fields_values` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Allow references to items which have strings as ids, eg. none db systems.',
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_filters`
--

CREATE TABLE `lbazs_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` text NOT NULL,
  `params` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links`
--

CREATE TABLE `lbazs_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 1,
  `state` int(5) DEFAULT 1,
  `access` int(5) DEFAULT 0,
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT 0,
  `sale_price` double UNSIGNED NOT NULL DEFAULT 0,
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms0`
--

CREATE TABLE `lbazs_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms1`
--

CREATE TABLE `lbazs_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms2`
--

CREATE TABLE `lbazs_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms3`
--

CREATE TABLE `lbazs_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms4`
--

CREATE TABLE `lbazs_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms5`
--

CREATE TABLE `lbazs_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms6`
--

CREATE TABLE `lbazs_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms7`
--

CREATE TABLE `lbazs_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms8`
--

CREATE TABLE `lbazs_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_terms9`
--

CREATE TABLE `lbazs_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_termsa`
--

CREATE TABLE `lbazs_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_termsb`
--

CREATE TABLE `lbazs_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_termsc`
--

CREATE TABLE `lbazs_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_termsd`
--

CREATE TABLE `lbazs_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_termse`
--

CREATE TABLE `lbazs_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_links_termsf`
--

CREATE TABLE `lbazs_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_taxonomy`
--

CREATE TABLE `lbazs_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lbazs_finder_taxonomy`
--

INSERT INTO `lbazs_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_taxonomy_map`
--

CREATE TABLE `lbazs_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_terms`
--

CREATE TABLE `lbazs_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `weight` float UNSIGNED NOT NULL DEFAULT 0,
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT 0,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_terms_common`
--

CREATE TABLE `lbazs_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lbazs_finder_terms_common`
--

INSERT INTO `lbazs_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('any', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_tokens`
--

CREATE TABLE `lbazs_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `weight` float UNSIGNED NOT NULL DEFAULT 1,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT 2,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_tokens_aggregate`
--

CREATE TABLE `lbazs_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT 2,
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_finder_types`
--

CREATE TABLE `lbazs_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_foxcontact_captcha`
--

CREATE TABLE `lbazs_foxcontact_captcha` (
  `session_id` varchar(200) NOT NULL,
  `form_uid` varchar(16) NOT NULL,
  `date` int(11) NOT NULL,
  `answer` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_foxcontact_enquiries`
--

CREATE TABLE `lbazs_foxcontact_enquiries` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `exported` tinyint(4) NOT NULL DEFAULT 0,
  `ip` varchar(15) NOT NULL,
  `url` text NOT NULL,
  `fields` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_foxcontact_sequences`
--

CREATE TABLE `lbazs_foxcontact_sequences` (
  `series` varchar(32) NOT NULL,
  `value` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_languages`
--

CREATE TABLE `lbazs_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_languages`
--

INSERT INTO `lbazs_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 0, 'en-GB', 'English (en-GB)', 'English (United Kingdom)', 'en', 'en_gb', '', '', '', '', 0, 1, 2),
(2, 60, 'es-ES', 'Spanish (español)', 'Español (España)', 'es', 'es_es', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_menu`
--

CREATE TABLE `lbazs_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to #__users.id',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_menu`
--

INSERT INTO `lbazs_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 69, 0, '*', 0),
(2, 'main', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 1, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'main', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'main', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 1, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'main', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'main', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'main', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 1, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'main', 'com_contact_contacts', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 1, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'main', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 1, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'main', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 1, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 20, 0, '*', 1),
(11, 'main', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 1, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(13, 'main', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 21, 26, 0, '*', 1),
(14, 'main', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 22, 23, 0, '*', 1),
(15, 'main', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 1, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 24, 25, 0, '*', 1),
(16, 'main', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 1, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 27, 28, 0, '*', 1),
(17, 'main', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 1, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 29, 30, 0, '*', 1),
(18, 'main', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 1, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 31, 32, 0, '*', 1),
(19, 'main', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 33, 34, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 1, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 35, 36, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 1, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 37, 38, 0, '*', 1),
(22, 'main', 'com_associations', 'Multilingual Associations', '', 'Multilingual Associations', 'index.php?option=com_associations', 'component', 1, 1, 1, 34, 0, '0000-00-00 00:00:00', 0, 0, 'class:associations', 0, '', 39, 40, 0, '*', 1),
(101, 'mainmenu', 'Inicio', 'home', '', 'home', 'index.php?option=com_content&view=article&id=1', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"1\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 41, 42, 1, '*', 0),
(104, 'main', 'COM_WIDGETKIT', 'com-widgetkit', '', 'com-widgetkit', 'index.php?option=com_widgetkit', 'component', 1, 1, 1, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'class:widgetkit', 0, '{}', 43, 44, 0, '', 1),
(105, 'main', 'COM_FOXCONTACT_MENU', 'com-foxcontact-menu', '', 'com-foxcontact-menu', 'index.php?option=com_foxcontact', 'component', 1, 1, 1, 10014, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 45, 46, 0, '', 1),
(106, 'main', 'COM_UNITEREVOLUTION2', 'com-uniterevolution2', '', 'com-uniterevolution2', 'index.php?option=com_uniterevolution2', 'component', 1, 1, 1, 10018, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_uniterevolution2/images/icon-16-revolution.png', 0, '{}', 47, 48, 0, '', 1),
(107, 'main', 'JCE', 'jce', '', 'jce', 'index.php?option=com_jce&view=cpanel', 'component', 1, 1, 1, 10020, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jce/media/img/menu/logo.png', 0, '{}', 49, 58, 0, '', 1),
(108, 'main', 'WF_MENU_CPANEL', 'wf-menu-cpanel', '', 'jce/wf-menu-cpanel', 'index.php?option=com_jce', 'component', 1, 107, 2, 10020, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jce/media/img/menu/jce-cpanel.png', 0, '{}', 50, 51, 0, '', 1),
(109, 'main', 'WF_MENU_CONFIG', 'wf-menu-config', '', 'jce/wf-menu-config', 'index.php?option=com_jce&view=config', 'component', 1, 107, 2, 10020, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jce/media/img/menu/jce-config.png', 0, '{}', 52, 53, 0, '', 1),
(110, 'main', 'WF_MENU_PROFILES', 'wf-menu-profiles', '', 'jce/wf-menu-profiles', 'index.php?option=com_jce&view=profiles', 'component', 1, 107, 2, 10020, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jce/media/img/menu/jce-profiles.png', 0, '{}', 54, 55, 0, '', 1),
(111, 'main', 'WF_MENU_INSTALL', 'wf-menu-install', '', 'jce/wf-menu-install', 'index.php?option=com_jce&view=installer', 'component', 1, 107, 2, 10020, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jce/media/img/menu/jce-install.png', 0, '{}', 56, 57, 0, '', 1),
(112, 'main', 'COM_SPSIMPLEPORTFOLIO', 'com-spsimpleportfolio', '', 'com-spsimpleportfolio', 'index.php?option=com_spsimpleportfolio&view=cpanel', 'component', 1, 1, 1, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 59, 60, 0, '', 1),
(113, 'mainmenu', 'Contenidos', 'contenidos', '', 'contenidos', 'index.php?option=com_content&view=article&id=2', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"0\",\"link_titles\":\"0\",\"show_intro\":\"0\",\"info_block_position\":\"\",\"info_block_show_title\":\"0\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_associations\":\"0\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 61, 62, 0, '*', 0),
(114, 'mainmenu', 'Contáctenos', 'contactenos', '', 'contactenos', 'index.php?option=com_content&view=article&id=3', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 63, 64, 0, '*', 0),
(115, 'mainmenu', 'Encuesta', 'encuesta', '', 'encuesta', 'index.php?option=com_content&view=article&id=4', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 65, 68, 0, '*', 0),
(116, 'mainmenu', 'Encuesta', 'encuesta', '', 'encuesta/encuesta', 'index.php?option=com_foxcontact&view=foxcontact&Itemid=116', 'component', 1, 115, 2, 10014, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"design\":\"{\\\"option\\\":{\\\"form\\\":{\\\"render\\\":\\\"stacked\\\",\\\"width\\\":{\\\"value\\\":100,\\\"unit\\\":\\\"auto\\\"},\\\"style\\\":\\\"bootstrap.css\\\"},\\\"label\\\":{\\\"position\\\":\\\"beside\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"px\\\"}},\\\"control\\\":{\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"px\\\"}}},\\\"rows\\\":[{\\\"columns\\\":[{\\\"size\\\":12,\\\"items\\\":[{\\\"type\\\":\\\"html\\\",\\\"html\\\":\\\"<h2>Responde la siguiente encuesta<\\/h2>\\\\n<p><\\/p>\\\",\\\"alignment\\\":\\\"labels\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"unique_id\\\":\\\"Html1\\\"},{\\\"type\\\":\\\"board\\\",\\\"alignment\\\":\\\"labels\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"unique_id\\\":\\\"Board\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"Cuantos a\\u00f1os tiene?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"10  a 20 a\\u00f1os\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"20 a 30 a\\u00f1os\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"30 a 40 a\\u00f1os\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown1\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"Nivel de escolaridad?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"T\\u00e9cnico\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Tecn\\u00f3logo\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Profesional\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown6\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"De donde es?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"Medell\\u00edn\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Cali\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Cartagena\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown5\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"Cual es el lenguaje de programaci\\u00f3n que mas le gusta?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"Php\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Javascript\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\".Net\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown4\\\"},{\\\"type\\\":\\\"submit\\\",\\\"render\\\":\\\"button\\\",\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"submit\\\":{\\\"label\\\":\\\"Enviar\\\",\\\"icon\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"tooltip\\\":\\\"\\\"},\\\"reset\\\":{\\\"enable\\\":false,\\\"label\\\":\\\"Reset\\\",\\\"icon\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"tooltip\\\":\\\"\\\"},\\\"alignment\\\":\\\"fields\\\",\\\"unique_id\\\":\\\"Submit\\\"},{\\\"type\\\":\\\"user_info\\\",\\\"info\\\":{\\\"device\\\":true,\\\"os\\\":true,\\\"browser\\\":true,\\\"ip\\\":false},\\\"unique_id\\\":\\\"UserInfo\\\"}]}],\\\"width\\\":{\\\"value\\\":100,\\\"unit\\\":\\\"inherited\\\"}}],\\\"version\\\":\\\"3.8.2\\\"}\",\"to_address\":\"carleon1792@gmail.com\",\"cc_address\":\"\",\"bcc_address\":\"\",\"email_subject\":\"Encuesta de la p\\u00e1gina web\",\"email_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t{field-table-full}\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"user_notification\":\"1\",\"email_copy_subject\":\"Thank you for contacting us\",\"email_copy_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t<p>This message is to confirm that your enquiry has been successfully submitted and will be processed as soon as possible.<\\/p>\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"jmessenger_user\":\"66\",\"jmessenger_subject\":\"Enquiry received through {sitename} by {name}\",\"jmessenger_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t{field-table-full}\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"show_onscreen_message\":\"1\",\"do_onscreen_redirect\":\"0\",\"onscreen_redirect_item\":\"101\",\"onscreen_message_content\":\"<p>Your message has been received. We will get in touch with you within a few days.<\\/p>\",\"delivery_db\":\"1\",\"spam_check\":\"1\",\"spam_words\":\"www,http,url=,href=,link=,.txt,shit,fuck\",\"spam_detected_text\":\"We are sorry, but your message contains unwanted advertising and <b>has been blocked<\\/b>.<br \\/>Please try again avoiding words such as <i>www, http,<\\/i> and similar.\",\"spam_detected_textdisplay\":\"1\",\"spam_log\":\"1\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 66, 67, 0, '*', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_menu_types`
--

CREATE TABLE `lbazs_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_menu_types`
--

INSERT INTO `lbazs_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`, `client_id`) VALUES
(1, 0, 'mainmenu', 'Main Menu', 'The main menu for the site', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_messages`
--

CREATE TABLE `lbazs_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_messages_cfg`
--

CREATE TABLE `lbazs_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_modules`
--

CREATE TABLE `lbazs_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_modules`
--

INSERT INTO `lbazs_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu', '', '', 1, 'home-1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"mainmenu\",\"base\":\"\",\"startLevel\":1,\"endLevel\":0,\"showAllChildren\":1,\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"_menu\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_login', 1, 1, '{\"greeting\":\"1\",\"name\":\"0\"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_breadcrumbs', 1, 1, '{\"moduleclass_sfx\":\"\",\"showHome\":\"1\",\"homeText\":\"\",\"showComponent\":\"1\",\"separator\":\"\",\"cache\":\"0\",\"cache_time\":\"0\",\"cachemode\":\"itemid\"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(87, 55, 'Sample Data', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_sampledata', 6, 1, '{}', 1, '*'),
(88, 58, 'Latest Actions', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latestactions', 6, 1, '{}', 1, '*'),
(89, 59, 'Privacy Dashboard', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_privacy_dashboard', 6, 1, '{}', 1, '*'),
(90, 61, 'Widgetkit', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_widgetkit', 1, 1, '', 0, '*'),
(91, 62, 'Widgetkit Twitter', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_widgetkit_twitter', 1, 1, '', 0, '*'),
(93, 66, 'Unite Revolution Slider 2', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_unite_revolution2', 1, 1, '', 0, '*'),
(94, 70, 'ITPShare', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_itpshare', 1, 1, '', 0, '*'),
(95, 71, 'S5 MailChimp Signup', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_s5_mailchimp_signup', 1, 1, '', 0, '*'),
(96, 72, 'SP Simple Portfolio Module', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_spsimpleportfolio', 1, 1, '', 0, '*'),
(97, 73, 'UT Contact Pro', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_ut_contact_pro', 1, 1, '', 0, '*'),
(98, 74, 'UT Multi Module', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_ut_multimodule', 1, 1, '', 0, '*'),
(99, 82, 'Contacto', '', NULL, 1, 'contacto', 66, '2020-03-25 02:51:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_foxcontact', 1, 1, '{\"design\":\"{\\\"option\\\":{\\\"form\\\":{\\\"render\\\":\\\"stacked\\\",\\\"width\\\":{\\\"value\\\":100,\\\"unit\\\":\\\"auto\\\"},\\\"style\\\":\\\"bootstrap.css\\\"},\\\"label\\\":{\\\"position\\\":\\\"beside\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"px\\\"}},\\\"control\\\":{\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"px\\\"}}},\\\"rows\\\":[{\\\"columns\\\":[{\\\"size\\\":12,\\\"items\\\":[{\\\"type\\\":\\\"html\\\",\\\"html\\\":\\\"<h2>Contacto<\\/h2>\\\",\\\"alignment\\\":\\\"labels\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"unique_id\\\":\\\"Html1\\\"},{\\\"type\\\":\\\"board\\\",\\\"alignment\\\":\\\"labels\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"unique_id\\\":\\\"Board\\\"},{\\\"type\\\":\\\"name\\\",\\\"label\\\":\\\"Nombre\\\",\\\"tooltip\\\":\\\"\\\",\\\"required\\\":true,\\\"autofill\\\":true,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":400,\\\"unit\\\":\\\"px\\\"},\\\"unique_id\\\":\\\"Name\\\",\\\"placeholder\\\":\\\"\\\"},{\\\"type\\\":\\\"email\\\",\\\"label\\\":\\\"Email\\\",\\\"tooltip\\\":\\\"Insert your email address.\\\",\\\"required\\\":true,\\\"autofill\\\":true,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":400,\\\"unit\\\":\\\"px\\\"},\\\"unique_id\\\":\\\"Email\\\",\\\"placeholder\\\":\\\"\\\"},{\\\"type\\\":\\\"text_field\\\",\\\"label\\\":\\\"Celular\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"def_val\\\":\\\"\\\",\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":400,\\\"unit\\\":\\\"px\\\"},\\\"unique_id\\\":\\\"TextField1\\\"},{\\\"type\\\":\\\"text_area\\\",\\\"label\\\":\\\"Describe tu mensaje\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"elastic\\\":true,\\\"max_length\\\":0,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":400,\\\"unit\\\":\\\"px\\\"},\\\"height\\\":{\\\"value\\\":180,\\\"unit\\\":\\\"auto\\\"},\\\"unique_id\\\":\\\"TextArea1\\\"},{\\\"type\\\":\\\"submit\\\",\\\"render\\\":\\\"button\\\",\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"submit\\\":{\\\"label\\\":\\\"Enviar\\\",\\\"icon\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"tooltip\\\":\\\"\\\"},\\\"reset\\\":{\\\"enable\\\":false,\\\"label\\\":\\\"Reset\\\",\\\"icon\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"tooltip\\\":\\\"\\\"},\\\"alignment\\\":\\\"fields\\\",\\\"unique_id\\\":\\\"Submit\\\"},{\\\"type\\\":\\\"user_info\\\",\\\"info\\\":{\\\"device\\\":true,\\\"os\\\":true,\\\"browser\\\":true,\\\"ip\\\":false},\\\"unique_id\\\":\\\"UserInfo\\\"}]}],\\\"width\\\":{\\\"value\\\":100,\\\"unit\\\":\\\"inherited\\\"}}],\\\"version\\\":\\\"3.8.2\\\"}\",\"to_address\":\"carleon1792@gmail.com\",\"cc_address\":\"\",\"bcc_address\":\"\",\"email_subject\":\"Email desde p\\u00e1gina web\",\"email_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t{field-table-full}\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"user_notification\":\"1\",\"email_copy_subject\":\"Thank you for contacting us\",\"email_copy_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t<p>This message is to confirm that your enquiry has been successfully submitted and will be processed as soon as possible.<\\/p>\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"jmessenger_user\":\"66\",\"jmessenger_subject\":\"Enquiry received through {sitename} by {name}\",\"jmessenger_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t{field-table-full}\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"show_onscreen_message\":\"1\",\"do_onscreen_redirect\":\"0\",\"onscreen_redirect_item\":\"101\",\"onscreen_message_content\":\"<p>Your message has been received. We will get in touch with you within a few days.<\\/p>\",\"delivery_db\":\"1\",\"spam_check\":\"1\",\"spam_words\":\"www,http,url=,href=,link=,.txt,shit,fuck\",\"spam_detected_text\":\"We are sorry, but your message contains unwanted advertising and <b>has been blocked<\\/b>.<br \\/>Please try again avoiding words such as <i>www, http,<\\/i> and similar.\",\"spam_detected_textdisplay\":\"1\",\"spam_log\":\"1\",\"moduleclass_sfx\":\"\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(100, 86, 'Noticias desarrollo', '', NULL, 1, 'desarrollo', 66, '2020-03-25 02:49:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_widgetkit', 1, 1, '{\"widgetkit\":\"{\\\"id\\\":\\\"1\\\",\\\"name\\\":\\\"Noticias Desarrollo\\\"}\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(101, 87, 'Noticias Comercial', '', NULL, 1, 'comercial', 66, '2020-03-25 02:49:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_widgetkit', 1, 1, '{\"widgetkit\":\"{\\\"id\\\":\\\"2\\\",\\\"name\\\":\\\"Noticias Comerciales\\\"}\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(102, 88, 'Noticias comunicaciones', '', NULL, 1, 'comunicaciones', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_widgetkit', 1, 1, '{\"widgetkit\":\"{\\\"id\\\":\\\"3\\\",\\\"name\\\":\\\"Noticias Comunicaciones\\\"}\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(103, 89, 'Encuesta', '', NULL, 1, 'encuesta', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_foxcontact', 1, 1, '{\"design\":\"{\\\"option\\\":{\\\"form\\\":{\\\"render\\\":\\\"stacked\\\",\\\"width\\\":{\\\"value\\\":100,\\\"unit\\\":\\\"auto\\\"},\\\"style\\\":\\\"bootstrap.css\\\"},\\\"label\\\":{\\\"position\\\":\\\"beside\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"px\\\"}},\\\"control\\\":{\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"px\\\"}}},\\\"rows\\\":[{\\\"columns\\\":[{\\\"size\\\":12,\\\"items\\\":[{\\\"type\\\":\\\"html\\\",\\\"html\\\":\\\"<h2>Responde la siguiente encuesta<\\/h2>\\\\n<p><\\/p>\\\",\\\"alignment\\\":\\\"labels\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"unique_id\\\":\\\"Html1\\\"},{\\\"type\\\":\\\"board\\\",\\\"alignment\\\":\\\"labels\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"unique_id\\\":\\\"Board\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"Que edad tiene?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"10 a 20 a\\u00f1os\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"20 a 30 a\\u00f1os\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"30 a 40 a\\u00f1os\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown1\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"En que ciudad se encuentra actualmente?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"Popay\\u00e1n\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Cali\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Medell\\u00edn\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown4\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"Cual es el lenguaje de programaci\\u00f3n que mas le gusta?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"Php\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Javascript\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\".Net\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown3\\\"},{\\\"type\\\":\\\"dropdown\\\",\\\"label\\\":\\\"Nivel de estudios?\\\",\\\"tooltip\\\":\\\"\\\",\\\"placeholder\\\":\\\"\\\",\\\"required\\\":false,\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"options\\\":[{\\\"text\\\":\\\"Bachiller\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"T\\u00e9cnico\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Tecn\\u00f3logo\\\",\\\"to\\\":\\\"\\\"},{\\\"text\\\":\\\"Profesional\\\",\\\"to\\\":\\\"\\\"}],\\\"unique_id\\\":\\\"Dropdown2\\\"},{\\\"type\\\":\\\"submit\\\",\\\"render\\\":\\\"button\\\",\\\"classes\\\":\\\"\\\",\\\"width\\\":{\\\"value\\\":230,\\\"unit\\\":\\\"inherited\\\"},\\\"submit\\\":{\\\"label\\\":\\\"Enviar\\\",\\\"icon\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"tooltip\\\":\\\"\\\"},\\\"reset\\\":{\\\"enable\\\":false,\\\"label\\\":\\\"Reset\\\",\\\"icon\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"tooltip\\\":\\\"\\\"},\\\"alignment\\\":\\\"fields\\\",\\\"unique_id\\\":\\\"Submit\\\"},{\\\"type\\\":\\\"user_info\\\",\\\"info\\\":{\\\"device\\\":true,\\\"os\\\":true,\\\"browser\\\":true,\\\"ip\\\":false},\\\"unique_id\\\":\\\"UserInfo\\\"}]}],\\\"width\\\":{\\\"value\\\":100,\\\"unit\\\":\\\"inherited\\\"}}],\\\"version\\\":\\\"3.8.2\\\"}\",\"to_address\":\"carleon1792@gmail.com\",\"cc_address\":\"\",\"bcc_address\":\"\",\"email_subject\":\"Enquiry received through {sitename} by {name}\",\"email_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t{field-table-full}\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"user_notification\":\"1\",\"email_copy_subject\":\"Thank you for contacting us\",\"email_copy_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t<p>This message is to confirm that your enquiry has been successfully submitted and will be processed as soon as possible.<\\/p>\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"jmessenger_user\":\"66\",\"jmessenger_subject\":\"Enquiry received through {sitename} by {name}\",\"jmessenger_body\":\"<div class=\\\"wrapper\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"inner\\\">\\r\\n\\t\\t\\t<div class=\\\"header sitename\\\">{sitename}<\\/div>\\r\\n\\t\\t\\t{field-table-full}\\r\\n\\t\\t\\t<div class=\\\"footer\\\">\\u00a9 {sitename}<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"show_onscreen_message\":\"1\",\"do_onscreen_redirect\":\"0\",\"onscreen_redirect_item\":\"101\",\"onscreen_message_content\":\"<p>Your message has been received. We will get in touch with you within a few days.<\\/p>\",\"delivery_db\":\"1\",\"spam_check\":\"1\",\"spam_words\":\"www,http,url=,href=,link=,.txt,shit,fuck\",\"spam_detected_text\":\"We are sorry, but your message contains unwanted advertising and <b>has been blocked<\\/b>.<br \\/>Please try again avoiding words such as <i>www, http,<\\/i> and similar.\",\"spam_detected_textdisplay\":\"1\",\"spam_log\":\"1\",\"moduleclass_sfx\":\"\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_modules_menu`
--

CREATE TABLE `lbazs_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT 0,
  `menuid` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_modules_menu`
--

INSERT INTO `lbazs_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(99, 101),
(99, 114),
(100, 0),
(101, 0),
(102, 0),
(103, 101),
(103, 115),
(103, 116);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_newsfeeds`
--

CREATE TABLE `lbazs_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT 0,
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT 3600,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `rtl` tinyint(4) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_overrider`
--

CREATE TABLE `lbazs_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_postinstall_messages`
--

CREATE TABLE `lbazs_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT 700 COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT 1,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_postinstall_messages`
--

INSERT INTO `lbazs_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1),
(3, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 1),
(4, 700, 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION', 'plg_system_updatenotification', 1, 'action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_condition', '3.6.3', 1),
(5, 700, 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_TITLE', 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/joomla40checks.php', 'admin_postinstall_joomla40checks_condition', '3.7.0', 1),
(6, 700, 'TPL_HATHOR_MESSAGE_POSTINSTALL_TITLE', 'TPL_HATHOR_MESSAGE_POSTINSTALL_BODY', 'TPL_HATHOR_MESSAGE_POSTINSTALL_ACTION', 'tpl_hathor', 1, 'action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_condition', '3.7.0', 1),
(7, 700, 'PLG_PLG_RECAPTCHA_VERSION_1_POSTINSTALL_TITLE', 'PLG_PLG_RECAPTCHA_VERSION_1_POSTINSTALL_BODY', 'PLG_PLG_RECAPTCHA_VERSION_1_POSTINSTALL_ACTION', 'plg_captcha_recaptcha', 1, 'action', 'site://plugins/captcha/recaptcha/postinstall/actions.php', 'recaptcha_postinstall_action', 'site://plugins/captcha/recaptcha/postinstall/actions.php', 'recaptcha_postinstall_condition', '3.8.6', 1),
(8, 700, 'COM_ACTIONLOGS_POSTINSTALL_TITLE', 'COM_ACTIONLOGS_POSTINSTALL_BODY', '', 'com_actionlogs', 1, 'message', '', '', '', '', '3.9.0', 1),
(9, 700, 'COM_PRIVACY_POSTINSTALL_TITLE', 'COM_PRIVACY_POSTINSTALL_BODY', '', 'com_privacy', 1, 'message', '', '', '', '', '3.9.0', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_privacy_consents`
--

CREATE TABLE `lbazs_privacy_consents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `state` int(10) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remind` tinyint(4) NOT NULL DEFAULT 0,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_privacy_requests`
--

CREATE TABLE `lbazs_privacy_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `requested_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `request_type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `confirm_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `confirm_token_created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_redirect_links`
--

CREATE TABLE `lbazs_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT 301
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_revslider_css`
--

CREATE TABLE `lbazs_revslider_css` (
  `id` int(10) UNSIGNED NOT NULL,
  `handle` text NOT NULL,
  `settings` text DEFAULT NULL,
  `hover` text DEFAULT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lbazs_revslider_css`
--

INSERT INTO `lbazs_revslider_css` (`id`, `handle`, `settings`, `hover`, `params`) VALUES
(1, '.tp-caption.medium_grey', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-family\":\"Arial\",\"padding\":\"2px 4px\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"background-color\":\"#888\",\"white-space\":\"nowrap\"}'),
(2, '.tp-caption.small_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"14px\",\"line-height\":\"20px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}'),
(3, '.tp-caption.medium_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}'),
(4, '.tp-caption.large_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"40px\",\"line-height\":\"40px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}'),
(5, '.tp-caption.very_large_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\",\"letter-spacing\":\"-2px\"}'),
(6, '.tp-caption.very_big_white', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\",\"padding\":\"0px 4px\",\"padding-top\":\"1px\",\"background-color\":\"#000\"}'),
(7, '.tp-caption.very_big_black', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"700\",\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\",\"padding\":\"0px 4px\",\"padding-top\":\"1px\",\"background-color\":\"#fff\"}'),
(8, '.tp-caption.modern_medium_fat', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"24px\",\"line-height\":\"20px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}'),
(9, '.tp-caption.modern_medium_fat_white', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"24px\",\"line-height\":\"20px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}'),
(10, '.tp-caption.modern_medium_light', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"24px\",\"line-height\":\"20px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}'),
(11, '.tp-caption.modern_big_bluebg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"padding\":\"3px 10px\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"background-color\":\"#4e5b6c\",\"letter-spacing\":\"0\"}'),
(12, '.tp-caption.modern_big_redbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"padding\":\"3px 10px\",\"padding-top\":\"1px\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"background-color\":\"#de543e\",\"letter-spacing\":\"0\"}'),
(13, '.tp-caption.modern_small_text_dark', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#555\",\"text-shadow\":\"none\",\"font-size\":\"14px\",\"line-height\":\"22px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}'),
(14, '.tp-caption.thinheadline_dark', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"rgba(0,0,0,0.85)\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"30px\",\"line-height\":\"30px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"transparent\"}'),
(15, '.tp-caption.thintext_dark', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"rgba(0,0,0,0.85)\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"16px\",\"line-height\":\"26px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"transparent\"}'),
(16, '.tp-caption.largeblackbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#000\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}'),
(17, '.tp-caption.largepinkbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#db4360\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}'),
(18, '.tp-caption.largewhitebg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#fff\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}'),
(19, '.tp-caption.largegreenbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#67ae73\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}'),
(20, '.tp-caption.excerpt', NULL, NULL, '{\"font-size\":\"36px\",\"line-height\":\"36px\",\"font-weight\":\"700\",\"font-family\":\"Arial\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"rgba(0, 0, 0, 1)\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"letter-spacing\":\"-1.5px\",\"padding\":\"1px 4px 0px 4px\",\"white-space\":\"normal !important\",\"height\":\"auto\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 255, 255)\",\"border-style\":\"none\"}'),
(21, '.tp-caption.large_bold_grey', NULL, NULL, '{\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(102, 102, 102)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"padding\":\"1px 4px 0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(22, '.tp-caption.medium_thin_grey', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"30px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(102, 102, 102)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"1px 4px 0px\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(23, '.tp-caption.small_thin_grey', NULL, NULL, '{\"font-size\":\"18px\",\"line-height\":\"26px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(117, 117, 117)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"1px 4px 0px\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(24, '.tp-caption.lightgrey_divider', NULL, NULL, '{\"text-decoration\":\"none\",\"background-color\":\"rgba(235, 235, 235, 1)\",\"width\":\"370px\",\"height\":\"3px\",\"background-position\":\"initial initial\",\"background-repeat\":\"initial initial\",\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}'),
(25, '.tp-caption.large_bold_darkblue', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(52, 73, 94)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(26, '.tp-caption.medium_bg_darkblue', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(52, 73, 94)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(27, '.tp-caption.medium_bold_red', NULL, NULL, '{\"font-size\":\"24px\",\"line-height\":\"30px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(227, 58, 12)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(28, '.tp-caption.medium_light_red', NULL, NULL, '{\"font-size\":\"21px\",\"line-height\":\"26px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(227, 58, 12)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(29, '.tp-caption.medium_bg_red', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(227, 58, 12)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(30, '.tp-caption.medium_bold_orange', NULL, NULL, '{\"font-size\":\"24px\",\"line-height\":\"30px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(243, 156, 18)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(31, '.tp-caption.medium_bg_orange', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(243, 156, 18)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(32, '.tp-caption.large_bold_white', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(33, '.tp-caption.medium_light_white', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(34, '.tp-caption.mediumlarge_light_white', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(35, '.tp-caption.mediumlarge_light_white_center', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px 0px 0px 0px\",\"text-align\":\"center\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(36, '.tp-caption.medium_bg_asbestos', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(127, 140, 141)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(37, '.tp-caption.medium_light_black', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(38, '.tp-caption.large_bold_black', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(39, '.tp-caption.mediumlarge_light_darkblue', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(52, 73, 94)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(40, '.tp-caption.small_light_white', NULL, NULL, '{\"font-size\":\"17px\",\"line-height\":\"28px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(41, '.tp-caption.roundedimage', NULL, NULL, '{\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}'),
(42, '.tp-caption.large_bg_black', NULL, NULL, '{\"font-size\":\"40px\",\"line-height\":\"40px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(0, 0, 0)\",\"padding\":\"10px 20px 15px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(43, '.tp-caption.mediumwhitebg', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"30px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(255, 255, 255)\",\"padding\":\"5px 15px 10px\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_revslider_layer_animations`
--

CREATE TABLE `lbazs_revslider_layer_animations` (
  `id` int(10) UNSIGNED NOT NULL,
  `handle` text NOT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_revslider_settings`
--

CREATE TABLE `lbazs_revslider_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `general` text NOT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_revslider_sliders`
--

CREATE TABLE `lbazs_revslider_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_revslider_slides`
--

CREATE TABLE `lbazs_revslider_slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `slide_order` int(11) NOT NULL,
  `params` text NOT NULL,
  `layers` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_revslider_static_slides`
--

CREATE TABLE `lbazs_revslider_static_slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_id` int(9) NOT NULL,
  `params` text NOT NULL,
  `layers` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_schemas`
--

CREATE TABLE `lbazs_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_schemas`
--

INSERT INTO `lbazs_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.9.16-2020-03-04'),
(10014, '3.6.6'),
(10025, '1.2-2015-02-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_session`
--

CREATE TABLE `lbazs_session` (
  `session_id` varbinary(192) NOT NULL,
  `client_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `guest` tinyint(3) UNSIGNED DEFAULT 1,
  `time` int(11) NOT NULL DEFAULT 0,
  `data` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT 0,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_session`
--

INSERT INTO `lbazs_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
(0x34626774316e726e6b697362743630686f6d63746b316d677676, 0, 1, 1585138914, 'joomla|s:736:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjM6e3M6NzoiY291bnRlciI7aTozO3M6NToidG9rZW4iO3M6MzI6ImNvbXl6eTBBVkMzS2xIN0FjVkRyWlBtSGE2NnJ0ZklWIjtzOjU6InRpbWVyIjtPOjg6InN0ZENsYXNzIjozOntzOjU6InN0YXJ0IjtpOjE1ODUxMzg5MTI7czo0OiJsYXN0IjtpOjE1ODUxMzg5MTI7czozOiJub3ciO2k6MTU4NTEzODkxNDt9fXM6ODoicmVnaXN0cnkiO086MjQ6Ikpvb21sYVxSZWdpc3RyeVxSZWdpc3RyeSI6Mzp7czo3OiIAKgBkYXRhIjtPOjg6InN0ZENsYXNzIjowOnt9czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MDtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9czo0OiJ1c2VyIjtPOjIwOiJKb29tbGFcQ01TXFVzZXJcVXNlciI6MTp7czoyOiJpZCI7aTowO319fXM6MTQ6IgAqAGluaXRpYWxpemVkIjtiOjA7czo5OiJzZXBhcmF0b3IiO3M6MToiLiI7fQ==\";', 0, ''),
(0x74716b366d38303062303235646c3039393567727238666b6c68, 1, 1, 1585129086, 'joomla|s:596:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjI6e3M6NzoiY291bnRlciI7aToxO3M6NToidG9rZW4iO3M6MzI6Inc4bUhNZGxGNTZKaFo5Y2dTcEJXaXVWaHVRc3hndEs3Ijt9czo4OiJyZWdpc3RyeSI7TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjA6e31zOjE0OiIAKgBpbml0aWFsaXplZCI7YjowO3M6OToic2VwYXJhdG9yIjtzOjE6Ii4iO31zOjQ6InVzZXIiO086MjA6Ikpvb21sYVxDTVNcVXNlclxVc2VyIjoxOntzOjI6ImlkIjtpOjA7fX19czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MDtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9\";', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_spsimpleportfolio_items`
--

CREATE TABLE `lbazs_spsimpleportfolio_items` (
  `spsimpleportfolio_item_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(55) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `video` text NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `spsimpleportfolio_tag_id` text NOT NULL,
  `url` text NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT 1,
  `language` varchar(255) NOT NULL DEFAULT '*',
  `access` int(5) NOT NULL DEFAULT 1,
  `ordering` int(10) NOT NULL DEFAULT 0,
  `created_by` bigint(20) NOT NULL DEFAULT 0,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT 0,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT 0,
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_spsimpleportfolio_tags`
--

CREATE TABLE `lbazs_spsimpleportfolio_tags` (
  `spsimpleportfolio_tag_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(55) NOT NULL,
  `language` varchar(255) NOT NULL DEFAULT '*'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_tags`
--

CREATE TABLE `lbazs_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lft` int(11) NOT NULL DEFAULT 0,
  `rgt` int(11) NOT NULL DEFAULT 0,
  `level` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_tags`
--

INSERT INTO `lbazs_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 66, '2020-03-24 23:42:44', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_template_styles`
--

CREATE TABLE `lbazs_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_template_styles`
--

INSERT INTO `lbazs_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"logo\":\"images\\/joomla_black.png\",\"sitetitle\":\"Joomla!\",\"sitedescription\":\"Open Source Content Management\",\"navposition\":\"left\",\"templatecolor\":\"personal\",\"html5\":\"0\"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{\"showSiteName\":\"0\",\"colourChoice\":\"\",\"boldText\":\"0\"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}'),
(8, 'isis', 1, '1', 'isis - Default', '{\"templateColor\":\"\",\"logoFile\":\"\"}'),
(9, 'ut_ufolio', 0, '1', 'ut_ufolio - Default', '{\"sticky_nav\":\"0\"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_ucm_base`
--

CREATE TABLE `lbazs_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_ucm_content`
--

CREATE TABLE `lbazs_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT 0,
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT 0,
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_content_item_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core_urls` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `core_ordering` int(11) NOT NULL DEFAULT 0,
  `core_metakey` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core_metadesc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_ucm_history`
--

CREATE TABLE `lbazs_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_ucm_history`
--

INSERT INTO `lbazs_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 1, 1, '', '2020-03-25 01:35:15', 66, 1747, '040f14da504d6d02d880e4e0518f257359308e49', '{\"id\":1,\"asset_id\":75,\"title\":\"Inicio\",\"alias\":\"inicio\",\"introtext\":\"\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:15\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 01:35:15\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2020-03-25 01:35:15\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(2, 2, 1, '', '2020-03-25 01:35:30', 66, 1755, '81db2c370ede65f39d450da1e6ded9d65a0d67ff', '{\"id\":2,\"asset_id\":76,\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 01:35:30\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(3, 3, 1, '', '2020-03-25 01:35:46', 66, 1762, '036fd96fe796152b0a66265b65bed1d3fa96ffb0', '{\"id\":3,\"asset_id\":77,\"title\":\"Cont\\u00e1ctenos\",\"alias\":\"contactenos\",\"introtext\":\"\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:46\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 01:35:46\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2020-03-25 01:35:46\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(4, 4, 1, '', '2020-03-25 01:35:58', 66, 1753, '0adf0a1949c6453bb08cf50ec1942061bbef4b9a', '{\"id\":4,\"asset_id\":78,\"title\":\"Encuestas\",\"alias\":\"encuestas\",\"introtext\":\"\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:58\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 01:35:58\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2020-03-25 01:35:58\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(5, 8, 5, '', '2020-03-25 01:36:56', 66, 565, '174c139fbc03ccf07b8de6b69f22d1156ddb161f', '{\"id\":8,\"asset_id\":79,\"parent_id\":\"1\",\"lft\":\"11\",\"rgt\":12,\"level\":1,\"path\":null,\"extension\":\"com_content\",\"title\":\"Comunicaciones\",\"alias\":\"comunicaciones\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"66\",\"created_time\":\"2020-03-25 01:36:56\",\"modified_user_id\":null,\"modified_time\":\"2020-03-25 01:36:56\",\"hits\":null,\"language\":\"*\",\"version\":null}', 0),
(6, 9, 5, '', '2020-03-25 01:37:10', 66, 577, 'bb0dea6763fe2b9326c5db11020e76e09f7bae72', '{\"id\":9,\"asset_id\":80,\"parent_id\":\"1\",\"lft\":\"13\",\"rgt\":14,\"level\":1,\"path\":null,\"extension\":\"com_content\",\"title\":\"Equipo de desarrollo\",\"alias\":\"equipo-de-desarrollo\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"66\",\"created_time\":\"2020-03-25 01:37:10\",\"modified_user_id\":null,\"modified_time\":\"2020-03-25 01:37:10\",\"hits\":null,\"language\":\"*\",\"version\":null}', 0),
(7, 10, 5, '', '2020-03-25 01:37:22', 66, 570, '545d3002af77bdf000126271696385f9e95c6fce', '{\"id\":10,\"asset_id\":81,\"parent_id\":\"1\",\"lft\":\"15\",\"rgt\":16,\"level\":1,\"path\":null,\"extension\":\"com_content\",\"title\":\"Equipo comercial\",\"alias\":\"equipo-comercial\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"66\",\"created_time\":\"2020-03-25 01:37:22\",\"modified_user_id\":null,\"modified_time\":\"2020-03-25 01:37:22\",\"hits\":null,\"language\":\"*\",\"version\":null}', 0),
(8, 3, 1, '', '2020-03-25 01:51:04', 66, 2084, '722e2f0b6e7a2cc4cfcc3e43afc8ce1547c21104', '{\"id\":3,\"asset_id\":\"77\",\"title\":\"Cont\\u00e1ctenos\",\"alias\":\"contactenos\",\"introtext\":\"<p>{loadmoduleid 97}<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:46\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 01:51:04\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 01:50:40\",\"publish_up\":\"2020-03-25 01:35:46\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"10\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(9, 3, 1, '', '2020-03-25 01:52:33', 66, 2090, '9f76c28e18aac3880792a65f59b22cc66afac921', '{\"id\":3,\"asset_id\":\"77\",\"title\":\"Cont\\u00e1ctenos\",\"alias\":\"contactenos\",\"introtext\":\"<p>{loadposition contacto}<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:46\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 01:52:33\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 01:51:04\",\"publish_up\":\"2020-03-25 01:35:46\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"12\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(10, 5, 1, '', '2020-03-25 02:01:34', 66, 2823, '868f7a279cf8d75b7f8deb1decd45501a9ca5753', '{\"id\":5,\"asset_id\":83,\"title\":\"Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores\",\"alias\":\"google-presenta-su-version-preliminar-android-11-para-desarrolladores\",\"introtext\":\"<h2 style=\\\"text-align: center;\\\">Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores<\\/h2>\\r\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\"><span style=\\\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\\\">\\u00a0<img src=\\\"images\\/android.jpeg\\\" alt=\\\"\\\" \\/><\\/span><\\/p>\\r\\n<p style=\\\"text-align: center;\\\">La pr\\u00f3xima versi\\u00f3n del sistema contempla terminales 5G, pantallas curvas y m\\u00e1s privacidad.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<h1 class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\">Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores<\\/h1>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 02:01:34\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:01:34\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2020-03-25 02:01:34\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(11, 5, 1, '', '2020-03-25 02:03:19', 66, 3061, '0ee6337ee6deeb2b0fa478e1166e92129aea625a', '{\"id\":5,\"asset_id\":\"83\",\"title\":\"Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores\",\"alias\":\"google-presenta-su-version-preliminar-android-11-para-desarrolladores\",\"introtext\":\"<h2 style=\\\"text-align: center;\\\">Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores<\\/h2>\\r\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\"><span style=\\\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\\\">\\u00a0<img src=\\\"images\\/android.jpeg\\\" alt=\\\"\\\" \\/><\\/span><\\/p>\\r\\n<p style=\\\"text-align: center;\\\">La pr\\u00f3xima versi\\u00f3n del sistema contempla terminales 5G, pantallas curvas y m\\u00e1s privacidad.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<h1 class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\">Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores<\\/h1>\\r\\n<iframe width=\\\"560\\\" height=\\\"315\\\" src=\\\"https:\\/\\/www.youtube.com\\/embed\\/tyx05coXixw\\\" frameborder=\\\"0\\\" allow=\\\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen><\\/iframe>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 02:01:34\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:03:19\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:02:39\",\"publish_up\":\"2020-03-25 02:01:34\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(12, 5, 1, '', '2020-03-25 02:03:44', 66, 3061, 'bfb994de3edfbd9d765027dea146afb44eec8440', '{\"id\":5,\"asset_id\":\"83\",\"title\":\"Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores\",\"alias\":\"google-presenta-su-version-preliminar-android-11-para-desarrolladores\",\"introtext\":\"<h2 style=\\\"text-align: center;\\\">Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores<\\/h2>\\r\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\"><span style=\\\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\\\">\\u00a0<img src=\\\"images\\/android.jpeg\\\" alt=\\\"\\\" \\/><\\/span><\\/p>\\r\\n<p style=\\\"text-align: center;\\\">La pr\\u00f3xima versi\\u00f3n del sistema contempla terminales 5G, pantallas curvas y m\\u00e1s privacidad.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<h1 class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\">Google presenta su versi\\u00f3n preliminar Android 11 para desarrolladores<\\/h1>\\r\\n<iframe width=\\\"560\\\" height=\\\"315\\\" src=\\\"https:\\/\\/www.youtube.com\\/embed\\/tyx05coXixw\\\" frameborder=\\\"0\\\" allow=\\\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen><\\/iframe>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\",\"state\":1,\"catid\":\"9\",\"created\":\"2020-03-25 02:01:34\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:03:44\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:03:30\",\"publish_up\":\"2020-03-25 02:01:34\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":4,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(13, 6, 1, '', '2020-03-25 02:08:01', 66, 2912, 'a384162b796c3038426cb108eacab1102da4dc38', '{\"id\":6,\"asset_id\":84,\"title\":\"Prochile busca aumentar comercio con Colombia\",\"alias\":\"prochile-busca-aumentar-comercio-con-colombia\",\"introtext\":\"<h2 style=\\\"text-align: center;\\\">Prochile busca aumentar comercio con Colombia<\\/h2>\\r\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\"><span style=\\\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\\\">\\u00a0<img src=\\\"images\\/ventas.jpg\\\" alt=\\\"\\\" \\/><\\/span><\\/p>\\r\\n<p style=\\\"text-align: center;\\\">La pr\\u00f3xima versi\\u00f3n del sistema contempla terminales 5G, pantallas curvas y m\\u00e1s privacidad.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<h2 style=\\\"text-align: center;\\\">Prochile busca aumentar comercio con Colombia<\\/h2>\\r\\n<iframe width=\\\"560\\\" height=\\\"315\\\" src=\\\"https:\\/\\/www.youtube.com\\/embed\\/FyqhwEEI5Ww\\\" frameborder=\\\"0\\\" allow=\\\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen><\\/iframe>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\",\"state\":1,\"catid\":\"10\",\"created\":\"2020-03-25 02:08:01\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:08:01\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2020-03-25 02:08:01\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(14, 7, 1, '', '2020-03-25 02:10:51', 66, 3049, 'bb1c1a9e055d8084c420aba5cfbd02c62b7068d7', '{\"id\":7,\"asset_id\":85,\"title\":\"Vicepresidenta habla de estudio que ubica al pa\\u00eds como el m\\u00e1s corrupto\",\"alias\":\"vicepresidenta-habla-de-estudio-que-ubica-al-pais-como-el-mas-corrupto\",\"introtext\":\"<h2 style=\\\"text-align: center;\\\">Vicepresidenta habla de estudio que ubica al pa\\u00eds como el m\\u00e1s corrupto<\\/h2>\\r\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\"><span style=\\\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\\\">\\u00a0<img src=\\\"images\\/comunicaciones.jpeg\\\" alt=\\\"\\\" \\/><\\/span><\\/p>\\r\\n<p style=\\\"text-align: center;\\\">Seg\\u00fan un informe de U.S. News, el mundo percibe a Colombia como el lugar con mayor corrupci\\u00f3n.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<h2 style=\\\"text-align: center;\\\">Vicepresidenta habla de estudio que ubica al pa\\u00eds como el m\\u00e1s corrupto<\\/h2>\\r\\n<iframe width=\\\"560\\\" height=\\\"315\\\" src=\\\"https:\\/\\/www.youtube.com\\/embed\\/2SLPFsdwd0I\\\" frameborder=\\\"0\\\" allow=\\\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen><\\/iframe>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\",\"state\":1,\"catid\":\"8\",\"created\":\"2020-03-25 02:10:51\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:10:51\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2020-03-25 02:10:51\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(15, 6, 1, '', '2020-03-25 02:11:34', 66, 2968, '644244e6d66fa1c90baeb9b8ce3f3993419d7475', '{\"id\":6,\"asset_id\":\"84\",\"title\":\"Prochile busca aumentar comercio con Colombia\",\"alias\":\"prochile-busca-aumentar-comercio-con-colombia\",\"introtext\":\"<h2 style=\\\"text-align: center;\\\">Prochile busca aumentar comercio con Colombia<\\/h2>\\r\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align: center;\\\"><span style=\\\"font-size: 12.0pt; line-height: 107%; font-family: \'Arial\',sans-serif;\\\">\\u00a0<img src=\\\"images\\/ventas.jpg\\\" alt=\\\"\\\" \\/><\\/span><\\/p>\\r\\n<p style=\\\"text-align: center;\\\">Al cierre de 2019, las exportaciones de Chile a Colombia sumaron US$643 millones. De esta cifra se destacan las ventas de salm\\u00f3n y de vino.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<h2 style=\\\"text-align: center;\\\">Prochile busca aumentar comercio con Colombia<\\/h2>\\r\\n<iframe width=\\\"560\\\" height=\\\"315\\\" src=\\\"https:\\/\\/www.youtube.com\\/embed\\/FyqhwEEI5Ww\\\" frameborder=\\\"0\\\" allow=\\\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen><\\/iframe>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<\\/p>\",\"state\":1,\"catid\":\"10\",\"created\":\"2020-03-25 02:08:01\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:11:34\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:11:06\",\"publish_up\":\"2020-03-25 02:08:01\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(16, 2, 1, '', '2020-03-25 02:14:18', 66, 2079, '973bde24a5eed77bbe1a684605362234aff06cf5', '{\"id\":2,\"asset_id\":\"76\",\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"<p>[widgetkit id=\\\"2\\\"]<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:14:18\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:11:42\",\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"3\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"2\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(17, 2, 1, '', '2020-03-25 02:15:15', 66, 2094, '0f368668cfa214ade2e2f3b8ba5afb341bc862c9', '{\"id\":2,\"asset_id\":\"76\",\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"<p>{loadmoduleid [widgetkit id=\\\"2\\\"]}<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:15:15\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:14:40\",\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":5,\"ordering\":\"3\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"4\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(18, 2, 1, '', '2020-03-25 02:15:29', 66, 2051, 'f58cb0be959517335df8f76ebba4c617822edaff', '{\"id\":2,\"asset_id\":\"76\",\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:15:29\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:15:15\",\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":6,\"ordering\":\"3\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"5\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0);
INSERT INTO `lbazs_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(19, 2, 1, '', '2020-03-25 02:18:33', 66, 2378, '562512752e369084139b0791f2aff34e49107e25', '{\"id\":2,\"asset_id\":\"76\",\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"<table style=\\\"height: 23px; margin-left: auto; margin-right: auto;\\\" width=\\\"250\\\">\\r\\n<tbody>\\r\\n<tr>\\r\\n<td style=\\\"width: 1px;\\\">{loadposition desarrollo}<\\/td>\\r\\n<td style=\\\"width: 1px;\\\">{loadposition comercial}<\\/td>\\r\\n<td style=\\\"width: 226px;\\\">{loadposition comunicaciones}<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:18:33\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:17:39\",\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":7,\"ordering\":\"3\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"5\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(20, 2, 1, '', '2020-03-25 02:19:33', 66, 2446, 'e71d02f92024709e02613981adeebea57b9800d8', '{\"id\":2,\"asset_id\":\"76\",\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"<table style=\\\"height: 23px; margin-left: auto; margin-right: auto; width: 922px;\\\">\\r\\n<tbody>\\r\\n<tr>\\r\\n<td style=\\\"width: 305.5px; text-align: center;\\\">{loadposition desarrollo}<\\/td>\\r\\n<td style=\\\"width: 271.5px; text-align: center;\\\">{loadposition comercial}<\\/td>\\r\\n<td style=\\\"width: 299px; text-align: center;\\\">{loadposition comunicaciones}<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:19:33\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:18:33\",\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":8,\"ordering\":\"3\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"6\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(21, 2, 1, '', '2020-03-25 02:20:11', 66, 2448, '115ca27fbcbc6e894469674f82af1d0eacab2cca', '{\"id\":2,\"asset_id\":\"76\",\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"<table style=\\\"height: 23px; margin-left: auto; margin-right: auto; width: 960px;\\\">\\r\\n<tbody>\\r\\n<tr>\\r\\n<td style=\\\"width: 305px; text-align: center;\\\">{loadposition desarrollo}<\\/td>\\r\\n<td style=\\\"width: 285px; text-align: center;\\\">{loadposition comercial}<\\/td>\\r\\n<td style=\\\"width: 331px; text-align: center;\\\">\\u00a0{loadposition comunicaciones}<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:20:11\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:19:33\",\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":9,\"ordering\":\"3\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"8\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(22, 2, 1, '', '2020-03-25 02:21:35', 66, 2721, '5810b977faa6a8812440ac042795b65fd4e489f1', '{\"id\":2,\"asset_id\":\"76\",\"title\":\"Contenidos\",\"alias\":\"contenidos\",\"introtext\":\"<table style=\\\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\\\">\\r\\n<tbody>\\r\\n<tr>\\r\\n<td style=\\\"width: 305px; text-align: center;\\\">{loadposition desarrollo}<\\/td>\\r\\n<td style=\\\"width: 285px; text-align: center;\\\">{loadposition comercial}<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\\r\\n<p>\\u00a0<\\/p>\\r\\n<table style=\\\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\\\">\\r\\n<tbody>\\r\\n<tr>\\r\\n<td style=\\\"width: 305px; text-align: center;\\\">\\u00a0{loadposition comunicaciones}<\\/td>\\r\\n<td style=\\\"width: 285px; text-align: center;\\\">\\u00a0{loadposition comunicaciones}<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\\r\\n<p>\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:30\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:21:35\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:20:11\",\"publish_up\":\"2020-03-25 01:35:30\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":10,\"ordering\":\"3\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"9\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(23, 4, 1, '', '2020-03-25 02:33:37', 66, 2080, '65722213a93c27f2b17e1448c5816f0fda98729b', '{\"id\":4,\"asset_id\":\"78\",\"title\":\"Encuestas\",\"alias\":\"encuestas\",\"introtext\":\"<p>{loadposition encuesta}<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:58\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:33:37\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:33:20\",\"publish_up\":\"2020-03-25 01:35:58\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"2\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(24, 1, 1, '', '2020-03-25 02:42:48', 66, 2128, '9df83b1fca7dac2bb0409d4deff083dbfd424b82', '{\"id\":1,\"asset_id\":\"75\",\"title\":\"Inicio\",\"alias\":\"inicio\",\"introtext\":\"<p><img src=\\\"images\\/banner1.jpg\\\" alt=\\\"\\\" width=\\\"100%\\\" height=\\\"100%\\\" \\/><\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:15\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:42:48\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:35:13\",\"publish_up\":\"2020-03-25 01:35:15\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"4\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"10\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(25, 1, 1, '', '2020-03-25 02:48:16', 66, 2869, '25eb36a1abb481c105f6d095ef0859a1eee2c6b9', '{\"id\":1,\"asset_id\":\"75\",\"title\":\"Inicio\",\"alias\":\"inicio\",\"introtext\":\"<p><img src=\\\"images\\/bannerguia-01.png\\\" alt=\\\"\\\" width=\\\"100%\\\" height=\\\"100%\\\" \\/><\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<table style=\\\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\\\">\\r\\n<tbody>\\r\\n<tr>\\r\\n<td style=\\\"width: 305px; text-align: center;\\\">{loadposition desarrollo}<\\/td>\\r\\n<td style=\\\"width: 285px; text-align: center;\\\">{loadposition comercial}<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\\r\\n<p style=\\\"text-align: center;\\\">\\u00a0<\\/p>\\r\\n<table style=\\\"height: 23px; margin-left: auto; margin-right: auto; width: 902px;\\\">\\r\\n<tbody>\\r\\n<tr>\\r\\n<td style=\\\"width: 305px; text-align: center;\\\">{loadposition encuesta}<\\/td>\\r\\n<td style=\\\"width: 285px; text-align: center;\\\">{loadposition contacto}<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\\r\\n<p style=\\\"text-align: center;\\\"><br \\/><br \\/><\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2020-03-25 01:35:15\",\"created_by\":\"66\",\"created_by_alias\":\"\",\"modified\":\"2020-03-25 02:48:16\",\"modified_by\":\"66\",\"checked_out\":\"66\",\"checked_out_time\":\"2020-03-25 02:42:48\",\"publish_up\":\"2020-03-25 01:35:15\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"show_icon\\\":\\\"0\\\",\\\"type\\\":\\\"standard\\\",\\\"video_provider\\\":\\\"youtube\\\",\\\"video_id\\\":\\\"\\\",\\\"audio_embed\\\":\\\"\\\",\\\"image_gallery\\\":[],\\\"controls\\\":\\\"1\\\",\\\"indicators\\\":\\\"0\\\",\\\"link_text\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"link_target\\\":\\\"_blank\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"4\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"12\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_updates`
--

CREATE TABLE `lbazs_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT 0,
  `extension_id` int(11) DEFAULT 0,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT 0,
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

--
-- Volcado de datos para la tabla `lbazs_updates`
--

INSERT INTO `lbazs_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 2, 0, 'Armenian', '', 'pkg_hy-AM', 'package', '', 0, '3.4.4.1', '', 'https://update.joomla.org/language/details3/hy-AM_details.xml', '', ''),
(2, 2, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.4.1.2', '', 'https://update.joomla.org/language/details3/ms-MY_details.xml', '', ''),
(3, 2, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '3.9.13.1', '', 'https://update.joomla.org/language/details3/ro-RO_details.xml', '', ''),
(4, 2, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/nl-BE_details.xml', '', ''),
(5, 2, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.8.0.1', '', 'https://update.joomla.org/language/details3/zh-TW_details.xml', '', ''),
(6, 2, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/fr-FR_details.xml', '', ''),
(7, 2, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/gl-ES_details.xml', '', ''),
(8, 2, 0, 'Georgian', '', 'pkg_ka-GE', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/ka-GE_details.xml', '', ''),
(9, 2, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/el-GR_details.xml', '', ''),
(10, 2, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/ja-JP_details.xml', '', ''),
(11, 2, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.2', '', 'https://update.joomla.org/language/details3/he-IL_details.xml', '', ''),
(12, 2, 0, 'Bengali', '', 'pkg_bn-BD', 'package', '', 0, '3.8.10.1', '', 'https://update.joomla.org/language/details3/bn-BD_details.xml', '', ''),
(13, 2, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.9.12.1', '', 'https://update.joomla.org/language/details3/hu-HU_details.xml', '', ''),
(14, 2, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/af-ZA_details.xml', '', ''),
(15, 2, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.9.15.1', '', 'https://update.joomla.org/language/details3/ar-AA_details.xml', '', ''),
(16, 2, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/be-BY_details.xml', '', ''),
(17, 2, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.6.5.2', '', 'https://update.joomla.org/language/details3/bg-BG_details.xml', '', ''),
(18, 2, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.9.11.2', '', 'https://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(19, 2, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.9.15.1', '', 'https://update.joomla.org/language/details3/zh-CN_details.xml', '', ''),
(20, 2, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/hr-HR_details.xml', '', ''),
(21, 2, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/cs-CZ_details.xml', '', ''),
(22, 2, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/da-DK_details.xml', '', ''),
(23, 2, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.9.15.1', '', 'https://update.joomla.org/language/details3/nl-NL_details.xml', '', ''),
(24, 2, 0, 'Esperanto', '', 'pkg_eo-XX', 'package', '', 0, '3.8.11.1', '', 'https://update.joomla.org/language/details3/eo-XX_details.xml', '', ''),
(25, 2, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.9.14.2', '', 'https://update.joomla.org/language/details3/et-EE_details.xml', '', ''),
(26, 2, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/it-IT_details.xml', '', ''),
(27, 2, 0, 'Khmer', '', 'pkg_km-KH', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/km-KH_details.xml', '', ''),
(28, 2, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.8.9.1', '', 'https://update.joomla.org/language/details3/ko-KR_details.xml', '', ''),
(29, 2, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.7.3.1', '', 'https://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(30, 2, 0, 'Lithuanian', '', 'pkg_lt-LT', 'package', '', 0, '3.9.6.1', '', 'https://update.joomla.org/language/details3/lt-LT_details.xml', '', ''),
(31, 2, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(32, 2, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.8.11.1', '', 'https://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(33, 2, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.2.1', '', 'https://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(34, 2, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(35, 2, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.9.13.1', '', 'https://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(36, 2, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.9.15.1', '', 'https://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(37, 2, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.9.12.1', '', 'https://update.joomla.org/language/details3/ru-RU_details.xml', '', ''),
(38, 2, 0, 'English AU', '', 'pkg_en-AU', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/en-AU_details.xml', '', ''),
(39, 2, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(40, 2, 0, 'English US', '', 'pkg_en-US', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/en-US_details.xml', '', ''),
(41, 2, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(42, 2, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(43, 2, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(44, 2, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/th-TH_details.xml', '', ''),
(45, 2, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.9.4.1', '', 'https://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(46, 2, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.7.1.1', '', 'https://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(47, 2, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.7.5.2', '', 'https://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(48, 2, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.2', '', 'https://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(49, 2, 0, 'Basque', '', 'pkg_eu-ES', 'package', '', 0, '3.7.5.1', '', 'https://update.joomla.org/language/details3/eu-ES_details.xml', '', ''),
(50, 2, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.2', '', 'https://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(51, 2, 0, 'German DE', '', 'pkg_de-DE', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/de-DE_details.xml', '', ''),
(52, 2, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/pt-BR_details.xml', '', ''),
(53, 2, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(55, 2, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.9.15.1', '', 'https://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(56, 2, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(57, 2, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(58, 2, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(59, 2, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(60, 2, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(61, 2, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(62, 2, 0, 'English CA', '', 'pkg_en-CA', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(63, 2, 0, 'French CA', '', 'pkg_fr-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(64, 2, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.9.14.1', '', 'https://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(65, 2, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(66, 2, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.4.3', '', 'https://update.joomla.org/language/details3/prs-AF_details.xml', '', ''),
(67, 2, 0, 'Turkmen', '', 'pkg_tk-TM', 'package', '', 0, '3.5.0.2', '', 'https://update.joomla.org/language/details3/tk-TM_details.xml', '', ''),
(68, 2, 0, 'Irish', '', 'pkg_ga-IE', 'package', '', 0, '3.8.13.1', '', 'https://update.joomla.org/language/details3/ga-IE_details.xml', '', ''),
(69, 2, 0, 'Dzongkha', '', 'pkg_dz-BT', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/dz-BT_details.xml', '', ''),
(70, 2, 0, 'Slovenian', '', 'pkg_sl-SI', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/sl-SI_details.xml', '', ''),
(71, 2, 0, 'Spanish CO', '', 'pkg_es-CO', 'package', '', 0, '3.9.15.1', '', 'https://update.joomla.org/language/details3/es-CO_details.xml', '', ''),
(72, 2, 0, 'German CH', '', 'pkg_de-CH', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/de-CH_details.xml', '', ''),
(73, 2, 0, 'German AT', '', 'pkg_de-AT', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/de-AT_details.xml', '', ''),
(74, 2, 0, 'German LI', '', 'pkg_de-LI', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/de-LI_details.xml', '', ''),
(75, 2, 0, 'German LU', '', 'pkg_de-LU', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/de-LU_details.xml', '', ''),
(76, 2, 0, 'English NZ', '', 'pkg_en-NZ', 'package', '', 0, '3.9.16.1', '', 'https://update.joomla.org/language/details3/en-NZ_details.xml', '', ''),
(77, 2, 0, 'Kazakh', '', 'pkg_kk-KZ', 'package', '', 0, '3.9.15.1', '', 'https://update.joomla.org/language/details3/kk-KZ_details.xml', '', ''),
(78, 6, 10020, 'JCE Editor Pro', '', 'com_jce', 'component', '', 1, '2.6.38', '', 'https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&id=1&file=extension.xml', '\n                https://www.joomlacontenteditor.net/news/jce-pro-2-6-38-released\n            ', ''),
(399, 8, 0, 'DT Register MailChimp Subscriber', '', 'mailchimp', 'plugin', 'dtreg', 0, '3.4', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtreg_mailchimp.xml', 'https://www.joomlart.com/update-steps', ''),
(400, 8, 0, 'DT Register Google Cal Export Plugin', '', 'plg_dtregexport', 'plugin', 'plg_dtregexport', 0, '1.6', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregexport.xml', 'https://www.joomlart.com/update-steps', ''),
(401, 8, 0, 'DT Register ICS Import Plugin', '', '1.1', 'plugin', 'dtregimport', 0, '1.4', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregimport_1.1.xml', 'https://www.joomlart.com/update-steps', ''),
(402, 8, 0, 'DT Register Authorize.net Plugin', '', 'authorizenet', 'plugin', 'dtregister', 0, '1.5', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregister_authorizenet.xml', 'https://www.joomlart.com/update-steps', ''),
(403, 8, 0, 'DT Register with Docusign Intergration', '', 'dtdocusign', 'plugin', 'dtregister', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregister_dtdocusign.xml', 'https://www.joomlart.com/update-steps', ''),
(404, 8, 0, 'DT Register eWay Plugin', '', 'eway', 'plugin', 'dtregister', 0, '1.5', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregister_eway.xml', 'https://www.joomlart.com/update-steps', ''),
(405, 8, 0, 'DT Mollie Plugin', '', 'mollie', 'plugin', 'dtregister', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregister_mollie.xml', 'https://www.joomlart.com/update-steps', ''),
(406, 8, 0, 'DT Register MyScript Plugin', '', 'myscript', 'plugin', 'dtregister', 0, '2.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregister_myscript.xml', 'https://www.joomlart.com/update-steps', ''),
(407, 8, 0, 'DT Register Records Plugin', '', 'records', 'plugin', 'dtregister', 0, '2.0.9', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregister_records.xml', 'https://www.joomlart.com/update-steps', ''),
(408, 8, 0, 'System - DT Register SSL', '', 'plg_dtregssl', 'plugin', 'plg_dtregssl', 0, '2.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtregssl.xml', 'https://www.joomlart.com/update-steps', ''),
(409, 8, 0, 'DT SMS Action Answer plugin', '', 'actionanswer', 'plugin', 'dtsms', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_actionanswer.xml', 'https://www.joomlart.com/update-steps', ''),
(410, 8, 0, 'DT SMS Action Forward Plugin', '', 'actionforward', 'plugin', 'dtsms', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_actionforward.xml', 'https://www.joomlart.com/update-steps', ''),
(411, 8, 0, 'DT SMS Action Subscribe Plugin', '', 'actionsubscribe', 'plugin', 'dtsms', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_actionsubscribe.xml', 'https://www.joomlart.com/update-steps', ''),
(412, 8, 0, 'DT SMS  Action Unsubscribe Plugin', '', 'actionunsubscribe', 'plugin', 'dtsms', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_actionunsubscribe.xml', 'https://www.joomlart.com/update-steps', ''),
(413, 8, 0, 'DT SMS Article Integration Plugin', '', 'articleintegration', 'plugin', 'dtsms', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_articleintegration.xml', 'https://www.joomlart.com/update-steps', ''),
(414, 8, 0, 'DT SMS Community Builder Integration Plugin', '', 'cbintegration', 'plugin', 'dtsms', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_cbintegration.xml', 'https://www.joomlart.com/update-steps', ''),
(415, 8, 0, 'DT SMS Component Integration Plugin', '', 'dtsmsintegration', 'plugin', 'dtsms', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_dtsmsintegration.xml', 'https://www.joomlart.com/update-steps', ''),
(416, 8, 0, 'DT SMS JomSocial Integration Plugin', '', 'jomintegration', 'plugin', 'dtsms', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_jomintegration.xml', 'https://www.joomlart.com/update-steps', ''),
(417, 8, 0, 'DT SMS Joomla User Integration Plugin', '', 'joomlaintegration', 'plugin', 'dtsms', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_joomlaintegration.xml', 'https://www.joomlart.com/update-steps', ''),
(418, 8, 0, 'DT SMS Onetime Plugin', '', 'smsonetime', 'plugin', 'dtsms', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_smsonetime.xml', 'https://www.joomlart.com/update-steps', ''),
(419, 8, 0, 'DT SMS Periodic Plugin', '', 'smsperiodic', 'plugin', 'dtsms', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_smsperiodic.xml', 'https://www.joomlart.com/update-steps', ''),
(420, 8, 0, 'DT SMS Plugin - Test Gateway', '', 'testgateway', 'plugin', 'dtsms', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_testgateway.xml', 'https://www.joomlart.com/update-steps', ''),
(421, 8, 0, 'DT SMS Plugin - Twilio Gateway', '', 'twiliogateway', 'plugin', 'dtsms', 0, '1.4', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtsms_twiliogateway.xml', 'https://www.joomlart.com/update-steps', ''),
(422, 8, 0, 'DT Register Installer Plugin', '', 'dtregister', 'plugin', 'installer', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_installer_dtregister.xml', 'https://www.joomlart.com/update-steps', ''),
(423, 8, 0, 'JB Library Plugin', '', 'zenlibrary', 'plugin', 'jblibrary', 0, '2.1.7', '', 'http://update.joomlart.com/service/tracking/j30/plg_jblibrary_zenlibrary.xml', 'https://www.joomlart.com/update-steps', ''),
(424, 8, 0, 'DT Register Activity (JomSocial) Plugin', '', 'dtreg_activity', 'plugin', 'jomsocial', 0, '1.9', '', 'http://update.joomlart.com/service/tracking/j30/plg_jomsocial_dtreg_activity.xml', 'https://www.joomlart.com/update-steps', ''),
(425, 8, 0, 'DT Register Records (JomSocial) Plugin', '', 'dtreg_records', 'plugin', 'jomsocial', 0, '1.12', '', 'http://update.joomlart.com/service/tracking/j30/plg_jomsocial_dtreg_records.xml', 'https://www.joomlart.com/update-steps', ''),
(426, 8, 0, 'GK Opengraph system plugin', '', 'gk_opengraph', 'plugin', 'sys', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gk_opengraph.xml', 'https://www.joomlart.com/update-steps', ''),
(427, 8, 0, 'GK Cache system plugin', '', 'gkcache', 'plugin', 'sys', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gkcache.xml', 'https://www.joomlart.com/update-steps', ''),
(428, 8, 0, 'GK Contact system plugin', '', 'gkcontact', 'plugin', 'sys', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gkcontact.xml', 'https://www.joomlart.com/update-steps', ''),
(429, 8, 0, 'GK Extmenu system plugin', '', 'gkextmenu', 'plugin', 'sys', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gkextmenu.xml', 'https://www.joomlart.com/update-steps', ''),
(430, 8, 0, 'System JBType Plugin', '', 'jbtype', 'plugin', 'sys', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_jbtype.xml', 'https://www.joomlart.com/update-steps', ''),
(431, 8, 0, 'JB Zen Shortcodes Plugin', '', 'zenshortcodes', 'plugin', 'sys', 0, '1.7.6', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_zenshortcodes.xml', 'https://www.joomlart.com/update-steps', ''),
(432, 8, 0, 'Plugin Zentools2 System', '', 'zentools2', 'plugin', 'sys', 0, '2.3.5', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(433, 8, 0, 'DT SMS System Plugin', '', 'dtsmsintegrator', 'plugin', 'system', 0, '1.4', '', 'http://update.joomlart.com/service/tracking/j30/plg_system_dtsmsintegrator.xml', 'https://www.joomlart.com/update-steps', ''),
(434, 8, 0, '', '', '', 'plugin', '', 0, '', '', 'http://update.joomlart.com/service/tracking/j16/.xml', 'https://www.joomlart.com/update-steps', ''),
(435, 8, 0, 'T3 B3 Blank Template', '', 't3_bs3_blank', 'template', '', 0, '2.2.1', '', 'http://update.joomlart.com/service/tracking/j30/tpl_t3_bs3_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(436, 8, 0, 'iSEO Component', '', 'ISEO', 'component', '', 1, '3.1.16', '', 'http://update.joomlart.com/service/tracking/j16/ISEO.xml', 'https://www.joomlart.com/update-steps', ''),
(437, 8, 0, 'JB Xero Template', '', 'xero', 'template', '', 0, '1.4.4', '', 'http://update.joomlart.com/service/tracking/j30/xero.xml', 'https://www.joomlart.com/update-steps', ''),
(438, 8, 0, 'JA Amazon S3 for joomla 16', '', 'com_com_jaamazons3', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/com_com_jaamazons3.xml', '', ''),
(439, 8, 0, 'GURU Payment VT.Web Veritrans Indonesia', '', 'GURU-Payment_VT.Web_Veritrans_Indonesia', 'plugin', 'GURU-Payment_VT.Web_', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/GURU-Payment_VT.Web_Veritrans_Indonesia.xml', 'https://www.joomlart.com/update-steps', ''),
(440, 8, 0, 'JA Extenstion Manager Component j16', '', 'com_com_jaextmanager', 'file', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/com_com_jaextmanager.xml', '', ''),
(441, 8, 0, 'MPDF', '', 'MPDF', 'package', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/MPDF.xml', 'https://www.joomlart.com/update-steps', ''),
(442, 8, 0, 'JA Amazon S3 for joomla 2.5 & 3.x', '', 'com_jaamazons3', 'component', '', 1, '2.5.9', '', 'http://update.joomlart.com/service/tracking/j16/com_jaamazons3.xml', 'https://www.joomlart.com/update-steps', ''),
(443, 8, 0, 'S5 Vertex Framework', '', 'Shape5_Vertex4', 'template', '', 0, '4.2.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_Vertex4.xml', 'https://www.joomlart.com/update-steps', ''),
(444, 8, 0, 'JA Comment Package for Joomla 2.5 & 3.x', '', 'com_jacomment', 'component', '', 1, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/com_jacomment.xml', 'https://www.joomlart.com/update-steps', ''),
(445, 8, 0, 'S5 Amazed Photography Template', '', 'Shape5_amazed_photography_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_amazed_photography_template.xml', 'https://www.joomlart.com/update-steps', ''),
(446, 8, 0, 'JA Extenstion Manager Component for J3.x', '', 'com_jaextmanager', 'component', '', 1, '2.6.5', '', 'http://update.joomlart.com/service/tracking/j16/com_jaextmanager.xml', 'https://www.joomlart.com/update-steps', ''),
(447, 8, 0, 'S5 Ambient Template', '', 'Shape5_ambient_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_ambient_template.xml', 'https://www.joomlart.com/update-steps', ''),
(448, 8, 0, 'JA Google Storage Package for J2.5 & J3.0', '', 'com_jagooglestorage', 'component', '', 1, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/com_jagooglestorage.xml', '', ''),
(449, 8, 0, 'S5 Ameritage Medical Template', '', 'Shape5_ameritage_medical_template', 'template', '', 0, '3.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_ameritage_medical_template.xml', 'https://www.joomlart.com/update-steps', ''),
(450, 8, 0, 'S5 Appwonder Template', '', 'Shape5_appwonder_template', 'template', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_appwonder_template.xml', 'https://www.joomlart.com/update-steps', ''),
(451, 8, 0, 'JA Job Board Package For J25', '', 'com_jajobboard', 'component', '', 1, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j16/com_jajobboard.xml', '', ''),
(452, 8, 0, 'S5 Arthur Template', '', 'Shape5_arthur_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_arthur_template.xml', 'https://www.joomlart.com/update-steps', ''),
(453, 8, 0, 'JA K2 Filter Package for J25 & J3.x', '', 'com_jak2filter', 'component', '', 1, '1.3.3', '', 'http://update.joomlart.com/service/tracking/j16/com_jak2filter.xml', 'https://www.joomlart.com/update-steps', ''),
(454, 8, 0, 'S5 Attractions Template', '', 'Shape5_attractions_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_attractions_template.xml', 'https://www.joomlart.com/update-steps', ''),
(455, 8, 0, 'JA K2 Filter Package for J25 & J30', '', 'com_jak2fiter', 'component', '', 1, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j16/com_jak2fiter.xml', '', ''),
(456, 8, 0, 'S5 Aurora Dawn Template', '', 'Shape5_aurora_dawn_template', 'template', '', 0, '3.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_aurora_dawn_template.xml', 'https://www.joomlart.com/update-steps', ''),
(457, 8, 0, 'JA Megafilter Component', '', 'com_jamegafilter', 'component', '', 1, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j16/com_jamegafilter.xml', 'https://www.joomlart.com/update-steps', ''),
(458, 8, 0, 'S5 Avignet Dream Template', '', 'Shape5_avignet_dream_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_avignet_dream_template.xml', 'https://www.joomlart.com/update-steps', ''),
(459, 8, 0, 'JA Showcase component for Joomla 1.7', '', 'com_jashowcase', 'component', '', 1, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j16/com_jashowcase.xml', '', ''),
(460, 8, 0, 'S5 Big Picture Template', '', 'Shape5_bigpicture_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_bigpicture_template.xml', 'https://www.joomlart.com/update-steps', ''),
(461, 8, 0, 'JA Voice Package for Joomla 2.5 & 3.x', '', 'com_javoice', 'component', '', 1, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j16/com_javoice.xml', '', ''),
(462, 8, 0, 'S5 Blogbox Template', '', 'Shape5_blog_box_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_blog_box_template.xml', 'https://www.joomlart.com/update-steps', ''),
(463, 8, 0, 'JA Appolio Theme for EasyBlog', '', 'easyblog_theme_appolio', 'custom', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_appolio.xml', 'https://www.joomlart.com/update-steps', ''),
(464, 8, 0, 'S5 BlueGroup Template', '', 'Shape5_bluegroup_template', 'template', '', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_bluegroup_template.xml', 'https://www.joomlart.com/update-steps', ''),
(465, 8, 0, 'JA Beranis Theme for EasyBlog', '', 'easyblog_theme_beranis', 'custom', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_beranis.xml', 'https://www.joomlart.com/update-steps', ''),
(466, 8, 0, 'S5 Business Line Template', '', 'Shape5_business_line_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_business_line_template.xml', 'https://www.joomlart.com/update-steps', ''),
(467, 8, 0, 'JA Biz Theme for EasyBlog', '', 'easyblog_theme_biz', 'custom', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_biz.xml', 'https://www.joomlart.com/update-steps', ''),
(468, 8, 0, 'S5 Business Pro Template', '', 'Shape5_business_pro_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_business_pro_template.xml', 'https://www.joomlart.com/update-steps', ''),
(469, 8, 0, 'JA Bookshop Theme for EasyBlog', '', 'easyblog_theme_bookshop', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_bookshop.xml', 'https://www.joomlart.com/update-steps', ''),
(470, 8, 0, 'S5 Callie Rush Template', '', 'Shape5_callie_rush_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_callie_rush_template.xml', 'https://www.joomlart.com/update-steps', ''),
(471, 8, 0, 'Theme Community Plus for Easyblog J25 & J30', '', 'easyblog_theme_community_plus', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_community_plus.xml', 'https://www.joomlart.com/update-steps', ''),
(472, 8, 0, 'S5 Campus Life Template', '', 'Shape5_campuslife_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_campuslife_template.xml', 'https://www.joomlart.com/update-steps', ''),
(473, 8, 0, 'JA Decor Theme for EasyBlog', '', 'easyblog_theme_decor', 'custom', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_decor.xml', 'https://www.joomlart.com/update-steps', ''),
(474, 8, 0, 'S5 Charity Template', '', 'Shape5_charity_template', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_charity_template.xml', 'https://www.joomlart.com/update-steps', ''),
(475, 8, 0, 'Theme Fixel for Easyblog J25 & J3x', '', 'easyblog_theme_fixel', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_fixel.xml', 'https://www.joomlart.com/update-steps', ''),
(476, 8, 0, 'S5 Charlestown Template', '', 'Shape5_charlestown_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_charlestown_template.xml', 'https://www.joomlart.com/update-steps', ''),
(477, 8, 0, 'Theme Magz for Easyblog J25 & J34', '', 'easyblog_theme_magz', 'custom', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_magz.xml', 'https://www.joomlart.com/update-steps', ''),
(478, 8, 0, 'S5 Cleanout Template', '', 'Shape5_cleanout_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_cleanout_template.xml', 'https://www.joomlart.com/update-steps', ''),
(479, 8, 0, 'JA Muzic Theme for EasyBlog', '', 'easyblog_theme_muzic', 'custom', '', 0, '1.1.7', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_muzic.xml', 'https://www.joomlart.com/update-steps', ''),
(480, 8, 0, 'S5 Comaxium Template', '', 'Shape5_comaxium_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_comaxium_template.xml', 'https://www.joomlart.com/update-steps', ''),
(481, 8, 0, 'JA Obelisk Theme for EasyBlog', '', 'easyblog_theme_obelisk', 'custom', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_obelisk.xml', 'https://www.joomlart.com/update-steps', ''),
(482, 8, 0, 'JA Sugite Theme for EasyBlog', '', 'easyblog_theme_sugite', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/easyblog_theme_sugite.xml', '', ''),
(483, 8, 0, 'S5 Compassion Template', '', 'Shape5_compassion_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_compassion_template.xml', 'https://www.joomlart.com/update-steps', ''),
(484, 8, 0, 'S5 Construction Template', '', 'Shape5_construction_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_construction_template.xml', 'https://www.joomlart.com/update-steps', ''),
(485, 8, 0, 'iSEO Component', '', 'iSEO', 'component', '', 1, '3.1.16', '', 'http://update.joomlart.com/service/tracking/j16/iSEO.xml', 'https://www.joomlart.com/update-steps', ''),
(486, 8, 0, 'S5 Content King Template', '', 'Shape5_contentking_template', 'template', '', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_contentking_template.xml', 'https://www.joomlart.com/update-steps', ''),
(487, 8, 0, 'JA Anion template for Joomla 3.x', '', 'ja_anion', 'template', '', 0, '2.5.8', '', 'http://update.joomlart.com/service/tracking/j16/ja_anion.xml', 'https://www.joomlart.com/update-steps', ''),
(488, 8, 0, 'S5 Corporate Response Template', '', 'Shape5_corporate_response_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_corporate_response_template.xml', 'https://www.joomlart.com/update-steps', ''),
(489, 8, 0, 'JA Appolio Template', '', 'ja_appolio', 'template', '', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j16/ja_appolio.xml', 'https://www.joomlart.com/update-steps', ''),
(490, 8, 0, 'S5 Corpway Template', '', 'Shape5_corpway_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_corpway_template.xml', 'https://www.joomlart.com/update-steps', ''),
(491, 8, 0, 'JA Argo Template for J3x', '', 'ja_argo', 'template', '', 0, '1.1.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_argo.xml', 'https://www.joomlart.com/update-steps', ''),
(492, 8, 0, 'S5 Curb Appeal Template', '', 'Shape5_curb_appeal_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_curb_appeal_template.xml', 'https://www.joomlart.com/update-steps', ''),
(493, 8, 0, 'JA Beranis Template', '', 'ja_beranis', 'template', '', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_beranis.xml', 'https://www.joomlart.com/update-steps', ''),
(494, 8, 0, 'S5 Cyan Template', '', 'Shape5_cyan_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_cyan_template.xml', 'https://www.joomlart.com/update-steps', ''),
(495, 8, 0, 'JA Bistro Template for Joomla 3.x', '', 'ja_bistro', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_bistro.xml', 'https://www.joomlart.com/update-steps', ''),
(496, 8, 0, 'S5 Design Control Template', '', 'Shape5_design_control_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_design_control_template.xml', 'https://www.joomlart.com/update-steps', ''),
(497, 8, 0, 'JA Blazes Template for J25 & J3x', '', 'ja_blazes', 'template', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_blazes.xml', 'https://www.joomlart.com/update-steps', ''),
(498, 8, 0, 'S5 Emma and Mason Template', '', 'Shape5_emma_and_mason_template', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_emma_and_mason_template.xml', 'https://www.joomlart.com/update-steps', ''),
(499, 8, 0, 'S5 Emusica Template', '', 'Shape5_emusica_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_emusica_template.xml', 'https://www.joomlart.com/update-steps', ''),
(500, 8, 0, 'JA Bookshop Template', '', 'ja_bookshop', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j16/ja_bookshop.xml', 'https://www.joomlart.com/update-steps', ''),
(501, 8, 0, 'S5 Eventfull Business Template', '', 'Shape5_eventfull_business_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_eventfull_business_template.xml', 'https://www.joomlart.com/update-steps', ''),
(502, 8, 0, 'JA Brisk Template for J25 & J3x', '', 'ja_brisk', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j16/ja_brisk.xml', 'https://www.joomlart.com/update-steps', ''),
(503, 8, 0, 'S5 Eventfull Template', '', 'Shape5_eventfull_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_eventfull_template.xml', 'https://www.joomlart.com/update-steps', ''),
(504, 8, 0, 'JA Business Template for Joomla 3.x', '', 'ja_business', 'template', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_business.xml', 'https://www.joomlart.com/update-steps', ''),
(505, 8, 0, 'S5 EZ Web Hosting Template', '', 'Shape5_ezwebhosting_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_ezwebhosting_template.xml', 'https://www.joomlart.com/update-steps', ''),
(506, 8, 0, 'JA Cloris Template for Joomla 3.x', '', 'ja_cloris', 'template', '', 0, '2.5.4', '', 'http://update.joomlart.com/service/tracking/j16/ja_cloris.xml', 'https://www.joomlart.com/update-steps', ''),
(507, 8, 0, 'S5 Fitness Center Template', '', 'Shape5_fitness_center_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_fitness_center_template.xml', 'https://www.joomlart.com/update-steps', ''),
(508, 8, 0, 'JA Community PLus Template for Joomla 3.x', '', 'ja_community_plus', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_community_plus.xml', 'https://www.joomlart.com/update-steps', ''),
(509, 8, 0, 'S5 Forte Template', '', 'Shape5_forte_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_forte_template.xml', 'https://www.joomlart.com/update-steps', ''),
(510, 8, 0, 'JA Decor Template', '', 'ja_decor', 'template', '', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j16/ja_decor.xml', 'https://www.joomlart.com/update-steps', ''),
(511, 8, 0, 'S5 Fresh Bistro Template', '', 'Shape5_fresh_bistro_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_fresh_bistro_template.xml', 'https://www.joomlart.com/update-steps', ''),
(512, 8, 0, 'JA Droid Template for Joomla 3.x', '', 'ja_droid', 'template', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_droid.xml', 'https://www.joomlart.com/update-steps', ''),
(513, 8, 0, 'S5 Fusion Template', '', 'Shape5_fusion_template', 'template', '', 0, '1.5.7', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_fusion_template.xml', 'https://www.joomlart.com/update-steps', ''),
(514, 8, 0, 'JA Edenite Template for J25 & J34', '', 'ja_edenite', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_edenite.xml', 'https://www.joomlart.com/update-steps', ''),
(515, 8, 0, 'S5 Game World Template', '', 'Shape5_game_world_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_game_world_template.xml', 'https://www.joomlart.com/update-steps', ''),
(516, 8, 0, 'JA Elastica Template for J25 & J3x', '', 'ja_elastica', 'template', '', 0, '2.5.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_elastica.xml', 'https://www.joomlart.com/update-steps', ''),
(517, 8, 0, 'S5 Gamers Template', '', 'Shape5_gamers_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_gamers_template.xml', 'https://www.joomlart.com/update-steps', ''),
(518, 8, 0, 'JA Erio Template for Joomla 2.5 & 3.x', '', 'ja_erio', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_erio.xml', 'https://www.joomlart.com/update-steps', ''),
(519, 8, 0, 'S5 GCK Store Template', '', 'Shape5_gck_store_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_gck_store_template.xml', 'https://www.joomlart.com/update-steps', ''),
(520, 8, 0, 'Ja Events Template for Joomla 2.5', '', 'ja_events', 'template', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_events.xml', 'https://www.joomlart.com/update-steps', ''),
(521, 8, 0, 'S5 General Commerce Template', '', 'Shape5_general_commerce_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_general_commerce_template.xml', 'https://www.joomlart.com/update-steps', ''),
(522, 8, 0, 'JA Fubix Template for J25 & J3x', '', 'ja_fubix', 'template', '', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_fubix.xml', 'https://www.joomlart.com/update-steps', ''),
(523, 8, 0, 'S5 General Contractor Template', '', 'Shape5_general_contractor_template', 'template', '', 0, '1.1.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_general_contractor_template.xml', 'https://www.joomlart.com/update-steps', ''),
(524, 8, 0, 'JA Graphite Template for Joomla 3x', '', 'ja_graphite', 'template', '', 0, '2.5.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_graphite.xml', 'https://www.joomlart.com/update-steps', ''),
(525, 8, 0, 'S5 GetReserved Template', '', 'Shape5_getreserved_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_getreserved_template.xml', 'https://www.joomlart.com/update-steps', ''),
(526, 8, 0, 'JA Hawkstore Template', '', 'ja_hawkstore', 'template', '', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_hawkstore.xml', 'https://www.joomlart.com/update-steps', ''),
(527, 8, 0, 'S5 Health Guide Template', '', 'Shape5_health_guide_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_health_guide_template.xml', 'https://www.joomlart.com/update-steps', ''),
(528, 8, 0, 'JA Ironis Template for Joomla 2.5 & 3.x', '', 'ja_ironis', 'template', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_ironis.xml', 'https://www.joomlart.com/update-steps', ''),
(529, 8, 0, 'S5 Helion Template', '', 'Shape5_helion_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_helion_template.xml', 'https://www.joomlart.com/update-steps', ''),
(530, 8, 0, 'JA Jason template', '', 'ja_jason', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_jason.xml', 'https://www.joomlart.com/update-steps', ''),
(531, 8, 0, 'S5 Hexa Corp Template', '', 'Shape5_hexa_corp_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_hexa_corp_template.xml', 'https://www.joomlart.com/update-steps', ''),
(532, 8, 0, 'JA Kranos Template for J2.5 & J3.x', '', 'ja_kranos', 'template', '', 0, '2.5.9', '', 'http://update.joomlart.com/service/tracking/j16/ja_kranos.xml', 'https://www.joomlart.com/update-steps', ''),
(533, 8, 0, 'S5 Hexicon Gamer Template', '', 'Shape5_hexicon_gamer_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_hexicon_gamer_template.xml', 'https://www.joomlart.com/update-steps', ''),
(534, 8, 0, 'JA Lens Template for Joomla 2.5 & 3.x', '', 'ja_lens', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j16/ja_lens.xml', 'https://www.joomlart.com/update-steps', ''),
(535, 8, 0, 'S5 Incline Template', '', 'Shape5_incline_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_incline_template.xml', 'https://www.joomlart.com/update-steps', ''),
(536, 8, 0, 'Ja Lime Template for Joomla 3x', '', 'ja_lime', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_lime.xml', '', ''),
(537, 8, 0, 'S5 Legal Lawyer Template', '', 'Shape5_legal_lawyer_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_legal_lawyer_template.xml', 'https://www.joomlart.com/update-steps', ''),
(538, 8, 0, 'JA Magz Template for J25 & J34', '', 'ja_magz', 'template', '', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_magz.xml', 'https://www.joomlart.com/update-steps', ''),
(539, 8, 0, 'S5 Life Journey Template', '', 'Shape5_life_journey_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_life_journey_template.xml', 'https://www.joomlart.com/update-steps', ''),
(540, 8, 0, 'JA Medicare Template', '', 'ja_medicare', 'template', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j16/ja_medicare.xml', 'https://www.joomlart.com/update-steps', ''),
(541, 8, 0, 'S5 Light Church Template', '', 'Shape5_lightchurch_template', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_lightchurch_template.xml', 'https://www.joomlart.com/update-steps', ''),
(542, 8, 0, 'JA Mendozite Template for J25 & J32', '', 'ja_mendozite', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j16/ja_mendozite.xml', 'https://www.joomlart.com/update-steps', ''),
(543, 8, 0, 'S5 Lime Light Template', '', 'Shape5_lime_light_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_lime_light_template.xml', 'https://www.joomlart.com/update-steps', ''),
(544, 8, 0, 'JA Mero Template for J25 & J3x', '', 'ja_mero', 'template', '', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_mero.xml', 'https://www.joomlart.com/update-steps', ''),
(545, 8, 0, 'S5 Luxon Template', '', 'Shape5_luxon_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_luxon_template.xml', 'https://www.joomlart.com/update-steps', ''),
(546, 8, 0, 'JA Mers Template for J25 & J3x', '', 'ja_mers', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j16/ja_mers.xml', 'https://www.joomlart.com/update-steps', ''),
(547, 8, 0, 'S5 M Ceative Agency Template', '', 'Shape5_m_creative_agency_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_m_creative_agency_template.xml', 'https://www.joomlart.com/update-steps', ''),
(548, 8, 0, 'S5 Maxed Mag Template', '', 'Shape5_maxedmag_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_maxedmag_template.xml', 'https://www.joomlart.com/update-steps', ''),
(549, 8, 0, 'JA Methys Template for Joomla 3x', '', 'ja_methys', 'template', '', 0, '2.5.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_methys.xml', 'https://www.joomlart.com/update-steps', ''),
(550, 8, 0, 'S5 MetroShows Template', '', 'Shape5_metroshows_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_metroshows_template.xml', 'https://www.joomlart.com/update-steps', ''),
(551, 8, 0, 'Ja Minisite Template for Joomla 3.4', '', 'ja_minisite', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_minisite.xml', '', ''),
(552, 8, 0, 'S5 Modern Flavor Template', '', 'Shape5_modern_flavor_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_modern_flavor_template.xml', 'https://www.joomlart.com/update-steps', ''),
(553, 8, 0, 'JA Mitius Template', '', 'ja_mitius', 'template', '', 0, '1.1.9', '', 'http://update.joomlart.com/service/tracking/j16/ja_mitius.xml', 'https://www.joomlart.com/update-steps', ''),
(554, 8, 0, 'S5 New Vision Template', '', 'Shape5_new_vision_template', 'template', '', 0, '3.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_new_vision_template.xml', 'https://www.joomlart.com/update-steps', ''),
(555, 8, 0, 'JA Mixmaz Template', '', 'ja_mixmaz', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_mixmaz.xml', 'https://www.joomlart.com/update-steps', ''),
(556, 8, 0, 'S5 News Blog Template', '', 'Shape5_newsblog_template', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_newsblog_template.xml', 'https://www.joomlart.com/update-steps', ''),
(557, 8, 0, 'JA Nex Template for J25 & J30', '', 'ja_nex', 'template', '', 0, '2.5.9', '', 'http://update.joomlart.com/service/tracking/j16/ja_nex.xml', 'https://www.joomlart.com/update-steps', ''),
(558, 8, 0, 'S5 Newsplace Template', '', 'Shape5_newsplace_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_newsplace_template.xml', 'https://www.joomlart.com/update-steps', ''),
(559, 8, 0, 'JA Nex T3 Template', '', 'ja_nex_t3', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j16/ja_nex_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(560, 8, 0, 'S5 No1 Shopping Template', '', 'Shape5_no1_shopping_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_no1_shopping_template.xml', 'https://www.joomlart.com/update-steps', ''),
(561, 8, 0, 'JA Norite Template for J2.5 & J31', '', 'ja_norite', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_norite.xml', 'https://www.joomlart.com/update-steps', ''),
(562, 8, 0, 'S5 Oasis Template', '', 'Shape5_oasis_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_oasis_template.xml', 'https://www.joomlart.com/update-steps', ''),
(563, 8, 0, 'JA Nuevo template', '', 'ja_nuevo', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_nuevo.xml', 'https://www.joomlart.com/update-steps', ''),
(564, 8, 0, 'S5 Outdoor Life Template', '', 'Shape5_outdoor_life_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_outdoor_life_template.xml', 'https://www.joomlart.com/update-steps', ''),
(565, 8, 0, 'JA Obelisk Template', '', 'ja_obelisk', 'template', '', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_obelisk.xml', 'https://www.joomlart.com/update-steps', ''),
(566, 8, 0, 'S5 Pantheon Template', '', 'Shape5_pantheon_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_pantheon_template.xml', 'https://www.joomlart.com/update-steps', ''),
(567, 8, 0, 'JA Onepage Template', '', 'ja_onepage', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j16/ja_onepage.xml', 'https://www.joomlart.com/update-steps', ''),
(568, 8, 0, 'S5 Paradigm Shift Template', '', 'Shape5_paradigm_shift_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_paradigm_shift_template.xml', 'https://www.joomlart.com/update-steps', ''),
(569, 8, 0, 'JA ores template for Joomla 3.x', '', 'ja_ores', 'template', '', 0, '2.5.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_ores.xml', 'https://www.joomlart.com/update-steps', ''),
(570, 8, 0, 'S5 Phosphorus Template', '', 'Shape5_phosphorus_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_phosphorus_template.xml', 'https://www.joomlart.com/update-steps', ''),
(571, 8, 0, 'JA Orisite Template  for J25 & J3x', '', 'ja_orisite', 'template', '', 0, '1.1.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_orisite.xml', 'https://www.joomlart.com/update-steps', ''),
(572, 8, 0, 'S5 Photobox Template', '', 'Shape5_photobox_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_photobox_template.xml', 'https://www.joomlart.com/update-steps', ''),
(573, 8, 0, 'JA Playmag Template', '', 'ja_playmag', 'template', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j16/ja_playmag.xml', 'https://www.joomlart.com/update-steps', ''),
(574, 8, 0, 'S5 Photofolio Template', '', 'Shape5_photofolio_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_photofolio_template.xml', 'https://www.joomlart.com/update-steps', '');
INSERT INTO `lbazs_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(575, 8, 0, 'JA Portfolio Real Estate template for Joomla 1.6.x', '', 'ja_portfolio', 'file', '', 0, '1.0.0 beta', '', 'http://update.joomlart.com/service/tracking/j16/ja_portfolio.xml', '', ''),
(576, 8, 0, 'S5 Prestige Academy Template', '', 'Shape5_prestige_academy_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_prestige_academy_template.xml', 'https://www.joomlart.com/update-steps', ''),
(577, 8, 0, 'JA Portfolio Template for Joomla 3.x', '', 'ja_portfolio_real_estate', 'template', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_portfolio_real_estate.xml', 'https://www.joomlart.com/update-steps', ''),
(578, 8, 0, 'S5 Real Estate Template', '', 'Shape5_realestate_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_realestate_template.xml', 'https://www.joomlart.com/update-steps', ''),
(579, 8, 0, 'JA Puresite Template for J25 & J3x', '', 'ja_puresite', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j16/ja_puresite.xml', 'https://www.joomlart.com/update-steps', ''),
(580, 8, 0, 'S5 Regan Tech Template', '', 'Shape5_regan_tech_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_regan_tech_template.xml', 'https://www.joomlart.com/update-steps', ''),
(581, 8, 0, 'JA Purity II template for Joomla 2.5 & 3.2', '', 'ja_purity_ii', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_purity_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(582, 8, 0, 'S5 Risen Hope Template', '', 'Shape5_risen_hope_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_risen_hope_template.xml', 'https://www.joomlart.com/update-steps', ''),
(583, 8, 0, 'JA Pyro Template for Joomla 3.x', '', 'ja_pyro', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_pyro.xml', 'https://www.joomlart.com/update-steps', ''),
(584, 8, 0, 'S5 Salon N Spa Template', '', 'Shape5_salonnspa_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_salonnspa_template.xml', 'https://www.joomlart.com/update-steps', ''),
(585, 8, 0, 'JA Rasite Template for J34', '', 'ja_rasite', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_rasite.xml', '', ''),
(586, 8, 0, 'S5 Samba Spa Template', '', 'Shape5_samba_template', 'template', '', 0, '1.3.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_samba_template.xml', 'https://www.joomlart.com/update-steps', ''),
(587, 8, 0, 'JA Rave Template for Joomla 3.x', '', 'ja_rave', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/ja_rave.xml', 'https://www.joomlart.com/update-steps', ''),
(588, 8, 0, 'S5 Vertex Template', '', 'Shape5_shape5_vertex_template', 'template', '', 0, '3.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_shape5_vertex_template.xml', 'https://www.joomlart.com/update-steps', ''),
(589, 8, 0, 'JA Smashboard Template', '', 'ja_smashboard', 'template', '', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_smashboard.xml', 'https://www.joomlart.com/update-steps', ''),
(590, 8, 0, 'S5 Shenandoah Template', '', 'Shape5_shenandoah_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_shenandoah_template.xml', 'https://www.joomlart.com/update-steps', ''),
(591, 8, 0, 'JA Social Template for Joomla 2.5', '', 'ja_social', 'template', '', 0, '2.5.8', '', 'http://update.joomlart.com/service/tracking/j16/ja_social.xml', 'https://www.joomlart.com/update-steps', ''),
(592, 8, 0, 'S5 ShoppingBag Template', '', 'Shape5_shoppingbag_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_shoppingbag_template.xml', 'https://www.joomlart.com/update-steps', ''),
(593, 8, 0, 'JA Social T3 Template for J25 & J3x', '', 'ja_social_ii', 'template', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j16/ja_social_ii.xml', '', ''),
(594, 8, 0, 'S5 Sienna Template', '', 'Shape5_sienna_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_sienna_template.xml', 'https://www.joomlart.com/update-steps', ''),
(595, 8, 0, 'JA Social T3 Template for J25 & J3x', '', 'ja_social_t3', 'template', '', 0, '1.1.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_social_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(596, 8, 0, 'S5 Simplex Template', '', 'Shape5_simplex_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_simplex_template.xml', 'https://www.joomlart.com/update-steps', ''),
(597, 8, 0, 'JA Sugite Template', '', 'ja_sugite', 'template', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j16/ja_sugite.xml', 'https://www.joomlart.com/update-steps', ''),
(598, 8, 0, 'S5 Soul Search Template', '', 'Shape5_soul_search_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_soul_search_template.xml', 'https://www.joomlart.com/update-steps', ''),
(599, 8, 0, 'JA System Pager Plugin for J25 & J3x', '', 'ja_system_japager', 'plugin', 'ja_system_japager', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j16/ja_system_japager.xml', 'https://www.joomlart.com/update-steps', ''),
(600, 8, 0, 'S5 Spectrum Template', '', 'Shape5_spectrum_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_spectrum_template.xml', 'https://www.joomlart.com/update-steps', ''),
(601, 8, 0, 'JA T3V2 Blank Template', '', 'ja_t3_blank', 'template', '', 0, '2.5.9', '', 'http://update.joomlart.com/service/tracking/j16/ja_t3_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(602, 8, 0, 'S5 Sports Nation Template', '', 'Shape5_sports_nation_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_sports_nation_template.xml', 'https://www.joomlart.com/update-steps', ''),
(603, 8, 0, 'S5 Store Pro Template', '', 'Shape5_storepro_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_storepro_template.xml', 'https://www.joomlart.com/update-steps', ''),
(604, 8, 0, 'JA T3 Blank template for joomla 1.6', '', 'ja_t3_blank_j16', 'template', '', 0, '1.0.0 Beta', '', 'http://update.joomlart.com/service/tracking/j16/ja_t3_blank_j16.xml', '', ''),
(605, 8, 0, 'S5 Swapps Template', '', 'Shape5_swapps_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_swapps_template.xml', 'https://www.joomlart.com/update-steps', ''),
(606, 8, 0, 'JA Blank Template for T3v3', '', 'ja_t3v3_blank', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/ja_t3v3_blank.xml', '', ''),
(607, 8, 0, 'S5 The Classifieds Template', '', 'Shape5_the_classifieds_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_the_classifieds_template.xml', 'https://www.joomlart.com/update-steps', ''),
(608, 8, 0, 'JA Teline III  Template for Joomla 1.6', '', 'ja_teline_iii', 'file', '', 0, '1.0.0 Beta', '', 'http://update.joomlart.com/service/tracking/j16/ja_teline_iii.xml', '', ''),
(609, 8, 0, 'S5 TheBlogazine Template', '', 'Shape5_theblogazine_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_theblogazine_template.xml', 'https://www.joomlart.com/update-steps', ''),
(610, 8, 0, 'JA Teline IV Template for J2.5 and J3.2', '', 'ja_teline_iv', 'template', '', 0, '2.5.4', '', 'http://update.joomlart.com/service/tracking/j16/ja_teline_iv.xml', '', ''),
(611, 8, 0, 'S5 Traction Template', '', 'Shape5_traction_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_traction_template.xml', 'https://www.joomlart.com/update-steps', ''),
(612, 8, 0, 'JA Teline IV T3 Template', '', 'ja_teline_iv_t3', 'template', '', 0, '1.1.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_teline_iv_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(613, 8, 0, 'S5 University Template', '', 'Shape5_university_template', 'template', '', 0, '2.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_university_template.xml', 'https://www.joomlart.com/update-steps', ''),
(614, 8, 0, 'JA Tiris Template for J25 & J3x', '', 'ja_tiris', 'template', '', 0, '2.6.1', '', 'http://update.joomlart.com/service/tracking/j16/ja_tiris.xml', 'https://www.joomlart.com/update-steps', ''),
(615, 8, 0, 'S5 Velocity Template', '', 'Shape5_velocity_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_velocity_template.xml', 'https://www.joomlart.com/update-steps', ''),
(616, 8, 0, 'JA Travel Template for Joomla 3x', '', 'ja_travel', 'template', '', 0, '2.5.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_travel.xml', 'https://www.joomlart.com/update-steps', ''),
(617, 8, 0, 'S5 Zoka Template', '', 'Shape5_zoka_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_zoka_template.xml', 'https://www.joomlart.com/update-steps', ''),
(618, 8, 0, 'JA University Template for J25 & J32', '', 'ja_university', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j16/ja_university.xml', '', ''),
(619, 8, 0, 'Installation Guide', '', 'adagency_installation_guide', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/adagency_installation_guide.xml', 'https://www.joomlart.com/update-steps', ''),
(620, 8, 0, 'JA University T3 template', '', 'ja_university_t3', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/ja_university_t3.xml', '', ''),
(621, 8, 0, 'Plugin System Admin template Helper', '', 'admintplhelper', 'plugin', 'admintplhelper', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/admintplhelper.xml', 'https://www.joomlart.com/update-steps', ''),
(622, 8, 0, 'JA Vintas Template for J25 & J31', '', 'ja_vintas', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j16/ja_vintas.xml', '', ''),
(623, 8, 0, 'JB Arcadia Template', '', 'arcadia', 'template', '', 0, '2.0.2', '', 'http://update.joomlart.com/service/tracking/j31/arcadia.xml', 'https://www.joomlart.com/update-steps', ''),
(624, 8, 0, 'JA Wall Template for J25 & J3x', '', 'ja_wall', 'template', '', 0, '1.2.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_wall.xml', 'https://www.joomlart.com/update-steps', ''),
(625, 8, 0, 'JA ZiteTemplate', '', 'ja_zite', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/ja_zite.xml', 'https://www.joomlart.com/update-steps', ''),
(626, 8, 0, 'JB Aussie Template', '', 'aussie', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/aussie.xml', 'https://www.joomlart.com/update-steps', ''),
(627, 8, 0, 'JA Bookmark plugin for Joomla 1.6.x', '', 'jabookmark', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jabookmark.xml', '', ''),
(628, 8, 0, 'JB Boost Template', '', 'boost', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/boost.xml', 'https://www.joomlart.com/update-steps', ''),
(629, 8, 0, 'JA Comment plugin for Joomla 1.6.x', '', 'jacomment', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jacomment.xml', '', ''),
(630, 8, 0, 'JB Buildr Template', '', 'buildr', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/buildr.xml', 'https://www.joomlart.com/update-steps', ''),
(631, 8, 0, 'JA Comment Off Plugin for Joomla 1.6', '', 'jacommentoff', 'file', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/jacommentoff.xml', '', ''),
(632, 8, 0, 'Publisher Pro', '', 'com-publisher-pro', 'component', '', 1, '3.0.20', '', 'http://update.joomlart.com/service/tracking/j31/com-publisher-pro.xml', 'https://www.joomlart.com/update-steps', ''),
(633, 8, 0, 'JA Comment On Plugin for Joomla 1.6', '', 'jacommenton', 'file', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/jacommenton.xml', '', ''),
(634, 8, 0, 'Adagency Pro Component', '', 'com_adagency-pro', 'component', '', 1, '6.1.2', '', 'http://update.joomlart.com/service/tracking/j31/com_adagency-pro.xml', 'https://www.joomlart.com/update-steps', ''),
(635, 8, 0, 'JA Content Extra Fields for Joomla 1.6', '', 'jacontentextrafields', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jacontentextrafields.xml', '', ''),
(636, 8, 0, 'DT Donate Component', '', 'com_dtdonate_preview', 'component', '', 1, '4.0.5', '', 'http://update.joomlart.com/service/tracking/j31/com_dtdonate_preview.xml', 'https://www.joomlart.com/update-steps', ''),
(637, 8, 0, 'JA Disqus Debate Echo plugin for Joomla 1.6.x', '', 'jadisqus_debate_echo', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jadisqus_debate_echo.xml', '', ''),
(638, 8, 0, 'DT SMS Component', '', 'com_dtsms', 'component', '', 1, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/com_dtsms.xml', 'https://www.joomlart.com/update-steps', ''),
(639, 8, 0, 'JA System Google Map plugin for Joomla 1.6.x', '', 'jagooglemap', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jagooglemap.xml', '', ''),
(640, 8, 0, 'Guru Pro', '', 'com_guru_pro', 'component', '', 1, '5.2.2', '', 'http://update.joomlart.com/service/tracking/j31/com_guru_pro.xml', 'https://www.joomlart.com/update-steps', ''),
(641, 8, 0, 'JA Google Translate plugin for Joomla 1.6.x', '', 'jagoogletranslate', 'plugin', 'jagoogletranslate', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jagoogletranslate.xml', '', ''),
(642, 8, 0, 'JA Builder Component', '', 'com_jabuilder', 'component', '', 1, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/com_jabuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(643, 8, 0, 'JA Highslide plugin for Joomla 1.6.x', '', 'jahighslide', 'plugin', 'jahighslide', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jahighslide.xml', '', ''),
(644, 8, 0, 'JA Extenstion Manager Component for J3.x', '', 'com_jaextmanager', 'component', '', 1, '2.6.5', '', 'http://update.joomlart.com/service/tracking/j31/com_jaextmanager.xml', 'https://www.joomlart.com/update-steps', ''),
(645, 8, 0, 'JA K2 Search Plugin for Joomla 2.5', '', 'jak2_filter', 'plugin', 'jak2_filter', 0, '1.0.0 Alpha', '', 'http://update.joomlart.com/service/tracking/j16/jak2_filter.xml', '', ''),
(646, 8, 0, 'JA Joomla GDPR Component', '', 'com_jagdpr', 'component', '', 1, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/com_jagdpr.xml', 'https://www.joomlart.com/update-steps', ''),
(647, 8, 0, 'JA K2 Extra Fields Plugin for Joomla 2.5', '', 'jak2_indexing', 'plugin', 'jak2_indexing', 0, '1.0.0 Alpha', '', 'http://update.joomlart.com/service/tracking/j16/jak2_indexing.xml', '', ''),
(648, 8, 0, 'JA K2 v3 Filter package for J33', '', 'com_jak2v3filter', 'component', '', 1, '3.0.0 preview ', '', 'http://update.joomlart.com/service/tracking/j31/com_jak2v3filter.xml', '', ''),
(649, 8, 0, 'JA Load module Plugin for Joomla 2.5', '', 'jaloadmodule', 'plugin', 'jaloadmodule', 0, '2.5.1', '', 'http://update.joomlart.com/service/tracking/j16/jaloadmodule.xml', '', ''),
(650, 8, 0, 'JA Multilingual Component for Joomla 2.5 & 3.4', '', 'com_jalang', 'component', '', 1, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/com_jalang.xml', 'https://www.joomlart.com/update-steps', ''),
(651, 8, 0, 'JA System Nrain plugin for Joomla 1.6.x', '', 'janrain', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/janrain.xml', '', ''),
(652, 8, 0, 'JA Page Builder Component', '', 'com_japagebuilder', 'component', '', 1, '1.0.0_alpha3', '', 'http://update.joomlart.com/service/tracking/j31/com_japagebuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(653, 8, 0, 'JA Popup plugin for Joomla 1.6', '', 'japopup', 'file', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/japopup.xml', '', ''),
(654, 8, 0, 'S5 Vertex Updater Component', '', 'com_s5_vertexupdater', 'component', '', 1, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/com_s5_vertexupdater.xml', 'https://www.joomlart.com/update-steps', ''),
(655, 8, 0, 'JA System Social plugin for Joomla 1.7', '', 'jasocial', 'file', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j16/jasocial.xml', '', ''),
(656, 8, 0, 'JA T3 System plugin for Joomla 1.6', '', 'jat3', 'plugin', 'jat3', 0, '1.0.0 Beta', '', 'http://update.joomlart.com/service/tracking/j16/jat3.xml', '', ''),
(657, 8, 0, 'JB Corporate Template', '', 'corporate', 'template', '', 0, '3.1.0', '', 'http://update.joomlart.com/service/tracking/j31/corporate.xml', 'https://www.joomlart.com/update-steps', ''),
(658, 8, 0, 'JA Tabs plugin for Joomla 1.6.x', '', 'jatabs', 'plugin', 'jatabs', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/jatabs.xml', '', ''),
(659, 8, 0, 'DT Donate Custom', '', 'dt_donate', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/dt_donate.xml', 'https://www.joomlart.com/update-steps', ''),
(660, 8, 0, 'JA Typo plugin For Joomla 1.6', '', 'jatypo', 'plugin', 'jatypo', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jatypo.xml', '', ''),
(661, 8, 0, 'DT Donate Campaigns Module', '', 'dtdonate_campaigns', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/dtdonate_campaigns.xml', 'https://www.joomlart.com/update-steps', ''),
(662, 8, 0, 'Jomsocial Theme 3.x for JA Social', '', 'jomsocial_theme_social', 'custom', '', 0, '4.7.x', '', 'http://update.joomlart.com/service/tracking/j16/jomsocial_theme_social.xml', 'https://www.joomlart.com/update-steps', ''),
(663, 8, 0, 'DT Donate Donors Module', '', 'dtdonate_donors', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/dtdonate_donors.xml', 'https://www.joomlart.com/update-steps', ''),
(664, 8, 0, 'JA Jomsocial theme for Joomla 2.5', '', 'jomsocial_theme_social_j16', 'file', '', 0, '2.5.1', '', 'http://update.joomlart.com/service/tracking/j16/jomsocial_theme_social_j16.xml', '', ''),
(665, 8, 0, 'JA Intranet Theme for EasyBlog', '', 'easyblog_theme_intranet', 'custom', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/easyblog_theme_intranet.xml', 'https://www.joomlart.com/update-steps', ''),
(666, 8, 0, 'JA Jomsocial theme for Joomla 2.5', '', 'jomsocial_theme_social_j16_26', 'custom', '', 0, '2.5.4', '', 'http://update.joomlart.com/service/tracking/j16/jomsocial_theme_social_j16_26.xml', '', ''),
(667, 8, 0, 'JA Resume Theme for EasyBlog', '', 'easyblog_theme_resume', 'custom', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/easyblog_theme_resume.xml', 'https://www.joomlart.com/update-steps', ''),
(668, 8, 0, 'JShopping Template for Ja Orisite', '', 'jshopping_theme_orisite', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jshopping_theme_orisite.xml', '', ''),
(669, 8, 0, 'JA Sugite Theme for EasyBlog', '', 'easyblog_theme_sugite', 'custom', '', 0, '2.0.3', '', 'http://update.joomlart.com/service/tracking/j31/easyblog_theme_sugite.xml', 'https://www.joomlart.com/update-steps', ''),
(670, 8, 0, 'JA Tiris Jshopping theme for J25 & J3x', '', 'jshopping_theme_tiris', 'custom', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/jshopping_theme_tiris.xml', '', ''),
(671, 8, 0, 'JA Intranet Theme for EasySocialv1 ', '', 'easysocial1_support_intranet', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/easysocial1_support_intranet.xml', 'https://www.joomlart.com/update-steps', ''),
(672, 8, 0, 'Theme for Jshopping j17', '', 'jshopping_theme_tiris_j17', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/jshopping_theme_tiris_j17.xml', '', ''),
(673, 8, 0, 'JA Social II Support EasySocial1', '', 'easysocial1_support_social_ii', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/easysocial1_support_social_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(674, 8, 0, 'JA Kranos kunena theme for Joomla 3.x', '', 'kunena_theme_kranos_j17', 'custom', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_kranos_j17.xml', 'https://www.joomlart.com/update-steps', ''),
(675, 8, 0, 'JB Ecolife Template', '', 'ecolife', 'template', '', 0, '1.3.5', '', 'http://update.joomlart.com/service/tracking/j31/ecolife.xml', 'https://www.joomlart.com/update-steps', ''),
(676, 8, 0, 'Kunena Template for JA Mendozite', '', 'kunena_theme_mendozite', 'custom', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_mendozite.xml', 'https://www.joomlart.com/update-steps', ''),
(677, 8, 0, 'JB Gazetta Template', '', 'gazetta', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/gazetta.xml', 'https://www.joomlart.com/update-steps', ''),
(678, 8, 0, 'JA Mitius Kunena Theme for Joomla 25 ', '', 'kunena_theme_mitius', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_mitius.xml', '', ''),
(679, 8, 0, 'GK Blend Template', '', 'gk_blend', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_blend.xml', 'https://www.joomlart.com/update-steps', ''),
(680, 8, 0, 'Kunena theme for JA Nex J2.5', '', 'kunena_theme_nex_j17', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_nex_j17.xml', '', ''),
(681, 8, 0, 'GK Decor template', '', 'gk_decor', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/gk_decor.xml', 'https://www.joomlart.com/update-steps', ''),
(682, 8, 0, 'Kunena theme for JA Nex T3', '', 'kunena_theme_nex_t3', 'custom', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_nex_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(683, 8, 0, 'GK EvoNews Template', '', 'gk_evonews', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/gk_evonews.xml', 'https://www.joomlart.com/update-steps', ''),
(684, 8, 0, 'Kunena Template for Ja Orisite', '', 'kunena_theme_orisite', 'custom', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_orisite.xml', 'https://www.joomlart.com/update-steps', ''),
(685, 8, 0, 'GK Folio Template', '', 'gk_folio', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_folio.xml', 'https://www.joomlart.com/update-steps', ''),
(686, 8, 0, 'Kunena theme for ja PlayMag', '', 'kunena_theme_playmag', 'custom', '', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_playmag.xml', 'https://www.joomlart.com/update-steps', ''),
(687, 8, 0, 'GK Infinity Template', '', 'gk_infinity', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_infinity.xml', 'https://www.joomlart.com/update-steps', ''),
(688, 8, 0, 'Kunena theme for JA Social T3', '', 'kunena_theme_social', 'custom', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_social.xml', 'https://www.joomlart.com/update-steps', ''),
(689, 8, 0, 'GK Paradise Template', '', 'gk_paradise', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_paradise.xml', 'https://www.joomlart.com/update-steps', ''),
(690, 8, 0, 'Kunena theme for Joomla 2.5', '', 'kunena_theme_social_j16', 'custom', '', 0, '2.5.1', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_social_j16.xml', '', ''),
(691, 8, 0, 'GK Quark template', '', 'gk_quark', 'template', '', 0, '1.27', '', 'http://update.joomlart.com/service/tracking/j31/gk_quark.xml', 'https://www.joomlart.com/update-steps', ''),
(692, 8, 0, 'Kunena theme for ja Techzone', '', 'kunena_theme_techzone', 'custom', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_techzone.xml', 'https://www.joomlart.com/update-steps', ''),
(693, 8, 0, 'GK Siggi Template', '', 'gk_siggi', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_siggi.xml', 'https://www.joomlart.com/update-steps', ''),
(694, 8, 0, 'JA Tiris kunena theme for Joomla 2.5', '', 'kunena_theme_tiris_j16', 'custom', '', 0, '2.5.3', '', 'http://update.joomlart.com/service/tracking/j16/kunena_theme_tiris_j16.xml', '', ''),
(695, 8, 0, 'GK Stora Template', '', 'gk_stora', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/gk_stora.xml', 'https://www.joomlart.com/update-steps', ''),
(696, 8, 0, 'JA Bookshop Theme for Mijoshop V2', '', 'mijoshop_theme_bookshop', 'custom', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/mijoshop_theme_bookshop.xml', 'https://www.joomlart.com/update-steps', ''),
(697, 8, 0, 'GK Wedding Template', '', 'gk_wedding', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_wedding.xml', 'https://www.joomlart.com/update-steps', ''),
(698, 8, 0, 'JA Decor Theme for Mijoshop', '', 'mijoshop_theme_decor', 'custom', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/mijoshop_theme_decor.xml', 'https://www.joomlart.com/update-steps', ''),
(699, 8, 0, 'DT Grid 2 Theme', '', 'grid-2_theme', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/grid-2_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(700, 8, 0, 'JA Decor Theme for Mijoshop V3', '', 'mijoshop_theme_decor_v3', 'custom', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j16/mijoshop_theme_decor_v3.xml', 'https://www.joomlart.com/update-steps', ''),
(701, 8, 0, 'JB Grid3 template', '', 'grid3', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/grid3.xml', 'https://www.joomlart.com/update-steps', ''),
(702, 8, 0, 'JA ACM Module', '', 'mod_ja_acm', 'module', '', 0, '2.1.6', '', 'http://update.joomlart.com/service/tracking/j16/mod_ja_acm.xml', 'https://www.joomlart.com/update-steps', ''),
(703, 8, 0, 'JB Grid4 Template', '', 'grid4', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/grid4.xml', 'https://www.joomlart.com/update-steps', ''),
(704, 8, 0, 'JA Jobs Tags module for Joomla 2.5', '', 'mod_ja_jobs_tags', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j16/mod_ja_jobs_tags.xml', '', ''),
(705, 8, 0, 'Guru Courses Module', '', 'guru-courses-module', 'module', '', 0, '4.0.2', '', 'http://update.joomlart.com/service/tracking/j31/guru-courses-module.xml', 'https://www.joomlart.com/update-steps', ''),
(706, 8, 0, 'JA Accordion Module for J25 & J3x', '', 'mod_jaaccordion', 'module', '', 0, '2.6.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaaccordion.xml', 'https://www.joomlart.com/update-steps', ''),
(707, 8, 0, 'Guru Search Module', '', 'guru-search-module', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/guru-search-module.xml', 'https://www.joomlart.com/update-steps', ''),
(708, 8, 0, 'JA Animation module for Joomla 2.5 & 3.2', '', 'mod_jaanimation', 'module', '', 0, '2.5.3', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaanimation.xml', 'https://www.joomlart.com/update-steps', ''),
(709, 8, 0, 'JA Bulletin Module for J3.x', '', 'mod_jabulletin', 'module', '', 0, '2.6.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_jabulletin.xml', 'https://www.joomlart.com/update-steps', ''),
(710, 8, 0, 'Guru Payment Authorize Plugin', '', 'guru_payauthorize_3.0.0', 'plugin', 'guru_payauthorize_3.', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/guru_payauthorize_3.0.0.xml', 'https://www.joomlart.com/update-steps', ''),
(711, 8, 0, 'JA Latest Comment Module for Joomla 2.5 & 3.3', '', 'mod_jaclatest_comments', 'module', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaclatest_comments.xml', 'https://www.joomlart.com/update-steps', ''),
(712, 8, 0, 'JB Highline2 Template', '', 'highline2', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/highline2.xml', 'https://www.joomlart.com/update-steps', ''),
(713, 8, 0, 'JA Content Popup Module for J25 & J34', '', 'mod_jacontentpopup', 'module', '', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j16/mod_jacontentpopup.xml', 'https://www.joomlart.com/update-steps', ''),
(714, 8, 0, 'JA Content Scroll module for Joomla 1.6', '', 'mod_jacontentscroll', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_jacontentscroll.xml', '', ''),
(715, 8, 0, 'JB Hub2 Template', '', 'hub2', 'template', '', 0, '2.1.3', '', 'http://update.joomlart.com/service/tracking/j31/hub2.xml', 'https://www.joomlart.com/update-steps', ''),
(716, 8, 0, 'JA Contenslider module for Joomla 1.6', '', 'mod_jacontentslide', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_jacontentslide.xml', '', ''),
(717, 8, 0, 'JB Inspire Template', '', 'inspire', 'template', '', 0, '2.1.2', '', 'http://update.joomlart.com/service/tracking/j31/inspire.xml', 'https://www.joomlart.com/update-steps', ''),
(718, 8, 0, 'JA Content Slider Module for J25 & J3x', '', 'mod_jacontentslider', 'module', '', 0, '2.7.4', '', 'http://update.joomlart.com/service/tracking/j16/mod_jacontentslider.xml', 'https://www.joomlart.com/update-steps', ''),
(719, 8, 0, 'JA CountDown Module for Joomla 2.5 & 3.4', '', 'mod_jacountdown', 'module', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/mod_jacountdown.xml', 'https://www.joomlart.com/update-steps', ''),
(720, 8, 0, 'Backend Template', '', 'ja_admin', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_admin.xml', 'https://www.joomlart.com/update-steps', ''),
(721, 8, 0, 'JA Facebook Activity Module for J25 & J30', '', 'mod_jafacebookactivity', 'module', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/mod_jafacebookactivity.xml', 'https://www.joomlart.com/update-steps', ''),
(722, 8, 0, 'JA Aiga Template', '', 'ja_aiga', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_aiga.xml', 'https://www.joomlart.com/update-steps', ''),
(723, 8, 0, 'JA Facebook Like Box Module for Joonla 25 & 34', '', 'mod_jafacebooklikebox', 'module', '', 0, '2.6.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_jafacebooklikebox.xml', 'https://www.joomlart.com/update-steps', ''),
(724, 8, 0, 'JA Allure Template', '', 'ja_allure', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_allure.xml', 'https://www.joomlart.com/update-steps', ''),
(725, 8, 0, 'JA Featured Employer module for Joomla 2.5', '', 'mod_jafeatured_employer', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j16/mod_jafeatured_employer.xml', '', ''),
(726, 8, 0, 'JA Alumni Template', '', 'ja_alumni', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_alumni.xml', 'https://www.joomlart.com/update-steps', ''),
(727, 8, 0, 'JA Filter Jobs module for Joomla 2.5', '', 'mod_jafilter_jobs', 'module', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j16/mod_jafilter_jobs.xml', '', ''),
(728, 8, 0, 'JA Autoshop Source File', '', 'ja_autoshop', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_autoshop.xml', 'https://www.joomlart.com/update-steps', ''),
(729, 8, 0, 'JA flowlist module for Joomla 2.5 & 3.0', '', 'mod_jaflowlist', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaflowlist.xml', 'https://www.joomlart.com/update-steps', ''),
(730, 8, 0, 'JA Beauty Source File', '', 'ja_beauty', 'source', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_beauty.xml', 'https://www.joomlart.com/update-steps', ''),
(731, 8, 0, 'JA Google chart 2', '', 'mod_jagooglechart_2', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/mod_jagooglechart_2.xml', 'https://www.joomlart.com/update-steps', ''),
(732, 8, 0, 'JAEC Halloween Module for Joomla 2.5 & 3', '', 'mod_jahalloween', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/mod_jahalloween.xml', '', ''),
(733, 8, 0, 'JA Biz Template', '', 'ja_biz', 'template', '', 0, '1.2.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_biz.xml', 'https://www.joomlart.com/update-steps', ''),
(734, 8, 0, 'JA Image Hotspot Module for Joomla 2.5 & 3.x', '', 'mod_jaimagehotspot', 'module', '', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaimagehotspot.xml', 'https://www.joomlart.com/update-steps', ''),
(735, 8, 0, 'JA Brickstore Template', '', 'ja_brickstore', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_brickstore.xml', 'https://www.joomlart.com/update-steps', ''),
(736, 8, 0, 'JA static module for Joomla 2.5', '', 'mod_jajb_statistic', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/mod_jajb_statistic.xml', '', ''),
(737, 8, 0, 'JA Builder Template', '', 'ja_builder', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_builder.xml', 'https://www.joomlart.com/update-steps', ''),
(738, 8, 0, 'JA Jobboard Menu module for Joomla 2.5', '', 'mod_jajobboard_menu', 'module', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/mod_jajobboard_menu.xml', '', ''),
(739, 8, 0, 'JA Builder Package', '', 'ja_builder_pkg', 'package', '', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_builder_pkg.xml', 'https://www.joomlart.com/update-steps', ''),
(740, 8, 0, 'JA Jobs Counter module for Joomla 2.5', '', 'mod_jajobs_counter', 'module', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j16/mod_jajobs_counter.xml', '', ''),
(741, 8, 0, 'JA Cago template', '', 'ja_cago', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_cago.xml', '', ''),
(742, 8, 0, 'JA Jobs Map module for Joomla 2.5', '', 'mod_jajobs_map', 'module', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/mod_jajobs_map.xml', '', ''),
(743, 8, 0, 'JA Cagox template', '', 'ja_cagox', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_cagox.xml', 'https://www.joomlart.com/update-steps', ''),
(744, 8, 0, 'JA K2 Fillter Module for Joomla 2.5', '', 'mod_jak2_filter', 'module', '', 0, '1.0.0 Alpha', '', 'http://update.joomlart.com/service/tracking/j16/mod_jak2_filter.xml', '', ''),
(745, 8, 0, 'JA Charity template', '', 'ja_charity', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_charity.xml', 'https://www.joomlart.com/update-steps', ''),
(746, 8, 0, 'JA K2 Filter Module for J25 & J3.x', '', 'mod_jak2filter', 'module', '', 0, '1.3.3', '', 'http://update.joomlart.com/service/tracking/j16/mod_jak2filter.xml', 'https://www.joomlart.com/update-steps', ''),
(747, 8, 0, 'JA City Guide Template', '', 'ja_cityguide', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_cityguide.xml', 'https://www.joomlart.com/update-steps', ''),
(748, 8, 0, 'JA K2 Timeline', '', 'mod_jak2timeline', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_jak2timeline.xml', 'https://www.joomlart.com/update-steps', ''),
(749, 8, 0, 'JA Company Template', '', 'ja_company', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_company.xml', 'https://www.joomlart.com/update-steps', ''),
(750, 8, 0, 'JA Latest Resumes module for Joomla 2.5', '', 'mod_jalatest_resumes', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j16/mod_jalatest_resumes.xml', '', ''),
(751, 8, 0, 'JA Conf Template', '', 'ja_conf', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/ja_conf.xml', 'https://www.joomlart.com/update-steps', ''),
(752, 8, 0, 'JA List Employer module for Joomla 2.5', '', 'mod_jalist_employers', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j16/mod_jalist_employers.xml', '', ''),
(753, 8, 0, 'JA Diner Template', '', 'ja_diner', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_diner.xml', 'https://www.joomlart.com/update-steps', ''),
(754, 8, 0, 'JA List Jobs module for Joomla 2.5', '', 'mod_jalist_jobs', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_jalist_jobs.xml', '', ''),
(755, 8, 0, 'JA Directory Template', '', 'ja_directory', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_directory.xml', 'https://www.joomlart.com/update-steps', ''),
(756, 8, 0, 'JA List Resumes module for Joomla 2.5', '', 'mod_jalist_resumes', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_jalist_resumes.xml', '', ''),
(757, 8, 0, 'JA Donate Template', '', 'ja_donate', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/ja_donate.xml', 'https://www.joomlart.com/update-steps', ''),
(758, 8, 0, 'JA Login module for J25 & J3x', '', 'mod_jalogin', 'module', '', 0, '2.7.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_jalogin.xml', 'https://www.joomlart.com/update-steps', ''),
(759, 8, 0, 'JA Edenite Template for J25 & J34', '', 'ja_edenite', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_edenite.xml', '', ''),
(760, 8, 0, 'JA Masshead Module for J25 & J3x', '', 'mod_jamasshead', 'module', '', 0, '2.6.1', '', 'http://update.joomlart.com/service/tracking/j16/mod_jamasshead.xml', 'https://www.joomlart.com/update-steps', ''),
(761, 8, 0, 'JA Edenite II Template', '', 'ja_edenite_ii', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_edenite_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(762, 8, 0, 'JA News Featured Module for J25 & J3x', '', 'mod_janews_featured', 'module', '', 0, '2.6.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_janews_featured.xml', 'https://www.joomlart.com/update-steps', ''),
(763, 8, 0, 'JA Elicyon Template', '', 'ja_elicyon', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_elicyon.xml', 'https://www.joomlart.com/update-steps', ''),
(764, 8, 0, 'JA Newsflash module for Joomla 1.6.x', '', 'mod_janewsflash', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_janewsflash.xml', '', ''),
(765, 8, 0, 'JA EventCamp Template', '', 'ja_eventcamp', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_eventcamp.xml', 'https://www.joomlart.com/update-steps', ''),
(766, 8, 0, 'JA Newsmoo module for Joomla 1.6.x', '', 'mod_janewsmoo', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_janewsmoo.xml', '', ''),
(767, 8, 0, 'JA Events II template', '', 'ja_events_ii', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_events_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(768, 8, 0, 'JA News Pro Module for J25 & J3x', '', 'mod_janewspro', 'module', '', 0, '2.6.4', '', 'http://update.joomlart.com/service/tracking/j16/mod_janewspro.xml', 'https://www.joomlart.com/update-steps', ''),
(769, 8, 0, 'JA Fit Template', '', 'ja_fit', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_fit.xml', 'https://www.joomlart.com/update-steps', ''),
(770, 8, 0, 'JA Newsticker Module for J3x', '', 'mod_janewsticker', 'module', '', 0, '2.6.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_janewsticker.xml', 'https://www.joomlart.com/update-steps', ''),
(771, 8, 0, 'JA Fixel Template', '', 'ja_fixel', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_fixel.xml', 'https://www.joomlart.com/update-steps', ''),
(772, 8, 0, 'JA Quick Contact Module for J3.x', '', 'mod_jaquickcontact', 'module', '', 0, '2.6.5', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaquickcontact.xml', 'https://www.joomlart.com/update-steps', ''),
(773, 8, 0, 'JA Flix Source File', '', 'ja_flix', 'source', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_flix.xml', 'https://www.joomlart.com/update-steps', ''),
(774, 8, 0, 'JA Recent Viewed Jobs module for Joomla 2.5', '', 'mod_jarecent_viewed_jobs', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/mod_jarecent_viewed_jobs.xml', '', ''),
(775, 8, 0, 'JA Focus Template', '', 'ja_focus', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/ja_focus.xml', 'https://www.joomlart.com/update-steps', ''),
(776, 8, 0, 'JA SideNews Module for J25 & J34', '', 'mod_jasidenews', 'module', '', 0, '2.6.8', '', 'http://update.joomlart.com/service/tracking/j16/mod_jasidenews.xml', 'https://www.joomlart.com/update-steps', ''),
(777, 8, 0, 'JA Good Template', '', 'ja_good', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_good.xml', 'https://www.joomlart.com/update-steps', ''),
(778, 8, 0, 'JA Slideshow Module for Joomla 2.5 & 3.x', '', 'mod_jaslideshow', 'module', '', 0, '2.7.7', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaslideshow.xml', 'https://www.joomlart.com/update-steps', ''),
(779, 8, 0, 'JA Healthcare Template', '', 'ja_healthcare', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_healthcare.xml', 'https://www.joomlart.com/update-steps', ''),
(780, 8, 0, 'JA Slideshow Lite Module for J25 & J3.x', '', 'mod_jaslideshowlite', 'module', '', 0, '1.2.4', '', 'http://update.joomlart.com/service/tracking/j16/mod_jaslideshowlite.xml', 'https://www.joomlart.com/update-steps', ''),
(781, 8, 0, 'JA Hotel Template', '', 'ja_hotel', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_hotel.xml', 'https://www.joomlart.com/update-steps', ''),
(782, 8, 0, 'JA Soccerway Module for J3x', '', 'mod_jasoccerway', 'module', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j16/mod_jasoccerway.xml', 'https://www.joomlart.com/update-steps', ''),
(783, 8, 0, 'JA Insight Template', '', 'ja_insight', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/ja_insight.xml', 'https://www.joomlart.com/update-steps', ''),
(784, 8, 0, 'JA Social Locker module', '', 'mod_jasocial_locker', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_jasocial_locker.xml', 'https://www.joomlart.com/update-steps', ''),
(785, 8, 0, 'JA Intranet Template', '', 'ja_intranet', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_intranet.xml', 'https://www.joomlart.com/update-steps', ''),
(786, 8, 0, 'JA Tab module for Joomla 2.5', '', 'mod_jatabs', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_jatabs.xml', '', ''),
(787, 8, 0, 'ja Justitia Template', '', 'ja_justitia', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_justitia.xml', 'https://www.joomlart.com/update-steps', ''),
(788, 8, 0, 'JA Toppanel Module for Joomla 2.5 & Joomla 3.4', '', 'mod_jatoppanel', 'module', '', 0, '2.5.8', '', 'http://update.joomlart.com/service/tracking/j16/mod_jatoppanel.xml', 'https://www.joomlart.com/update-steps', ''),
(789, 8, 0, 'JA Kids Corner Source File', '', 'ja_kidscorner', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_kidscorner.xml', 'https://www.joomlart.com/update-steps', ''),
(790, 8, 0, 'JA Twitter Module for J25 & j3.x', '', 'mod_jatwitter', 'module', '', 0, '2.6.6', '', 'http://update.joomlart.com/service/tracking/j16/mod_jatwitter.xml', 'https://www.joomlart.com/update-steps', ''),
(791, 8, 0, 'JA User module', '', 'mod_jausers', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/mod_jausers.xml', 'https://www.joomlart.com/update-steps', ''),
(792, 8, 0, 'JA Landscape Template', '', 'ja_landscape', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_landscape.xml', 'https://www.joomlart.com/update-steps', ''),
(793, 8, 0, 'JA List of Voices Module for J2.5 & J3.x', '', 'mod_javlist_voices', 'module', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_javlist_voices.xml', '', ''),
(794, 8, 0, 'JA Lawfirm Template ', '', 'ja_lawfirm', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_lawfirm.xml', 'https://www.joomlart.com/update-steps', ''),
(795, 8, 0, 'JA VMProducts Module', '', 'mod_javmproducts', 'module', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/mod_javmproducts.xml', 'https://www.joomlart.com/update-steps', ''),
(796, 8, 0, 'JA Magz II Template', '', 'ja_magz_ii', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_magz_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(797, 8, 0, 'JA Voice  Work Flow Module for J2.5 & J3.x', '', 'mod_javwork_flow', 'module', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j16/mod_javwork_flow.xml', '', ''),
(798, 8, 0, 'JA Mason Template', '', 'ja_mason', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_mason.xml', 'https://www.joomlart.com/update-steps', ''),
(799, 8, 0, 'S5 Accordion Menu Module', '', 'mod_s5_accordion_menu', 'module', '', 0, '2.3.2', '', 'http://update.joomlart.com/service/tracking/j16/mod_s5_accordion_menu.xml', 'https://www.joomlart.com/update-steps', ''),
(800, 8, 0, 'JA Megafilter Template', '', 'ja_megafilter', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_megafilter.xml', 'https://www.joomlart.com/update-steps', ''),
(801, 8, 0, 'JA Amazon S3 Button Plugin for joomla 2.5 & 3.x', '', 'jaamazons3', 'plugin', 'button', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/plg_button_jaamazons3.xml', 'https://www.joomlart.com/update-steps', ''),
(802, 8, 0, 'JA Megastore Template', '', 'ja_megastore', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_megastore.xml', 'https://www.joomlart.com/update-steps', ''),
(803, 8, 0, 'JA AVTracklist Button plugin for J2.5 & J3.3', '', 'jaavtracklist', 'plugin', 'button', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/plg_button_jaavtracklist.xml', 'https://www.joomlart.com/update-steps', ''),
(804, 8, 0, 'JA Mixstore Source File', '', 'ja_mixstore', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_mixstore.xml', 'https://www.joomlart.com/update-steps', ''),
(805, 8, 0, 'JA Comment Off Plugin for Joomla 2.5 & 3.3', '', 'jacommentoff', 'plugin', 'button', 0, '2.5.3', '', 'http://update.joomlart.com/service/tracking/j16/plg_button_jacommentoff.xml', 'https://www.joomlart.com/update-steps', ''),
(806, 8, 0, 'JA Mono Template', '', 'ja_mono', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_mono.xml', 'https://www.joomlart.com/update-steps', ''),
(807, 8, 0, 'JA Comment On Plugin for Joomla 2.5 & 3.3', '', 'jacommenton', 'plugin', 'button', 0, '2.5.2', '', 'http://update.joomlart.com/service/tracking/j16/plg_button_jacommenton.xml', 'https://www.joomlart.com/update-steps', ''),
(808, 8, 0, 'JA Mood Template', '', 'ja_mood', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_mood.xml', 'https://www.joomlart.com/update-steps', ''),
(809, 8, 0, 'JA Amazon S3 System plugin for joomla 2.5 & 3.x', '', 'plg_jaamazons3', 'plugin', 'plg_jaamazons3', 0, '2.5.9', '', 'http://update.joomlart.com/service/tracking/j16/plg_jaamazons3.xml', 'https://www.joomlart.com/update-steps', ''),
(810, 8, 0, 'JA Morgan Template', '', 'ja_morgan', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_morgan.xml', 'https://www.joomlart.com/update-steps', ''),
(811, 8, 0, 'JA AVTracklist plugin for J2.5 & J3.x', '', 'plg_jaavtracklist', 'plugin', 'plg_jaavtracklist', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/plg_jaavtracklist.xml', 'https://www.joomlart.com/update-steps', ''),
(812, 8, 0, 'JA Moviemax Template', '', 'ja_moviemax', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_moviemax.xml', 'https://www.joomlart.com/update-steps', ''),
(813, 8, 0, 'JA Bookmark plugin for J3.x', '', 'plg_jabookmark', 'plugin', 'plg_jabookmark', 0, '2.6.3', '', 'http://update.joomlart.com/service/tracking/j16/plg_jabookmark.xml', 'https://www.joomlart.com/update-steps', ''),
(814, 8, 0, 'JA Muzic Template for J25 & J3x', '', 'ja_muzic', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_muzic.xml', 'https://www.joomlart.com/update-steps', ''),
(815, 8, 0, 'JA Comment Plugin for Joomla 2.5 & 3.3', '', 'plg_jacomment', 'plugin', 'plg_jacomment', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/plg_jacomment.xml', 'https://www.joomlart.com/update-steps', ''),
(816, 8, 0, 'JA Oslo Template', '', 'ja_oslo', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_oslo.xml', 'https://www.joomlart.com/update-steps', ''),
(817, 8, 0, 'JA Disqus Debate Echo plugin for J3x', '', 'debate_echo', 'plugin', 'jadisqus', 0, '2.6.4', '', 'http://update.joomlart.com/service/tracking/j16/plg_jadisqus_debate_echo.xml', 'https://www.joomlart.com/update-steps', ''),
(818, 8, 0, 'JA Platon Template', '', 'ja_platon', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_platon.xml', 'https://www.joomlart.com/update-steps', '');
INSERT INTO `lbazs_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(819, 8, 0, 'JA GDPR EasyBlog Plugin', '', 'easyblog', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_easyblog.xml', 'https://www.joomlart.com/update-steps', ''),
(820, 8, 0, 'JA Platon Template for DT Register', '', 'ja_platon_for_dt_register', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_platon_for_dt_register.xml', 'https://www.joomlart.com/update-steps', ''),
(821, 8, 0, 'JA GDPR EasyDiscuss Plugin', '', 'easydiscuss', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_easydiscuss.xml', 'https://www.joomlart.com/update-steps', ''),
(822, 8, 0, 'JA Play School Template', '', 'ja_playschool', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_playschool.xml', 'https://www.joomlart.com/update-steps', ''),
(823, 8, 0, 'JA GDPR EasySocial Plugin', '', 'easysocial', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_easysocial.xml', 'https://www.joomlart.com/update-steps', ''),
(824, 8, 0, 'JA Playstore Template', '', 'ja_playstore', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_playstore.xml', 'https://www.joomlart.com/update-steps', ''),
(825, 8, 0, 'JA GDPR Guru Plugin', '', 'guru', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_guru.xml', 'https://www.joomlart.com/update-steps', ''),
(826, 8, 0, 'JA Property Template', '', 'ja_property', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_property.xml', 'https://www.joomlart.com/update-steps', ''),
(827, 8, 0, 'JA GDPR JomSocial Plugin', '', 'jomsocial', 'plugin', 'jagdpr', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_jomsocial.xml', 'https://www.joomlart.com/update-steps', ''),
(828, 8, 0, 'JA Rave Template for Joomla 3.x', '', 'ja_rave', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_rave.xml', '', ''),
(829, 8, 0, 'JA GDPR K2 Plugin', '', 'k2', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_k2.xml', 'https://www.joomlart.com/update-steps', ''),
(830, 8, 0, 'JA Rent template', '', 'ja_rent', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_rent.xml', 'https://www.joomlart.com/update-steps', ''),
(831, 8, 0, 'JA GDPR Kunena Plugin', '', 'kunena', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_kunena.xml', 'https://www.joomlart.com/update-steps', ''),
(832, 8, 0, 'JA Resume Template', '', 'ja_resume', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_resume.xml', 'https://www.joomlart.com/update-steps', ''),
(833, 8, 0, 'JA GDPR Publisher Plugin', '', 'publisher', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_publisher.xml', 'https://www.joomlart.com/update-steps', ''),
(834, 8, 0, 'JA Sensei Source File', '', 'ja_sensei', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_sensei.xml', 'https://www.joomlart.com/update-steps', ''),
(835, 8, 0, 'JA GDPR Virtuemart Plugin', '', 'virtuemart', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagdpr_virtuemart.xml', 'https://www.joomlart.com/update-steps', ''),
(836, 8, 0, 'JA Shoe Template', '', 'ja_shoe', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_shoe.xml', 'https://www.joomlart.com/update-steps', ''),
(837, 8, 0, 'JA Google Storage Plugin for j25 & j30', '', 'plg_jagooglestorage', 'plugin', 'plg_jagooglestorage', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagooglestorage.xml', '', ''),
(838, 8, 0, 'JA Simpli Template', '', 'ja_simpli', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_simpli.xml', 'https://www.joomlart.com/update-steps', ''),
(839, 8, 0, 'JA Translate plugin for Joomla 1.6.x', '', 'plg_jagoogletranslate', 'file', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_jagoogletranslate.xml', '', ''),
(840, 8, 0, 'JA Smallbiz Template', '', 'ja_smallbiz', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_smallbiz.xml', 'https://www.joomlart.com/update-steps', ''),
(841, 8, 0, 'JA Megafilter VirtueMart Plugin', '', 'virtuemart', 'plugin', 'jamegafilter', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j16/plg_jamegafilter_virtuemart.xml', 'https://www.joomlart.com/update-steps', ''),
(842, 8, 0, 'JA Social II template', '', 'ja_social_ii', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_social_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(843, 8, 0, 'JA Thumbnail Plugin for J25 & J3', '', 'plg_jathumbnail', 'plugin', 'plg_jathumbnail', 0, '2.6.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_jathumbnail.xml', 'https://www.joomlart.com/update-steps', ''),
(844, 8, 0, 'JA Space Template', '', 'ja_space', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_space.xml', 'https://www.joomlart.com/update-steps', ''),
(845, 8, 0, 'JA Tooltips plugin for Joomla 1.6.x', '', 'plg_jatooltips', 'plugin', 'plg_jatooltips', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_jatooltips.xml', '', ''),
(846, 8, 0, 'JA Stark Template', '', 'ja_stark', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_stark.xml', 'https://www.joomlart.com/update-steps', ''),
(847, 8, 0, 'JA Typo Button Plugin for J25 & J3x', '', 'plg_jatypobutton', 'plugin', 'plg_jatypobutton', 0, '2.6.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_jatypobutton.xml', 'https://www.joomlart.com/update-steps', ''),
(848, 8, 0, 'JA Symphony Template', '', 'ja_symphony', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_symphony.xml', 'https://www.joomlart.com/update-steps', ''),
(849, 8, 0, 'JA K2 Filter Plg for J25 & J3.x', '', 'jak2filter', 'plugin', 'k2', 0, '1.3.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_k2_jak2filter.xml', 'https://www.joomlart.com/update-steps', ''),
(850, 8, 0, 'JA K2 Timeline Plugin', '', 'jak2timeline', 'plugin', 'k2', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_k2_jak2timeline.xml', 'https://www.joomlart.com/update-steps', ''),
(851, 8, 0, 'JA Techzone Template', '', 'ja_techzone', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_techzone.xml', 'https://www.joomlart.com/update-steps', ''),
(852, 8, 0, 'Multi Capcha Engine Plugin for J3.x', '', 'captcha_engine', 'plugin', 'multiple', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j16/plg_multiple_captcha_engine.xml', 'https://www.joomlart.com/update-steps', ''),
(853, 8, 0, 'JA Teline V Template', '', 'ja_teline_v', 'template', '', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_teline_v.xml', 'https://www.joomlart.com/update-steps', ''),
(854, 8, 0, 'JA JobBoard Payment Plugin Authorize for Joomla 2.5', '', 'plg_payment_jajb_authorize_25', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_payment_jajb_authorize_25.xml', '', ''),
(855, 8, 0, 'JA JobBoard Payment Plugin MoneyBooker for Joomla 2.5', '', 'plg_payment_jajb_moneybooker_25', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_payment_jajb_moneybooker_25.xml', '', ''),
(856, 8, 0, 'JA University Template for J25 & J32', '', 'ja_university', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_university.xml', 'https://www.joomlart.com/update-steps', ''),
(857, 8, 0, 'JA JobBoard Payment Plugin Paypal for Joomla 2.5', '', 'plg_payment_jajb_paypal_25', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_payment_jajb_paypal_25.xml', '', ''),
(858, 8, 0, 'JA University T3 template', '', 'ja_university_t3', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_university_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(859, 8, 0, 'JA JobBoard Payment Plugin BankWire for Joomla 2.5', '', 'plg_payment_jajb_wirebank_25', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_payment_jajb_wirebank_25.xml', '', ''),
(860, 8, 0, 'JA Vintas Template for J25 & J3x', '', 'ja_vintas', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/ja_vintas.xml', 'https://www.joomlart.com/update-steps', ''),
(861, 8, 0, 'JA Search Comment Plugin for Joomla J2.5 & 3.x', '', 'jacomment', 'plugin', 'search', 0, '2.5.2', '', 'http://update.joomlart.com/service/tracking/j16/plg_search_jacomment.xml', 'https://www.joomlart.com/update-steps', ''),
(862, 8, 0, 'JA Wall Template for J25 & J34', '', 'ja_wall', 'template', '', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_wall.xml', '', ''),
(863, 8, 0, 'JA Search Jobs plugin for Joomla 2.5', '', 'jajob', 'plugin', 'search', 0, '1.0.0 stable', '', 'http://update.joomlart.com/service/tracking/j16/plg_search_jajob.xml', '', ''),
(864, 8, 0, 'JB Ascent2 Template', '', 'jb_ascent2', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_ascent2.xml', 'https://www.joomlart.com/update-steps', ''),
(865, 8, 0, 'JA Builder System Plugin', '', 'jabuilder', 'plugin', 'system', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jabuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(866, 8, 0, 'JB Base3 Template', '', 'jb_base3', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/jb_base3.xml', 'https://www.joomlart.com/update-steps', ''),
(867, 8, 0, 'JA System Comment Plugin for Joomla 2.5 & 3.3', '', 'jacomment', 'plugin', 'system', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jacomment.xml', 'https://www.joomlart.com/update-steps', ''),
(868, 8, 0, 'JB Blanko Template', '', 'jb_blanko', 'template', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_blanko.xml', 'https://www.joomlart.com/update-steps', ''),
(869, 8, 0, 'JA Content Extra Fields for Joomla 2.5', '', 'jacontentextrafields', 'plugin', 'system', 0, '2.5.1', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jacontentextrafields.xml', '', ''),
(870, 8, 0, 'JB Client Template', '', 'jb_client', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_client.xml', 'https://www.joomlart.com/update-steps', ''),
(871, 8, 0, 'JA System Google Map plugin for Joomla 2.5 & J3x', '', 'jagooglemap', 'plugin', 'system', 0, '2.6.6', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jagooglemap.xml', 'https://www.joomlart.com/update-steps', ''),
(872, 8, 0, 'JB Colourshift2 Template', '', 'jb_colourshift2', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/jb_colourshift2.xml', 'https://www.joomlart.com/update-steps', ''),
(873, 8, 0, 'JAEC PLG System Jobboad Jomsocial Synchonization', '', 'jajb_jomsocial', 'plugin', 'system', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jajb_jomsocial.xml', '', ''),
(874, 8, 0, 'JB Corporation Template', '', 'jb_corporation', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/jb_corporation.xml', 'https://www.joomlart.com/update-steps', ''),
(875, 8, 0, 'JA System Lazyload Plugin for J25 & J3x', '', 'jalazyload', 'plugin', 'system', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jalazyload.xml', 'https://www.joomlart.com/update-steps', ''),
(876, 8, 0, 'JB Flux Template', '', 'jb_flux', 'template', '', 0, '2.0.4', '', 'http://update.joomlart.com/service/tracking/j31/jb_flux.xml', 'https://www.joomlart.com/update-steps', ''),
(877, 8, 0, 'JA Megafilter Plugin', '', 'jamegafilter', 'plugin', 'system', 0, '1.0.0 Beta 1', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jamegafilter.xml', 'https://www.joomlart.com/update-steps', ''),
(878, 8, 0, 'JB Focus2 Template', '', 'jb_focus2', 'template', '', 0, '1.3.7', '', 'http://update.joomlart.com/service/tracking/j31/jb_focus2.xml', 'https://www.joomlart.com/update-steps', ''),
(879, 8, 0, 'JA System Nrain Plugin for Joomla 2.5 & 3.3', '', 'janrain', 'plugin', 'system', 0, '2.5.4', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_janrain.xml', '', ''),
(880, 8, 0, 'JB Italian Template', '', 'jb_italian', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/jb_italian.xml', 'https://www.joomlart.com/update-steps', ''),
(881, 8, 0, 'JA System Pager Plugin for J25 & J3x', '', 'japager', 'plugin', 'system', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_japager.xml', 'https://www.joomlart.com/update-steps', ''),
(882, 8, 0, 'JB Maxbiz2 Template', '', 'jb_maxbiz2', 'template', '', 0, '1.4.3', '', 'http://update.joomlart.com/service/tracking/j31/jb_maxbiz2.xml', 'https://www.joomlart.com/update-steps', ''),
(883, 8, 0, 'JA Popup Plugin for Joomla 25 & 3x', '', 'japopup', 'plugin', 'system', 0, '2.6.4', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_japopup.xml', 'https://www.joomlart.com/update-steps', ''),
(884, 8, 0, 'JB Moments Template', '', 'jb_moments', 'template', '', 0, '1.4.3', '', 'http://update.joomlart.com/service/tracking/j31/jb_moments.xml', 'https://www.joomlart.com/update-steps', ''),
(885, 8, 0, 'JA System Social Plugin for Joomla 3.x', '', 'jasocial', 'plugin', 'system', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jasocial.xml', 'https://www.joomlart.com/update-steps', ''),
(886, 8, 0, 'JB Motion Template', '', 'jb_motion', 'template', '', 0, '2.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_motion.xml', 'https://www.joomlart.com/update-steps', ''),
(887, 8, 0, 'JA System Social Feed Plugin for Joomla 2.5 & 3.x', '', 'jasocial_feed', 'plugin', 'system', 0, '1.4.2', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jasocial_feed.xml', 'https://www.joomlart.com/update-steps', ''),
(888, 8, 0, 'JB Revision Template', '', 'jb_revision', 'template', '', 0, '1.2.9', '', 'http://update.joomlart.com/service/tracking/j31/jb_revision.xml', 'https://www.joomlart.com/update-steps', ''),
(889, 8, 0, 'JA T3v2 System Plugin for J3.x', '', 'jat3', 'plugin', 'system', 0, '2.7.6', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jat3.xml', 'https://www.joomlart.com/update-steps', ''),
(890, 8, 0, 'JB SideWinder tpl', '', 'jb_sidewinder', 'template', '', 0, '2.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_sidewinder.xml', 'https://www.joomlart.com/update-steps', ''),
(891, 8, 0, 'JA T3v3 System Plugin', '', 'jat3v3', 'plugin', 'system', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jat3v3.xml', 'https://www.joomlart.com/update-steps', ''),
(892, 8, 0, 'JB Utafiti Template', '', 'jb_utafiti', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/jb_utafiti.xml', 'https://www.joomlart.com/update-steps', ''),
(893, 8, 0, 'JA Tabs Plugin for J3.x', '', 'jatabs', 'plugin', 'system', 0, '2.6.7', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jatabs.xml', 'https://www.joomlart.com/update-steps', ''),
(894, 8, 0, 'JB Venture Template', '', 'jb_venture', 'template', '', 0, '1.3.6', '', 'http://update.joomlart.com/service/tracking/j31/jb_venture.xml', 'https://www.joomlart.com/update-steps', ''),
(895, 8, 0, 'JA Typo Plugin for Joomla 2.5 & J3x', '', 'jatypo', 'plugin', 'system', 0, '2.5.8', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_jatypo.xml', 'https://www.joomlart.com/update-steps', ''),
(896, 8, 0, 'JS Column Theme', '', 'jomsocial_column', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_column.xml', 'https://www.joomlart.com/update-steps', ''),
(897, 8, 10024, 'T3 System Plugin', '', 't3', 'plugin', 'system', 0, '2.7.5', '', 'http://update.joomlart.com/service/tracking/j16/plg_system_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(898, 8, 0, 'JS Flat Theme', '', 'jomsocial_flat', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_flat.xml', 'https://www.joomlart.com/update-steps', ''),
(899, 8, 0, 'T3 B3 Blank Template', '', 't3_bs3_blank', 'template', '', 0, '2.1.4', '', 'http://update.joomlart.com/service/tracking/j16/t3_bs3_blank.xml', '', ''),
(900, 8, 0, 'JS Column Theme', '', 'jomsocial_jasocial', 'custom', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_jasocial.xml', 'https://www.joomlart.com/update-steps', ''),
(901, 8, 0, 'JA Teline III Template for Joomla 2.5', '', 'teline_iii', 'template', '', 0, '2.5.3', '', 'http://update.joomlart.com/service/tracking/j16/teline_iii.xml', 'https://www.joomlart.com/update-steps', ''),
(902, 8, 0, 'JS Kikiriki Theme', '', 'jomsocial_kikiriki', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_kikiriki.xml', 'https://www.joomlart.com/update-steps', ''),
(903, 8, 0, 'Thirdparty Extensions Compatibility Bundle', '', 'thirdparty_exts_compatibility', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j16/thirdparty_exts_compatibility.xml', '', ''),
(904, 8, 0, 'JS Shadow Theme', '', 'jomsocial_shadow', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_shadow.xml', 'https://www.joomlart.com/update-steps', ''),
(905, 8, 0, 'T3 Blank Template', '', 't3_blank', 'template', '', 0, '2.2.1', '', 'http://update.joomlart.com/service/tracking/j16/tpl_t3_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(906, 8, 0, 'Jomsocial theme for Platon', '', 'jomsocial_theme_platon', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_theme_platon.xml', 'https://www.joomlart.com/update-steps', ''),
(907, 8, 0, 'T4 Blank Template', '', 't4_blank', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j16/tpl_t4_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(908, 8, 0, 'JS Column Theme', '', 'js_column', 'custom', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/js_column.xml', 'https://www.joomlart.com/update-steps', ''),
(909, 8, 0, 'Uber Template', '', 'uber', 'template', '', 0, '2.2.0', '', 'http://update.joomlart.com/service/tracking/j16/uber.xml', 'https://www.joomlart.com/update-steps', ''),
(910, 8, 0, 'JS Flat Theme', '', 'js_flat', 'custom', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/js_flat.xml', 'https://www.joomlart.com/update-steps', ''),
(911, 8, 0, 'S5 Big Business Template', '', 'Shape5_big_business_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j30/Shape5_big_business_template.xml', 'https://www.joomlart.com/update-steps', ''),
(912, 8, 0, 'JS Kikiriki Theme', '', 'js_kikiriki', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/js_kikiriki.xml', 'https://www.joomlart.com/update-steps', ''),
(913, 8, 0, 'S5 Light Church Template', '', 'Shape5_lightchurch_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j30/Shape5_lightchurch_template.xml', 'https://www.joomlart.com/update-steps', ''),
(914, 8, 0, 'JS Shadow Theme', '', 'js_shadow', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/js_shadow.xml', 'https://www.joomlart.com/update-steps', ''),
(915, 8, 0, 'Backend Template Package', '', 'backend_template_package', 'package', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j30/backend_template_package.xml', 'https://www.joomlart.com/update-steps', ''),
(916, 8, 0, 'Theme Fixel for JShopping J25 & J3x', '', 'jshopping_theme_fixel', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/jshopping_theme_fixel.xml', 'https://www.joomlart.com/update-steps', ''),
(917, 8, 0, 'Backend Template', '', 'backend_template_pkg', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j30/backend_template_pkg.xml', '', ''),
(918, 8, 0, 'JA Tiris Jshopping theme for J3x', '', 'jshopping_theme_tiris_j3x', 'custom', '', 0, '2.5.8', '', 'http://update.joomlart.com/service/tracking/j31/jshopping_theme_tiris_j3x.xml', 'https://www.joomlart.com/update-steps', ''),
(919, 8, 0, 'JomSocial Free Bonuses', '', 'bonus', 'package', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j30/bonus.xml', 'https://www.joomlart.com/update-steps', ''),
(920, 8, 0, 'JB Koan Template', '', 'koan', 'template', '', 0, '1.2.2', '', 'http://update.joomlart.com/service/tracking/j31/koan.xml', 'https://www.joomlart.com/update-steps', ''),
(921, 8, 0, 'Adagency Light Component', '', 'com_adagency-light', 'component', '', 1, '6.0.21', '', 'http://update.joomlart.com/service/tracking/j30/com_adagency-light.xml', 'https://www.joomlart.com/update-steps', ''),
(922, 8, 0, 'JA Mitius Kunena Theme for Joomla 3x', '', 'kunena_theme_mitius', 'custom', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_mitius.xml', 'https://www.joomlart.com/update-steps', ''),
(923, 8, 0, 'DT Register Component', '', 'com_dtregister', 'component', '', 1, '4.2.6', '', 'http://update.joomlart.com/service/tracking/j30/com_dtregister.xml', 'https://www.joomlart.com/update-steps', ''),
(924, 8, 0, 'JA Tiris Kunena Theme for Joomla 3x', '', 'kunena_theme_mitius_j31', 'custom', '', 0, '2.5.4', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_mitius_j31.xml', '', ''),
(925, 8, 0, 'DT SMS Package', '', 'com_dtsms_pkg', 'package', '', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j30/com_dtsms_pkg.xml', 'https://www.joomlart.com/update-steps', ''),
(926, 8, 0, 'Kunena Theme MovieMax', '', 'kunena_theme_moviemax', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_moviemax.xml', 'https://www.joomlart.com/update-steps', ''),
(927, 8, 0, 'Guru Light', '', 'com_guru_light', 'component', '', 1, '5.2.2', '', 'http://update.joomlart.com/service/tracking/j30/com_guru_light.xml', 'https://www.joomlart.com/update-steps', ''),
(928, 8, 0, 'Kunena Theme Platon', '', 'kunena_theme_platon', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_platon.xml', 'https://www.joomlart.com/update-steps', ''),
(929, 8, 0, 'Guru Pro', '', 'com_guru_pro', 'component', '', 1, '5.1.3', '', 'http://update.joomlart.com/service/tracking/j30/com_guru_pro.xml', 'https://www.joomlart.com/update-steps', ''),
(930, 8, 0, 'Kunena Theme Playstore', '', 'kunena_theme_playstore', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_playstore.xml', 'https://www.joomlart.com/update-steps', ''),
(931, 8, 0, 'iSEO Light', '', 'com_iseo-light', 'component', '', 1, '3.2.0', '', 'http://update.joomlart.com/service/tracking/j30/com_iseo-light.xml', 'https://www.joomlart.com/update-steps', ''),
(932, 8, 0, 'JA Tiris Kunena Theme for Joomla 3x', '', 'kunena_theme_tiris_j3x', 'custom', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_tiris_j3x.xml', 'https://www.joomlart.com/update-steps', ''),
(933, 8, 0, 'iSEO Pro', '', 'com_iseo-pro', 'component', '', 1, '3.2.0', '', 'http://update.joomlart.com/service/tracking/j30/com_iseo-pro.xml', 'https://www.joomlart.com/update-steps', ''),
(934, 8, 0, 'JomSocial Pro', '', 'com_jomsocial', 'component', '', 1, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j30/com_jomsocial.xml', 'https://www.joomlart.com/update-steps', ''),
(935, 8, 0, 'DT Linear Theme', '', 'linear_theme', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/linear_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(936, 8, 0, 'DT Register JEvents Update', '', 'dt_reg_jevents_update_plg_3.2', 'plugin', 'dt_reg_jevents_updat', 0, '3.3', '', 'http://update.joomlart.com/service/tracking/j30/dt_reg_jevents_update_plg_3.2.xml', 'https://www.joomlart.com/update-steps', ''),
(937, 8, 0, 'JB Medica Template', '', 'medica', 'template', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/medica.xml', 'https://www.joomlart.com/update-steps', ''),
(938, 8, 0, 'DT Acajoom Subscriber', '', 'dtregister_acajoom', 'plugin', 'dtregister_acajoom', 0, '1.2', '', 'http://update.joomlart.com/service/tracking/j30/dtregister_acajoom.xml', 'https://www.joomlart.com/update-steps', ''),
(939, 8, 0, 'Mijoshop V2 Modules Accordion', '', 'mijoshop_mod_accordion', 'custom', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_accordion.xml', 'https://www.joomlart.com/update-steps', ''),
(940, 8, 0, 'DT Register Falang plugin', '', 'dtregister_falang', 'plugin', 'dtregister_falang', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j30/dtregister_falang.xml', 'https://www.joomlart.com/update-steps', ''),
(941, 8, 0, 'Mijoshop V3 Modules Accordion', '', 'mijoshop_mod_accordion_v3', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_accordion_v3.xml', 'https://www.joomlart.com/update-steps', ''),
(942, 8, 0, 'JB Novus Easyblog Theme', '', 'easyblog5_theme_novus', 'custom', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j30/easyblog5_theme_novus.xml', 'https://www.joomlart.com/update-steps', ''),
(943, 8, 0, 'Mijoshop V2 Modules Slider', '', 'mijoshop_mod_slider', 'custom', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_slider.xml', 'https://www.joomlart.com/update-steps', ''),
(944, 8, 0, 'Mijoshop V3 Modules Slider', '', 'mijoshop_mod_slider_v3', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_slider_v3.xml', 'https://www.joomlart.com/update-steps', ''),
(945, 8, 0, 'GK bluap', '', 'gk_bluap', 'template', '', 0, '3.24', '', 'http://update.joomlart.com/service/tracking/j30/gk_bluap.xml', 'https://www.joomlart.com/update-steps', ''),
(946, 8, 0, 'JA Bookshop Theme for Mijoshop V3', '', 'mijoshop_theme_bookshop_v3', 'custom', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_theme_bookshop_v3.xml', 'https://www.joomlart.com/update-steps', ''),
(947, 8, 0, 'GK box J!3', '', 'gk_box', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j30/gk_box.xml', 'https://www.joomlart.com/update-steps', ''),
(948, 8, 0, 'S5 Tell A Friend Module', '', 'mod_S5tellafriend', 'module', '', 0, '3.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_S5tellafriend.xml', 'https://www.joomlart.com/update-steps', ''),
(949, 8, 0, 'GK cloudhost J!3', '', 'gk_cloudhost', 'template', '', 0, '3.24', '', 'http://update.joomlart.com/service/tracking/j30/gk_cloudhost.xml', 'https://www.joomlart.com/update-steps', ''),
(950, 8, 0, 'JS Toolbar Module', '', 'mod_community_bar', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_bar.xml', 'https://www.joomlart.com/update-steps', ''),
(951, 8, 0, 'GK creativity J!3', '', 'gk_creativity', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_creativity.xml', 'https://www.joomlart.com/update-steps', ''),
(952, 8, 0, 'JS Birthdays Module', '', 'mod_community_birthdays', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_birthdays.xml', 'https://www.joomlart.com/update-steps', ''),
(953, 8, 0, 'GK events J!3', '', 'gk_events', 'template', '', 0, '3.26', '', 'http://update.joomlart.com/service/tracking/j30/gk_events.xml', 'https://www.joomlart.com/update-steps', ''),
(954, 8, 0, 'JS Dating Search Module', '', 'mod_community_dating', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_dating.xml', 'https://www.joomlart.com/update-steps', ''),
(955, 8, 0, 'GK game J!3', '', 'gk_game', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_game.xml', 'https://www.joomlart.com/update-steps', ''),
(956, 8, 0, 'JS Events Suggestions', '', 'mod_community_eventssuggestions', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_eventssuggestions.xml', 'https://www.joomlart.com/update-steps', ''),
(957, 8, 0, 'GK hotel J!3', '', 'gk_hotel', 'template', '', 0, '1.2.3', '', 'http://update.joomlart.com/service/tracking/j30/gk_hotel.xml', 'https://www.joomlart.com/update-steps', ''),
(958, 8, 0, 'JS Friends Suggestions', '', 'mod_community_friendssuggestions', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_friendssuggestions.xml', 'https://www.joomlart.com/update-steps', ''),
(959, 8, 0, 'GK instyle J!3', '', 'gk_instyle', 'template', '', 0, '3.32', '', 'http://update.joomlart.com/service/tracking/j30/gk_instyle.xml', 'https://www.joomlart.com/update-steps', ''),
(960, 8, 0, 'JS Groups Suggestions', '', 'mod_community_groupssuggestions', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_groupssuggestions.xml', 'https://www.joomlart.com/update-steps', ''),
(961, 8, 0, 'GK john s J!3', '', 'gk_john_s', 'template', '', 0, '3.26', '', 'http://update.joomlart.com/service/tracking/j30/gk_john_s.xml', 'https://www.joomlart.com/update-steps', ''),
(962, 8, 0, 'JS Members Map', '', 'mod_community_membersmap', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_membersmap.xml', 'https://www.joomlart.com/update-steps', ''),
(963, 8, 0, 'GK magazine J!3', '', 'gk_magazine', 'template', '', 0, '3.23', '', 'http://update.joomlart.com/service/tracking/j30/gk_magazine.xml', 'https://www.joomlart.com/update-steps', ''),
(964, 8, 0, 'JS Popular Events', '', 'mod_community_popular_events', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_popular_events.xml', 'https://www.joomlart.com/update-steps', ''),
(965, 8, 0, 'GK mo J!3', '', 'gk_mo', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_mo.xml', 'https://www.joomlart.com/update-steps', ''),
(966, 8, 0, 'JS Popular Groups', '', 'mod_community_popular_groups', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_popular_groups.xml', 'https://www.joomlart.com/update-steps', ''),
(967, 8, 0, 'GK msocial J!3', '', 'gk_msocial', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_msocial.xml', 'https://www.joomlart.com/update-steps', ''),
(968, 8, 0, 'JS Profile Completeness', '', 'mod_community_profilecompleteness', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_profilecompleteness.xml', 'https://www.joomlart.com/update-steps', ''),
(969, 8, 0, 'GK musicstate J!3', '', 'gk_musicstate', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_musicstate.xml', 'https://www.joomlart.com/update-steps', ''),
(970, 8, 0, 'Community Toolbar Module', '', 'mod_community_toolbar', 'module', '', 0, '2.1.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_toolbar.xml', 'https://www.joomlart.com/update-steps', ''),
(971, 8, 0, 'GK news J!3', '', 'gk_news', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_news.xml', 'https://www.joomlart.com/update-steps', ''),
(972, 8, 0, 'JS Trending Events', '', 'mod_community_trending_events', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_events.xml', 'https://www.joomlart.com/update-steps', ''),
(973, 8, 0, 'GK news2 J!3', '', 'gk_news2', 'template', '', 0, '3.26', '', 'http://update.joomlart.com/service/tracking/j30/gk_news2.xml', 'https://www.joomlart.com/update-steps', ''),
(974, 8, 0, 'JS Trending Groups', '', 'mod_community_trending_groups', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_groups.xml', 'https://www.joomlart.com/update-steps', ''),
(975, 8, 0, 'JS Trending Hashtags', '', 'mod_community_trending_hashtags', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_hashtags.xml', 'https://www.joomlart.com/update-steps', ''),
(976, 8, 0, 'GK news refreshed J!3', '', 'gk_news_refreshed', 'template', '', 0, '3.29', '', 'http://update.joomlart.com/service/tracking/j30/gk_news_refreshed.xml', 'https://www.joomlart.com/update-steps', ''),
(977, 8, 0, 'JS Trending Photo', '', 'mod_community_trending_photos', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_photos.xml', 'https://www.joomlart.com/update-steps', ''),
(978, 8, 0, 'GK photo J!3', '', 'gk_photo', 'template', '', 0, '3.29', '', 'http://update.joomlart.com/service/tracking/j30/gk_photo.xml', 'https://www.joomlart.com/update-steps', ''),
(979, 8, 0, 'JS Trending Video', '', 'mod_community_trending_videos', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_videos.xml', 'https://www.joomlart.com/update-steps', ''),
(980, 8, 0, 'GK portfolio J!3', '', 'gk_portfolio', 'template', '', 0, '1.2.2', '', 'http://update.joomlart.com/service/tracking/j30/gk_portfolio.xml', 'https://www.joomlart.com/update-steps', ''),
(981, 8, 0, 'Dribbblr Module', '', 'mod_dribbblr', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_dribbblr.xml', 'https://www.joomlart.com/update-steps', ''),
(982, 8, 0, 'GK rockwall J!3', '', 'gk_rockwall', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_rockwall.xml', 'https://www.joomlart.com/update-steps', ''),
(983, 8, 0, 'JA Google Analytics Frontend', '', 'mod_ja_ga', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_ja_ga.xml', 'https://www.joomlart.com/update-steps', ''),
(984, 8, 0, 'GK Shop & Buy Template', '', 'gk_shop_and_buy', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_shop_and_buy.xml', 'https://www.joomlart.com/update-steps', ''),
(985, 8, 0, 'JA Latest  Article Module', '', 'mod_jaarticles_latest', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jaarticles_latest.xml', 'https://www.joomlart.com/update-steps', ''),
(986, 8, 0, 'GK simplicity J!3', '', 'gk_simplicity', 'template', '', 0, '3.23', '', 'http://update.joomlart.com/service/tracking/j30/gk_simplicity.xml', 'https://www.joomlart.com/update-steps', ''),
(987, 8, 0, 'JA Builder Admin Menu Module', '', 'mod_jabuilder_admin_menu', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jabuilder_admin_menu.xml', 'https://www.joomlart.com/update-steps', ''),
(988, 8, 0, 'GK steakhouse J!3', '', 'gk_steakhouse', 'template', '', 0, '3.26', '', 'http://update.joomlart.com/service/tracking/j30/gk_steakhouse.xml', 'https://www.joomlart.com/update-steps', ''),
(989, 8, 0, 'JA Builder Quickicons Module', '', 'mod_jabuilder_quickicons', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jabuilder_quickicons.xml', 'https://www.joomlart.com/update-steps', ''),
(990, 8, 0, 'GK storebox J!3', '', 'gk_storebox', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_storebox.xml', 'https://www.joomlart.com/update-steps', ''),
(991, 8, 0, 'JA Finance Module', '', 'mod_jafinance', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jafinance.xml', 'https://www.joomlart.com/update-steps', ''),
(992, 8, 0, 'GK storefront J!3', '', 'gk_storefront', 'template', '', 0, '3.25', '', 'http://update.joomlart.com/service/tracking/j30/gk_storefront.xml', 'https://www.joomlart.com/update-steps', ''),
(993, 8, 0, 'JA Google Analytics', '', 'mod_jagoogle_analytics', 'module', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/mod_jagoogle_analytics.xml', 'https://www.joomlart.com/update-steps', ''),
(994, 8, 0, 'GK technews J!3', '', 'gk_technews', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j30/gk_technews.xml', 'https://www.joomlart.com/update-steps', ''),
(995, 8, 0, 'JA Google Chart Module', '', 'mod_jagooglechart', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jagooglechart.xml', 'https://www.joomlart.com/update-steps', ''),
(996, 8, 0, 'GK university J!3', '', 'gk_university', 'template', '', 0, '3.26', '', 'http://update.joomlart.com/service/tracking/j30/gk_university.xml', 'https://www.joomlart.com/update-steps', ''),
(997, 8, 0, 'JA Halloween Game for Joomla 3.x', '', 'mod_jahalloweengame', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jahalloweengame.xml', 'https://www.joomlart.com/update-steps', ''),
(998, 8, 0, 'GK writer J!3', '', 'gk_writer', 'template', '', 0, '3.27', '', 'http://update.joomlart.com/service/tracking/j30/gk_writer.xml', 'https://www.joomlart.com/update-steps', ''),
(999, 8, 0, 'JA K2 v3 Filter Module for J33', '', 'mod_jak2v3filter', 'module', '', 0, '3.0.0 preview ', '', 'http://update.joomlart.com/service/tracking/j31/mod_jak2v3filter.xml', '', ''),
(1000, 8, 0, 'DT Grid Theme', '', 'grid_theme', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j30/grid_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(1001, 8, 0, 'JA Masthead Module ', '', 'mod_jamasthead', 'module', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_jamasthead.xml', 'https://www.joomlart.com/update-steps', ''),
(1002, 8, 0, 'GURU Payment Plugin - 2checkout', '', 'guru-2checkout-plugin_1.0.0', 'plugin', 'guru-2checkout-plugi', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j30/guru-2checkout-plugin_1.0.0.xml', 'https://www.joomlart.com/update-steps', ''),
(1003, 8, 0, 'JA Megafilter Module', '', 'mod_jamegafilter', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/mod_jamegafilter.xml', 'https://www.joomlart.com/update-steps', ''),
(1004, 8, 0, 'Guru Courses Module', '', 'guru-courses-module', 'module', '', 0, '3.0.5', '', 'http://update.joomlart.com/service/tracking/j30/guru-courses-module.xml', 'https://www.joomlart.com/update-steps', ''),
(1005, 8, 0, 'JA Promo Bar module', '', 'mod_japromobar', 'module', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_japromobar.xml', 'https://www.joomlart.com/update-steps', ''),
(1006, 8, 0, 'System - iJSEO Page Scan Plugin', '', 'ijseo_page_scan_plugin', 'plugin', 'ijseo_page_scan_plug', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j30/ijseo_page_scan_plugin.xml', 'https://www.joomlart.com/update-steps', ''),
(1007, 8, 0, 'Ja Yahoo Finance', '', 'mod_jayahoo_finance', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jayahoo_finance.xml', 'https://www.joomlart.com/update-steps', ''),
(1008, 8, 0, 'JA Restaurant Template', '', 'ja_restaurant', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j30/ja_restaurant.xml', 'https://www.joomlart.com/update-steps', ''),
(1009, 8, 0, 'JA Yahoo Weather', '', 'mod_jayahoo_weather', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jayahoo_weather.xml', 'https://www.joomlart.com/update-steps', ''),
(1010, 8, 0, 'JB Ignite Template', '', 'jb_ignite', 'template', '', 0, '1.4.7', '', 'http://update.joomlart.com/service/tracking/j30/jb_ignite.xml', 'https://www.joomlart.com/update-steps', ''),
(1011, 8, 0, 'JB Contact Module', '', 'mod_jbcontact', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbcontact.xml', 'https://www.joomlart.com/update-steps', ''),
(1012, 8, 0, 'JB Responsive Template', '', 'jb_responsive', 'template', '', 0, '1.2.8', '', 'http://update.joomlart.com/service/tracking/j30/jb_responsive.xml', 'https://www.joomlart.com/update-steps', ''),
(1013, 8, 0, 'JB LifeStyle2 Template', '', 'lifestyle2', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j30/lifestyle2.xml', 'https://www.joomlart.com/update-steps', ''),
(1014, 8, 0, 'JB Countdown Module', '', 'mod_jbcountdown', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbcountdown.xml', 'https://www.joomlart.com/update-steps', ''),
(1015, 8, 0, 'DT List Theme ', '', 'list_theme', 'custom', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j30/list_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(1016, 8, 0, 'Jbmaps Module', '', 'mod_jbmaps', 'module', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbmaps.xml', 'https://www.joomlart.com/update-steps', ''),
(1017, 8, 0, 'DT Calendar Module ', '', 'mod_dt_calendar', 'module', '', 0, '3.5.2', '', 'http://update.joomlart.com/service/tracking/j30/mod_dt_calendar.xml', 'https://www.joomlart.com/update-steps', ''),
(1018, 8, 0, 'JB Maps2 Module', '', 'mod_jbmaps2', 'module', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbmaps2.xml', 'https://www.joomlart.com/update-steps', ''),
(1019, 8, 0, 'DT Cart Module', '', 'mod_dt_cart', 'module', '', 0, '3.5.6', '', 'http://update.joomlart.com/service/tracking/j30/mod_dt_cart.xml', 'https://www.joomlart.com/update-steps', ''),
(1020, 8, 0, 'DT Register Upcoming Events Module', '', 'mod_dt_upcoming_event', 'module', '', 0, '3.8.4', '', 'http://update.joomlart.com/service/tracking/j30/mod_dt_upcoming_event.xml', 'https://www.joomlart.com/update-steps', ''),
(1021, 8, 0, 'JFlickr Module', '', 'mod_jflickr', 'module', '', 0, '1.4.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jflickr.xml', 'https://www.joomlart.com/update-steps', ''),
(1022, 8, 0, 'DT Register Search Module', '', 'mod_dtsearch', 'module', '', 0, '3.4.2', '', 'http://update.joomlart.com/service/tracking/j30/mod_dtsearch.xml', 'https://www.joomlart.com/update-steps', ''),
(1023, 8, 0, 'S5 Accordion Menu Module', '', 'mod_s5_accordion_menu', 'module', '', 0, '2.3.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_accordion_menu.xml', 'https://www.joomlart.com/update-steps', ''),
(1024, 8, 0, 'Highlighter GK5 J!3 Module', '', 'mod_highlighter_gk5', 'module', '', 0, '1.2.3', '', 'http://update.joomlart.com/service/tracking/j30/mod_highlighter_gk5.xml', 'https://www.joomlart.com/update-steps', ''),
(1025, 8, 0, 'S5 Box Module', '', 'mod_s5_box', 'module', '', 0, '6.1.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_box.xml', 'https://www.joomlart.com/update-steps', ''),
(1026, 8, 0, 'GK Image Show GK4 Module', '', 'mod_image_show_gk4', 'module', '', 0, '1.6.7', '', 'http://update.joomlart.com/service/tracking/j30/mod_image_show_gk4.xml', 'https://www.joomlart.com/update-steps', ''),
(1027, 8, 0, 'S5 Contact Popup Module', '', 'mod_s5_contact_popup', 'module', '', 0, '1.5.3', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_contact_popup.xml', 'https://www.joomlart.com/update-steps', ''),
(1028, 8, 0, 'Jbmaps Module', '', 'mod_jbmaps', 'module', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j30/mod_jbmaps.xml', 'https://www.joomlart.com/update-steps', ''),
(1029, 8, 0, 'S5 Domain Checker Module', '', 'mod_s5_domain_check', 'module', '', 0, '1.6.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_domain_check.xml', 'https://www.joomlart.com/update-steps', ''),
(1030, 8, 0, 'JB Maps2 Module', '', 'mod_jbmaps2', 'module', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j30/mod_jbmaps2.xml', 'https://www.joomlart.com/update-steps', ''),
(1031, 8, 0, 'S5 Flow Module', '', 'mod_s5_flow', 'module', '', 0, '3.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_flow.xml', 'https://www.joomlart.com/update-steps', ''),
(1032, 8, 0, 'News Pro GK5 J!3', '', 'mod_news_pro_gk5', 'module', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j30/mod_news_pro_gk5.xml', 'https://www.joomlart.com/update-steps', ''),
(1033, 8, 0, 'S5 Frontpage Display V2 Module', '', 'mod_s5_frontpage_display2', 'module', '', 0, '2.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_frontpage_display2.xml', 'https://www.joomlart.com/update-steps', ''),
(1034, 8, 0, 'Weather GK4 J!3', '', 'mod_weather_gk4', 'module', '', 0, '1.8.0', '', 'http://update.joomlart.com/service/tracking/j30/mod_weather_gk4.xml', 'https://www.joomlart.com/update-steps', ''),
(1035, 8, 0, 'S5 Frontpage Display V3 Module', '', 'mod_s5_frontpage_display3', 'module', '', 0, '3.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_frontpage_display3.xml', 'https://www.joomlart.com/update-steps', ''),
(1036, 8, 0, 'JB Zen Social Module', '', 'mod_zensocial', 'module', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j30/mod_zensocial.xml', 'https://www.joomlart.com/update-steps', ''),
(1037, 8, 0, 'S5 Habla Chat Module', '', 'mod_s5_habla_chat', 'module', '', 0, '1.6.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_habla_chat.xml', 'https://www.joomlart.com/update-steps', ''),
(1038, 8, 0, 'Zentools Module', '', 'mod_zentools', 'module', '', 0, '1.14.6', '', 'http://update.joomlart.com/service/tracking/j30/mod_zentools.xml', 'https://www.joomlart.com/update-steps', ''),
(1039, 8, 0, 'S5 Horizontal Accordion Module', '', 'mod_s5_horizontal_accordion', 'module', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_horizontal_accordion.xml', 'https://www.joomlart.com/update-steps', ''),
(1040, 8, 0, 'Zentools 2 Module', '', 'mod_zentools2', 'module', '', 0, '2.4.1', '', 'http://update.joomlart.com/service/tracking/j30/mod_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1041, 8, 0, 'S5 Images And Content Fader V3 Module', '', 'mod_s5_image_and_content_faderv3', 'module', '', 0, '3.2.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_image_and_content_faderv3.xml', 'https://www.joomlart.com/update-steps', ''),
(1042, 8, 0, 'JB Novus Template', '', 'novus', 'template', '', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j30/novus.xml', 'https://www.joomlart.com/update-steps', ''),
(1043, 8, 0, 'S5 Image Content Fader v4 Module', '', 'mod_s5_image_and_content_faderv4', 'module', '', 0, '4.4.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_image_and_content_faderv4.xml', 'https://www.joomlart.com/update-steps', ''),
(1044, 8, 0, 'Plugin Ajax Zentools2 ', '', 'zentools2', 'plugin', 'ajax', 0, '2.3.5', '', 'http://update.joomlart.com/service/tracking/j30/plg_ajax_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1045, 8, 0, 'S5 Image Scroller Module', '', 'mod_s5_image_scroller', 'module', '', 0, '3.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_image_scroller.xml', 'https://www.joomlart.com/update-steps', ''),
(1046, 8, 0, 'DT AUTH SIM Proxy Plugin', '', 'authsimproxy', 'plugin', 'dt', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dt_authsimproxy.xml', 'https://www.joomlart.com/update-steps', ''),
(1047, 8, 0, 'S5 Image Slide Module', '', 'mod_s5_imageslide', 'module', '', 0, '4.2.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_imageslide.xml', 'https://www.joomlart.com/update-steps', ''),
(1048, 8, 0, 'DT Register Converge plugin', '', 'converge', 'plugin', 'dt', 0, '1.3', '', 'http://update.joomlart.com/service/tracking/j30/plg_dt_converge.xml', 'https://www.joomlart.com/update-steps', ''),
(1049, 8, 0, 'S5 Instagram Feed Module', '', 'mod_s5_instagram_feed', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_instagram_feed.xml', 'https://www.joomlart.com/update-steps', ''),
(1050, 8, 0, 'DT Register PayPal Express Checkout Plugin', '', 'expresscheckout', 'plugin', 'dt', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_dt_expresscheckout.xml', 'https://www.joomlart.com/update-steps', ''),
(1051, 8, 0, 'S5 Live Search Module', '', 'mod_s5_live_search', 'module', '', 0, '3.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_live_search.xml', 'https://www.joomlart.com/update-steps', ''),
(1052, 8, 0, 'DT AsiaPay PayDollar Plugin', '', 'paydollar', 'plugin', 'dt', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_dt_paydollar.xml', 'https://www.joomlart.com/update-steps', ''),
(1053, 8, 10027, 'S5 Mailchimp Signup Module', '', 'mod_s5_mailchimp_signup', 'module', '', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_mailchimp_signup.xml', 'https://www.joomlart.com/update-steps', ''),
(1054, 8, 0, 'DT Payway Plugin', '', 'payway', 'plugin', 'dt', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dt_payway.xml', 'https://www.joomlart.com/update-steps', ''),
(1055, 8, 0, 'S5 Masonry Module', '', 'mod_s5_masonry', 'module', '', 0, '2.9.9', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_masonry.xml', 'https://www.joomlart.com/update-steps', ''),
(1056, 8, 0, 'DT Register Stripe Plugin', '', 'stripe', 'plugin', 'dt', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dt_stripe.xml', 'https://www.joomlart.com/update-steps', ''),
(1057, 8, 0, 'S5 MP3 Gallery Module', '', 'mod_s5_mp3_gallery', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_mp3_gallery.xml', 'https://www.joomlart.com/update-steps', ''),
(1058, 8, 0, 'System - DT Register No Cache Plugin', '', 'plg_dtcache', 'plugin', 'plg_dtcache', 0, '3.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtcache.xml', 'https://www.joomlart.com/update-steps', '');
INSERT INTO `lbazs_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1059, 8, 0, 'S5 News Display 2 Module', '', 'mod_s5_news_display_2', 'module', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_news_display_2.xml', 'https://www.joomlart.com/update-steps', ''),
(1060, 8, 0, 'DT Register EventLink Plugin', '', 'plg_dteventlink', 'plugin', 'plg_dteventlink', 0, '3.2.8', '', 'http://update.joomlart.com/service/tracking/j30/plg_dteventlink.xml', 'https://www.joomlart.com/update-steps', ''),
(1061, 8, 0, 'S5 New Sticker V3 Module', '', 'mod_s5_newstickerv3', 'module', '', 0, '4.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_newstickerv3.xml', 'https://www.joomlart.com/update-steps', ''),
(1062, 8, 0, 'DT Register JEvents Update', '', 'plg_dtjeventupdate', 'plugin', 'plg_dtjeventupdate', 0, '3.2', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtjeventupdate.xml', 'https://www.joomlart.com/update-steps', ''),
(1063, 8, 0, 'S5 Open Table Module', '', 'mod_s5_opentable', 'module', '', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_opentable.xml', 'https://www.joomlart.com/update-steps', ''),
(1064, 8, 0, 'DT Register System Plugin', '', 'plg_dtreg', 'plugin', 'plg_dtreg', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtreg.xml', 'https://www.joomlart.com/update-steps', ''),
(1065, 8, 0, 'S5 Photo Showcase Module', '', 'mod_s5_photo_showcase', 'module', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_photo_showcase.xml', 'https://www.joomlart.com/update-steps', ''),
(1066, 8, 0, 'DT AcyMailing Subscriber', '', 'acymailing', 'plugin', 'dtreg', 0, '3.6', '', 'http://update.joomlart.com/service/tracking/j30/plg_dtreg_acymailing.xml', 'https://www.joomlart.com/update-steps', ''),
(1067, 8, 0, 'S5 Photo Showcase V2 Module', '', 'mod_s5_photo_showcase_v2', 'module', '', 0, '3.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_photo_showcase_v2.xml', 'https://www.joomlart.com/update-steps', ''),
(1068, 8, 0, 'DT Register Activity (JomSocial) Plugin', '', 'dtreg_activity', 'plugin', 'jomsocial', 0, '1.9', '', 'http://update.joomlart.com/service/tracking/j30/plg_jomsocial_dtreg_activity.xml', 'https://www.joomlart.com/update-steps', ''),
(1069, 8, 0, 'S5 Quick Contact Module', '', 'mod_s5_quick_contact', 'module', '', 0, '4.3.3', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_quick_contact.xml', 'https://www.joomlart.com/update-steps', ''),
(1070, 8, 0, 'DT Register Records (JomSocial) Plugin', '', 'dtreg_records', 'plugin', 'jomsocial', 0, '1.12', '', 'http://update.joomlart.com/service/tracking/j30/plg_jomsocial_dtreg_records.xml', 'https://www.joomlart.com/update-steps', ''),
(1071, 8, 0, 'S5 Register Module', '', 'mod_s5_register', 'module', '', 0, '4.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_register.xml', 'https://www.joomlart.com/update-steps', ''),
(1072, 8, 0, 'GK Opengraph system plugin', '', 'gk_opengraph', 'plugin', 'sys', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gk_opengraph.xml', 'https://www.joomlart.com/update-steps', ''),
(1073, 8, 0, 'S5 Reservations Module', '', 'mod_s5_reservations', 'module', '', 0, '4.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_reservations.xml', 'https://www.joomlart.com/update-steps', ''),
(1074, 8, 0, 'S5 Snipcart Module', '', 'mod_s5_snipcart', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_snipcart.xml', 'https://www.joomlart.com/update-steps', ''),
(1075, 8, 0, 'GK Cache system plugin', '', 'gkcache', 'plugin', 'sys', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gkcache.xml', 'https://www.joomlart.com/update-steps', ''),
(1076, 8, 0, 'GK Contact system plugin', '', 'gkcontact', 'plugin', 'sys', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gkcontact.xml', 'https://www.joomlart.com/update-steps', ''),
(1077, 8, 0, 'S5 Spotlight News Module', '', 'mod_s5_spotlight_news', 'module', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_spotlight_news.xml', 'https://www.joomlart.com/update-steps', ''),
(1078, 8, 0, 'GK Extmenu system plugin', '', 'gkextmenu', 'plugin', 'sys', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_gkextmenu.xml', 'https://www.joomlart.com/update-steps', ''),
(1079, 8, 0, 'S5 Tab Show v3 Module', '', 'mod_s5_tab_show', 'module', '', 0, '3.3.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_tab_show.xml', 'https://www.joomlart.com/update-steps', ''),
(1080, 8, 0, 'System JBType Plugin', '', 'jbtype', 'plugin', 'sys', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_jbtype.xml', 'https://www.joomlart.com/update-steps', ''),
(1081, 8, 0, 'S5 Tab Show v2 Module', '', 'mod_s5_tabshow_v2', 'module', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_tabshow_v2.xml', 'https://www.joomlart.com/update-steps', ''),
(1082, 8, 0, 'JB Zen Shortcodes Plugin', '', 'zenshortcodes', 'plugin', 'sys', 0, '1.7.6', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_zenshortcodes.xml', 'https://www.joomlart.com/update-steps', ''),
(1083, 8, 0, 'S5 Typed Text Module', '', 'mod_s5_typed_text', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_typed_text.xml', 'https://www.joomlart.com/update-steps', ''),
(1084, 8, 0, 'Plugin Zentools2 System', '', 'zentools2', 'plugin', 'sys', 0, '2.3.5', '', 'http://update.joomlart.com/service/tracking/j30/plg_sys_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1085, 8, 0, 'S5 Vertical Accordion Module', '', 'mod_s5_vertical_accordion', 'module', '', 0, '4.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_vertical_accordion.xml', 'https://www.joomlart.com/update-steps', ''),
(1086, 8, 0, 'S5 Weather Module', '', 'mod_s5_weather', 'module', '', 0, '3.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_weather.xml', 'https://www.joomlart.com/update-steps', ''),
(1087, 8, 0, 'DT SMS System Plugin', '', 'dtsmsintegrator', 'plugin', 'system', 0, '1.4', '', 'http://update.joomlart.com/service/tracking/j30/plg_system_dtsmsintegrator.xml', 'https://www.joomlart.com/update-steps', ''),
(1088, 8, 0, 'S5 Mapit Module', '', 'mod_s5mapit', 'module', '', 0, '1.8.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5mapit.xml', 'https://www.joomlart.com/update-steps', ''),
(1089, 8, 0, 'T3 B3 Blank Template', '', 't3_bs3_blank', 'template', '', 0, '2.2.1', '', 'http://update.joomlart.com/service/tracking/j30/tpl_t3_bs3_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(1090, 8, 0, 'JB Skillset Module', '', 'mod_skillset', 'module', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_skillset.xml', 'https://www.joomlart.com/update-steps', ''),
(1091, 8, 0, 'JB Xero Template', '', 'xero', 'template', '', 0, '1.4.4', '', 'http://update.joomlart.com/service/tracking/j30/xero.xml', 'https://www.joomlart.com/update-steps', ''),
(1092, 8, 0, 'SP Contact Module', '', 'mod_sp_quickcontact', 'module', '', 0, '1.4', '', 'http://update.joomlart.com/service/tracking/j31/mod_sp_quickcontact.xml', 'https://www.joomlart.com/update-steps', ''),
(1093, 8, 0, 'GURU Payment VT.Web Veritrans Indonesia', '', 'GURU-Payment_VT.Web_Veritrans_Indonesia', 'plugin', 'GURU-Payment_VT.Web_', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/GURU-Payment_VT.Web_Veritrans_Indonesia.xml', 'https://www.joomlart.com/update-steps', ''),
(1094, 8, 0, 'JB Zenfeature Table Module', '', 'mod_zenfeaturetable', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_zenfeaturetable.xml', 'https://www.joomlart.com/update-steps', ''),
(1095, 8, 0, 'JB Zen Social Module', '', 'mod_zensocial', 'module', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_zensocial.xml', 'https://www.joomlart.com/update-steps', ''),
(1096, 8, 0, 'MPDF', '', 'MPDF', 'package', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/MPDF.xml', 'https://www.joomlart.com/update-steps', ''),
(1097, 8, 0, 'JB ZenTools Module', '', 'mod_zentools', 'module', '', 0, '1.14.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_zentools.xml', 'https://www.joomlart.com/update-steps', ''),
(1098, 8, 0, 'S5 Vertex Framework', '', 'Shape5_Vertex4', 'template', '', 0, '4.2.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_Vertex4.xml', 'https://www.joomlart.com/update-steps', ''),
(1099, 8, 0, 'Zentools 2 Module', '', 'mod_zentools2', 'module', '', 0, '2.4.8', '', 'http://update.joomlart.com/service/tracking/j31/mod_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1100, 8, 0, 'S5 Amazed Photography Template', '', 'Shape5_amazed_photography_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_amazed_photography_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1101, 8, 0, 'JB Nebula Template', '', 'nebula', 'template', '', 0, '1.4.3', '', 'http://update.joomlart.com/service/tracking/j31/nebula.xml', 'https://www.joomlart.com/update-steps', ''),
(1102, 8, 0, 'S5 Ambient Template', '', 'Shape5_ambient_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_ambient_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1103, 8, 0, 'JB Newlifestyle Template', '', 'newlifestyle', 'template', '', 0, '2.0.9', '', 'http://update.joomlart.com/service/tracking/j31/newlifestyle.xml', 'https://www.joomlart.com/update-steps', ''),
(1104, 8, 0, 'S5 Ameritage Medical Template', '', 'Shape5_ameritage_medical_template', 'template', '', 0, '3.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_ameritage_medical_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1105, 8, 0, 'JB Newstream2 Template', '', 'newstream2', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/newstream2.xml', 'https://www.joomlart.com/update-steps', ''),
(1106, 8, 0, 'S5 Appwonder Template', '', 'Shape5_appwonder_template', 'template', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_appwonder_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1107, 8, 0, 'JB Onepage Template', '', 'onepage', 'template', '', 0, '1.4.2', '', 'http://update.joomlart.com/service/tracking/j31/onepage.xml', 'https://www.joomlart.com/update-steps', ''),
(1108, 8, 0, 'S5 Arthur Template', '', 'Shape5_arthur_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_arthur_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1109, 8, 0, 'JB OnePageTwo Template', '', 'onepagetwo', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/onepagetwo.xml', 'https://www.joomlart.com/update-steps', ''),
(1110, 8, 0, 'S5 Attractions Template', '', 'Shape5_attractions_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_attractions_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1111, 8, 0, 'GURU Payment Plugin - PayPal Pro', '', 'paypalpro', 'plugin', 'paypalpro', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/paypalpro.xml', 'https://www.joomlart.com/update-steps', ''),
(1112, 8, 0, 'S5 Aurora Dawn Template', '', 'Shape5_aurora_dawn_template', 'template', '', 0, '3.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_aurora_dawn_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1113, 8, 0, 'Plugin Ajax JA Content Type', '', 'jacontenttype', 'plugin', 'ajax', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_ajax_jacontenttype.xml', 'https://www.joomlart.com/update-steps', ''),
(1114, 8, 0, 'S5 Avignet Dream Template', '', 'Shape5_avignet_dream_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_avignet_dream_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1115, 8, 0, 'JB Zengrid Framework 5', '', 'zengridframework', 'plugin', 'ajax', 0, '5.1.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_ajax_zengridframework.xml', 'https://www.joomlart.com/update-steps', ''),
(1116, 8, 0, 'S5 Big Picture Template', '', 'Shape5_bigpicture_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_bigpicture_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1117, 8, 0, 'Plugin Ajax Zentools2 ', '', 'zentools2', 'plugin', 'ajax', 0, '2.3.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_ajax_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1118, 8, 0, 'S5 Blogbox Template', '', 'Shape5_blog_box_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_blog_box_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1119, 8, 0, 'DT SMS Amazonsns Plugin', '', 'amazonsns', 'plugin', 'dtsms', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_dtsms_amazonsns.xml', 'https://www.joomlart.com/update-steps', ''),
(1120, 8, 0, 'S5 BlueGroup Template', '', 'Shape5_bluegroup_template', 'template', '', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_bluegroup_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1121, 8, 0, 'DT SMS Draft Plugin', '', 'smsdraft', 'plugin', 'dtsms', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_dtsms_smsdraft.xml', 'https://www.joomlart.com/update-steps', ''),
(1122, 8, 0, 'S5 Business Line Template', '', 'Shape5_business_line_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_business_line_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1123, 8, 0, 'JS Force Avatar', '', 'plg_forceavatar', 'plugin', 'plg_forceavatar', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_forceavatar.xml', 'https://www.joomlart.com/update-steps', ''),
(1124, 8, 0, 'S5 Business Pro Template', '', 'Shape5_business_pro_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_business_pro_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1125, 8, 0, 'JS Force Fields', '', 'plg_forcefields', 'plugin', 'plg_forcefields', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_forcefields.xml', 'https://www.joomlart.com/update-steps', ''),
(1126, 8, 0, 'S5 Callie Rush Template', '', 'Shape5_callie_rush_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_callie_rush_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1127, 8, 0, 'GK Typography Plugin', '', 'typography', 'plugin', 'gk', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/plg_gk_typography.xml', 'https://www.joomlart.com/update-steps', ''),
(1128, 8, 0, 'S5 Campus Life Template', '', 'Shape5_campuslife_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_campuslife_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1129, 8, 0, 'JA GDPR AcyMailing Plugin', '', 'acymailing', 'plugin', 'jagdpr', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_acymailing.xml', 'https://www.joomlart.com/update-steps', ''),
(1130, 8, 0, 'S5 Charity Template', '', 'Shape5_charity_template', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_charity_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1131, 8, 0, 'JA GDPR Adagency Plugin', '', 'adagency', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_adagency.xml', 'https://www.joomlart.com/update-steps', ''),
(1132, 8, 0, 'S5 Charlestown Template', '', 'Shape5_charlestown_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_charlestown_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1133, 8, 0, 'JA GDPR DJ Catalog Plugin', '', 'catalog', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_catalog.xml', 'https://www.joomlart.com/update-steps', ''),
(1134, 8, 0, 'S5 Cleanout Template', '', 'Shape5_cleanout_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_cleanout_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1135, 8, 0, 'JA GDPR DJ Classifieds Plugin', '', 'classifieds', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_classifieds.xml', 'https://www.joomlart.com/update-steps', ''),
(1136, 8, 0, 'JA GDPR Community Builder Plugin', '', 'communitybuilder', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_communitybuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1137, 8, 0, 'S5 Comaxium Template', '', 'Shape5_comaxium_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_comaxium_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1138, 8, 0, 'JA GDPR Custom Plugin', '', 'custom', 'plugin', 'jagdpr', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_custom.xml', 'https://www.joomlart.com/update-steps', ''),
(1139, 8, 0, 'S5 Compassion Template', '', 'Shape5_compassion_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_compassion_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1140, 8, 0, 'JA GDPR General Plugin', '', 'general', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_general.xml', 'https://www.joomlart.com/update-steps', ''),
(1141, 8, 0, 'S5 Construction Template', '', 'Shape5_construction_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_construction_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1142, 8, 0, 'JA GDPR Hikashop Plugin', '', 'hikashop', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_hikashop.xml', 'https://www.joomlart.com/update-steps', ''),
(1143, 8, 0, 'S5 Content King Template', '', 'Shape5_contentking_template', 'template', '', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_contentking_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1144, 8, 0, 'S5 Corporate Response Template', '', 'Shape5_corporate_response_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_corporate_response_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1145, 8, 0, 'JA GDPR J2Store Plugin', '', 'j2store', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_j2store.xml', 'https://www.joomlart.com/update-steps', ''),
(1146, 8, 0, 'S5 Corpway Template', '', 'Shape5_corpway_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_corpway_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1147, 8, 0, 'JA GDPR Joomla Plugin', '', 'joomla', 'plugin', 'jagdpr', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_joomla.xml', 'https://www.joomlart.com/update-steps', ''),
(1148, 8, 0, 'S5 Curb Appeal Template', '', 'Shape5_curb_appeal_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_curb_appeal_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1149, 8, 0, 'JA GDPR JShopping Plugin', '', 'jshopping', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_jshopping.xml', 'https://www.joomlart.com/update-steps', ''),
(1150, 8, 0, 'S5 Cyan Template', '', 'Shape5_cyan_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_cyan_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1151, 8, 0, 'JA GDPR Mijoshop Plugin', '', 'mijoshop', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_mijoshop.xml', 'https://www.joomlart.com/update-steps', ''),
(1152, 8, 0, 'S5 Design Control Template', '', 'Shape5_design_control_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_design_control_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1153, 8, 0, 'JA GDPR RSForm Plugin', '', 'rsform', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_rsform.xml', 'https://www.joomlart.com/update-steps', ''),
(1154, 8, 0, 'S5 Emma and Mason Template', '', 'Shape5_emma_and_mason_template', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_emma_and_mason_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1155, 8, 0, 'JA K2 Data Migration plugin', '', 'plg_jak2tocomcontentmigration', 'plugin', 'plg_jak2tocomcontent', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_jak2tocomcontentmigration.xml', 'https://www.joomlart.com/update-steps', ''),
(1156, 8, 0, 'S5 Emusica Template', '', 'Shape5_emusica_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_emusica_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1157, 8, 0, 'Plgin JA K2 import to Joomla Content', '', 'plg_jak2tocontent', 'plugin', 'plg_jak2tocontent', 0, '1.0.0 beta', '', 'http://update.joomlart.com/service/tracking/j31/plg_jak2tocontent.xml', '', ''),
(1158, 8, 0, 'S5 Eventfull Business Template', '', 'Shape5_eventfull_business_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_eventfull_business_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1159, 8, 0, 'JA Megafilter Joomla Custom Fields Plugin', '', 'content', 'plugin', 'jamegafilter', 0, '1.1.4', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_content.xml', 'https://www.joomlart.com/update-steps', ''),
(1160, 8, 0, 'S5 Eventfull Template', '', 'Shape5_eventfull_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_eventfull_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1161, 8, 0, 'JA Megafilter DOCman Plugin', '', 'docman', 'plugin', 'jamegafilter', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_docman.xml', 'https://www.joomlart.com/update-steps', ''),
(1162, 8, 0, 'S5 EZ Web Hosting Template', '', 'Shape5_ezwebhosting_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_ezwebhosting_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1163, 8, 0, 'S5 Fitness Center Template', '', 'Shape5_fitness_center_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_fitness_center_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1164, 8, 0, 'JA Megafilter EShop Plugin', '', 'eshop', 'plugin', 'jamegafilter', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_eshop.xml', 'https://www.joomlart.com/update-steps', ''),
(1165, 8, 0, 'S5 Forte Template', '', 'Shape5_forte_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_forte_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1166, 8, 0, 'JA Megafilter HikaShop Plugin', '', 'hikashop', 'plugin', 'jamegafilter', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_hikashop.xml', 'https://www.joomlart.com/update-steps', ''),
(1167, 8, 0, 'S5 Fresh Bistro Template', '', 'Shape5_fresh_bistro_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_fresh_bistro_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1168, 8, 0, 'JA Megafilter J2store Plugin', '', 'j2store', 'plugin', 'jamegafilter', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_j2store.xml', 'https://www.joomlart.com/update-steps', ''),
(1169, 8, 0, 'S5 Fusion Template', '', 'Shape5_fusion_template', 'template', '', 0, '1.5.7', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_fusion_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1170, 8, 0, 'JA Megafilter for Hikashop plg', '', 'ja_hikashop', 'plugin', 'jamegafilter', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_ja_hikashop.xml', '', ''),
(1171, 8, 0, 'S5 Game World Template', '', 'Shape5_game_world_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_game_world_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1172, 8, 0, 'JA Megafilter JoomShopping Plugin', '', 'jshopping', 'plugin', 'jamegafilter', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_jshopping.xml', 'https://www.joomlart.com/update-steps', ''),
(1173, 8, 0, 'S5 Gamers Template', '', 'Shape5_gamers_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_gamers_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1174, 8, 0, 'JA Megafilter K2 Plugin', '', 'k2', 'plugin', 'jamegafilter', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_k2.xml', 'https://www.joomlart.com/update-steps', ''),
(1175, 8, 0, 'S5 GCK Store Template', '', 'Shape5_gck_store_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_gck_store_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1176, 8, 0, 'JA Megafilter Mijoshop Plugin', '', 'mijoshop', 'plugin', 'jamegafilter', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_mijoshop.xml', 'https://www.joomlart.com/update-steps', ''),
(1177, 8, 0, 'S5 General Commerce Template', '', 'Shape5_general_commerce_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_general_commerce_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1178, 8, 0, 'JA Payment Gateway', '', 'gateway', 'plugin', 'joomart', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_joomart_gateway.xml', 'https://www.joomlart.com/update-steps', ''),
(1179, 8, 0, 'S5 General Contractor Template', '', 'Shape5_general_contractor_template', 'template', '', 0, '1.1.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_general_contractor_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1180, 8, 0, 'JA K2 Extrafields', '', 'jak2extrafields', 'plugin', 'k2', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_k2_jak2extrafields.xml', 'https://www.joomlart.com/update-steps', ''),
(1181, 8, 0, 'S5 GetReserved Template', '', 'Shape5_getreserved_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_getreserved_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1182, 8, 0, 'JA K2 v3 Filter Plugin for J33', '', 'jak2v3filter', 'plugin', 'k2', 0, '3.0.0 preview ', '', 'http://update.joomlart.com/service/tracking/j31/plg_k2_jak2v3filter.xml', '', ''),
(1183, 8, 0, 'S5 Health Guide Template', '', 'Shape5_health_guide_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_health_guide_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1184, 8, 0, 'GK Reservation system plugin', '', 'gkreservation', 'plugin', 'sys', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_gkreservation.xml', 'https://www.joomlart.com/update-steps', ''),
(1185, 8, 0, 'S5 Helion Template', '', 'Shape5_helion_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_helion_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1186, 8, 0, 'System JB Type Plugin', '', 'jbtype', 'plugin', 'sys', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_jbtype.xml', 'https://www.joomlart.com/update-steps', ''),
(1187, 8, 0, 'S5 Hexa Corp Template', '', 'Shape5_hexa_corp_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_hexa_corp_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1188, 8, 0, 'System Zenanimate Plugin', '', 'zenanimate', 'plugin', 'sys', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zenanimate.xml', 'https://www.joomlart.com/update-steps', ''),
(1189, 8, 0, 'S5 Hexicon Gamer Template', '', 'Shape5_hexicon_gamer_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_hexicon_gamer_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1190, 8, 0, 'JB Zenbridge Plugin', '', 'zenbridge', 'plugin', 'sys', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zenbridge.xml', 'https://www.joomlart.com/update-steps', ''),
(1191, 8, 0, 'S5 Incline Template', '', 'Shape5_incline_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_incline_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1192, 8, 0, 'System Zencompiler Plugin', '', 'zencompiler', 'plugin', 'sys', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zencompiler.xml', 'https://www.joomlart.com/update-steps', ''),
(1193, 8, 0, 'S5 Legal Lawyer Template', '', 'Shape5_legal_lawyer_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_legal_lawyer_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1194, 8, 0, 'JB Zen Menu Plugin', '', 'zenmenu', 'plugin', 'sys', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zenmenu.xml', 'https://www.joomlart.com/update-steps', ''),
(1195, 8, 0, 'S5 Life Journey Template', '', 'Shape5_life_journey_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_life_journey_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1196, 8, 0, 'JB Zen Shortcodes Plugin', '', 'zenshortcodes', 'plugin', 'sys', 0, '1.7.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zenshortcodes.xml', 'https://www.joomlart.com/update-steps', ''),
(1197, 8, 0, 'S5 Light Church Template', '', 'Shape5_lightchurch_template', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_lightchurch_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1198, 8, 0, 'Plugin Zentools2 System', '', 'zentools2', 'plugin', 'sys', 0, '2.3.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1199, 8, 0, 'S5 Lime Light Template', '', 'Shape5_lime_light_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_lime_light_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1200, 8, 0, 'JA System Designit Plugin', '', 'designit', 'plugin', 'system', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_designit.xml', 'https://www.joomlart.com/update-steps', ''),
(1201, 8, 0, 'S5 Luxon Template', '', 'Shape5_luxon_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_luxon_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1202, 8, 0, 'DT SMS System Activitymonitor Plugin', '', 'dtsmsactivitymonitor', 'plugin', 'system', 0, '1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_dtsmsactivitymonitor.xml', 'https://www.joomlart.com/update-steps', ''),
(1203, 8, 0, 'S5 M Ceative Agency Template', '', 'Shape5_m_creative_agency_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_m_creative_agency_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1204, 8, 0, 'JA Builder System Plugin', '', 'jabuilder', 'plugin', 'system', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_jabuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1205, 8, 0, 'S5 Maxed Mag Template', '', 'Shape5_maxedmag_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_maxedmag_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1206, 8, 0, 'Plugin JA Content Type', '', 'jacontenttype', 'plugin', 'system', 0, '1.1.4', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_jacontenttype.xml', 'https://www.joomlart.com/update-steps', ''),
(1207, 8, 0, 'S5 MetroShows Template', '', 'Shape5_metroshows_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_metroshows_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1208, 8, 0, 'JA Open Street Map ', '', 'jaosmap', 'plugin', 'system', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_jaosmap.xml', 'https://www.joomlart.com/update-steps', ''),
(1209, 8, 0, 'S5 Modern Flavor Template', '', 'Shape5_modern_flavor_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_modern_flavor_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1210, 8, 10024, 'T3 System Plugin', '', 't3', 'plugin', 'system', 0, '2.7.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(1211, 8, 0, 'S5 New Vision Template', '', 'Shape5_new_vision_template', 'template', '', 0, '3.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_new_vision_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1212, 8, 0, 'T4 System Plugin ', '', 't4', 'plugin', 'system', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_t4.xml', 'https://www.joomlart.com/update-steps', ''),
(1213, 8, 0, 'S5 News Blog Template', '', 'Shape5_newsblog_template', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_newsblog_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1214, 8, 0, 'DT SMS User Plugin', '', 'dtsms', 'plugin', 'user', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_user_dtsms.xml', 'https://www.joomlart.com/update-steps', ''),
(1215, 8, 0, 'S5 Newsplace Template', '', 'Shape5_newsplace_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_newsplace_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1216, 8, 0, 'JB Zenkit Plugin', '', 'zenkit', 'plugin', 'zenkit', 0, '2.1.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_zenkit_zenkit.xml', 'https://www.joomlart.com/update-steps', ''),
(1217, 8, 0, 'S5 No1 Shopping Template', '', 'Shape5_no1_shopping_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_no1_shopping_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1218, 8, 0, 'S5 Css And Js Compressor Plugin', '', 'plugin_s5_css_and_js_compressor', 'plugin', 'plugin_s5_css_and_js', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_css_and_js_compressor.xml', 'https://www.joomlart.com/update-steps', ''),
(1219, 8, 0, 'S5 Oasis Template', '', 'Shape5_oasis_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_oasis_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1220, 8, 0, 'S5 Disqus Comments Plugin', '', 'plugin_s5_disqus_comments', 'plugin', 'plugin_s5_disqus_com', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_disqus_comments.xml', 'https://www.joomlart.com/update-steps', ''),
(1221, 8, 0, 'S5 Flex Menu Plugin', '', 'plugin_s5_flex_menu', 'plugin', 'plugin_s5_flex_menu', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_flex_menu.xml', 'https://www.joomlart.com/update-steps', ''),
(1222, 8, 0, 'S5 Outdoor Life Template', '', 'Shape5_outdoor_life_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_outdoor_life_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1223, 8, 0, 'S5 Ie6 Warning Plugin', '', 'plugin_s5_ie6_warning', 'plugin', 'plugin_s5_ie6_warnin', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_ie6_warning.xml', 'https://www.joomlart.com/update-steps', ''),
(1224, 8, 0, 'S5 Pantheon Template', '', 'Shape5_pantheon_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_pantheon_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1225, 8, 0, 'S5 Paradigm Shift Template', '', 'Shape5_paradigm_shift_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_paradigm_shift_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1226, 8, 0, 'S5 Phosphorus Template', '', 'Shape5_phosphorus_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_phosphorus_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1227, 8, 0, 'S5 Like Me Plugin', '', 'plugin_s5_likeme', 'plugin', 'plugin_s5_likeme', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_likeme.xml', 'https://www.joomlart.com/update-steps', ''),
(1228, 8, 0, 'S5 Media Player 2 Plugin', '', 'plugin_s5_media_player', 'plugin', 'plugin_s5_media_play', 0, '2.4.2', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_media_player.xml', 'https://www.joomlart.com/update-steps', ''),
(1229, 8, 0, 'S5 Photobox Template', '', 'Shape5_photobox_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_photobox_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1230, 8, 0, 'S5 Mp3 Player Plugin', '', 'plugin_s5_mp3_player', 'plugin', 'plugin_s5_mp3_player', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_mp3_player.xml', 'https://www.joomlart.com/update-steps', ''),
(1231, 8, 0, 'S5 Photofolio Template', '', 'Shape5_photofolio_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_photofolio_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1232, 8, 0, 'JB Portal Template', '', 'portal', 'template', '', 0, '1.4.5', '', 'http://update.joomlart.com/service/tracking/j31/portal.xml', 'https://www.joomlart.com/update-steps', ''),
(1233, 8, 0, 'S5 Prestige Academy Template', '', 'Shape5_prestige_academy_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_prestige_academy_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1234, 8, 0, 'JB Presto Template', '', 'presto', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/presto.xml', 'https://www.joomlart.com/update-steps', ''),
(1235, 8, 0, 'S5 Real Estate Template', '', 'Shape5_realestate_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_realestate_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1236, 8, 0, 'JB Profile Template', '', 'profile', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/profile.xml', 'https://www.joomlart.com/update-steps', ''),
(1237, 8, 0, 'S5 Regan Tech Template', '', 'Shape5_regan_tech_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_regan_tech_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1238, 8, 0, 'JB Profilr Template', '', 'profilr', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/profilr.xml', 'https://www.joomlart.com/update-steps', ''),
(1239, 8, 0, 'S5 Risen Hope Template', '', 'Shape5_risen_hope_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_risen_hope_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1240, 8, 0, 'Purity III Template', '', 'purity_iii', 'template', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/purity_iii.xml', 'https://www.joomlart.com/update-steps', ''),
(1241, 8, 0, 'S5 Salon N Spa Template', '', 'Shape5_salonnspa_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_salonnspa_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1242, 8, 0, 'JB Rasa2 Template', '', 'rasa2', 'template', '', 0, '1.4.0', '', 'http://update.joomlart.com/service/tracking/j31/rasa2.xml', 'https://www.joomlart.com/update-steps', ''),
(1243, 8, 0, 'S5 Samba Spa Template', '', 'Shape5_samba_template', 'template', '', 0, '1.3.5', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_samba_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1244, 8, 0, 'JB Responsive2 Template', '', 'responsive2', 'template', '', 0, '1.4.4', '', 'http://update.joomlart.com/service/tracking/j31/responsive2.xml', 'https://www.joomlart.com/update-steps', ''),
(1245, 8, 0, 'S5 Vertex Template', '', 'Shape5_shape5_vertex_template', 'template', '', 0, '3.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_shape5_vertex_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1246, 8, 0, 'DT Retro Theme', '', 'retro_theme', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/retro_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(1247, 8, 0, 'S5 Shenandoah Template', '', 'Shape5_shenandoah_template', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_shenandoah_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1248, 8, 0, 'JS Socialize template', '', 'socialize', 'template', '', 0, '2.1.6', '', 'http://update.joomlart.com/service/tracking/j31/socialize.xml', 'https://www.joomlart.com/update-steps', ''),
(1249, 8, 0, 'S5 ShoppingBag Template', '', 'Shape5_shoppingbag_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_shoppingbag_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1250, 8, 0, 'Socialize Bonus', '', 'socialize_bonus', 'package', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/socialize_bonus.xml', 'https://www.joomlart.com/update-steps', ''),
(1251, 8, 0, 'S5 Sienna Template', '', 'Shape5_sienna_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_sienna_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1252, 8, 0, 'GURU Payment Plugin - Stripe', '', 'stripe', 'plugin', 'stripe', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/stripe.xml', 'https://www.joomlart.com/update-steps', ''),
(1253, 8, 0, 'S5 Simplex Template', '', 'Shape5_simplex_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_simplex_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1254, 8, 0, 'T4 Package', '', 't4', 'package', '', 0, '1.0.0_preview', '', 'http://update.joomlart.com/service/tracking/j31/t4.xml', 'https://www.joomlart.com/update-steps', ''),
(1255, 8, 0, 'S5 Soul Search Template', '', 'Shape5_soul_search_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_soul_search_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1256, 8, 0, 'T4 Page Builder Package', '', 't4pagebuilder', 'package', '', 0, '1.0.0_preview', '', 'http://update.joomlart.com/service/tracking/j31/t4pagebuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1257, 8, 0, 'S5 Spectrum Template', '', 'Shape5_spectrum_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_spectrum_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1258, 8, 0, 'T3 B3 Blank Template', '', 't3_bs3_blank', 'template', '', 0, '2.2.1', '', 'http://update.joomlart.com/service/tracking/j31/tpl_t3_bs3_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(1259, 8, 0, 'T4 Blank Template', '', 't4_blank', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/tpl_t4_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(1260, 8, 0, 'S5 Sports Nation Template', '', 'Shape5_sports_nation_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_sports_nation_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1261, 8, 0, 'Sample package for Uber App', '', 'uber_app', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_app.xml', 'https://www.joomlart.com/update-steps', ''),
(1262, 8, 0, 'S5 Store Pro Template', '', 'Shape5_storepro_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_storepro_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1263, 8, 0, 'Sample package for Uber Bookstore', '', 'uber_bookstore', 'sample_package', '', 0, '2.1.7', '', 'http://update.joomlart.com/service/tracking/j31/uber_bookstore.xml', 'https://www.joomlart.com/update-steps', ''),
(1264, 8, 0, 'S5 Swapps Template', '', 'Shape5_swapps_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_swapps_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1265, 8, 0, 'Sample package for Uber Business', '', 'uber_business', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_business.xml', 'https://www.joomlart.com/update-steps', ''),
(1266, 8, 0, 'S5 The Classifieds Template', '', 'Shape5_the_classifieds_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_the_classifieds_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1267, 8, 0, 'Sample package for Uber Charity', '', 'uber_charity', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_charity.xml', 'https://www.joomlart.com/update-steps', ''),
(1268, 8, 0, 'S5 TheBlogazine Template', '', 'Shape5_theblogazine_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_theblogazine_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1269, 8, 0, 'Sample package for Uber Church', '', 'uber_church', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_church.xml', 'https://www.joomlart.com/update-steps', ''),
(1270, 8, 0, 'S5 Traction Template', '', 'Shape5_traction_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_traction_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1271, 8, 0, 'Sample package for Uber Construction', '', 'uber_construction', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_construction.xml', 'https://www.joomlart.com/update-steps', ''),
(1272, 8, 0, 'S5 University Template', '', 'Shape5_university_template', 'template', '', 0, '2.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_university_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1273, 8, 0, 'Sample package for Uber Corporate', '', 'uber_corporate', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_corporate.xml', 'https://www.joomlart.com/update-steps', ''),
(1274, 8, 0, 'S5 Velocity Template', '', 'Shape5_velocity_template', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_velocity_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1275, 8, 0, 'Sample package for Uber Gym', '', 'uber_gym', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_gym.xml', 'https://www.joomlart.com/update-steps', ''),
(1276, 8, 0, 'S5 Zoka Template', '', 'Shape5_zoka_template', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/Shape5_zoka_template.xml', 'https://www.joomlart.com/update-steps', ''),
(1277, 8, 0, 'Sample package for Uber Home', '', 'uber_home', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_home.xml', 'https://www.joomlart.com/update-steps', ''),
(1278, 8, 0, 'Installation Guide', '', 'adagency_installation_guide', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/adagency_installation_guide.xml', 'https://www.joomlart.com/update-steps', ''),
(1279, 8, 0, 'Sample package for Uber Landing ', '', 'uber_landing', 'sample_package', '', 0, '2.1.3', '', 'http://update.joomlart.com/service/tracking/j31/uber_landing.xml', 'https://www.joomlart.com/update-steps', ''),
(1280, 8, 0, 'Plugin System Admin template Helper', '', 'admintplhelper', 'plugin', 'admintplhelper', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/admintplhelper.xml', 'https://www.joomlart.com/update-steps', ''),
(1281, 8, 0, 'Sample package for Uber Lawyer', '', 'uber_lawyer', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_lawyer.xml', 'https://www.joomlart.com/update-steps', ''),
(1282, 8, 0, 'JB Arcadia Template', '', 'arcadia', 'template', '', 0, '2.0.2', '', 'http://update.joomlart.com/service/tracking/j31/arcadia.xml', 'https://www.joomlart.com/update-steps', ''),
(1283, 8, 0, 'Sample package for Uber Medicare', '', 'uber_medicare', 'sample_package', '', 0, '2.1.4', '', 'http://update.joomlart.com/service/tracking/j31/uber_medicare.xml', 'https://www.joomlart.com/update-steps', ''),
(1284, 8, 0, 'JB Aussie Template', '', 'aussie', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/aussie.xml', 'https://www.joomlart.com/update-steps', '');
INSERT INTO `lbazs_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1285, 8, 0, 'JB Boost Template', '', 'boost', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/boost.xml', 'https://www.joomlart.com/update-steps', ''),
(1286, 8, 0, 'Sample package for Uber Music', '', 'uber_music', 'sample_package', '', 0, '2.0.7', '', 'http://update.joomlart.com/service/tracking/j31/uber_music.xml', 'https://www.joomlart.com/update-steps', ''),
(1287, 8, 0, 'JB Buildr Template', '', 'buildr', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/buildr.xml', 'https://www.joomlart.com/update-steps', ''),
(1288, 8, 0, 'Sample package for Uber Restaurant', '', 'uber_restaurant', 'sample_package', '', 0, '2.0.7', '', 'http://update.joomlart.com/service/tracking/j31/uber_restaurant.xml', 'https://www.joomlart.com/update-steps', ''),
(1289, 8, 0, 'Publisher Pro', '', 'com-publisher-pro', 'component', '', 1, '3.0.20', '', 'http://update.joomlart.com/service/tracking/j31/com-publisher-pro.xml', 'https://www.joomlart.com/update-steps', ''),
(1290, 8, 0, 'Sample package for Uber Spa', '', 'uber_spa', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_spa.xml', 'https://www.joomlart.com/update-steps', ''),
(1291, 8, 0, 'Adagency Pro Component', '', 'com_adagency-pro', 'component', '', 1, '6.1.2', '', 'http://update.joomlart.com/service/tracking/j31/com_adagency-pro.xml', 'https://www.joomlart.com/update-steps', ''),
(1292, 8, 0, 'Sample package for Uber University', '', 'uber_university', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_university.xml', 'https://www.joomlart.com/update-steps', ''),
(1293, 8, 0, 'DT Donate Component', '', 'com_dtdonate_preview', 'component', '', 1, '4.0.5', '', 'http://update.joomlart.com/service/tracking/j31/com_dtdonate_preview.xml', 'https://www.joomlart.com/update-steps', ''),
(1294, 8, 0, 'Sample package for Uber Wedding', '', 'uber_wedding', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_wedding.xml', 'https://www.joomlart.com/update-steps', ''),
(1295, 8, 0, 'DT SMS Component', '', 'com_dtsms', 'component', '', 1, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/com_dtsms.xml', 'https://www.joomlart.com/update-steps', ''),
(1296, 8, 0, 'JB Wedding Template', '', 'wedding', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/wedding.xml', 'https://www.joomlart.com/update-steps', ''),
(1297, 8, 0, 'Guru Pro', '', 'com_guru_pro', 'component', '', 1, '5.2.2', '', 'http://update.joomlart.com/service/tracking/j31/com_guru_pro.xml', 'https://www.joomlart.com/update-steps', ''),
(1298, 8, 0, 'JB Zenbase Template', '', 'zenbase', 'template', '', 0, '1.2.6', '', 'http://update.joomlart.com/service/tracking/j31/zenbase.xml', 'https://www.joomlart.com/update-steps', ''),
(1299, 8, 0, 'JA Builder Component', '', 'com_jabuilder', 'component', '', 1, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/com_jabuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1300, 8, 0, 'JB Zenhost Template', '', 'zenhost', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/zenhost.xml', 'https://www.joomlart.com/update-steps', ''),
(1301, 8, 0, 'JA Extenstion Manager Component for J3.x', '', 'com_jaextmanager', 'component', '', 1, '2.6.5', '', 'http://update.joomlart.com/service/tracking/j31/com_jaextmanager.xml', 'https://www.joomlart.com/update-steps', ''),
(1302, 8, 0, 'JA Joomla GDPR Component', '', 'com_jagdpr', 'component', '', 1, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/com_jagdpr.xml', 'https://www.joomlart.com/update-steps', ''),
(1303, 8, 0, 'JA K2 v3 Filter package for J33', '', 'com_jak2v3filter', 'component', '', 1, '3.0.0 preview ', '', 'http://update.joomlart.com/service/tracking/j31/com_jak2v3filter.xml', '', ''),
(1304, 8, 0, 'JA Multilingual Component for Joomla 2.5 & 3.4', '', 'com_jalang', 'component', '', 1, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/com_jalang.xml', 'https://www.joomlart.com/update-steps', ''),
(1305, 8, 0, 'JA Page Builder Component', '', 'com_japagebuilder', 'component', '', 1, '1.0.0_alpha3', '', 'http://update.joomlart.com/service/tracking/j31/com_japagebuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1306, 8, 0, 'S5 Vertex Updater Component', '', 'com_s5_vertexupdater', 'component', '', 1, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/com_s5_vertexupdater.xml', 'https://www.joomlart.com/update-steps', ''),
(1307, 8, 0, 'JB Corporate Template', '', 'corporate', 'template', '', 0, '3.1.0', '', 'http://update.joomlart.com/service/tracking/j31/corporate.xml', 'https://www.joomlart.com/update-steps', ''),
(1308, 8, 0, 'DT Donate Custom', '', 'dt_donate', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/dt_donate.xml', 'https://www.joomlart.com/update-steps', ''),
(1309, 8, 0, 'DT Donate Campaigns Module', '', 'dtdonate_campaigns', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/dtdonate_campaigns.xml', 'https://www.joomlart.com/update-steps', ''),
(1310, 8, 0, 'DT Donate Donors Module', '', 'dtdonate_donors', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/dtdonate_donors.xml', 'https://www.joomlart.com/update-steps', ''),
(1311, 8, 0, 'JA Intranet Theme for EasyBlog', '', 'easyblog_theme_intranet', 'custom', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/easyblog_theme_intranet.xml', 'https://www.joomlart.com/update-steps', ''),
(1312, 8, 0, 'JA Resume Theme for EasyBlog', '', 'easyblog_theme_resume', 'custom', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/easyblog_theme_resume.xml', 'https://www.joomlart.com/update-steps', ''),
(1313, 8, 0, 'JA Sugite Theme for EasyBlog', '', 'easyblog_theme_sugite', 'custom', '', 0, '2.0.3', '', 'http://update.joomlart.com/service/tracking/j31/easyblog_theme_sugite.xml', 'https://www.joomlart.com/update-steps', ''),
(1314, 8, 0, 'JA Intranet Theme for EasySocialv1 ', '', 'easysocial1_support_intranet', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/easysocial1_support_intranet.xml', 'https://www.joomlart.com/update-steps', ''),
(1315, 8, 0, 'JA Social II Support EasySocial1', '', 'easysocial1_support_social_ii', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/easysocial1_support_social_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(1316, 8, 0, 'JB Ecolife Template', '', 'ecolife', 'template', '', 0, '1.3.5', '', 'http://update.joomlart.com/service/tracking/j31/ecolife.xml', 'https://www.joomlart.com/update-steps', ''),
(1317, 8, 0, 'JB Gazetta Template', '', 'gazetta', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/gazetta.xml', 'https://www.joomlart.com/update-steps', ''),
(1318, 8, 0, 'GK Blend Template', '', 'gk_blend', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_blend.xml', 'https://www.joomlart.com/update-steps', ''),
(1319, 8, 0, 'GK Decor template', '', 'gk_decor', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/gk_decor.xml', 'https://www.joomlart.com/update-steps', ''),
(1320, 8, 0, 'GK EvoNews Template', '', 'gk_evonews', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/gk_evonews.xml', 'https://www.joomlart.com/update-steps', ''),
(1321, 8, 0, 'GK Folio Template', '', 'gk_folio', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_folio.xml', 'https://www.joomlart.com/update-steps', ''),
(1323, 8, 0, 'GK Infinity Template', '', 'gk_infinity', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_infinity.xml', 'https://www.joomlart.com/update-steps', ''),
(1324, 8, 0, 'GK Paradise Template', '', 'gk_paradise', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_paradise.xml', 'https://www.joomlart.com/update-steps', ''),
(1325, 8, 0, 'GK Quark template', '', 'gk_quark', 'template', '', 0, '1.27', '', 'http://update.joomlart.com/service/tracking/j31/gk_quark.xml', 'https://www.joomlart.com/update-steps', ''),
(1326, 8, 0, 'GK Siggi Template', '', 'gk_siggi', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_siggi.xml', 'https://www.joomlart.com/update-steps', ''),
(1327, 8, 0, 'GK Stora Template', '', 'gk_stora', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/gk_stora.xml', 'https://www.joomlart.com/update-steps', ''),
(1328, 8, 0, 'GK Wedding Template', '', 'gk_wedding', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/gk_wedding.xml', 'https://www.joomlart.com/update-steps', ''),
(1329, 8, 0, 'DT Grid 2 Theme', '', 'grid-2_theme', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/grid-2_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(1330, 8, 0, 'JB Grid3 template', '', 'grid3', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/grid3.xml', 'https://www.joomlart.com/update-steps', ''),
(1331, 8, 0, 'JB Grid4 Template', '', 'grid4', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/grid4.xml', 'https://www.joomlart.com/update-steps', ''),
(1332, 8, 0, 'Guru Courses Module', '', 'guru-courses-module', 'module', '', 0, '4.0.2', '', 'http://update.joomlart.com/service/tracking/j31/guru-courses-module.xml', 'https://www.joomlart.com/update-steps', ''),
(1333, 8, 0, 'Guru Search Module', '', 'guru-search-module', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/guru-search-module.xml', 'https://www.joomlart.com/update-steps', ''),
(1334, 8, 0, 'Guru Payment Authorize Plugin', '', 'guru_payauthorize_3.0.0', 'plugin', 'guru_payauthorize_3.', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/guru_payauthorize_3.0.0.xml', 'https://www.joomlart.com/update-steps', ''),
(1335, 8, 0, 'JB Highline2 Template', '', 'highline2', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/highline2.xml', 'https://www.joomlart.com/update-steps', ''),
(1336, 8, 0, 'JB Hub2 Template', '', 'hub2', 'template', '', 0, '2.1.3', '', 'http://update.joomlart.com/service/tracking/j31/hub2.xml', 'https://www.joomlart.com/update-steps', ''),
(1337, 8, 0, 'JB Inspire Template', '', 'inspire', 'template', '', 0, '2.1.2', '', 'http://update.joomlart.com/service/tracking/j31/inspire.xml', 'https://www.joomlart.com/update-steps', ''),
(1338, 8, 0, 'Backend Template', '', 'ja_admin', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_admin.xml', 'https://www.joomlart.com/update-steps', ''),
(1339, 8, 0, 'JA Aiga Template', '', 'ja_aiga', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_aiga.xml', 'https://www.joomlart.com/update-steps', ''),
(1340, 8, 0, 'JA Allure Template', '', 'ja_allure', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_allure.xml', 'https://www.joomlart.com/update-steps', ''),
(1341, 8, 0, 'JA Alumni Template', '', 'ja_alumni', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_alumni.xml', 'https://www.joomlart.com/update-steps', ''),
(1342, 8, 0, 'JA Autoshop Source File', '', 'ja_autoshop', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_autoshop.xml', 'https://www.joomlart.com/update-steps', ''),
(1344, 8, 0, 'JA Beauty Source File', '', 'ja_beauty', 'source', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_beauty.xml', 'https://www.joomlart.com/update-steps', ''),
(1345, 8, 0, 'JA Biz Template', '', 'ja_biz', 'template', '', 0, '1.2.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_biz.xml', 'https://www.joomlart.com/update-steps', ''),
(1346, 8, 0, 'JA Brickstore Template', '', 'ja_brickstore', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_brickstore.xml', 'https://www.joomlart.com/update-steps', ''),
(1347, 8, 0, 'JA Builder Template', '', 'ja_builder', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_builder.xml', 'https://www.joomlart.com/update-steps', ''),
(1348, 8, 0, 'JA Builder Package', '', 'ja_builder_pkg', 'package', '', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_builder_pkg.xml', 'https://www.joomlart.com/update-steps', ''),
(1349, 8, 0, 'JA Cago template', '', 'ja_cago', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_cago.xml', '', ''),
(1350, 8, 0, 'JA Cagox template', '', 'ja_cagox', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_cagox.xml', 'https://www.joomlart.com/update-steps', ''),
(1351, 8, 0, 'JA Charity template', '', 'ja_charity', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_charity.xml', 'https://www.joomlart.com/update-steps', ''),
(1352, 8, 0, 'JA City Guide Template', '', 'ja_cityguide', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_cityguide.xml', 'https://www.joomlart.com/update-steps', ''),
(1353, 8, 0, 'JA Company Template', '', 'ja_company', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_company.xml', 'https://www.joomlart.com/update-steps', ''),
(1354, 8, 0, 'JA Conf Template', '', 'ja_conf', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/ja_conf.xml', 'https://www.joomlart.com/update-steps', ''),
(1355, 8, 0, 'JA Diner Template', '', 'ja_diner', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_diner.xml', 'https://www.joomlart.com/update-steps', ''),
(1356, 8, 0, 'JA Directory Template', '', 'ja_directory', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_directory.xml', 'https://www.joomlart.com/update-steps', ''),
(1357, 8, 0, 'JA Donate Template', '', 'ja_donate', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/ja_donate.xml', 'https://www.joomlart.com/update-steps', ''),
(1358, 8, 0, 'JA Edenite Template for J25 & J34', '', 'ja_edenite', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_edenite.xml', '', ''),
(1359, 8, 0, 'JA Edenite II Template', '', 'ja_edenite_ii', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_edenite_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(1360, 8, 0, 'JA Elicyon Template', '', 'ja_elicyon', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_elicyon.xml', 'https://www.joomlart.com/update-steps', ''),
(1361, 8, 0, 'JA EventCamp Template', '', 'ja_eventcamp', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_eventcamp.xml', 'https://www.joomlart.com/update-steps', ''),
(1362, 8, 0, 'JA Events II template', '', 'ja_events_ii', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_events_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(1363, 8, 0, 'JA Fit Template', '', 'ja_fit', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_fit.xml', 'https://www.joomlart.com/update-steps', ''),
(1365, 8, 0, 'JA Fixel Template', '', 'ja_fixel', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_fixel.xml', 'https://www.joomlart.com/update-steps', ''),
(1367, 8, 0, 'JA Flix Source File', '', 'ja_flix', 'source', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_flix.xml', 'https://www.joomlart.com/update-steps', ''),
(1369, 8, 0, 'JA Focus Template', '', 'ja_focus', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/ja_focus.xml', 'https://www.joomlart.com/update-steps', ''),
(1371, 8, 0, 'JA Good Template', '', 'ja_good', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_good.xml', 'https://www.joomlart.com/update-steps', ''),
(1372, 8, 0, 'JA Healthcare Template', '', 'ja_healthcare', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_healthcare.xml', 'https://www.joomlart.com/update-steps', ''),
(1373, 8, 0, 'JA Hotel Template', '', 'ja_hotel', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_hotel.xml', 'https://www.joomlart.com/update-steps', ''),
(1374, 8, 0, 'JA Insight Template', '', 'ja_insight', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/ja_insight.xml', 'https://www.joomlart.com/update-steps', ''),
(1375, 8, 0, 'JA Intranet Template', '', 'ja_intranet', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_intranet.xml', 'https://www.joomlart.com/update-steps', ''),
(1376, 8, 0, 'ja Justitia Template', '', 'ja_justitia', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_justitia.xml', 'https://www.joomlart.com/update-steps', ''),
(1377, 8, 0, 'JA Kids Corner Source File', '', 'ja_kidscorner', 'template', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_kidscorner.xml', 'https://www.joomlart.com/update-steps', ''),
(1378, 8, 0, 'JA Landscape Template', '', 'ja_landscape', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_landscape.xml', 'https://www.joomlart.com/update-steps', ''),
(1379, 8, 0, 'JA Lawfirm Template ', '', 'ja_lawfirm', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_lawfirm.xml', 'https://www.joomlart.com/update-steps', ''),
(1380, 8, 0, 'JA Magz II Template', '', 'ja_magz_ii', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_magz_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(1381, 8, 0, 'JA Mason Template', '', 'ja_mason', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_mason.xml', 'https://www.joomlart.com/update-steps', ''),
(1382, 8, 0, 'JA Megafilter Template', '', 'ja_megafilter', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_megafilter.xml', 'https://www.joomlart.com/update-steps', ''),
(1383, 8, 0, 'JA Megastore Template', '', 'ja_megastore', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_megastore.xml', 'https://www.joomlart.com/update-steps', ''),
(1384, 8, 0, 'JA Mixstore Source File', '', 'ja_mixstore', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_mixstore.xml', 'https://www.joomlart.com/update-steps', ''),
(1385, 8, 0, 'JA Mono Template', '', 'ja_mono', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_mono.xml', 'https://www.joomlart.com/update-steps', ''),
(1386, 8, 0, 'JA Mood Template', '', 'ja_mood', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_mood.xml', 'https://www.joomlart.com/update-steps', ''),
(1387, 8, 0, 'JA Morgan Template', '', 'ja_morgan', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_morgan.xml', 'https://www.joomlart.com/update-steps', ''),
(1388, 8, 0, 'JA Moviemax Template', '', 'ja_moviemax', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_moviemax.xml', 'https://www.joomlart.com/update-steps', ''),
(1389, 8, 0, 'JA Muzic Template for J25 & J3x', '', 'ja_muzic', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_muzic.xml', 'https://www.joomlart.com/update-steps', ''),
(1390, 8, 0, 'JA Oslo Template', '', 'ja_oslo', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_oslo.xml', 'https://www.joomlart.com/update-steps', ''),
(1391, 8, 0, 'JA Platon Template', '', 'ja_platon', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_platon.xml', 'https://www.joomlart.com/update-steps', ''),
(1392, 8, 0, 'JA Platon Template for DT Register', '', 'ja_platon_for_dt_register', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_platon_for_dt_register.xml', 'https://www.joomlart.com/update-steps', ''),
(1393, 8, 0, 'JA Play School Template', '', 'ja_playschool', 'template', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/ja_playschool.xml', 'https://www.joomlart.com/update-steps', ''),
(1394, 8, 0, 'JA Playstore Template', '', 'ja_playstore', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_playstore.xml', 'https://www.joomlart.com/update-steps', ''),
(1395, 8, 0, 'JA Property Template', '', 'ja_property', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_property.xml', 'https://www.joomlart.com/update-steps', ''),
(1396, 8, 0, 'JA Rave Template for Joomla 3.x', '', 'ja_rave', 'template', '', 0, '2.5.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_rave.xml', '', ''),
(1397, 8, 0, 'JA Rent template', '', 'ja_rent', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_rent.xml', 'https://www.joomlart.com/update-steps', ''),
(1398, 8, 0, 'JA Resume Template', '', 'ja_resume', 'template', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_resume.xml', 'https://www.joomlart.com/update-steps', ''),
(1399, 8, 0, 'JA Sensei Source File', '', 'ja_sensei', 'template', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_sensei.xml', 'https://www.joomlart.com/update-steps', ''),
(1400, 8, 0, 'JA Shoe Template', '', 'ja_shoe', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_shoe.xml', 'https://www.joomlart.com/update-steps', ''),
(1401, 8, 0, 'JA Simpli Template', '', 'ja_simpli', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/ja_simpli.xml', 'https://www.joomlart.com/update-steps', ''),
(1402, 8, 0, 'JA Smallbiz Template', '', 'ja_smallbiz', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/ja_smallbiz.xml', 'https://www.joomlart.com/update-steps', ''),
(1403, 8, 0, 'JA Social II template', '', 'ja_social_ii', 'template', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/ja_social_ii.xml', 'https://www.joomlart.com/update-steps', ''),
(1404, 8, 0, 'JA Space Template', '', 'ja_space', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_space.xml', 'https://www.joomlart.com/update-steps', ''),
(1405, 8, 0, 'JA Stark Template', '', 'ja_stark', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_stark.xml', 'https://www.joomlart.com/update-steps', ''),
(1406, 8, 0, 'JA Symphony Template', '', 'ja_symphony', 'template', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/ja_symphony.xml', 'https://www.joomlart.com/update-steps', ''),
(1407, 8, 0, 'JA Techzone Template', '', 'ja_techzone', 'template', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_techzone.xml', 'https://www.joomlart.com/update-steps', ''),
(1408, 8, 0, 'JA Teline V Template', '', 'ja_teline_v', 'template', '', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_teline_v.xml', 'https://www.joomlart.com/update-steps', ''),
(1409, 8, 0, 'JA University Template for J25 & J32', '', 'ja_university', 'template', '', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/ja_university.xml', 'https://www.joomlart.com/update-steps', ''),
(1410, 8, 0, 'JA University T3 template', '', 'ja_university_t3', 'template', '', 0, '1.1.8', '', 'http://update.joomlart.com/service/tracking/j31/ja_university_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(1411, 8, 0, 'JA Vintas Template for J25 & J3x', '', 'ja_vintas', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/ja_vintas.xml', 'https://www.joomlart.com/update-steps', ''),
(1412, 8, 0, 'JA Wall Template for J25 & J34', '', 'ja_wall', 'template', '', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/ja_wall.xml', '', ''),
(1413, 8, 0, 'JB Ascent2 Template', '', 'jb_ascent2', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_ascent2.xml', 'https://www.joomlart.com/update-steps', ''),
(1414, 8, 0, 'JB Base3 Template', '', 'jb_base3', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/jb_base3.xml', 'https://www.joomlart.com/update-steps', ''),
(1415, 8, 0, 'JB Blanko Template', '', 'jb_blanko', 'template', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_blanko.xml', 'https://www.joomlart.com/update-steps', ''),
(1416, 8, 0, 'JB Client Template', '', 'jb_client', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_client.xml', 'https://www.joomlart.com/update-steps', ''),
(1417, 8, 0, 'JB Colourshift2 Template', '', 'jb_colourshift2', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/jb_colourshift2.xml', 'https://www.joomlart.com/update-steps', ''),
(1418, 8, 0, 'JB Corporation Template', '', 'jb_corporation', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/jb_corporation.xml', 'https://www.joomlart.com/update-steps', ''),
(1419, 8, 0, 'JB Flux Template', '', 'jb_flux', 'template', '', 0, '2.0.4', '', 'http://update.joomlart.com/service/tracking/j31/jb_flux.xml', 'https://www.joomlart.com/update-steps', ''),
(1420, 8, 0, 'JB Focus2 Template', '', 'jb_focus2', 'template', '', 0, '1.3.7', '', 'http://update.joomlart.com/service/tracking/j31/jb_focus2.xml', 'https://www.joomlart.com/update-steps', ''),
(1421, 8, 0, 'JB Italian Template', '', 'jb_italian', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/jb_italian.xml', 'https://www.joomlart.com/update-steps', ''),
(1422, 8, 0, 'JB Maxbiz2 Template', '', 'jb_maxbiz2', 'template', '', 0, '1.4.3', '', 'http://update.joomlart.com/service/tracking/j31/jb_maxbiz2.xml', 'https://www.joomlart.com/update-steps', ''),
(1423, 8, 0, 'JB Moments Template', '', 'jb_moments', 'template', '', 0, '1.4.3', '', 'http://update.joomlart.com/service/tracking/j31/jb_moments.xml', 'https://www.joomlart.com/update-steps', ''),
(1424, 8, 0, 'JB Motion Template', '', 'jb_motion', 'template', '', 0, '2.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_motion.xml', 'https://www.joomlart.com/update-steps', ''),
(1425, 8, 0, 'JB Revision Template', '', 'jb_revision', 'template', '', 0, '1.2.9', '', 'http://update.joomlart.com/service/tracking/j31/jb_revision.xml', 'https://www.joomlart.com/update-steps', ''),
(1426, 8, 0, 'JB SideWinder tpl', '', 'jb_sidewinder', 'template', '', 0, '2.0.5', '', 'http://update.joomlart.com/service/tracking/j31/jb_sidewinder.xml', 'https://www.joomlart.com/update-steps', ''),
(1427, 8, 0, 'JB Utafiti Template', '', 'jb_utafiti', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/jb_utafiti.xml', 'https://www.joomlart.com/update-steps', ''),
(1428, 8, 0, 'JB Venture Template', '', 'jb_venture', 'template', '', 0, '1.3.6', '', 'http://update.joomlart.com/service/tracking/j31/jb_venture.xml', 'https://www.joomlart.com/update-steps', ''),
(1429, 8, 0, 'JS Column Theme', '', 'jomsocial_column', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_column.xml', 'https://www.joomlart.com/update-steps', ''),
(1430, 8, 0, 'JS Flat Theme', '', 'jomsocial_flat', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_flat.xml', 'https://www.joomlart.com/update-steps', ''),
(1431, 8, 0, 'JS Column Theme', '', 'jomsocial_jasocial', 'custom', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_jasocial.xml', 'https://www.joomlart.com/update-steps', ''),
(1432, 8, 0, 'JS Kikiriki Theme', '', 'jomsocial_kikiriki', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_kikiriki.xml', 'https://www.joomlart.com/update-steps', ''),
(1433, 8, 0, 'JS Shadow Theme', '', 'jomsocial_shadow', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_shadow.xml', 'https://www.joomlart.com/update-steps', ''),
(1434, 8, 0, 'Jomsocial theme for Platon', '', 'jomsocial_theme_platon', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/jomsocial_theme_platon.xml', 'https://www.joomlart.com/update-steps', ''),
(1435, 8, 0, 'JS Column Theme', '', 'js_column', 'custom', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/js_column.xml', 'https://www.joomlart.com/update-steps', ''),
(1436, 8, 0, 'JS Flat Theme', '', 'js_flat', 'custom', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/js_flat.xml', 'https://www.joomlart.com/update-steps', ''),
(1437, 8, 0, 'JS Kikiriki Theme', '', 'js_kikiriki', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/js_kikiriki.xml', 'https://www.joomlart.com/update-steps', ''),
(1438, 8, 0, 'JS Shadow Theme', '', 'js_shadow', 'custom', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/js_shadow.xml', 'https://www.joomlart.com/update-steps', ''),
(1439, 8, 0, 'Theme Fixel for JShopping J25 & J3x', '', 'jshopping_theme_fixel', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/jshopping_theme_fixel.xml', 'https://www.joomlart.com/update-steps', ''),
(1440, 8, 0, 'JA Tiris Jshopping theme for J3x', '', 'jshopping_theme_tiris_j3x', 'custom', '', 0, '2.5.8', '', 'http://update.joomlart.com/service/tracking/j31/jshopping_theme_tiris_j3x.xml', 'https://www.joomlart.com/update-steps', ''),
(1441, 8, 0, 'JB Koan Template', '', 'koan', 'template', '', 0, '1.2.2', '', 'http://update.joomlart.com/service/tracking/j31/koan.xml', 'https://www.joomlart.com/update-steps', ''),
(1442, 8, 0, 'JA Mitius Kunena Theme for Joomla 3x', '', 'kunena_theme_mitius', 'custom', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_mitius.xml', 'https://www.joomlart.com/update-steps', ''),
(1443, 8, 0, 'JA Tiris Kunena Theme for Joomla 3x', '', 'kunena_theme_mitius_j31', 'custom', '', 0, '2.5.4', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_mitius_j31.xml', '', ''),
(1444, 8, 0, 'Kunena Theme MovieMax', '', 'kunena_theme_moviemax', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_moviemax.xml', 'https://www.joomlart.com/update-steps', ''),
(1445, 8, 0, 'Kunena Theme Platon', '', 'kunena_theme_platon', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_platon.xml', 'https://www.joomlart.com/update-steps', ''),
(1446, 8, 0, 'Kunena Theme Playstore', '', 'kunena_theme_playstore', 'custom', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_playstore.xml', 'https://www.joomlart.com/update-steps', ''),
(1447, 8, 0, 'JA Tiris Kunena Theme for Joomla 3x', '', 'kunena_theme_tiris_j3x', 'custom', '', 0, '2.5.6', '', 'http://update.joomlart.com/service/tracking/j31/kunena_theme_tiris_j3x.xml', 'https://www.joomlart.com/update-steps', ''),
(1448, 8, 0, 'DT Linear Theme', '', 'linear_theme', 'custom', '', 0, '1.0.8', '', 'http://update.joomlart.com/service/tracking/j31/linear_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(1449, 8, 0, 'JB Medica Template', '', 'medica', 'template', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/medica.xml', 'https://www.joomlart.com/update-steps', ''),
(1450, 8, 0, 'Mijoshop V2 Modules Accordion', '', 'mijoshop_mod_accordion', 'custom', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_accordion.xml', 'https://www.joomlart.com/update-steps', ''),
(1451, 8, 0, 'Mijoshop V3 Modules Accordion', '', 'mijoshop_mod_accordion_v3', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_accordion_v3.xml', 'https://www.joomlart.com/update-steps', ''),
(1452, 8, 0, 'Mijoshop V2 Modules Slider', '', 'mijoshop_mod_slider', 'custom', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_slider.xml', 'https://www.joomlart.com/update-steps', ''),
(1453, 8, 0, 'Mijoshop V3 Modules Slider', '', 'mijoshop_mod_slider_v3', 'custom', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_mod_slider_v3.xml', 'https://www.joomlart.com/update-steps', ''),
(1454, 8, 0, 'JA Bookshop Theme for Mijoshop V3', '', 'mijoshop_theme_bookshop_v3', 'custom', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/mijoshop_theme_bookshop_v3.xml', 'https://www.joomlart.com/update-steps', ''),
(1455, 8, 0, 'S5 Tell A Friend Module', '', 'mod_S5tellafriend', 'module', '', 0, '3.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_S5tellafriend.xml', 'https://www.joomlart.com/update-steps', ''),
(1456, 8, 0, 'JS Toolbar Module', '', 'mod_community_bar', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_bar.xml', 'https://www.joomlart.com/update-steps', ''),
(1457, 8, 0, 'JS Birthdays Module', '', 'mod_community_birthdays', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_birthdays.xml', 'https://www.joomlart.com/update-steps', ''),
(1458, 8, 0, 'JS Dating Search Module', '', 'mod_community_dating', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_dating.xml', 'https://www.joomlart.com/update-steps', ''),
(1459, 8, 0, 'JS Events Suggestions', '', 'mod_community_eventssuggestions', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_eventssuggestions.xml', 'https://www.joomlart.com/update-steps', ''),
(1460, 8, 0, 'JS Friends Suggestions', '', 'mod_community_friendssuggestions', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_friendssuggestions.xml', 'https://www.joomlart.com/update-steps', ''),
(1461, 8, 0, 'JS Groups Suggestions', '', 'mod_community_groupssuggestions', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_groupssuggestions.xml', 'https://www.joomlart.com/update-steps', ''),
(1462, 8, 0, 'JS Members Map', '', 'mod_community_membersmap', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_membersmap.xml', 'https://www.joomlart.com/update-steps', ''),
(1463, 8, 0, 'JS Popular Events', '', 'mod_community_popular_events', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_popular_events.xml', 'https://www.joomlart.com/update-steps', ''),
(1464, 8, 0, 'JS Popular Groups', '', 'mod_community_popular_groups', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_popular_groups.xml', 'https://www.joomlart.com/update-steps', ''),
(1465, 8, 0, 'JS Profile Completeness', '', 'mod_community_profilecompleteness', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_profilecompleteness.xml', 'https://www.joomlart.com/update-steps', ''),
(1466, 8, 0, 'Community Toolbar Module', '', 'mod_community_toolbar', 'module', '', 0, '2.1.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_toolbar.xml', 'https://www.joomlart.com/update-steps', ''),
(1467, 8, 0, 'JS Trending Events', '', 'mod_community_trending_events', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_events.xml', 'https://www.joomlart.com/update-steps', ''),
(1468, 8, 0, 'JS Trending Groups', '', 'mod_community_trending_groups', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_groups.xml', 'https://www.joomlart.com/update-steps', ''),
(1469, 8, 0, 'JS Trending Hashtags', '', 'mod_community_trending_hashtags', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_hashtags.xml', 'https://www.joomlart.com/update-steps', ''),
(1470, 8, 0, 'JS Trending Photo', '', 'mod_community_trending_photos', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_photos.xml', 'https://www.joomlart.com/update-steps', ''),
(1471, 8, 0, 'JS Trending Video', '', 'mod_community_trending_videos', 'module', '', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_community_trending_videos.xml', 'https://www.joomlart.com/update-steps', ''),
(1472, 8, 0, 'Dribbblr Module', '', 'mod_dribbblr', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_dribbblr.xml', 'https://www.joomlart.com/update-steps', ''),
(1473, 8, 0, 'JA Google Analytics Frontend', '', 'mod_ja_ga', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_ja_ga.xml', 'https://www.joomlart.com/update-steps', ''),
(1474, 8, 0, 'JA Latest  Article Module', '', 'mod_jaarticles_latest', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jaarticles_latest.xml', 'https://www.joomlart.com/update-steps', ''),
(1475, 8, 0, 'JA Builder Admin Menu Module', '', 'mod_jabuilder_admin_menu', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jabuilder_admin_menu.xml', 'https://www.joomlart.com/update-steps', ''),
(1476, 8, 0, 'JA Builder Quickicons Module', '', 'mod_jabuilder_quickicons', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jabuilder_quickicons.xml', 'https://www.joomlart.com/update-steps', ''),
(1477, 8, 0, 'JA Finance Module', '', 'mod_jafinance', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jafinance.xml', 'https://www.joomlart.com/update-steps', ''),
(1478, 8, 0, 'JA Google Analytics', '', 'mod_jagoogle_analytics', 'module', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/mod_jagoogle_analytics.xml', 'https://www.joomlart.com/update-steps', ''),
(1479, 8, 0, 'JA Google Chart Module', '', 'mod_jagooglechart', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jagooglechart.xml', 'https://www.joomlart.com/update-steps', ''),
(1480, 8, 0, 'JA Halloween Game for Joomla 3.x', '', 'mod_jahalloweengame', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jahalloweengame.xml', 'https://www.joomlart.com/update-steps', ''),
(1481, 8, 0, 'JA K2 v3 Filter Module for J33', '', 'mod_jak2v3filter', 'module', '', 0, '3.0.0 preview ', '', 'http://update.joomlart.com/service/tracking/j31/mod_jak2v3filter.xml', '', ''),
(1482, 8, 0, 'JA Masthead Module ', '', 'mod_jamasthead', 'module', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_jamasthead.xml', 'https://www.joomlart.com/update-steps', ''),
(1483, 8, 0, 'JA Megafilter Module', '', 'mod_jamegafilter', 'module', '', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/mod_jamegafilter.xml', 'https://www.joomlart.com/update-steps', ''),
(1484, 8, 0, 'JA Promo Bar module', '', 'mod_japromobar', 'module', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_japromobar.xml', 'https://www.joomlart.com/update-steps', ''),
(1485, 8, 0, 'Ja Yahoo Finance', '', 'mod_jayahoo_finance', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_jayahoo_finance.xml', 'https://www.joomlart.com/update-steps', ''),
(1486, 8, 0, 'JA Yahoo Weather', '', 'mod_jayahoo_weather', 'module', '', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jayahoo_weather.xml', 'https://www.joomlart.com/update-steps', ''),
(1487, 8, 0, 'JB Contact Module', '', 'mod_jbcontact', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbcontact.xml', 'https://www.joomlart.com/update-steps', ''),
(1488, 8, 0, 'JB Countdown Module', '', 'mod_jbcountdown', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbcountdown.xml', 'https://www.joomlart.com/update-steps', ''),
(1489, 8, 0, 'Jbmaps Module', '', 'mod_jbmaps', 'module', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbmaps.xml', 'https://www.joomlart.com/update-steps', ''),
(1490, 8, 0, 'JB Maps2 Module', '', 'mod_jbmaps2', 'module', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_jbmaps2.xml', 'https://www.joomlart.com/update-steps', ''),
(1491, 8, 0, 'JFlickr Module', '', 'mod_jflickr', 'module', '', 0, '1.4.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_jflickr.xml', 'https://www.joomlart.com/update-steps', ''),
(1492, 8, 0, 'S5 Accordion Menu Module', '', 'mod_s5_accordion_menu', 'module', '', 0, '2.3.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_accordion_menu.xml', 'https://www.joomlart.com/update-steps', ''),
(1493, 8, 0, 'S5 Box Module', '', 'mod_s5_box', 'module', '', 0, '6.1.5', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_box.xml', 'https://www.joomlart.com/update-steps', ''),
(1494, 8, 0, 'S5 Contact Popup Module', '', 'mod_s5_contact_popup', 'module', '', 0, '1.5.3', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_contact_popup.xml', 'https://www.joomlart.com/update-steps', ''),
(1495, 8, 0, 'S5 Domain Checker Module', '', 'mod_s5_domain_check', 'module', '', 0, '1.6.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_domain_check.xml', 'https://www.joomlart.com/update-steps', ''),
(1496, 8, 0, 'S5 Flow Module', '', 'mod_s5_flow', 'module', '', 0, '3.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_flow.xml', 'https://www.joomlart.com/update-steps', ''),
(1497, 8, 0, 'S5 Frontpage Display V2 Module', '', 'mod_s5_frontpage_display2', 'module', '', 0, '2.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_frontpage_display2.xml', 'https://www.joomlart.com/update-steps', ''),
(1498, 8, 0, 'S5 Frontpage Display V3 Module', '', 'mod_s5_frontpage_display3', 'module', '', 0, '3.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_frontpage_display3.xml', 'https://www.joomlart.com/update-steps', ''),
(1499, 8, 0, 'S5 Habla Chat Module', '', 'mod_s5_habla_chat', 'module', '', 0, '1.6.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_habla_chat.xml', 'https://www.joomlart.com/update-steps', ''),
(1500, 8, 0, 'S5 Horizontal Accordion Module', '', 'mod_s5_horizontal_accordion', 'module', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_horizontal_accordion.xml', 'https://www.joomlart.com/update-steps', ''),
(1501, 8, 0, 'S5 Images And Content Fader V3 Module', '', 'mod_s5_image_and_content_faderv3', 'module', '', 0, '3.2.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_image_and_content_faderv3.xml', 'https://www.joomlart.com/update-steps', ''),
(1502, 8, 0, 'S5 Image Content Fader v4 Module', '', 'mod_s5_image_and_content_faderv4', 'module', '', 0, '4.4.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_image_and_content_faderv4.xml', 'https://www.joomlart.com/update-steps', ''),
(1503, 8, 0, 'S5 Image Scroller Module', '', 'mod_s5_image_scroller', 'module', '', 0, '3.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_image_scroller.xml', 'https://www.joomlart.com/update-steps', ''),
(1504, 8, 0, 'S5 Image Slide Module', '', 'mod_s5_imageslide', 'module', '', 0, '4.2.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_imageslide.xml', 'https://www.joomlart.com/update-steps', ''),
(1505, 8, 0, 'S5 Instagram Feed Module', '', 'mod_s5_instagram_feed', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_instagram_feed.xml', 'https://www.joomlart.com/update-steps', ''),
(1506, 8, 0, 'S5 Live Search Module', '', 'mod_s5_live_search', 'module', '', 0, '3.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_live_search.xml', 'https://www.joomlart.com/update-steps', ''),
(1507, 8, 10027, 'S5 Mailchimp Signup Module', '', 'mod_s5_mailchimp_signup', 'module', '', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_mailchimp_signup.xml', 'https://www.joomlart.com/update-steps', ''),
(1508, 8, 0, 'S5 Masonry Module', '', 'mod_s5_masonry', 'module', '', 0, '2.9.9', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_masonry.xml', 'https://www.joomlart.com/update-steps', ''),
(1509, 8, 0, 'S5 MP3 Gallery Module', '', 'mod_s5_mp3_gallery', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_mp3_gallery.xml', 'https://www.joomlart.com/update-steps', ''),
(1510, 8, 0, 'S5 News Display 2 Module', '', 'mod_s5_news_display_2', 'module', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_news_display_2.xml', 'https://www.joomlart.com/update-steps', ''),
(1511, 8, 0, 'S5 New Sticker V3 Module', '', 'mod_s5_newstickerv3', 'module', '', 0, '4.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_newstickerv3.xml', 'https://www.joomlart.com/update-steps', ''),
(1512, 8, 0, 'S5 Open Table Module', '', 'mod_s5_opentable', 'module', '', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_opentable.xml', 'https://www.joomlart.com/update-steps', ''),
(1513, 8, 0, 'S5 Photo Showcase Module', '', 'mod_s5_photo_showcase', 'module', '', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_photo_showcase.xml', 'https://www.joomlart.com/update-steps', ''),
(1514, 8, 0, 'S5 Photo Showcase V2 Module', '', 'mod_s5_photo_showcase_v2', 'module', '', 0, '3.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_photo_showcase_v2.xml', 'https://www.joomlart.com/update-steps', ''),
(1515, 8, 0, 'S5 Quick Contact Module', '', 'mod_s5_quick_contact', 'module', '', 0, '4.3.3', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_quick_contact.xml', 'https://www.joomlart.com/update-steps', ''),
(1516, 8, 0, 'S5 Register Module', '', 'mod_s5_register', 'module', '', 0, '4.0.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_register.xml', 'https://www.joomlart.com/update-steps', ''),
(1517, 8, 0, 'S5 Reservations Module', '', 'mod_s5_reservations', 'module', '', 0, '4.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_reservations.xml', 'https://www.joomlart.com/update-steps', ''),
(1518, 8, 0, 'S5 Snipcart Module', '', 'mod_s5_snipcart', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_snipcart.xml', 'https://www.joomlart.com/update-steps', ''),
(1519, 8, 0, 'S5 Spotlight News Module', '', 'mod_s5_spotlight_news', 'module', '', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_spotlight_news.xml', 'https://www.joomlart.com/update-steps', ''),
(1520, 8, 0, 'S5 Tab Show v3 Module', '', 'mod_s5_tab_show', 'module', '', 0, '3.3.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_tab_show.xml', 'https://www.joomlart.com/update-steps', ''),
(1521, 8, 0, 'S5 Tab Show v2 Module', '', 'mod_s5_tabshow_v2', 'module', '', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_tabshow_v2.xml', 'https://www.joomlart.com/update-steps', ''),
(1522, 8, 0, 'S5 Typed Text Module', '', 'mod_s5_typed_text', 'module', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_typed_text.xml', 'https://www.joomlart.com/update-steps', ''),
(1523, 8, 0, 'S5 Vertical Accordion Module', '', 'mod_s5_vertical_accordion', 'module', '', 0, '4.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_vertical_accordion.xml', 'https://www.joomlart.com/update-steps', ''),
(1524, 8, 0, 'S5 Weather Module', '', 'mod_s5_weather', 'module', '', 0, '3.1.0', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5_weather.xml', 'https://www.joomlart.com/update-steps', ''),
(1525, 8, 0, 'S5 Mapit Module', '', 'mod_s5mapit', 'module', '', 0, '1.8.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_s5mapit.xml', 'https://www.joomlart.com/update-steps', ''),
(1526, 8, 0, 'JB Skillset Module', '', 'mod_skillset', 'module', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_skillset.xml', 'https://www.joomlart.com/update-steps', ''),
(1527, 8, 0, 'SP Contact Module', '', 'mod_sp_quickcontact', 'module', '', 0, '1.4', '', 'http://update.joomlart.com/service/tracking/j31/mod_sp_quickcontact.xml', 'https://www.joomlart.com/update-steps', ''),
(1528, 8, 0, 'JB Zenfeature Table Module', '', 'mod_zenfeaturetable', 'module', '', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/mod_zenfeaturetable.xml', 'https://www.joomlart.com/update-steps', ''),
(1529, 8, 0, 'JB Zen Social Module', '', 'mod_zensocial', 'module', '', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/mod_zensocial.xml', 'https://www.joomlart.com/update-steps', ''),
(1530, 8, 0, 'JB ZenTools Module', '', 'mod_zentools', 'module', '', 0, '1.14.6', '', 'http://update.joomlart.com/service/tracking/j31/mod_zentools.xml', 'https://www.joomlart.com/update-steps', ''),
(1531, 8, 0, 'Zentools 2 Module', '', 'mod_zentools2', 'module', '', 0, '2.4.8', '', 'http://update.joomlart.com/service/tracking/j31/mod_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1532, 8, 0, 'JB Nebula Template', '', 'nebula', 'template', '', 0, '1.4.3', '', 'http://update.joomlart.com/service/tracking/j31/nebula.xml', 'https://www.joomlart.com/update-steps', '');
INSERT INTO `lbazs_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1533, 8, 0, 'JB Newlifestyle Template', '', 'newlifestyle', 'template', '', 0, '2.0.9', '', 'http://update.joomlart.com/service/tracking/j31/newlifestyle.xml', 'https://www.joomlart.com/update-steps', ''),
(1534, 8, 0, 'JB Newstream2 Template', '', 'newstream2', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/newstream2.xml', 'https://www.joomlart.com/update-steps', ''),
(1535, 8, 0, 'JB Onepage Template', '', 'onepage', 'template', '', 0, '1.4.2', '', 'http://update.joomlart.com/service/tracking/j31/onepage.xml', 'https://www.joomlart.com/update-steps', ''),
(1536, 8, 0, 'JB OnePageTwo Template', '', 'onepagetwo', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/onepagetwo.xml', 'https://www.joomlart.com/update-steps', ''),
(1537, 8, 0, 'GURU Payment Plugin - PayPal Pro', '', 'paypalpro', 'plugin', 'paypalpro', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/paypalpro.xml', 'https://www.joomlart.com/update-steps', ''),
(1538, 8, 0, 'Plugin Ajax JA Content Type', '', 'jacontenttype', 'plugin', 'ajax', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_ajax_jacontenttype.xml', 'https://www.joomlart.com/update-steps', ''),
(1539, 8, 0, 'JB Zengrid Framework 5', '', 'zengridframework', 'plugin', 'ajax', 0, '5.1.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_ajax_zengridframework.xml', 'https://www.joomlart.com/update-steps', ''),
(1540, 8, 0, 'Plugin Ajax Zentools2 ', '', 'zentools2', 'plugin', 'ajax', 0, '2.3.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_ajax_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1541, 8, 0, 'DT SMS Amazonsns Plugin', '', 'amazonsns', 'plugin', 'dtsms', 0, '1.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_dtsms_amazonsns.xml', 'https://www.joomlart.com/update-steps', ''),
(1542, 8, 0, 'DT SMS Draft Plugin', '', 'smsdraft', 'plugin', 'dtsms', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_dtsms_smsdraft.xml', 'https://www.joomlart.com/update-steps', ''),
(1543, 8, 0, 'JS Force Avatar', '', 'plg_forceavatar', 'plugin', 'plg_forceavatar', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_forceavatar.xml', 'https://www.joomlart.com/update-steps', ''),
(1544, 8, 0, 'JS Force Fields', '', 'plg_forcefields', 'plugin', 'plg_forcefields', 0, '4.7.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_forcefields.xml', 'https://www.joomlart.com/update-steps', ''),
(1545, 8, 0, 'GK Typography Plugin', '', 'typography', 'plugin', 'gk', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/plg_gk_typography.xml', 'https://www.joomlart.com/update-steps', ''),
(1546, 8, 0, 'JA GDPR AcyMailing Plugin', '', 'acymailing', 'plugin', 'jagdpr', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_acymailing.xml', 'https://www.joomlart.com/update-steps', ''),
(1547, 8, 0, 'JA GDPR Adagency Plugin', '', 'adagency', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_adagency.xml', 'https://www.joomlart.com/update-steps', ''),
(1548, 8, 0, 'JA GDPR DJ Catalog Plugin', '', 'catalog', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_catalog.xml', 'https://www.joomlart.com/update-steps', ''),
(1549, 8, 0, 'JA GDPR DJ Classifieds Plugin', '', 'classifieds', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_classifieds.xml', 'https://www.joomlart.com/update-steps', ''),
(1550, 8, 0, 'JA GDPR Community Builder Plugin', '', 'communitybuilder', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_communitybuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1551, 8, 0, 'JA GDPR Custom Plugin', '', 'custom', 'plugin', 'jagdpr', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_custom.xml', 'https://www.joomlart.com/update-steps', ''),
(1552, 8, 0, 'JA GDPR General Plugin', '', 'general', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_general.xml', 'https://www.joomlart.com/update-steps', ''),
(1553, 8, 0, 'JA GDPR Hikashop Plugin', '', 'hikashop', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_hikashop.xml', 'https://www.joomlart.com/update-steps', ''),
(1554, 8, 0, 'JA GDPR J2Store Plugin', '', 'j2store', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_j2store.xml', 'https://www.joomlart.com/update-steps', ''),
(1555, 8, 0, 'JA GDPR Joomla Plugin', '', 'joomla', 'plugin', 'jagdpr', 0, '1.0.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_joomla.xml', 'https://www.joomlart.com/update-steps', ''),
(1556, 8, 0, 'JA GDPR JShopping Plugin', '', 'jshopping', 'plugin', 'jagdpr', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_jshopping.xml', 'https://www.joomlart.com/update-steps', ''),
(1557, 8, 0, 'JA GDPR Mijoshop Plugin', '', 'mijoshop', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_mijoshop.xml', 'https://www.joomlart.com/update-steps', ''),
(1558, 8, 0, 'JA GDPR RSForm Plugin', '', 'rsform', 'plugin', 'jagdpr', 0, '1.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jagdpr_rsform.xml', 'https://www.joomlart.com/update-steps', ''),
(1559, 8, 0, 'JA K2 Data Migration plugin', '', 'plg_jak2tocomcontentmigration', 'plugin', 'plg_jak2tocomcontent', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_jak2tocomcontentmigration.xml', 'https://www.joomlart.com/update-steps', ''),
(1560, 8, 0, 'Plgin JA K2 import to Joomla Content', '', 'plg_jak2tocontent', 'plugin', 'plg_jak2tocontent', 0, '1.0.0 beta', '', 'http://update.joomlart.com/service/tracking/j31/plg_jak2tocontent.xml', '', ''),
(1561, 8, 0, 'JA Megafilter Joomla Custom Fields Plugin', '', 'content', 'plugin', 'jamegafilter', 0, '1.1.4', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_content.xml', 'https://www.joomlart.com/update-steps', ''),
(1562, 8, 0, 'JA Megafilter DOCman Plugin', '', 'docman', 'plugin', 'jamegafilter', 0, '1.1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_docman.xml', 'https://www.joomlart.com/update-steps', ''),
(1563, 8, 0, 'JA Megafilter EShop Plugin', '', 'eshop', 'plugin', 'jamegafilter', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_eshop.xml', 'https://www.joomlart.com/update-steps', ''),
(1564, 8, 0, 'JA Megafilter HikaShop Plugin', '', 'hikashop', 'plugin', 'jamegafilter', 0, '1.1.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_hikashop.xml', 'https://www.joomlart.com/update-steps', ''),
(1565, 8, 0, 'JA Megafilter J2store Plugin', '', 'j2store', 'plugin', 'jamegafilter', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_j2store.xml', 'https://www.joomlart.com/update-steps', ''),
(1566, 8, 0, 'JA Megafilter for Hikashop plg', '', 'ja_hikashop', 'plugin', 'jamegafilter', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_ja_hikashop.xml', '', ''),
(1567, 8, 0, 'JA Megafilter JoomShopping Plugin', '', 'jshopping', 'plugin', 'jamegafilter', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_jshopping.xml', 'https://www.joomlart.com/update-steps', ''),
(1568, 8, 0, 'JA Megafilter K2 Plugin', '', 'k2', 'plugin', 'jamegafilter', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_k2.xml', 'https://www.joomlart.com/update-steps', ''),
(1569, 8, 0, 'JA Megafilter Mijoshop Plugin', '', 'mijoshop', 'plugin', 'jamegafilter', 0, '1.0.9', '', 'http://update.joomlart.com/service/tracking/j31/plg_jamegafilter_mijoshop.xml', 'https://www.joomlart.com/update-steps', ''),
(1570, 8, 0, 'JA Payment Gateway', '', 'gateway', 'plugin', 'joomart', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_joomart_gateway.xml', 'https://www.joomlart.com/update-steps', ''),
(1571, 8, 0, 'JA K2 Extrafields', '', 'jak2extrafields', 'plugin', 'k2', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_k2_jak2extrafields.xml', 'https://www.joomlart.com/update-steps', ''),
(1572, 8, 0, 'JA K2 v3 Filter Plugin for J33', '', 'jak2v3filter', 'plugin', 'k2', 0, '3.0.0 preview ', '', 'http://update.joomlart.com/service/tracking/j31/plg_k2_jak2v3filter.xml', '', ''),
(1573, 8, 0, 'GK Reservation system plugin', '', 'gkreservation', 'plugin', 'sys', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_gkreservation.xml', 'https://www.joomlart.com/update-steps', ''),
(1574, 8, 0, 'System Zenanimate Plugin', '', 'zenanimate', 'plugin', 'sys', 0, '1.1.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zenanimate.xml', 'https://www.joomlart.com/update-steps', ''),
(1575, 8, 0, 'JB Zenbridge Plugin', '', 'zenbridge', 'plugin', 'sys', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zenbridge.xml', 'https://www.joomlart.com/update-steps', ''),
(1576, 8, 0, 'System Zencompiler Plugin', '', 'zencompiler', 'plugin', 'sys', 0, '1.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zencompiler.xml', 'https://www.joomlart.com/update-steps', ''),
(1577, 8, 0, 'JB Zen Menu Plugin', '', 'zenmenu', 'plugin', 'sys', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zenmenu.xml', 'https://www.joomlart.com/update-steps', ''),
(1578, 8, 0, 'Plugin Zentools2 System', '', 'zentools2', 'plugin', 'sys', 0, '2.3.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_sys_zentools2.xml', 'https://www.joomlart.com/update-steps', ''),
(1579, 8, 0, 'JA System Designit Plugin', '', 'designit', 'plugin', 'system', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_designit.xml', 'https://www.joomlart.com/update-steps', ''),
(1580, 8, 0, 'DT SMS System Activitymonitor Plugin', '', 'dtsmsactivitymonitor', 'plugin', 'system', 0, '1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_dtsmsactivitymonitor.xml', 'https://www.joomlart.com/update-steps', ''),
(1581, 8, 0, 'JA Builder System Plugin', '', 'jabuilder', 'plugin', 'system', 0, '1.1.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_jabuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1582, 8, 0, 'Plugin JA Content Type', '', 'jacontenttype', 'plugin', 'system', 0, '1.1.4', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_jacontenttype.xml', 'https://www.joomlart.com/update-steps', ''),
(1583, 8, 0, 'JA Open Street Map ', '', 'jaosmap', 'plugin', 'system', 0, '1.1.3', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_jaosmap.xml', 'https://www.joomlart.com/update-steps', ''),
(1584, 8, 10024, 'T3 System Plugin', '', 't3', 'plugin', 'system', 0, '2.7.2', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_t3.xml', 'https://www.joomlart.com/update-steps', ''),
(1585, 8, 0, 'T4 System Plugin ', '', 't4', 'plugin', 'system', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/plg_system_t4.xml', 'https://www.joomlart.com/update-steps', ''),
(1586, 8, 0, 'DT SMS User Plugin', '', 'dtsms', 'plugin', 'user', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plg_user_dtsms.xml', 'https://www.joomlart.com/update-steps', ''),
(1587, 8, 0, 'JB Zenkit Plugin', '', 'zenkit', 'plugin', 'zenkit', 0, '2.1.6', '', 'http://update.joomlart.com/service/tracking/j31/plg_zenkit_zenkit.xml', 'https://www.joomlart.com/update-steps', ''),
(1588, 8, 0, 'S5 Css And Js Compressor Plugin', '', 'plugin_s5_css_and_js_compressor', 'plugin', 'plugin_s5_css_and_js', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_css_and_js_compressor.xml', 'https://www.joomlart.com/update-steps', ''),
(1589, 8, 0, 'S5 Disqus Comments Plugin', '', 'plugin_s5_disqus_comments', 'plugin', 'plugin_s5_disqus_com', 0, '1.2.1', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_disqus_comments.xml', 'https://www.joomlart.com/update-steps', ''),
(1590, 8, 0, 'S5 Flex Menu Plugin', '', 'plugin_s5_flex_menu', 'plugin', 'plugin_s5_flex_menu', 0, '1.1.2', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_flex_menu.xml', 'https://www.joomlart.com/update-steps', ''),
(1591, 8, 0, 'S5 Ie6 Warning Plugin', '', 'plugin_s5_ie6_warning', 'plugin', 'plugin_s5_ie6_warnin', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_ie6_warning.xml', 'https://www.joomlart.com/update-steps', ''),
(1592, 8, 0, 'S5 Like Me Plugin', '', 'plugin_s5_likeme', 'plugin', 'plugin_s5_likeme', 0, '2.0.0', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_likeme.xml', 'https://www.joomlart.com/update-steps', ''),
(1593, 8, 0, 'S5 Media Player 2 Plugin', '', 'plugin_s5_media_player', 'plugin', 'plugin_s5_media_play', 0, '2.4.2', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_media_player.xml', 'https://www.joomlart.com/update-steps', ''),
(1594, 8, 0, 'S5 Mp3 Player Plugin', '', 'plugin_s5_mp3_player', 'plugin', 'plugin_s5_mp3_player', 0, '2.0.1', '', 'http://update.joomlart.com/service/tracking/j31/plugin_s5_mp3_player.xml', 'https://www.joomlart.com/update-steps', ''),
(1595, 8, 0, 'JB Portal Template', '', 'portal', 'template', '', 0, '1.4.5', '', 'http://update.joomlart.com/service/tracking/j31/portal.xml', 'https://www.joomlart.com/update-steps', ''),
(1596, 8, 0, 'JB Presto Template', '', 'presto', 'template', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/presto.xml', 'https://www.joomlart.com/update-steps', ''),
(1597, 8, 0, 'JB Profile Template', '', 'profile', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/profile.xml', 'https://www.joomlart.com/update-steps', ''),
(1598, 8, 0, 'JB Profilr Template', '', 'profilr', 'template', '', 0, '1.0.7', '', 'http://update.joomlart.com/service/tracking/j31/profilr.xml', 'https://www.joomlart.com/update-steps', ''),
(1599, 8, 0, 'Purity III Template', '', 'purity_iii', 'template', '', 0, '1.2.5', '', 'http://update.joomlart.com/service/tracking/j31/purity_iii.xml', 'https://www.joomlart.com/update-steps', ''),
(1600, 8, 0, 'JB Rasa2 Template', '', 'rasa2', 'template', '', 0, '1.4.0', '', 'http://update.joomlart.com/service/tracking/j31/rasa2.xml', 'https://www.joomlart.com/update-steps', ''),
(1601, 8, 0, 'JB Responsive2 Template', '', 'responsive2', 'template', '', 0, '1.4.4', '', 'http://update.joomlart.com/service/tracking/j31/responsive2.xml', 'https://www.joomlart.com/update-steps', ''),
(1602, 8, 0, 'DT Retro Theme', '', 'retro_theme', 'custom', '', 0, '1.0.6', '', 'http://update.joomlart.com/service/tracking/j31/retro_theme.xml', 'https://www.joomlart.com/update-steps', ''),
(1603, 8, 0, 'JS Socialize template', '', 'socialize', 'template', '', 0, '2.1.6', '', 'http://update.joomlart.com/service/tracking/j31/socialize.xml', 'https://www.joomlart.com/update-steps', ''),
(1604, 8, 0, 'Socialize Bonus', '', 'socialize_bonus', 'package', '', 0, '1.0.0', '', 'http://update.joomlart.com/service/tracking/j31/socialize_bonus.xml', 'https://www.joomlart.com/update-steps', ''),
(1605, 8, 0, 'GURU Payment Plugin - Stripe', '', 'stripe', 'plugin', 'stripe', 0, '1.0.3', '', 'http://update.joomlart.com/service/tracking/j31/stripe.xml', 'https://www.joomlart.com/update-steps', ''),
(1606, 8, 0, 'T4 Package', '', 't4', 'package', '', 0, '1.0.0_preview', '', 'http://update.joomlart.com/service/tracking/j31/t4.xml', 'https://www.joomlart.com/update-steps', ''),
(1607, 8, 0, 'T4 Page Builder Package', '', 't4pagebuilder', 'package', '', 0, '1.0.0_preview', '', 'http://update.joomlart.com/service/tracking/j31/t4pagebuilder.xml', 'https://www.joomlart.com/update-steps', ''),
(1608, 8, 0, 'T3 B3 Blank Template', '', 't3_bs3_blank', 'template', '', 0, '2.2.1', '', 'http://update.joomlart.com/service/tracking/j31/tpl_t3_bs3_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(1609, 8, 0, 'T4 Blank Template', '', 't4_blank', 'template', '', 0, '1.0.5', '', 'http://update.joomlart.com/service/tracking/j31/tpl_t4_blank.xml', 'https://www.joomlart.com/update-steps', ''),
(1610, 8, 0, 'Sample package for Uber App', '', 'uber_app', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_app.xml', 'https://www.joomlart.com/update-steps', ''),
(1611, 8, 0, 'Sample package for Uber Bookstore', '', 'uber_bookstore', 'sample_package', '', 0, '2.1.7', '', 'http://update.joomlart.com/service/tracking/j31/uber_bookstore.xml', 'https://www.joomlart.com/update-steps', ''),
(1612, 8, 0, 'Sample package for Uber Business', '', 'uber_business', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_business.xml', 'https://www.joomlart.com/update-steps', ''),
(1613, 8, 0, 'Sample package for Uber Charity', '', 'uber_charity', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_charity.xml', 'https://www.joomlart.com/update-steps', ''),
(1614, 8, 0, 'Sample package for Uber Church', '', 'uber_church', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_church.xml', 'https://www.joomlart.com/update-steps', ''),
(1615, 8, 0, 'Sample package for Uber Construction', '', 'uber_construction', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_construction.xml', 'https://www.joomlart.com/update-steps', ''),
(1616, 8, 0, 'Sample package for Uber Corporate', '', 'uber_corporate', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_corporate.xml', 'https://www.joomlart.com/update-steps', ''),
(1617, 8, 0, 'Sample package for Uber Gym', '', 'uber_gym', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_gym.xml', 'https://www.joomlart.com/update-steps', ''),
(1618, 8, 0, 'Sample package for Uber Home', '', 'uber_home', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_home.xml', 'https://www.joomlart.com/update-steps', ''),
(1619, 8, 0, 'Sample package for Uber Landing ', '', 'uber_landing', 'sample_package', '', 0, '2.1.3', '', 'http://update.joomlart.com/service/tracking/j31/uber_landing.xml', 'https://www.joomlart.com/update-steps', ''),
(1620, 8, 0, 'Sample package for Uber Lawyer', '', 'uber_lawyer', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_lawyer.xml', 'https://www.joomlart.com/update-steps', ''),
(1621, 8, 0, 'Sample package for Uber Medicare', '', 'uber_medicare', 'sample_package', '', 0, '2.1.4', '', 'http://update.joomlart.com/service/tracking/j31/uber_medicare.xml', 'https://www.joomlart.com/update-steps', ''),
(1622, 8, 0, 'Sample package for Uber Music', '', 'uber_music', 'sample_package', '', 0, '2.0.7', '', 'http://update.joomlart.com/service/tracking/j31/uber_music.xml', 'https://www.joomlart.com/update-steps', ''),
(1623, 8, 0, 'Sample package for Uber Restaurant', '', 'uber_restaurant', 'sample_package', '', 0, '2.0.7', '', 'http://update.joomlart.com/service/tracking/j31/uber_restaurant.xml', 'https://www.joomlart.com/update-steps', ''),
(1624, 8, 0, 'Sample package for Uber Spa', '', 'uber_spa', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_spa.xml', 'https://www.joomlart.com/update-steps', ''),
(1625, 8, 0, 'Sample package for Uber University', '', 'uber_university', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_university.xml', 'https://www.joomlart.com/update-steps', ''),
(1626, 8, 0, 'Sample package for Uber Wedding', '', 'uber_wedding', 'sample_package', '', 0, '2.0.6', '', 'http://update.joomlart.com/service/tracking/j31/uber_wedding.xml', 'https://www.joomlart.com/update-steps', ''),
(1627, 8, 0, 'JB Wedding Template', '', 'wedding', 'template', '', 0, '1.4.1', '', 'http://update.joomlart.com/service/tracking/j31/wedding.xml', 'https://www.joomlart.com/update-steps', ''),
(1628, 8, 0, 'JB Zenbase Template', '', 'zenbase', 'template', '', 0, '1.2.6', '', 'http://update.joomlart.com/service/tracking/j31/zenbase.xml', 'https://www.joomlart.com/update-steps', ''),
(1629, 8, 0, 'JB Zenhost Template', '', 'zenhost', 'template', '', 0, '1.0.4', '', 'http://update.joomlart.com/service/tracking/j31/zenhost.xml', 'https://www.joomlart.com/update-steps', ''),
(1630, 9, 10025, 'SP Simple Portfolio', '', 'com_spsimpleportfolio', 'component', '', 1, '1.8', '', 'http://www.joomshaper.com/updates/com-sp-simple-portfolio.xml', '', ''),
(1631, 10, 0, 'SP Simple Portfolio Module', '', '', 'module', '', 0, '1.8', '', 'http://www.joomshaper.com/updates/mod-sp-simple-portfolio.xml', '', ''),
(1632, 11, 10030, 'UT Multimodule', '', 'mod_ut_multimodule', 'module', '', 0, '2.6.0', '', 'http://update.unitemplates.com/extensions/mod_ut_multimodule.xml', '', ''),
(1633, 11, 10029, 'UT Contact Pro', '', 'mod_ut_contact_pro', 'module', '', 0, '1.6.0', '', 'http://update.unitemplates.com/extensions/mod_ut_contact_pro.xml', '', ''),
(1634, 11, 0, 'UT Twittembed', '', 'mod_ut_twittembed', 'module', '', 0, '1.2.0', '', 'http://update.unitemplates.com/extensions/mod_ut_twittembed.xml', '', ''),
(1635, 11, 0, 'UT Joomstagram', '', 'mod_ut_joomstagram', 'module', '', 0, '1.0.0', '', 'http://update.unitemplates.com/extensions/mod_ut_joomstagram.xml', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_update_sites`
--

CREATE TABLE `lbazs_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT 0,
  `last_check_timestamp` bigint(20) DEFAULT 0,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';

--
-- Volcado de datos para la tabla `lbazs_update_sites`
--

INSERT INTO `lbazs_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 1, 1585138911, ''),
(2, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 1, 1585093429, ''),
(3, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1585099560, ''),
(4, 'YOOtheme Installer', 'extension', 'http://yootheme.com/api/update/installer_yootheme_j33.xml', 1, 1585101712, ''),
(5, 'Foxcontact update site', 'extension', 'https://cdn.fox.ra.it/download/foxcontact.xml', 1, 1585101713, ''),
(6, 'JCE Editor Updates', 'extension', 'https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&id=1&file=extension.xml', 1, 1585101716, ''),
(7, '', 'collection', 'http://update.unitemplates.com/templates/list.xml', 1, 1585101717, ''),
(8, '', 'collection', 'http://update.joomlart.com/service/tracking/list.xml', 1, 1585101830, ''),
(9, 'SP Simple Portfolio', 'extension', 'http://www.joomshaper.com/updates/com-sp-simple-portfolio.xml', 1, 1585101832, ''),
(10, 'SP Simple Portfolio Module', 'extension', 'http://www.joomshaper.com/updates/mod-sp-simple-portfolio.xml', 1, 1585101833, ''),
(11, '', 'collection', 'http://update.unitemplates.com/extensions/list.xml', 1, 1585101835, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_update_sites_extensions`
--

CREATE TABLE `lbazs_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT 0,
  `extension_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';

--
-- Volcado de datos para la tabla `lbazs_update_sites_extensions`
--

INSERT INTO `lbazs_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 802),
(2, 10002),
(3, 28),
(4, 10011),
(5, 10014),
(6, 10020),
(7, 10023),
(8, 10024),
(9, 10025),
(10, 10028),
(11, 10029);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_usergroups`
--

CREATE TABLE `lbazs_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_usergroups`
--

INSERT INTO `lbazs_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_users`
--

CREATE TABLE `lbazs_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT 0,
  `sendEmail` tinyint(4) DEFAULT 0,
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT 0 COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Require user to reset password on next login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_users`
--

INSERT INTO `lbazs_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(66, 'Super User', 'admin', 'carleon1792@gmail.com', '$2y$10$syY2hm4fApdn.Jg5DQNUuupAh8PnO8i6yg1bdLqN7UXpppGR10CB.', 0, 1, '2020-03-24 23:42:45', '2020-03-25 01:17:00', '0', '', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_user_keys`
--

CREATE TABLE `lbazs_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_user_notes`
--

CREATE TABLE `lbazs_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_user_profiles`
--

CREATE TABLE `lbazs_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_user_usergroup_map`
--

CREATE TABLE `lbazs_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_user_usergroup_map`
--

INSERT INTO `lbazs_user_usergroup_map` (`user_id`, `group_id`) VALUES
(66, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_utf8_conversion`
--

CREATE TABLE `lbazs_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_utf8_conversion`
--

INSERT INTO `lbazs_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_viewlevels`
--

CREATE TABLE `lbazs_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `lbazs_viewlevels`
--

INSERT INTO `lbazs_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_wf_profiles`
--

CREATE TABLE `lbazs_wf_profiles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `users` text NOT NULL,
  `types` text NOT NULL,
  `components` text NOT NULL,
  `area` tinyint(3) NOT NULL,
  `device` varchar(255) NOT NULL,
  `rows` text NOT NULL,
  `plugins` text NOT NULL,
  `published` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` tinyint(3) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lbazs_wf_profiles`
--

INSERT INTO `lbazs_wf_profiles` (`id`, `name`, `description`, `users`, `types`, `components`, `area`, `device`, `rows`, `plugins`, `published`, `ordering`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Default', 'Default Profile for all users', '', '3,4,5,6,8,7', '', 0, 'desktop,tablet,phone', 'help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,blockquote,formatselect,styleselect,removeformat,cleanup;fontselect,fontsizeselect,forecolor,backcolor,spacer,clipboard,indent,outdent,lists,sub,sup,textcase,charmap,hr;directionality,fullscreen,preview,source,print,searchreplace,spacer,table;visualaid,visualchars,visualblocks,nonbreaking,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article', 'charmap,contextmenu,browser,inlinepopups,media,help,clipboard,searchreplace,directionality,fullscreen,preview,source,table,textcase,print,style,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,anchor,link,spellchecker,article,lists', 1, 1, 0, '0000-00-00 00:00:00', ''),
(2, 'Front End', 'Sample Front-end Profile', '', '3,4,5', '', 1, 'desktop,tablet,phone', 'help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,formatselect,styleselect;clipboard,searchreplace,indent,outdent,lists,cleanup,charmap,removeformat,hr,sub,sup,textcase,nonbreaking,visualchars,visualblocks;fullscreen,preview,print,visualaid,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article', 'charmap,contextmenu,inlinepopups,help,clipboard,searchreplace,fullscreen,preview,print,style,textcase,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,anchor,link,spellchecker,article,lists', 0, 2, 0, '0000-00-00 00:00:00', ''),
(3, 'Blogger', 'Simple Blogging Profile', '', '3,4,5,6,8,7', '', 0, 'desktop,tablet,phone', 'bold,italic,strikethrough,lists,blockquote,spacer,justifyleft,justifycenter,justifyright,spacer,link,unlink,imgmanager,article,spellchecker,fullscreen,kitchensink;formatselect,underline,justifyfull,forecolor,clipboard,removeformat,charmap,indent,outdent,undo,redo,help', 'link,imgmanager,article,spellchecker,fullscreen,kitchensink,clipboard,contextmenu,inlinepopups,lists', 0, 3, 0, '0000-00-00 00:00:00', '{\"editor\":{\"toggle\":\"0\"}}'),
(4, 'Mobile', 'Sample Mobile Profile', '', '3,4,5,6,8,7', '', 0, 'tablet,phone', 'undo,redo,spacer,bold,italic,underline,formatselect,spacer,justifyleft,justifycenter,justifyfull,justifyright,spacer,fullscreen,kitchensink;styleselect,lists,spellchecker,article,link,unlink', 'fullscreen,kitchensink,spellchecker,article,link,inlinepopups,lists', 0, 4, 0, '0000-00-00 00:00:00', '{\"editor\":{\"toolbar_theme\":\"mobile\",\"resizing\":\"0\",\"resize_horizontal\":\"0\",\"resizing_use_cookie\":\"0\",\"toggle\":\"0\",\"links\":{\"popups\":{\"default\":\"\",\"jcemediabox\":{\"enable\":\"0\"},\"window\":{\"enable\":\"0\"}}}}}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_widgetkit`
--

CREATE TABLE `lbazs_widgetkit` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lbazs_widgetkit`
--

INSERT INTO `lbazs_widgetkit` (`id`, `name`, `type`, `data`) VALUES
(1, 'Noticias Desarrollo', 'joomla', '{\"_widget\":{\"name\":\"grid\",\"data\":{\"grid\":\"default\",\"gutter\":\"default\",\"gutter_dynamic\":\"20\",\"gutter_v_dynamic\":\"\",\"filter\":\"none\",\"filter_align\":\"left\",\"filter_all\":true,\"columns\":\"1\",\"columns_small\":0,\"columns_medium\":0,\"columns_large\":0,\"columns_xlarge\":0,\"panel\":\"blank\",\"panel_link\":false,\"animation\":\"none\",\"media\":true,\"image_width\":\"auto\",\"image_height\":\"auto\",\"media_align\":\"teaser\",\"media_width\":\"1-2\",\"media_breakpoint\":\"medium\",\"content_align\":true,\"media_border\":\"none\",\"media_overlay\":\"icon\",\"overlay_animation\":\"fade\",\"media_animation\":\"scale\",\"title\":true,\"content\":true,\"social_buttons\":true,\"title_size\":\"panel\",\"text_align\":\"left\",\"link\":true,\"link_style\":\"button\",\"link_text\":\"Read more\",\"badge\":true,\"badge_style\":\"badge\",\"badge_position\":\"panel\",\"link_target\":false,\"class\":\"\"}},\"number\":1,\"category\":[\"9\"],\"subcategories\":\"0\",\"featured\":\"\",\"content\":\"intro\",\"image\":\"intro\",\"link\":\"\",\"order_by\":\"ordering\",\"date\":\"publish_up\",\"author\":\"author\",\"categories\":\"categories\"}'),
(2, 'Noticias Comerciales', 'joomla', '{\"_widget\":{\"name\":\"grid\",\"data\":{\"grid\":\"default\",\"gutter\":\"default\",\"gutter_dynamic\":\"20\",\"gutter_v_dynamic\":\"\",\"filter\":\"none\",\"filter_align\":\"left\",\"filter_all\":true,\"columns\":\"1\",\"columns_small\":0,\"columns_medium\":0,\"columns_large\":0,\"columns_xlarge\":0,\"panel\":\"blank\",\"panel_link\":false,\"animation\":\"none\",\"media\":true,\"image_width\":\"auto\",\"image_height\":\"auto\",\"media_align\":\"teaser\",\"media_width\":\"1-2\",\"media_breakpoint\":\"medium\",\"content_align\":true,\"media_border\":\"none\",\"media_overlay\":\"icon\",\"overlay_animation\":\"fade\",\"media_animation\":\"scale\",\"title\":true,\"content\":true,\"social_buttons\":true,\"title_size\":\"panel\",\"text_align\":\"left\",\"link\":true,\"link_style\":\"button\",\"link_text\":\"Read more\",\"badge\":true,\"badge_style\":\"badge\",\"badge_position\":\"panel\",\"link_target\":false,\"class\":\"\"}},\"number\":1,\"category\":[\"10\"],\"subcategories\":\"0\",\"featured\":\"\",\"content\":\"intro\",\"image\":\"intro\",\"link\":\"\",\"order_by\":\"ordering\",\"date\":\"publish_up\",\"author\":\"author\",\"categories\":\"categories\"}'),
(3, 'Noticias Comunicaciones', 'joomla', '{\"_widget\":{\"name\":\"grid\",\"data\":{\"grid\":\"default\",\"gutter\":\"default\",\"gutter_dynamic\":\"20\",\"gutter_v_dynamic\":\"\",\"filter\":\"none\",\"filter_align\":\"left\",\"filter_all\":true,\"columns\":\"1\",\"columns_small\":0,\"columns_medium\":0,\"columns_large\":0,\"columns_xlarge\":0,\"panel\":\"blank\",\"panel_link\":false,\"animation\":\"none\",\"media\":true,\"image_width\":\"auto\",\"image_height\":\"auto\",\"media_align\":\"teaser\",\"media_width\":\"1-2\",\"media_breakpoint\":\"medium\",\"content_align\":true,\"media_border\":\"none\",\"media_overlay\":\"icon\",\"overlay_animation\":\"fade\",\"media_animation\":\"scale\",\"title\":true,\"content\":true,\"social_buttons\":true,\"title_size\":\"panel\",\"text_align\":\"left\",\"link\":true,\"link_style\":\"button\",\"link_text\":\"Read more\",\"badge\":true,\"badge_style\":\"badge\",\"badge_position\":\"panel\",\"link_target\":false,\"class\":\"\"}},\"number\":1,\"category\":[\"8\"],\"subcategories\":\"0\",\"featured\":\"\",\"content\":\"intro\",\"image\":\"intro\",\"link\":\"\",\"order_by\":\"ordering\",\"date\":\"publish_up\",\"author\":\"author\",\"categories\":\"categories\"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lbazs_widgetkit_widget`
--

CREATE TABLE `lbazs_widgetkit_widget` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `style` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `lbazs_action_logs`
--
ALTER TABLE `lbazs_action_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_user_id_logdate` (`user_id`,`log_date`),
  ADD KEY `idx_user_id_extension` (`user_id`,`extension`),
  ADD KEY `idx_extension_item_id` (`extension`,`item_id`);

--
-- Indices de la tabla `lbazs_action_logs_extensions`
--
ALTER TABLE `lbazs_action_logs_extensions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_action_logs_users`
--
ALTER TABLE `lbazs_action_logs_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `idx_notify` (`notify`);

--
-- Indices de la tabla `lbazs_action_log_config`
--
ALTER TABLE `lbazs_action_log_config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_assets`
--
ALTER TABLE `lbazs_assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_asset_name` (`name`),
  ADD KEY `idx_lft_rgt` (`lft`,`rgt`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Indices de la tabla `lbazs_associations`
--
ALTER TABLE `lbazs_associations`
  ADD PRIMARY KEY (`context`,`id`),
  ADD KEY `idx_key` (`key`);

--
-- Indices de la tabla `lbazs_banners`
--
ALTER TABLE `lbazs_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100)),
  ADD KEY `idx_banner_catid` (`catid`),
  ADD KEY `idx_language` (`language`);

--
-- Indices de la tabla `lbazs_banner_clients`
--
ALTER TABLE `lbazs_banner_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Indices de la tabla `lbazs_banner_tracks`
--
ALTER TABLE `lbazs_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  ADD KEY `idx_track_date` (`track_date`),
  ADD KEY `idx_track_type` (`track_type`),
  ADD KEY `idx_banner_id` (`banner_id`);

--
-- Indices de la tabla `lbazs_categories`
--
ALTER TABLE `lbazs_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indices de la tabla `lbazs_contact_details`
--
ALTER TABLE `lbazs_contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indices de la tabla `lbazs_content`
--
ALTER TABLE `lbazs_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`),
  ADD KEY `idx_alias` (`alias`(191));

--
-- Indices de la tabla `lbazs_contentitem_tag_map`
--
ALTER TABLE `lbazs_contentitem_tag_map`
  ADD UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  ADD KEY `idx_tag_type` (`tag_id`,`type_id`),
  ADD KEY `idx_date_id` (`tag_date`,`tag_id`),
  ADD KEY `idx_core_content_id` (`core_content_id`);

--
-- Indices de la tabla `lbazs_content_frontpage`
--
ALTER TABLE `lbazs_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indices de la tabla `lbazs_content_rating`
--
ALTER TABLE `lbazs_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indices de la tabla `lbazs_content_types`
--
ALTER TABLE `lbazs_content_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `idx_alias` (`type_alias`(100));

--
-- Indices de la tabla `lbazs_extensions`
--
ALTER TABLE `lbazs_extensions`
  ADD PRIMARY KEY (`extension_id`),
  ADD KEY `element_clientid` (`element`,`client_id`),
  ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Indices de la tabla `lbazs_fields`
--
ALTER TABLE `lbazs_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_user_id` (`created_user_id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indices de la tabla `lbazs_fields_categories`
--
ALTER TABLE `lbazs_fields_categories`
  ADD PRIMARY KEY (`field_id`,`category_id`);

--
-- Indices de la tabla `lbazs_fields_groups`
--
ALTER TABLE `lbazs_fields_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indices de la tabla `lbazs_fields_values`
--
ALTER TABLE `lbazs_fields_values`
  ADD KEY `idx_field_id` (`field_id`),
  ADD KEY `idx_item_id` (`item_id`(191));

--
-- Indices de la tabla `lbazs_finder_filters`
--
ALTER TABLE `lbazs_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indices de la tabla `lbazs_finder_links`
--
ALTER TABLE `lbazs_finder_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_title` (`title`(100)),
  ADD KEY `idx_md5` (`md5sum`),
  ADD KEY `idx_url` (`url`(75)),
  ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Indices de la tabla `lbazs_finder_links_terms0`
--
ALTER TABLE `lbazs_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms1`
--
ALTER TABLE `lbazs_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms2`
--
ALTER TABLE `lbazs_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms3`
--
ALTER TABLE `lbazs_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms4`
--
ALTER TABLE `lbazs_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms5`
--
ALTER TABLE `lbazs_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms6`
--
ALTER TABLE `lbazs_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms7`
--
ALTER TABLE `lbazs_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms8`
--
ALTER TABLE `lbazs_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_terms9`
--
ALTER TABLE `lbazs_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_termsa`
--
ALTER TABLE `lbazs_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_termsb`
--
ALTER TABLE `lbazs_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_termsc`
--
ALTER TABLE `lbazs_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_termsd`
--
ALTER TABLE `lbazs_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_termse`
--
ALTER TABLE `lbazs_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_links_termsf`
--
ALTER TABLE `lbazs_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indices de la tabla `lbazs_finder_taxonomy`
--
ALTER TABLE `lbazs_finder_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `state` (`state`),
  ADD KEY `ordering` (`ordering`),
  ADD KEY `access` (`access`),
  ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Indices de la tabla `lbazs_finder_taxonomy_map`
--
ALTER TABLE `lbazs_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Indices de la tabla `lbazs_finder_terms`
--
ALTER TABLE `lbazs_finder_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `idx_term` (`term`),
  ADD KEY `idx_term_phrase` (`term`,`phrase`),
  ADD KEY `idx_stem_phrase` (`stem`,`phrase`),
  ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Indices de la tabla `lbazs_finder_terms_common`
--
ALTER TABLE `lbazs_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`),
  ADD KEY `idx_lang` (`language`);

--
-- Indices de la tabla `lbazs_finder_tokens`
--
ALTER TABLE `lbazs_finder_tokens`
  ADD KEY `idx_word` (`term`),
  ADD KEY `idx_context` (`context`);

--
-- Indices de la tabla `lbazs_finder_tokens_aggregate`
--
ALTER TABLE `lbazs_finder_tokens_aggregate`
  ADD KEY `token` (`term`),
  ADD KEY `keyword_id` (`term_id`);

--
-- Indices de la tabla `lbazs_finder_types`
--
ALTER TABLE `lbazs_finder_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indices de la tabla `lbazs_foxcontact_captcha`
--
ALTER TABLE `lbazs_foxcontact_captcha`
  ADD PRIMARY KEY (`session_id`,`form_uid`);

--
-- Indices de la tabla `lbazs_foxcontact_enquiries`
--
ALTER TABLE `lbazs_foxcontact_enquiries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_time` (`date`);

--
-- Indices de la tabla `lbazs_foxcontact_sequences`
--
ALTER TABLE `lbazs_foxcontact_sequences`
  ADD PRIMARY KEY (`series`);

--
-- Indices de la tabla `lbazs_languages`
--
ALTER TABLE `lbazs_languages`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_langcode` (`lang_code`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indices de la tabla `lbazs_menu`
--
ALTER TABLE `lbazs_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`(100),`language`),
  ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  ADD KEY `idx_menutype` (`menutype`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indices de la tabla `lbazs_menu_types`
--
ALTER TABLE `lbazs_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Indices de la tabla `lbazs_messages`
--
ALTER TABLE `lbazs_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Indices de la tabla `lbazs_messages_cfg`
--
ALTER TABLE `lbazs_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Indices de la tabla `lbazs_modules`
--
ALTER TABLE `lbazs_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`,`access`),
  ADD KEY `newsfeeds` (`module`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- Indices de la tabla `lbazs_modules_menu`
--
ALTER TABLE `lbazs_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Indices de la tabla `lbazs_newsfeeds`
--
ALTER TABLE `lbazs_newsfeeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indices de la tabla `lbazs_overrider`
--
ALTER TABLE `lbazs_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_postinstall_messages`
--
ALTER TABLE `lbazs_postinstall_messages`
  ADD PRIMARY KEY (`postinstall_message_id`);

--
-- Indices de la tabla `lbazs_privacy_consents`
--
ALTER TABLE `lbazs_privacy_consents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`);

--
-- Indices de la tabla `lbazs_privacy_requests`
--
ALTER TABLE `lbazs_privacy_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_redirect_links`
--
ALTER TABLE `lbazs_redirect_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_old_url` (`old_url`(100)),
  ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Indices de la tabla `lbazs_revslider_css`
--
ALTER TABLE `lbazs_revslider_css`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_revslider_layer_animations`
--
ALTER TABLE `lbazs_revslider_layer_animations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_revslider_settings`
--
ALTER TABLE `lbazs_revslider_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_revslider_sliders`
--
ALTER TABLE `lbazs_revslider_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_revslider_slides`
--
ALTER TABLE `lbazs_revslider_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_revslider_static_slides`
--
ALTER TABLE `lbazs_revslider_static_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_schemas`
--
ALTER TABLE `lbazs_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Indices de la tabla `lbazs_session`
--
ALTER TABLE `lbazs_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`),
  ADD KEY `client_id_guest` (`client_id`,`guest`);

--
-- Indices de la tabla `lbazs_spsimpleportfolio_items`
--
ALTER TABLE `lbazs_spsimpleportfolio_items`
  ADD PRIMARY KEY (`spsimpleportfolio_item_id`);

--
-- Indices de la tabla `lbazs_spsimpleportfolio_tags`
--
ALTER TABLE `lbazs_spsimpleportfolio_tags`
  ADD PRIMARY KEY (`spsimpleportfolio_tag_id`);

--
-- Indices de la tabla `lbazs_tags`
--
ALTER TABLE `lbazs_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_idx` (`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indices de la tabla `lbazs_template_styles`
--
ALTER TABLE `lbazs_template_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_client_id` (`client_id`),
  ADD KEY `idx_client_id_home` (`client_id`,`home`);

--
-- Indices de la tabla `lbazs_ucm_base`
--
ALTER TABLE `lbazs_ucm_base`
  ADD PRIMARY KEY (`ucm_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_item_id`),
  ADD KEY `idx_ucm_type_id` (`ucm_type_id`),
  ADD KEY `idx_ucm_language_id` (`ucm_language_id`);

--
-- Indices de la tabla `lbazs_ucm_content`
--
ALTER TABLE `lbazs_ucm_content`
  ADD PRIMARY KEY (`core_content_id`),
  ADD KEY `tag_idx` (`core_state`,`core_access`),
  ADD KEY `idx_access` (`core_access`),
  ADD KEY `idx_alias` (`core_alias`(100)),
  ADD KEY `idx_language` (`core_language`),
  ADD KEY `idx_title` (`core_title`(100)),
  ADD KEY `idx_modified_time` (`core_modified_time`),
  ADD KEY `idx_created_time` (`core_created_time`),
  ADD KEY `idx_content_type` (`core_type_alias`(100)),
  ADD KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  ADD KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  ADD KEY `idx_core_created_user_id` (`core_created_user_id`),
  ADD KEY `idx_core_type_id` (`core_type_id`);

--
-- Indices de la tabla `lbazs_ucm_history`
--
ALTER TABLE `lbazs_ucm_history`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  ADD KEY `idx_save_date` (`save_date`);

--
-- Indices de la tabla `lbazs_updates`
--
ALTER TABLE `lbazs_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indices de la tabla `lbazs_update_sites`
--
ALTER TABLE `lbazs_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Indices de la tabla `lbazs_update_sites_extensions`
--
ALTER TABLE `lbazs_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Indices de la tabla `lbazs_usergroups`
--
ALTER TABLE `lbazs_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  ADD KEY `idx_usergroup_title_lookup` (`title`),
  ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Indices de la tabla `lbazs_users`
--
ALTER TABLE `lbazs_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_username` (`username`),
  ADD KEY `idx_name` (`name`(100)),
  ADD KEY `idx_block` (`block`),
  ADD KEY `email` (`email`);

--
-- Indices de la tabla `lbazs_user_keys`
--
ALTER TABLE `lbazs_user_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `series` (`series`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `lbazs_user_notes`
--
ALTER TABLE `lbazs_user_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_category_id` (`catid`);

--
-- Indices de la tabla `lbazs_user_profiles`
--
ALTER TABLE `lbazs_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Indices de la tabla `lbazs_user_usergroup_map`
--
ALTER TABLE `lbazs_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indices de la tabla `lbazs_viewlevels`
--
ALTER TABLE `lbazs_viewlevels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- Indices de la tabla `lbazs_wf_profiles`
--
ALTER TABLE `lbazs_wf_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_widgetkit`
--
ALTER TABLE `lbazs_widgetkit`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lbazs_widgetkit_widget`
--
ALTER TABLE `lbazs_widgetkit_widget`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `lbazs_action_logs`
--
ALTER TABLE `lbazs_action_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT de la tabla `lbazs_action_logs_extensions`
--
ALTER TABLE `lbazs_action_logs_extensions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `lbazs_action_log_config`
--
ALTER TABLE `lbazs_action_log_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `lbazs_assets`
--
ALTER TABLE `lbazs_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT de la tabla `lbazs_banners`
--
ALTER TABLE `lbazs_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_banner_clients`
--
ALTER TABLE `lbazs_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_categories`
--
ALTER TABLE `lbazs_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `lbazs_contact_details`
--
ALTER TABLE `lbazs_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_content`
--
ALTER TABLE `lbazs_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `lbazs_content_types`
--
ALTER TABLE `lbazs_content_types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10000;

--
-- AUTO_INCREMENT de la tabla `lbazs_extensions`
--
ALTER TABLE `lbazs_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10031;

--
-- AUTO_INCREMENT de la tabla `lbazs_fields`
--
ALTER TABLE `lbazs_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_fields_groups`
--
ALTER TABLE `lbazs_fields_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_finder_filters`
--
ALTER TABLE `lbazs_finder_filters`
  MODIFY `filter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_finder_links`
--
ALTER TABLE `lbazs_finder_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_finder_taxonomy`
--
ALTER TABLE `lbazs_finder_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `lbazs_finder_terms`
--
ALTER TABLE `lbazs_finder_terms`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_finder_types`
--
ALTER TABLE `lbazs_finder_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_foxcontact_enquiries`
--
ALTER TABLE `lbazs_foxcontact_enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_languages`
--
ALTER TABLE `lbazs_languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `lbazs_menu`
--
ALTER TABLE `lbazs_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT de la tabla `lbazs_menu_types`
--
ALTER TABLE `lbazs_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `lbazs_messages`
--
ALTER TABLE `lbazs_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_modules`
--
ALTER TABLE `lbazs_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT de la tabla `lbazs_newsfeeds`
--
ALTER TABLE `lbazs_newsfeeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_overrider`
--
ALTER TABLE `lbazs_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';

--
-- AUTO_INCREMENT de la tabla `lbazs_postinstall_messages`
--
ALTER TABLE `lbazs_postinstall_messages`
  MODIFY `postinstall_message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `lbazs_privacy_consents`
--
ALTER TABLE `lbazs_privacy_consents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_privacy_requests`
--
ALTER TABLE `lbazs_privacy_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_redirect_links`
--
ALTER TABLE `lbazs_redirect_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_revslider_css`
--
ALTER TABLE `lbazs_revslider_css`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `lbazs_revslider_layer_animations`
--
ALTER TABLE `lbazs_revslider_layer_animations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_revslider_settings`
--
ALTER TABLE `lbazs_revslider_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_revslider_sliders`
--
ALTER TABLE `lbazs_revslider_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_revslider_slides`
--
ALTER TABLE `lbazs_revslider_slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_revslider_static_slides`
--
ALTER TABLE `lbazs_revslider_static_slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_spsimpleportfolio_items`
--
ALTER TABLE `lbazs_spsimpleportfolio_items`
  MODIFY `spsimpleportfolio_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_spsimpleportfolio_tags`
--
ALTER TABLE `lbazs_spsimpleportfolio_tags`
  MODIFY `spsimpleportfolio_tag_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_tags`
--
ALTER TABLE `lbazs_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `lbazs_template_styles`
--
ALTER TABLE `lbazs_template_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `lbazs_ucm_content`
--
ALTER TABLE `lbazs_ucm_content`
  MODIFY `core_content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_ucm_history`
--
ALTER TABLE `lbazs_ucm_history`
  MODIFY `version_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `lbazs_updates`
--
ALTER TABLE `lbazs_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1636;

--
-- AUTO_INCREMENT de la tabla `lbazs_update_sites`
--
ALTER TABLE `lbazs_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `lbazs_usergroups`
--
ALTER TABLE `lbazs_usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `lbazs_users`
--
ALTER TABLE `lbazs_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT de la tabla `lbazs_user_keys`
--
ALTER TABLE `lbazs_user_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_user_notes`
--
ALTER TABLE `lbazs_user_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lbazs_viewlevels`
--
ALTER TABLE `lbazs_viewlevels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `lbazs_wf_profiles`
--
ALTER TABLE `lbazs_wf_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `lbazs_widgetkit`
--
ALTER TABLE `lbazs_widgetkit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `lbazs_widgetkit_widget`
--
ALTER TABLE `lbazs_widgetkit_widget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
